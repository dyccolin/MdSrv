1、如果安装是32位的python2.7，将MdApi\lib\v140\Win32\Release下DLL文件及配置文件复制到当前目录
   如果安装是64位的python2.7，将MdApi\lib\v140\x64\Release下DLL文件及配置文件复制到到当前目录（VS2013对应的是v120下的库）
   三个配置文件：MdApi.dat
		 TDFApi.ini
		 MdApi.log4cxx
2、需要安装对应版本的VCForPython27
   下载地址https://download.microsoft.com/download/7/9/6/796EF2E4-801B-4FC4-AB28-B59FBF6D907B/VCForPython27.msi
3、需要安装cffi(pip install cffi)
4、高版本的cffi依赖的pycparser包可能与PyMdApi不兼容，需要降低pycparser到2.17版(pip uninstall pycparser -> pip install pycparser==2.17)
5、python testPyMdApi.py
6、如果无没有数据接收，请检查配置ini文件，MdServer地址是否正确
7、市场代码：SH（上海A）、SZ（深圳A）、CF（中金所）、SHF（上期所）、DCE（大商所）、CZCE（郑商所）