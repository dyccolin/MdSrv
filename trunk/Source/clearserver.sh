#clear MdServer
rm -rf ./MdServer/obj/x64/Release/
rm -rf ./MdCom/obj/x64/Release/
rm -rf ./MdServer/bin/x64/Release/
rm -rf ./MdCom/bin/x64/Release/

rm -rf ./Server.ini
rm -rf ./MdServer.log4cxx
rm -rf ./SubScriptions.txt
rm -rf ./MdServer.out
rm -rf ./TDFAPI27_LOG*

rm -rf ./deamon_outfile
