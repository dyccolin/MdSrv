#pragma once
#include <pthread.h>  

//namespace mdcom
//{
	class CLock
	{
	private:
		mutable pthread_mutex_t m_mutex;
	public:

		CLock()
		{
			pthread_mutex_init(&m_mutex, NULL);
		}

		~CLock()
		{
			pthread_mutex_destroy(&m_mutex);
		}

		void Lock() const
		{
			pthread_mutex_unlock(&m_mutex);
		}
		void Unlock() const
		{
			pthread_mutex_unlock(&m_mutex);
		}
	};
//}

