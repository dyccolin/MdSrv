#pragma once

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <vector>
#include <list>
#include <map>
#include <deque>
#include <queue>
#include <string>
#include <set>
#include <stdarg.h>
#include <memory>
#include <thread> 
#include <errno.h>

#include <log4cxx/logger.h>
#include <log4cxx/propertyconfigurator.h>

using namespace log4cxx;
using namespace log4cxx::helpers;