#pragma once
#if (defined WIN32) || (defined _WINDOWS)
#include "windows.h"
#define MUTEX_LOCK(m) EnterCriticalSection(m)
#define MUTEX_UNLOCK(m) LeaveCriticalSection(m)
#define MUTEX_INIT(m) InitializeCriticalSection(m)
#define MUTEX_DESTROY(m) DeleteCriticalSection(m)
#define MUTEX_T CRITICAL_SECTION

#define PTHREAD_COND_INITIALIZER CreateEvent(NULL, TRUE, FALSE, NULL)
#define pthread_cond_destroy(x) CloseHandle(*x)
#define pthread_cond_signal(x) SetEvent(*x)
#define pthread_cond_broadcast(x) ResetEvent(*x)
#define pthread_cond_wait(x,y) WaitForSingleObject(x, INFINITE)
#define pthread_cond_t HANDLE
#else
#include <pthread.h>
#define MUTEX_TRYLOCK(m) pthread_mutex_trylock(m)
#define MUTEX_LOCK(m) pthread_mutex_lock(m)
#define MUTEX_UNLOCK(m) pthread_mutex_unlock(m)
#define MUTEX_INIT(m) pthread_mutex_init(m, nullptr)
#define MUTEX_DESTROY(m) pthread_mutex_destroy(m)
#define MUTEX_T pthread_mutex_t
#endif


