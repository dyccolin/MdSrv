#ifndef SINGLETON_H
#define SINGLETON_H

#define DECLARE_SINGLETON(cls)\
private:\
    static cls* m_poInstance;\
public:\
    static bool CreateInstance()\
    {\
        if(nullptr == m_poInstance)\
            m_poInstance = new cls;\
        return m_poInstance != nullptr;\
    }\
    static cls* Instance(){ \
		if(nullptr == m_poInstance)\
			m_poInstance = new cls;\
		return m_poInstance; \
	}\
    static void DestroyInstance()\
    {\
    if(m_poInstance != nullptr)\
    {\
        delete m_poInstance;\
        m_poInstance = nullptr;\
    }\
}

#define IMPLEMENT_SINGLETON(cls) \
    cls* cls::m_poInstance = nullptr;


#endif

