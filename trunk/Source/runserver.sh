#run
mkdir -p ./Api/lib
ln -sf ../../../ThirdParty/TDFAPI/lib/libTDFAPI_v2.7.so ./Api/lib/libTDFAPI_v2.7.so
ln -sf ../../../ThirdParty/ZMQ/lib/libzmq.so ./Api/lib/libzmq.so.5
ln -sf ../../../ThirdParty/msgpack/lib/libmsgpackc.so ./Api/lib/libmsgpackc.so.2
ln -sf ../../../ThirdParty/log4cxx/lib/liblog4cxx.so ./Api/lib/liblog4cxx.so.10
ln -sf ../../../ThirdParty/log4cxx/lib/libapr-1.so ./Api/lib/libapr-1.so.0
ln -sf ../../../ThirdParty/log4cxx/lib/libaprutil-1.so ./Api/lib/libaprutil-1.so.0
ln -sf ../../../ThirdParty/log4cxx/lib/libexpat.so ./Api/lib/libexpat.so.0

rm -f deamon_outfile

#kill 除grep外的MdServer进程
ps -efww|grep MdServer|grep -v grep|cut -c 9-15|xargs kill -9

export LD_LIBRARY_PATH=./Api/lib
./MdServer.out
