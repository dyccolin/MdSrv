#build MdServer release
export PATH=/usr/local/gcc-4.8.5/bin:$PATH

mkdir -p ./MdServer/obj/x64/Release/
mkdir -p ./MdCom/obj/x64/Release/
mkdir -p ./MdServer/bin/x64/Release/
mkdir -p ./MdCom/bin/x64/Release/

#创建link
ln -sf libzmq.so.5 ../ThirdParty/ZMQ/lib/libzmq.so
ln -sf libmsgpackc.so.2 ../ThirdParty/msgpack/lib/libmsgpackc.so
ln -sf liblog4cxx.so.10 ../ThirdParty/log4cxx/lib/liblog4cxx.so
ln -sf libapr-1.so.0 ../ThirdParty/log4cxx/lib/libapr-1.so
ln -sf libaprutil-1.so.0 ../ThirdParty/log4cxx/lib/libaprutil-1.so
ln -sf libexpat.so.0 ../ThirdParty/log4cxx/lib/libexpat.so

#编译
g++ -c -x c++ "./MdCom/IniFile.cpp" -I "./MdCom/../../ThirdParty/ZMQ/include" -I "./MdCom/../../ThirdParty/msgpack/include" -I "./MdCom/../../ThirdParty/log4cxx/include" -g1 -o "./MdCom/obj/x64/Release/IniFile.o" -Wall -Wswitch -W"no-deprecated-declarations" -W"empty-body" -Wconversion -W"return-type" -Wparentheses -W"no-format" -Wuninitialized -W"unreachable-code" -W"unused-function" -W"unused-value" -W"unused-variable" -O3 -fno-strict-aliasing -fomit-frame-pointer -DNDEBUG -fpic -fthreadsafe-statics -fexceptions -frtti -std=c++11
ar -rsc  "./MdCom/bin/x64/Release/libMdCom.a" "./MdCom/obj/x64/Release/IniFile.o"
g++ -c -x c++ "./MdServer/main.cpp" -I "./MdServer/../MdCom" -I "./MdServer/../../ThirdParty/ZMQ/include" -I "./MdServer/../../ThirdParty/TDFAPI/include" -I "./MdServer/../../ThirdParty/msgpack/include" -I "./MdServer/../../ThirdParty/log4cxx/include" -g1 -o "./MdServer/obj/x64/Release/main.o" -Wall -Wswitch -W"no-deprecated-declarations" -W"empty-body" -Wconversion -W"return-type" -Wparentheses -W"no-format" -Wuninitialized -W"unreachable-code" -W"unused-function" -W"unused-value" -W"unused-variable" -O3 -fno-strict-aliasing -fomit-frame-pointer -DNDEBUG -fthreadsafe-statics -fexceptions -frtti -std=c++11
g++ -c -x c++ "./MdServer/MyEvent.cpp" -I "./MdServer/../MdCom" -I "./MdServer/../../ThirdParty/ZMQ/include" -I "./MdServer/../../ThirdParty/TDFAPI/include" -I "./MdServer/../../ThirdParty/msgpack/include" -I "./MdServer/../../ThirdParty/log4cxx/include" -g1 -o "./MdServer/obj/x64/Release/MyEvent.o" -Wall -Wswitch -W"no-deprecated-declarations" -W"empty-body" -Wconversion -W"return-type" -Wparentheses -W"no-format" -Wuninitialized -W"unreachable-code" -W"unused-function" -W"unused-value" -W"unused-variable" -O3 -fno-strict-aliasing -fomit-frame-pointer -DNDEBUG -fthreadsafe-statics -fexceptions -frtti -std=c++11
g++ -c -x c++ "./MdServer/Publisher.cpp" -I "./MdServer/../MdCom" -I "./MdServer/../../ThirdParty/ZMQ/include" -I "./MdServer/../../ThirdParty/TDFAPI/include" -I "./MdServer/../../ThirdParty/msgpack/include" -I "./MdServer/../../ThirdParty/log4cxx/include" -g1 -o "./MdServer/obj/x64/Release/Publisher.o" -Wall -Wswitch -W"no-deprecated-declarations" -W"empty-body" -Wconversion -W"return-type" -Wparentheses -W"no-format" -Wuninitialized -W"unreachable-code" -W"unused-function" -W"unused-value" -W"unused-variable" -O3 -fno-strict-aliasing -fomit-frame-pointer -DNDEBUG -fthreadsafe-statics -fexceptions -frtti -std=c++11
g++ -o "./MdServer/bin/x64/Release/MdServer.out" -Wl,--no-undefined -Wl,-L"../ThirdParty/TDFAPI/lib" -Wl,-L"../ThirdParty/ZMQ/lib" -Wl,-L"../ThirdParty/msgpack/lib" -Wl,-L"../ThirdParty/log4cxx/lib" -Wl,-z,relro -Wl,-z,now -Wl,-z,noexecstack "./MdServer/obj/x64/Release/main.o" "./MdServer/obj/x64/Release/MyEvent.o" "./MdServer/obj/x64/Release/Publisher.o" "./MdCom/bin/x64/Release/libMdCom.a" -lTDFAPI_v2.7 -lzmq -lmsgpackc -llog4cxx -l"apr-1" -l"aprutil-1" -lexpat
ln -sf "./MdServer/bin/x64/Release/MdServer.out" ./

ln -sf ./MdServer/Server.ini ./
ln -sf ./MdServer/MdServer.log4cxx ./
ln -sf ./MdServer/SubScriptions.txt ./
