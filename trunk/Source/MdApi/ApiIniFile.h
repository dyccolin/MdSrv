#pragma once
#include <string>

#if (!defined WIN32) && (!defined _WINDOWS)
#include "IniFile.h"
#endif

using namespace std;
//using namespace mdcom;

class CApiIniFile
{
public:
	char Log4cxxProperties[200];

	char MaketDataServerPort[100];

public:

	CApiIniFile()
	{
		memset(Log4cxxProperties, 0, sizeof(Log4cxxProperties));
		memset(MaketDataServerPort, 0, sizeof(MaketDataServerPort));
		Read();
	}

	~CApiIniFile()
	{
	}

private:
	void Read()
	{
		const char* configFileName = "./TDFApi.ini";

#if (!defined WIN32) && (!defined _WINDOWS)
		IniFile ini(configFileName);

		// ZMQ����
		ini.read_profile_string("zmq", "Port", MaketDataServerPort, sizeof(MaketDataServerPort), nullptr, configFileName);
		// Log4cxxProperties����
		ini.read_profile_string("log4cxx", "log4cxx_properties", Log4cxxProperties, sizeof(Log4cxxProperties), nullptr, configFileName);
#else
		// ZMQ����
		GetPrivateProfileStringA("zmq", "Port", "", MaketDataServerPort, sizeof(MaketDataServerPort), configFileName);
		// Log4cxxProperties����
		GetPrivateProfileStringA("log4cxx", "log4cxx_properties", "", Log4cxxProperties, sizeof(Log4cxxProperties), configFileName);
#endif

	}
};

