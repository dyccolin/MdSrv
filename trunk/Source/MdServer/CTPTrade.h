#pragma once
#include "CTPMd.h"
#include "ThostFtdcTraderApi.h"
#include "ThostFtdcUserApiStruct.h"


static log4cxx::LoggerPtr logger_ctptrade = log4cxx::Logger::getLogger("CTPTrade");
class CCTPTrade : public CThostFtdcTraderSpi
{
public:
	//构造函数
	CCTPTrade();
	~CCTPTrade();

	///当客户端与交易后台建立起通信连接时（还未登录前），该方法被调用。
	virtual void OnFrontConnected();

	///客户端认证响应
	virtual void OnRspAuthenticate(CThostFtdcRspAuthenticateField *pRspAuthenticateField, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);

	///登录请求响应
	virtual void OnRspUserLogin(CThostFtdcRspUserLoginField *pRspUserLogin, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);

	///投资者结算结果确认响应
	virtual void OnRspSettlementInfoConfirm(CThostFtdcSettlementInfoConfirmField *pSettlementInfoConfirm, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);

	///请求查询合约响应
	virtual void OnRspQryInstrument(CThostFtdcInstrumentField *pInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);

	///错误应答
	virtual void OnRspError(CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);

	///当客户端与交易后台通信连接断开时，该方法被调用。当发生这个情况后，API会自动重新连接，客户端可不做处理。
	virtual void OnFrontDisconnected(int nReason);

	///心跳超时警告。当长时间未收到报文时，该方法被调用。
	virtual void OnHeartBeatWarning(int nTimeLapse);

private:
	CCTPMd* m_CtpMd;
	CThostFtdcTraderApi* m_ThostTradeApi;
	int m_RequestId;
	char account[32];
	char password[32];
	char broker[11];

	std::vector<CThostFtdcInstrumentField> m_Instruments;
	MUTEX_T m_mtxInstrument;

	// 是否收到成功的响应
	bool IsErrorRspInfo(CThostFtdcRspInfoField *pRspInfo);
	int ReqAuthenticate();
	int ReqUserLogin();		// 登录
	
public:
	void Start();
};

CCTPTrade::CCTPTrade():m_RequestId(0), m_ThostTradeApi(nullptr)
{
	MUTEX_INIT(&m_mtxInstrument);
}

CCTPTrade::~CCTPTrade()
{
	if (m_ThostTradeApi != nullptr)
	{
		m_ThostTradeApi->Release();
		m_ThostTradeApi = nullptr;
	}

	if (m_CtpMd != nullptr)
	{
		delete m_CtpMd;
		m_CtpMd = nullptr;
	}

	MUTEX_DESTROY(&m_mtxInstrument);
}

void CCTPTrade::OnFrontConnected()
{
	char buff[1024];
	sprintf(buff, "%s", "CTPTrade:OnFrontConnected");
	LOG4CXX_INFO(logger_ctptrade, buff);

	//ReqAuthenticate();
	ReqUserLogin();
}

inline void CCTPTrade::OnRspAuthenticate(CThostFtdcRspAuthenticateField * pRspAuthenticateField, CThostFtdcRspInfoField * pRspInfo, int nRequestID, bool bIsLast)
{
	if (!IsErrorRspInfo(pRspInfo))
	{
		ReqUserLogin();
	}
	else
	{
		char buff[1024];
		sprintf(buff, "CTPTrade:OnRspAuthenticate(UserID:%s):%s", pRspAuthenticateField->UserID, pRspInfo->ErrorMsg);
		LOG4CXX_ERROR(logger_ctptrade, buff);
	}
}

void CCTPTrade::OnRspUserLogin(CThostFtdcRspUserLoginField * pRspUserLogin, CThostFtdcRspInfoField * pRspInfo, int nRequestID, bool bIsLast)
{
	char buff[1024];
	if (!IsErrorRspInfo(pRspInfo))
	{
		CThostFtdcSettlementInfoConfirmField req;
		memset(&req, 0, sizeof(req));
		strcpy(req.BrokerID, broker);
		strcpy(req.InvestorID, account);
		int iResult = m_ThostTradeApi->ReqSettlementInfoConfirm(&req, m_RequestId++);
		
		sprintf(buff, "CTPTrade:ReqSettlementInfoConfirm(UserID:%s) %s (%d)", pRspUserLogin->UserID, iResult == 0 ? "Success" : "failure", iResult);
		LOG4CXX_INFO(logger_ctptrade, buff);
	}
	else
	{
		sprintf(buff, "CTPTrade:OnRspUserLogin(UserID:%s):%s", pRspUserLogin->UserID, pRspInfo->ErrorMsg);
		LOG4CXX_ERROR(logger_ctptrade, buff);
	}
}

void CCTPTrade::OnRspSettlementInfoConfirm(CThostFtdcSettlementInfoConfirmField * pSettlementInfoConfirm, CThostFtdcRspInfoField * pRspInfo, int nRequestID, bool bIsLast)
{
	char buff[1024];
	if (!IsErrorRspInfo(pRspInfo))
	{
		// 重新查询前清空合约
		m_Instruments.clear();

		CThostFtdcQryInstrumentField req;
		memset(&req, 0, sizeof(CThostFtdcQryInstrumentField));
		int iResult = m_ThostTradeApi->ReqQryInstrument(&req, m_RequestId++);

		sprintf(buff, "CTPTrade:ReqQryInstrument(UserID:%s) %s (%d)", pSettlementInfoConfirm->InvestorID, iResult == 0 ? "Success" : "failure", iResult);
		LOG4CXX_INFO(logger_ctptrade, buff);
	}
	else
	{
		sprintf(buff, "CTPTrade:OnRspSettlementInfoConfirm(UserID:%s):%s", pSettlementInfoConfirm->InvestorID, pRspInfo->ErrorMsg);
		LOG4CXX_ERROR(logger_ctptrade, buff);
	}
}

void CCTPTrade::OnRspQryInstrument(CThostFtdcInstrumentField * pInstrument, CThostFtdcRspInfoField * pRspInfo, int nRequestID, bool bIsLast)
{
	if (!IsErrorRspInfo(pRspInfo))
	{
		if (pInstrument != nullptr)
		{
			if (pInstrument->ProductClass == THOST_FTDC_PC_Futures&&
				pInstrument->InstLifePhase == THOST_FTDC_IP_Started)
			{
				//char buff[1024];
				//sprintf(buff, "CTPTrade:OnRspQryInstrument(Instrument:%s, ExchangeID:%s)", pInstrument->InstrumentID, pInstrument->ExchangeID);
				//LOG4CXX_INFO(logger_ctptrade, buff);

				CThostFtdcInstrumentField field;
				memcpy(&field, pInstrument, sizeof(CThostFtdcInstrumentField));
				MUTEX_LOCK(&m_mtxInstrument);
				m_Instruments.push_back(field);
				MUTEX_UNLOCK(&m_mtxInstrument);
				//// 订阅行情
				//m_pCTPGateway->Get_pCTPMdGateWay()->SubscribeMarketData(pInstrument->InstrumentID);

				//SRV_LOG(logger, SRV_LOG_LEVEL_INFO, "OnRspQryInstrument(%s,InstrumentName:%s,ExchangeID:%s,ExchangeInstID:%s,ProductID:%s,ProductClass:%c,VolumeMultiple:%d,PriceTick:%.2f,InstLifePhase:%c,IsTrading:%d,LongMarginRatio:%.2f,ShortMarginRatio:%.2f)",
				//	pInstrument->InstrumentID, pInstrument->InstrumentName, pInstrument->ExchangeID, pInstrument->ExchangeInstID, pInstrument->ProductID, pInstrument->ProductClass,
				//	pInstrument->VolumeMultiple,
				//	pInstrument->PriceTick,
				//	pInstrument->InstLifePhase,
				//	pInstrument->IsTrading,
				//	pInstrument->LongMarginRatio,
				//	pInstrument->ShortMarginRatio);

				//ReqQryInstrumentMarginRate(pInstrument->InstrumentID);
				//ReqQryInstrumentCommissionRate(pInstrument->InstrumentID);
			}
		}
	}

	if (bIsLast)
	{
		char buff[1024];
		sprintf(buff, "CTPTrade:OnRspQryInstrument ended");
		LOG4CXX_INFO(logger_ctptrade, buff);

		// 订阅期货行情
		MUTEX_LOCK(&m_mtxInstrument);
		m_CtpMd->SubscribeMarketData(m_Instruments);
		MUTEX_UNLOCK(&m_mtxInstrument);
	}
}

void CCTPTrade::OnRspError(CThostFtdcRspInfoField * pRspInfo, int nRequestID, bool bIsLast)
{
	IsErrorRspInfo(pRspInfo);
}

void CCTPTrade::OnFrontDisconnected(int nReason)
{
	char buff[1024];
	sprintf(buff, "CTPTrade:OnFrontDisconnected:%d", nReason);
	LOG4CXX_ERROR(logger_ctptrade, buff);
}

void CCTPTrade::OnHeartBeatWarning(int nTimeLapse)
{
}

bool CCTPTrade::IsErrorRspInfo(CThostFtdcRspInfoField * pRspInfo)
{
	// 如果ErrorID != 0, 说明收到了错误的响应
	bool bResult = pRspInfo && (pRspInfo->ErrorID != 0);
	if (bResult) {
		char buff[1024];
		sprintf(buff, "CTPTrade:(AccountID:%s):%s", account, pRspInfo->ErrorMsg);
		LOG4CXX_ERROR(logger_ctptrade, buff);
	}
	return bResult;
}

inline int CCTPTrade::ReqAuthenticate()
{
	CThostFtdcReqAuthenticateField req;
	memset(&req, 0, sizeof(req));
	strcpy(req.BrokerID, broker);
	strcpy(req.UserID, account);
	//strcpy(req.UserProductInfo, "Q7");
	//strcpy(req.AuthCode, "8117");


	int iRet = this->m_ThostTradeApi->ReqAuthenticate(&req, m_RequestId++);
	char buff[1024];
	sprintf(buff, "CTPTrade:ReqUserLogin:(UserID:%s, BrokerID:%s) %s (%d)", req.UserID, req.BrokerID, iRet == 0 ? "Success" : "failure", iRet);
	LOG4CXX_INFO(logger_ctptrade, buff);

	return iRet;
}

int CCTPTrade::ReqUserLogin()
{
	CThostFtdcReqUserLoginField req;
	memset(&req, 0, sizeof(req));
	strcpy(req.BrokerID, broker);
	strcpy(req.UserID, account);
	strcpy(req.Password, password);

	char EncryptedPwd[32] = { 0 };
	int len = strlen(password);
	for (int i = 0; i < len; i++)
	{
		EncryptedPwd[i] = '*';
	}

	int iRet = this->m_ThostTradeApi->ReqUserLogin(&req, m_RequestId++);
	char buff[1024];
	sprintf(buff, "CTPTrade:ReqUserLogin:(UserID:%s, BrokerID:%s, Passowrd:%s) %s (%d)", req.UserID, req.BrokerID, EncryptedPwd, iRet == 0 ? "Success" : "failure", iRet);
	LOG4CXX_INFO(logger_ctptrade, buff);

	return iRet;
}

inline void CCTPTrade::Start()
{
	// 先启动CTPMD，以便查询合约后订阅行情时，CTPMD已经登录
	m_CtpMd = new CCTPMd();
	m_CtpMd->Start();

	std::string configFileName("Server.ini");
	IniFile ini(configFileName);

	// ctp配置
	char frontAddr[128];
	ini.read_profile_string("ctp", "trade_frontAddr", frontAddr, sizeof(frontAddr), nullptr, configFileName.c_str());
	ini.read_profile_string("ctp", "broker", broker, sizeof(broker), nullptr, configFileName.c_str());
	ini.read_profile_string("ctp", "account", account, sizeof(account), nullptr, configFileName.c_str());
	ini.read_profile_string("ctp", "password", password, sizeof(password), nullptr, configFileName.c_str());

	// 初始化UserApi
	m_ThostTradeApi = CThostFtdcTraderApi::CreateFtdcTraderApi();		// 创建UserApi
	m_ThostTradeApi->RegisterSpi(this);									// 注册事件类
	m_ThostTradeApi->SubscribePublicTopic(THOST_TERT_QUICK);			// 注册公有流
	m_ThostTradeApi->SubscribePrivateTopic(THOST_TERT_QUICK);			// 注册私有流
	m_ThostTradeApi->RegisterFront(frontAddr);							// 设置交易前置
	m_ThostTradeApi->Init();

	char buff[1024];
	sprintf(buff, "CTPTrade:Starting...");
	LOG4CXX_INFO(logger_ctptrade, buff);
}
