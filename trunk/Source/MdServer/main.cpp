#ifdef TDFAPI30
#include "../../ThirdParty/TDFAPI3/include/TDFAPI.h"
#include "../../ThirdParty/TDFAPI3/include/NonMDMsgDecoder.h"
#include "../../ThirdParty/TDFAPI3/include/TDFAPIInner.h"
#include "../../ThirdParty/TDFAPI3/include/TDFAPIStruct.h"
#else
#include "../../ThirdParty/TDFAPI/include/TDFAPI.h"
#include "../../ThirdParty/TDFAPI/include/TDFAPIStruct.h"
#endif // TDFAPI30

#include "CTPTrade.h"

#include "MdData.h"
#include "Common.h"

#include "Publisher.h"
#include "ServerIniFile.h"
#include "LicenseHelper.h"

#include <wchar.h>
#include <iconv.h>

#include<stdio.h>  
#include<stdlib.h>  
#include<string.h>  
#include<fcntl.h>// open  
#include<sys/types.h>  
#include<sys/stat.h>  
#include<unistd.h>  
#include<sys/wait.h>  
#include<signal.h>

#define MAXFILE 65535  

volatile sig_atomic_t _running = 1;
// signal handler  
void sigterm_handler(int arg)
{
	_running = 0;
}

static log4cxx::LoggerPtr logger = log4cxx::Logger::getLogger("syslog");

int code_convert(char *from_charset, char *to_charset, char *inbuf, size_t* inlen, char *outbuf, size_t* outlen)
{
	iconv_t cd;
	int rc;
	char **pin = &inbuf;
	char **pout = &outbuf;

	cd = iconv_open(to_charset, from_charset);
	if (cd == 0)
		return -1;
	memset(outbuf, 0, *outlen);
	if (iconv(cd, pin, (size_t*)inlen, pout, (size_t*)outlen) == -1)
		return -1;
	iconv_close(cd);
	return 0;
}

char* DeepCopy(const char*szIn)
{
	if (szIn)
	{
		int nLen = strlen(szIn);
		char* pRet = new char[nLen + 1];
		pRet[nLen] = 0;
		strncpy(pRet, szIn, nLen + 1);
		return pRet;
	}
	else
	{
		return NULL;
	}
}

AutoResetEvent m_autoEvent;
// map<szWindCode, subCode>
std::map<std::string, std::string> vSubCodeTables;
std::map<TDF_ERR, const char*> mapErrStr;
const char* GetErrStr(TDF_ERR nErr)
{
	
	mapErrStr.insert(std::make_pair(TDF_ERR_UNKOWN, "TDF_ERR_UNKOWN"));
	mapErrStr.insert(std::make_pair(TDF_ERR_INITIALIZE_FAILURE, "TDF_ERR_INITIALIZE_FAILURE"));
	mapErrStr.insert(std::make_pair(TDF_ERR_NETWORK_ERROR, "TDF_ERR_NETWORK_ERROR"));
	mapErrStr.insert(std::make_pair(TDF_ERR_INVALID_PARAMS, "TDF_ERR_INVALID_PARAMS"));
	mapErrStr.insert(std::make_pair(TDF_ERR_VERIFY_FAILURE, "TDF_ERR_VERIFY_FAILURE"));
	mapErrStr.insert(std::make_pair(TDF_ERR_NO_AUTHORIZED_MARKET, "TDF_ERR_NO_AUTHORIZED_MARKET"));
	mapErrStr.insert(std::make_pair(TDF_ERR_NO_CODE_TABLE, "TDF_ERR_NO_CODE_TABLE"));
	mapErrStr.insert(std::make_pair(TDF_ERR_SUCCESS, "TDF_ERR_SUCCESS"));
	if (mapErrStr.find(nErr) == mapErrStr.end())
	{
		return "TDF_ERR_UNKOWN";
	}
	else
	{
		return mapErrStr[nErr];
	}
}

THANDLE hTDF = NULL;
// ticker:such as 600000.SH;000002.SZ
int subscribe(const char* ticker);
int unsubscribe(const char* ticker);

void RecvData(THANDLE hTdf, TDF_MSG* pMsgHead);
void RecvSys(THANDLE hTdf, TDF_MSG* pSysMsg);

int main()
{
#ifndef VS_DEBUG
	// 创建守护进程
	pid_t pid;

	/* 屏蔽一些有关控制终端操作的信号
	* 防止在守护进程没有正常运转起来时，因控制终端受到干扰退出或挂起
	* */
	signal(SIGINT, SIG_IGN);// 终端中断  
	signal(SIGHUP, SIG_IGN);// 连接挂断  
	signal(SIGQUIT, SIG_IGN);// 终端退出  
	signal(SIGPIPE, SIG_IGN);// 向无读进程的管道写数据  
	signal(SIGTTOU, SIG_IGN);// 后台程序尝试写操作  
	signal(SIGTTIN, SIG_IGN);// 后台程序尝试读操作  
	signal(SIGTERM, SIG_IGN);// 终止  

							 // [1] fork child process and exit father process  
	pid = fork();
	if (pid < 0)
	{
		perror("fork error!");
		exit(1);
	}
	else if (pid > 0)
	{
		exit(0);
	}

	// [2] create a new session  
	setsid();

	// [3] set current path  
	char szPath[1024];
	if (getcwd(szPath, sizeof(szPath)) == NULL)
	{
		perror("getcwd");
		exit(1);
	}
	else
	{
		chdir(szPath);
		printf("set current path succ:%s\n", szPath);
	}

	// [4] umask 0  
	umask(0);

	// [5] close useless fd  
	int i;
	//for (i = 0; i < MAXFILE; ++i)  
	for (i = 3; i < MAXFILE; ++i)
	{
		close(i);
	}

	// [6] set termianl signal  
	signal(SIGTERM, sigterm_handler);

#endif // !VS_DEBUG

	// licence check
	LicenseHelper licenseHlp;
	string decryptTxt = licenseHlp.readCipher();
	string mac = IOUtil::GetLocalMac();

	int sub_pos = decryptTxt.rfind("_");
	int str_len = decryptTxt.length();
	if (sub_pos < 0 || sub_pos + 1 > str_len)
	{
		printf("License infomation string format error!\nThis machine ID is:[%s]\n", mac.c_str());
		exit(0);
	}

	if (decryptTxt.find("any") == -1 &&
		decryptTxt.find("Any") == -1)
	{
		if (mac.empty())
		{
			printf("Can't get the machine ID, license check failed, please contact the provider.\n", mac.c_str());
			exit(0);
		}
		else if (decryptTxt.find(mac) == -1)
		{
			printf("License invalid, lease use after authorized!\nThis machine ID is:[%s]\n", mac.c_str());
			exit(0);
		}
	}

	string auth_end_day = decryptTxt.substr(sub_pos + 1, str_len - sub_pos - 1);
	if (strlen(auth_end_day.c_str()) != 8)
	{
		printf("License invalid: date format error(%s)", auth_end_day.c_str());
		exit(0);
	}

	time_t timep;
	struct tm *p;
	time(&timep);
	p = localtime(&timep); /*取得当地时间*/
	char today[32];
	sprintf(today, "%04d%02d%02d", 1900 + p->tm_year, 1 + p->tm_mon, p->tm_mday);
	int nAuthEndDay = atoi(auth_end_day.c_str());
	if (atoi(today) > nAuthEndDay)
	{
		printf("License invalid: out of end date(%s)", auth_end_day.c_str());
		exit(0);
	}

	CServerIniFile serverIni;
	int major, minor, patch;
	zmq_version(&major, &minor, &patch);
	printf("zmq_verion %d.%d.%d\n", major, minor, patch);
	PropertyConfigurator::configure("MdServer.log4cxx");
	LOG4CXX_INFO(logger, "Program starting...");

	// 启动宏汇
	TDF_SetEnv(TDF_ENVIRON_OUT_LOG, 0);

	char server[64];
	sprintf(server, "Server:%s:%s User:%s", serverIni.IP, serverIni.Port, serverIni.User);
	LOG4CXX_INFO(logger, server);

	TDF_ERR nErr = TDF_ERR_SUCCESS;


#ifdef TDFAPI30
	TDF_OPEN_SETTING_EXT settings = { 0 };
	strcpy(settings.siServer[0].szIp, serverIni.IP);
	strcpy(settings.siServer[0].szPort, serverIni.Port);
	strcpy(settings.siServer[0].szUser, serverIni.User);
	strcpy(settings.siServer[0].szPwd, serverIni.Password);
	settings.nServerNum = 1; //必须设置： 有效的连接配置个数（当前版本应<=2)

	settings.pfnMsgHandler = RecvData; //设置数据消息回调函数
	settings.pfnSysMsgNotify = RecvSys;//设置系统消息回调函数
	//市场列表，不用区分大小写，用英文字符; 分割；市场订阅和代码订阅不能同时为空；订阅格式为：市场简称 - 数据level - 备用字段：SH-2-0; SZ-2-0; CF-2-0; SHF-2-0; CZC-2-0; DCE-2-0;
	//MarketList = SH-2-0;SZ-2-0;
	settings.szMarkets = DeepCopy(serverIni.Markets);
	settings.szSubScriptions = DeepCopy(serverIni.StockList); // 订阅列表，对于已知的国内6所(sh;sz;cf;shf;czc;dce)代码不用区分大小写，未来加入的交易所则必须严格大小写。代码之间用英文字符 ; 分割。如果为空，则订阅全部代码。代码格式如：000001.sz; 600000.sh
	settings.nDate = serverIni.Date;//请求的日期，格式YYMMDD，为0则请求今天
	settings.nTime = serverIni.Time;//请求的时间，格式HHMMSS，为0则请求实时行情，-1从头请求
	settings.nTypeFlags = DATA_TYPE_NONE; //只要行情，其他都不订阅；订阅多个类型DATA_TYPE_TRANSACTION | DATA_TYPE_ORDER | DATA_TYPE_ORDERQUEUE
	std::string strDataType = std::string(serverIni.DataType);
	if (strDataType.find("TRANSACTION") != -1)
		settings.nTypeFlags |= DATA_TYPE_TRANSACTION; // 逐笔成交
	if (strDataType.find("ORDER") != -1)
		settings.nTypeFlags |= DATA_TYPE_ORDER;		  // 逐笔委托
	if (strDataType.find("ORDERQUEUE") != -1)
		settings.nTypeFlags |= DATA_TYPE_ORDERQUEUE;  // 委托队列

	printf("settings[szMarkets:%s][szSubScriptions:%s][nTime:%d][nTypeFlags:%d]", serverIni.Markets, serverIni.StockList, serverIni.Time, DATA_TYPE_NONE);

	hTDF = TDF_OpenExt(&settings, &nErr);
#else
	TDF_OPEN_SETTING settings = { 0 };
	strcpy(settings.szIp, serverIni.IP);
	strcpy(settings.szPort, serverIni.Port);
	strcpy(settings.szUser, serverIni.User);
	strcpy(settings.szPwd, serverIni.Password);

	settings.nReconnectCount = 99999999;
	settings.nReconnectGap = 5;
	settings.pfnMsgHandler = RecvData; //设置数据消息回调函数
	settings.pfnSysMsgNotify = RecvSys;//设置系统消息回调函数
	settings.nProtocol = 0;
	settings.szMarkets = DeepCopy(serverIni.Markets); //需要订阅的市场列表 不用区分大小写，用英文字符 ; 分割；如果为空，则订阅全部市场。sh;sz;cf;shf;czc;dce;
	//settings.szSubScriptions = "999999.SH";
	settings.szSubScriptions = DeepCopy(serverIni.StockList); // 订阅列表，对于已知的国内6所(sh;sz;cf;shf;czc;dce)代码不用区分大小写，未来加入的交易所则必须严格大小写。代码之间用英文字符 ; 分割。如果为空，则订阅全部代码。代码格式如：000001.sz; 600000.sh
	settings.nDate = serverIni.Date;//请求的日期，格式YYMMDD，为0则请求今天
	settings.nTime = serverIni.Time;//请求的时间，格式HHMMSS，为0则请求实时行情，-1从头请求
	settings.nTypeFlags = DATA_TYPE_ALL; // 不能初始化为0，0表示全订阅
	std::string strDataType = std::string(serverIni.DataType);
	if (strDataType.find("INDEX") != -1)
		settings.nTypeFlags |= DATA_TYPE_INDEX;		  // 指数
	if (strDataType.find("TRANSACTION") != -1)
		settings.nTypeFlags |= DATA_TYPE_TRANSACTION; // 逐笔成交
	if (strDataType.find("ORDER") != -1)
		settings.nTypeFlags |= DATA_TYPE_ORDER;		  // 逐笔委托
	if (strDataType.find("ORDERQUEUE") != -1)
		settings.nTypeFlags |= DATA_TYPE_ORDERQUEUE;  // 委托队列
	if (strDataType.find("FUTURE_CX") != -1)
		settings.nTypeFlags |= DATA_TYPE_FUTURE_CX;   // 期货连线
	hTDF = TDF_Open(&settings, &nErr);
#endif // TDFAPI30

	if (hTDF == NULL)
	{
		char buffer[128];
		snprintf(buffer, sizeof(buffer), "TDF_Open return error: %s", GetErrStr(nErr));
		LOG4CXX_ERROR(logger, buffer);
		return -1;
	}
	
	LOG4CXX_INFO(logger, "Waiting for codetables...");
	m_autoEvent.wait();

	// 启动CTP
	CCTPTrade ctpTrade;  // 需要定义为全局变量
	if (serverIni.bLoginCTP)
	{
		ctpTrade.Start();
	}

	// 启动发布线程
	Publisher::CreateInstance();
	Publisher::Instance()->Start();

	// do sth in loop  
	while (_running)
	{
		char ch = getchar();
		if (ch == 'q' || ch == 'Q')
		{
			printf("Program stopped");
			break;
		}

		//usleep(60 * 1000 * 1000);// 60 s  
	}

	TDF_Close(hTDF);
	return 0;
}

#define GETRECORD(pBase, TYPE, nIndex) ((TYPE*)((char*)(pBase) + sizeof(TYPE)*(nIndex)))

std::map<std::string, std::shared_ptr<StockQuote>> lastStockQuoteMap;
std::map<std::string, std::shared_ptr<FutureQuote>> lastFutureQuoteMap;
std::map<std::string, std::shared_ptr<IndexQuote>> lastIndexQuoteMap;
std::map<std::string, std::shared_ptr<Order>> lastStockOrderMap;
std::map<std::string, std::shared_ptr<OrderQueue>> lastStockOrderQueueMap;
std::map<std::string, std::shared_ptr<Transaction>> lastStockTransactionMap;

void RecvData(THANDLE hTdf, TDF_MSG* pMsgHead)
{
	if (!pMsgHead->pData)
	{
		LOG4CXX_ERROR(logger, "Message Head is null");
		return;
	}

	unsigned int nItemCount = pMsgHead->pAppHead->nItemCount;
	unsigned int nItemSize = pMsgHead->pAppHead->nItemSize;

	if (!nItemCount)
	{
		LOG4CXX_ERROR(logger, "No ItemCount when RecvData");
		return;
	}

	{
		//char buffer22[50];
		//snprintf(buffer22, "DataType : %d", pMsgHead->nDataType);

		//LOG4CXX_INFO(logger, buffer22);
	}

	switch (pMsgHead->nDataType)
	{
	case MSG_DATA_MARKET:
	{
		//TDF_MARKET_DATA* pMarketData = (TDF_MARKET_DATA*)pMsgHead->pData;
		for (int ii = 0; ii < nItemCount; ii++)
		{
			TDF_MARKET_DATA* pMarketData = GETRECORD(pMsgHead->pData, TDF_MARKET_DATA, ii);

			//LOG4CXX_INFO(logger, pMarketData->szWindCode);
			//const TDF_MARKET_DATA& marketData = pMarketData[ii];
			std::shared_ptr<StockQuote> quote(new StockQuote());
			quote->szWindCode = pMarketData->szWindCode;
			quote->szCode = pMarketData->szCode;
			quote->nActionDay = pMarketData->nActionDay;
			quote->nTradingDay = pMarketData->nTradingDay;
			quote->nTime = pMarketData->nTime;
			quote->nStatus = pMarketData->nStatus;

			quote->nPreClose = pMarketData->nPreClose;
			quote->nOpen = pMarketData->nOpen;
			quote->nHigh = pMarketData->nHigh;
			quote->nLow = pMarketData->nLow;
			quote->nMatch = pMarketData->nMatch;

			for (int jj = 0; jj < 10; jj++)
			{
				quote->nAskPrice.push_back(pMarketData->nAskPrice[jj]);
				quote->nAskVol.push_back(pMarketData->nAskVol[jj]);
				quote->nBidPrice.push_back(pMarketData->nBidPrice[jj]);
				quote->nBidVol.push_back(pMarketData->nBidVol[jj]);
			}
			quote->nNumTrades = pMarketData->nNumTrades;
			quote->iVolume = pMarketData->iVolume;
			quote->iTurnover = pMarketData->iTurnover;
			quote->nTotalBidVol = pMarketData->nTotalBidVol;
			quote->nTotalAskVol = pMarketData->nTotalAskVol;

			quote->nWeightedAvgBidPrice = pMarketData->nWeightedAvgBidPrice;
			quote->nWeightedAvgAskPrice = pMarketData->nWeightedAvgAskPrice;
			quote->nIOPV = pMarketData->nIOPV;
			quote->nYieldToMaturity = pMarketData->nYieldToMaturity;
			quote->nHighLimited = pMarketData->nHighLimited;
			quote->nLowLimited = pMarketData->nLowLimited;
			quote->chPrefix = pMarketData->chPrefix;

			quote->nSyl1 = pMarketData->nSyl1;
			quote->nSyl2 = pMarketData->nSyl2;
			quote->nSD2 = pMarketData->nSD2;

			char buff[256];
			sprintf(buff, "%s %s %d %d %d %d %d", pMarketData->szWindCode, pMarketData->szCode, pMarketData->nActionDay, pMarketData->nTradingDay, pMarketData->nTime, pMarketData->nMatch, pMarketData->nStatus);
			LOG4CXX_INFO(logger, buff);

			if (lastStockQuoteMap[quote->szWindCode] == nullptr || lastStockQuoteMap[quote->szWindCode]->different(*quote.get()))
			{
				Publisher::Instance()->Add(quote);
			}
			else 
			{
				sprintf(buff, "Ignore equals quote:%s", quote->szWindCode.c_str());
				LOG4CXX_WARN(logger, buff);
			}

			lastStockQuoteMap[quote->szWindCode] = quote;
		}

		break;
	}
	case MSG_DATA_FUTURE:
	{
		//TDF_FUTURE_DATA* pDepthMarketData = (TDF_FUTURE_DATA*)pMsgHead->pData;
		for (int ii = 0; ii < nItemCount; ii++)
		{
			TDF_FUTURE_DATA* pDepthMarketData = GETRECORD(pMsgHead->pData, TDF_FUTURE_DATA, ii);
			//const TDF_FUTURE_DATA& futureData = pDepthMarketData[ii];

			//char buffer[128];
			//snprintf(buffer, "FutureData : %s %d", pDepthMarketData->szWindCode, pDepthMarketData->nTime);

			//LOG4CXX_INFO(logger, buffer);
			std::shared_ptr<FutureQuote> quote(new FutureQuote());
			quote->szWindCode = pDepthMarketData->szWindCode;
			quote->szCode = pDepthMarketData->szCode;
			quote->nActionDay = pDepthMarketData->nActionDay;
			quote->nTradingDay = pDepthMarketData->nTradingDay;
			quote->nTime = pDepthMarketData->nTime;
			quote->nStatus = pDepthMarketData->nStatus;
			quote->iPreOpenInterest = pDepthMarketData->iPreOpenInterest;
			quote->nPreClose = pDepthMarketData->nPreClose;
			quote->nPreSettlePrice = pDepthMarketData->nPreSettlePrice;
			quote->nOpen = pDepthMarketData->nOpen;
			quote->nHigh = pDepthMarketData->nHigh;
			quote->nLow = pDepthMarketData->nLow;
			quote->nMatch = pDepthMarketData->nMatch;

			quote->iVolume = pDepthMarketData->iVolume;
			quote->iTurnover = pDepthMarketData->iTurnover;
			quote->iOpenInterest = pDepthMarketData->iOpenInterest;
			quote->nClose = pDepthMarketData->nClose;
			quote->nSettlePrice = pDepthMarketData->nSettlePrice;
			quote->nHighLimited = pDepthMarketData->nHighLimited;
			quote->nLowLimited = pDepthMarketData->nLowLimited;
			quote->nPreDelta = pDepthMarketData->nPreDelta;
			quote->nCurrDelta = pDepthMarketData->nCurrDelta;

			for (int jj = 0; jj < 5; jj++)
			{
				quote->nAskPrice.push_back(pDepthMarketData->nAskPrice[jj]);
				quote->nAskVol.push_back(pDepthMarketData->nAskVol[jj]);
				quote->nBidPrice.push_back(pDepthMarketData->nBidPrice[jj]);
				quote->nBidVol.push_back(pDepthMarketData->nBidVol[jj]);
			}

#ifndef TDFAPI30
			quote->lAuctionPrice = pDepthMarketData->lAuctionPrice;
			quote->lAuctionQty = pDepthMarketData->lAuctionQty;
			quote->lAvgPrice = pDepthMarketData->lAvgPrice;
#endif // !TDFAPI30

			char buff[256];
			sprintf(buff, "%s %s %d %d %d %d %d", pDepthMarketData->szWindCode, pDepthMarketData->szCode, pDepthMarketData->nActionDay, pDepthMarketData->nTradingDay, pDepthMarketData->nTime, pDepthMarketData->nMatch, pDepthMarketData->nStatus);
			LOG4CXX_INFO(logger, buff);

			if (lastFutureQuoteMap[quote->szWindCode] == nullptr || lastFutureQuoteMap[quote->szWindCode]->different(*quote.get()))
			{
				Publisher::Instance()->Add(quote);
			}
			else
			{
				sprintf(buff, "Ignore equals quote:%s", quote->szWindCode.c_str());
				LOG4CXX_WARN(logger, buff);
			}

			lastFutureQuoteMap[quote->szWindCode] = quote;
		}
	}
	break;

	case MSG_DATA_INDEX:
	{
		//TDF_INDEX_DATA* pIndexData = (TDF_INDEX_DATA*)pMsgHead->pData;
		for (int ii = 0; ii < nItemCount; ii++)
		{
			TDF_INDEX_DATA* pIndexData = GETRECORD(pMsgHead->pData, TDF_INDEX_DATA, ii);
			//TDF_INDEX_DATA& indexData = pIndexData[ii];
			//LOG4CXX_INFO(logger, pIndexData->szWindCode);
			std::shared_ptr<IndexQuote> quote(new IndexQuote());
			quote->szWindCode = pIndexData->szWindCode;
			quote->szCode = pIndexData->szCode;
			quote->nActionDay = pIndexData->nActionDay;
			quote->nTradingDay = pIndexData->nTradingDay;
			quote->nTime = pIndexData->nTime;

			quote->nOpenIndex = pIndexData->nOpenIndex;
			quote->nHighIndex = pIndexData->nHighIndex;
			quote->nLowIndex = pIndexData->nLowIndex;
			quote->nLastIndex = pIndexData->nLastIndex;
			quote->iTotalVolume = pIndexData->iTotalVolume;
			quote->iTurnover = pIndexData->iTurnover;
			quote->nPreCloseIndex = pIndexData->nPreCloseIndex;
			
			char buff[256];
			sprintf(buff, "%s %s %d %d %d %d", pIndexData->szWindCode, pIndexData->szCode, pIndexData->nActionDay, pIndexData->nTradingDay, pIndexData->nTime, pIndexData->nLastIndex);
			LOG4CXX_INFO(logger, buff);

			if (lastIndexQuoteMap[quote->szWindCode] == nullptr || lastIndexQuoteMap[quote->szWindCode]->different(*quote.get()))
			{
				Publisher::Instance()->Add(quote);
			}
			else
			{
				sprintf(buff, "Ignore equals quote:%s", quote->szWindCode.c_str());
				LOG4CXX_WARN(logger, buff);
			}

			lastIndexQuoteMap[quote->szWindCode] = quote;
		}
	}
	break;
	case MSG_DATA_TRANSACTION:
	{
		//TDF_TRANSACTION* pTransactionData = (TDF_TRANSACTION*)pMsgHead->pData;
		for (int ii = 0; ii < nItemCount; ii++)
		{
			TDF_TRANSACTION* pTransactionData = GETRECORD(pMsgHead->pData, TDF_TRANSACTION, ii);
			//const TDF_TRANSACTION& transactionData = pTransactionData[ii];
			//char buffer[128];
			//snprintf(buffer, "TRANSACTION : %s %d", pTransactionData->szWindCode, pTransactionData->nTime);

			//LOG4CXX_INFO(logger, buffer);
			std::shared_ptr<Transaction> quote(new Transaction());
			quote->szWindCode = pTransactionData->szWindCode;
			quote->szCode = pTransactionData->szCode;
			quote->nActionDay = pTransactionData->nActionDay;
			quote->nTime = pTransactionData->nTime;

			quote->nIndex = pTransactionData->nIndex;

			quote->nPrice = pTransactionData->nPrice;
			quote->nVolume = pTransactionData->nVolume;
			quote->nTurnover = pTransactionData->nTurnover;

			quote->nBSFlag = pTransactionData->nBSFlag;
			quote->chOrderKind = pTransactionData->chOrderKind;
			quote->chFunctionCode = pTransactionData->chFunctionCode;

			quote->nAskOrder = pTransactionData->nAskOrder;
			quote->nBidOrder = pTransactionData->nBidOrder;

			//quote->nType = pTransactionData->pCodeInfo->nType;

			//char buffer1[128];
			//snprintf(buffer1, sizeof(buffer1), "Transaction: %s %s %d %d %d %d %d %d %d %d %d %d %d", 
			//	pTransactionData->szWindCode, pTransactionData->szCode, pTransactionData->nActionDay, pTransactionData->nTime,
			//	pTransactionData->nIndex, pTransactionData->nPrice, pTransactionData->nVolume, pTransactionData->nTurnover,
			//	pTransactionData->nBSFlag, pTransactionData->chOrderKind, pTransactionData->chFunctionCode, 
			//	pTransactionData->nAskOrder, pTransactionData->nBidOrder);
			//LOG4CXX_INFO(logger, buffer1);

			if (lastStockTransactionMap[quote->szWindCode] == nullptr || lastStockTransactionMap[quote->szWindCode]->different(*quote.get()))
			{
				Publisher::Instance()->Add(quote);
			}
			else
			{
				char buff[128];
				sprintf(buff, "Ignore equals transaction:%s", quote->szWindCode.c_str());
				LOG4CXX_WARN(logger, buff);
			}

			lastStockTransactionMap[quote->szWindCode] = quote;
		}
	}
	break;
	case MSG_DATA_ORDERQUEUE:
	{
		//TDF_ORDER_QUEUE* pOrderQueueData = (TDF_ORDER_QUEUE*)pMsgHead->pData;
		for (int ii = 0; ii < nItemCount; ii++)
		{
			TDF_ORDER_QUEUE* pOrderQueueData = GETRECORD(pMsgHead->pData, TDF_ORDER_QUEUE, ii);

			//char buffer[128];
			//snprintf(buffer, "ORDERQUEUE : %s %d", pOrderQueueData->szWindCode, pOrderQueueData->nTime);

			//LOG4CXX_INFO(logger, buffer);

			//const TDF_ORDER_QUEUE& orderQueueData = pOrderQueueData[ii];
			std::shared_ptr<OrderQueue> quote(new OrderQueue());
			quote->szWindCode = pOrderQueueData->szWindCode;
			quote->szCode = pOrderQueueData->szCode;
			quote->nActionDay = pOrderQueueData->nActionDay;
			quote->nTime = pOrderQueueData->nTime;

			quote->nSide = pOrderQueueData->nSide;
			quote->nPrice = pOrderQueueData->nPrice;
			quote->nOrders = pOrderQueueData->nOrders;
			quote->nABItems = pOrderQueueData->nABItems;

			for (int jj = 0; jj < 200; jj++)
			{
				quote->nABVolume.push_back(pOrderQueueData->nABVolume[jj]);
			}
			//quote->nType = pOrderQueueData->pCodeInfo->nType;

			//char buffer1[128];
			//snprintf(buffer1, sizeof(buffer1), "OrderQueue: %s %s %d %d %d %d %d %d",
			//	pOrderQueueData->szWindCode, pOrderQueueData->szCode, pOrderQueueData->nActionDay, pOrderQueueData->nTime,
			//	pOrderQueueData->nSide, pOrderQueueData->nPrice, pOrderQueueData->nOrders, pOrderQueueData->nABItems);
			//LOG4CXX_INFO(logger, buffer1);
			
			if (lastStockOrderQueueMap[quote->szWindCode] == nullptr || lastStockOrderQueueMap[quote->szWindCode]->different(*quote.get()))
			{
				Publisher::Instance()->Add(quote);
			}
			else
			{
				char buff[128];
				sprintf(buff, "Ignore equals order queue:%s", quote->szWindCode.c_str());
				LOG4CXX_WARN(logger, buff);
			}

			lastStockOrderQueueMap[quote->szWindCode] = quote;
		}
	}
	break;
	case MSG_DATA_ORDER:
	{
		//TDF_ORDER* pOrderData = (TDF_ORDER*)pMsgHead->pData;
		for (int ii = 0; ii < nItemCount; ii++)
		{
			TDF_ORDER* pOrderData = GETRECORD(pMsgHead->pData, TDF_ORDER, ii);

			//char buffer[128];
			//snprintf(buffer, "ORDER : %s %d", pOrderData->szWindCode, pOrderData->nTime);

			//LOG4CXX_INFO(logger, buffer);

			//const TDF_ORDER& orderData = pOrderData[ii];
			std::shared_ptr<Order> quote(new Order());
			quote->szWindCode = pOrderData->szWindCode;
			quote->szCode = pOrderData->szCode;
			quote->nActionDay = pOrderData->nActionDay;
			quote->nTime = pOrderData->nTime;

			quote->nOrder = pOrderData->nOrder;
			quote->nPrice = pOrderData->nPrice;
			quote->nVolume = pOrderData->nVolume;
			quote->chOrderKind = pOrderData->chOrderKind;
			quote->chFunctionCode = pOrderData->chFunctionCode;
			//quote->nType = pOrderData->pCodeInfo->nType;

			//char buffer1[128];
			//snprintf(buffer1, sizeof(buffer1), "Order: %s %s %d %d %d %d %d %d %d",
			//	pOrderData->szWindCode, pOrderData->szCode, pOrderData->nActionDay, pOrderData->nTime,
			//	pOrderData->nOrder, pOrderData->nPrice, pOrderData->nVolume,
			//	pOrderData->chOrderKind, pOrderData->chFunctionCode);
			//LOG4CXX_INFO(logger, buffer1);

			if (lastStockOrderMap[quote->szWindCode] == nullptr || lastStockOrderMap[quote->szWindCode]->different(*quote.get()))
			{
				Publisher::Instance()->Add(quote);
			}
			else
			{
				char buff[128];
				sprintf(buff, "Ignore equals order:%s", quote->szWindCode.c_str());
				LOG4CXX_WARN(logger, buff);
			}

			lastStockOrderMap[quote->szWindCode] = quote;
		}
	}
	break;
	default:
	{
		//assert(0); log?
	}
	break;
	}
}

void RecvSys(THANDLE hTdf, TDF_MSG* pSysMsg)
{
	if (!pSysMsg || !hTdf)
	{
		return;
	}

	switch (pSysMsg->nDataType)
	{
	case MSG_SYS_DISCONNECT_NETWORK:
	{
		LOG4CXX_INFO(logger, "Network Disconnected");
	}
	break;
	case MSG_SYS_CONNECT_RESULT:
	{
		TDF_CONNECT_RESULT* pConnResult = (TDF_CONNECT_RESULT*)pSysMsg->pData;

		char buffer[128];
		if (pConnResult && pConnResult->nConnResult)
		{
			snprintf(buffer, sizeof(buffer), "Connected %s:%s user:%s", pConnResult->szIp, pConnResult->szPort, pConnResult->szUser);
			
		}
		else
		{
			snprintf(buffer, sizeof(buffer), "Connected %s:%s user:%s, Failed!", pConnResult->szIp, pConnResult->szPort, pConnResult->szUser);
		}
		LOG4CXX_INFO(logger, buffer);
	}
	break;
	case MSG_SYS_LOGIN_RESULT:
	{
		TDF_LOGIN_RESULT* pLoginResult = (TDF_LOGIN_RESULT*)pSysMsg->pData;

		//convert gb2312 to utf-8
		char utf_info[128];
		size_t len2 = 128;
		size_t len1 = strlen(pLoginResult->szInfo);
		code_convert("gb2312", "utf-8", pLoginResult->szInfo, &len1, utf_info, &len2);

		if (pLoginResult && pLoginResult->nLoginResult)
		{
			char buffer[128];
			snprintf(buffer, sizeof(buffer), "Login Successful! nMarkets:%d Info:%s", pLoginResult->nMarkets, utf_info);
			LOG4CXX_INFO(logger, buffer);

			for (int i = 0; i < pLoginResult->nMarkets; i++)
			{
				char buffer[128];
				snprintf(buffer, sizeof(buffer), "market:%s, dyn_date:%d", pLoginResult->szMarket[i], pLoginResult->nDynDate[i]);
				LOG4CXX_INFO(logger, buffer);
			}
		}
		else
		{
			char buffer[128];
			snprintf(buffer, sizeof(buffer), "Login Failed! info：%s", utf_info);
			LOG4CXX_ERROR(logger, buffer);
		}
	}
	break;
	case MSG_SYS_CODETABLE_RESULT:
	{
		TDF_CODE_RESULT* pCodeResult = (TDF_CODE_RESULT*)pSysMsg->pData;
		if (pCodeResult)
		{
			//convert gbk to utf-8
			char utf_info[128];
			size_t len2 = 128;
			size_t len1 = strlen(pCodeResult->szInfo);
			code_convert("gbk", "utf-8", pCodeResult->szInfo, &len1, utf_info, &len2);

			char buffer[128];
			snprintf(buffer, sizeof(buffer), "Receive CodeTable info:%s, Market Count : %d", utf_info, pCodeResult->nMarkets);
			LOG4CXX_INFO(logger, buffer);

			// reset vSubCodeTables
			vSubCodeTables.clear();;
			for (int i = 0; i < pCodeResult->nMarkets; i++)
			{
				char buffer[128];
				sprintf(buffer, "Market:%s, CodeCount:%d, CodeDate:%d", pCodeResult->szMarket[i], pCodeResult->nCodeCount[i], pCodeResult->nCodeDate[i]);
				LOG4CXX_INFO(logger, buffer);

				//获取代码表
				TDF_CODE* pCodeTable;
				unsigned int nItems;
				TDF_GetCodeTable(hTdf, pCodeResult->szMarket[i], &pCodeTable, &nItems);
				for (int i = 0; i < nItems; i++)
				{
					TDF_CODE& code = pCodeTable[i];
					//convert gbk to utf-8
					char utf_name[128];
					size_t len2 = 128;
					size_t len1 = strlen(code.szCNName);
					code_convert("gbk", "utf-8", code.szCNName, &len1, utf_name, &len2);

					std::shared_ptr<CodeTable> data(new CodeTable());
					data->szWindCode = code.szWindCode;
					data->szCode = code.szCode;
					data->szMarket = code.szMarket;
					data->szENName = code.szENName;
					data->szCNName = code.szCNName;
					data->nType = code.nType;

					//LOG4CXX_INFO(logger, pIndexData->szWindCode);
					Publisher::Instance()->Add(data);

					// 过滤不需要的数据
					int nDetailType = code.nType & 0xFF;

					int Filter = 0;
					switch (nDetailType)
					{
					case 0X01: // 交易所指数
						break;
					case 0X10: // A股
						break;
					case 0X11: // 中小板股
						break;
					case 0X12: // 创业板股
						break;
					case 0X70: // 指数期货
						break;
					case 0X71: // 商品期货
						break;
					case 0X78: // 指数期货连线CX
						break;
					case 0X79: // 指数期货连线CC
						break;
					case 0X7a: // 商品期货连线CX
						break;
					case 0X7b: // 商品期货连线CC
						break;
						// 这两个应该是位与前的值
					case 0x4000010: // 上海ST
						break;
					case 0x4000011: // 深圳ST
						break;
					//case 0X16:continue;	// B股
					//case 0X17:continue;	// H股
					//case 0X1a:continue;	// US
					//case 0X1b:continue;	// US ADR
					//case 0X1e:continue;	// 扩展板块股票（港）
					//case 0X20:continue;	// 基金
					//case 0X21:continue;	// 未上市开放基金
					//case 0X22:continue;	// 上市开放基金
					//case 0X23:continue;	// 交易型开放式指数基金
					//case 0X25:continue;	// 扩展板块基金（港）
					//case 0X26:continue;	// 分级基金
					//case 0X27:continue;	// 仅申赎基金
					//case 0X30:continue;	// 政府债券
					//case 0X31:continue;	// 企业债券
					//case 0X32:continue;	// 金融债券
					//case 0X33:continue;	// 可转债券
					//case 0X34:continue;	// 债券预发行
					//case 0X35:continue;	// 文档未说明
					//case 0X40:continue;	// 国债回购
					//case 0X41:continue;	// 企债回购
					//case 0X60:continue;	// 权证
					//case 0X61:continue;	// 认购权证
					//case 0X62:continue;	// 认沽权证
					//case 0X64:continue;	// 认购权证（B股）
					//case 0X65:continue;	// 认沽权证（B股）
					//case 0X66:continue;	// 牛证（moo-cow）
					//case 0X67:continue;	// 熊证（bear）
					default:
						continue; // 过滤
					}
					
					//char buffer1[128];
					//sprintf(buffer1, "windcode:%s, code:%s, market:%s, name:%s, nType:0x%x", code.szWindCode, code.szCode, code.szMarket, utf_name, code.nType);
					//LOG4CXX_INFO(logger, buffer1);

					std::string subCodeStr;
					std::string strMarket(code.szMarket);
					int idx = strMarket.find_first_of("-"); 
					if (idx != -1)
					{
						// TDFAPI3 SH-2-0格式
						subCodeStr = (std::string(code.szCode) + "." + std::string(code.szMarket).substr(0, idx));
					}
					else
					{
						// TDFAPI2 SH、SZ格式
						subCodeStr = (std::string(code.szCode) + "." + strMarket);
					}
						
					vSubCodeTables[code.szWindCode] = subCodeStr;
				}
				TDF_FreeArr(pCodeTable);
			}

			LOG4CXX_INFO(logger, "End of Receiving CodeTable");

			m_autoEvent.signal();
			
			//  订阅过滤后的代码(需要放到主线程订阅，回调函数中订阅不起效果)
			//TDF_SetSubscription(hTdf, subCodeStr.c_str(), SUBSCRIPTION_SET);
		}
	}
	break;
	case MSG_SYS_QUOTATIONDATE_CHANGE:
	{
		TDF_QUOTATIONDATE_CHANGE* pChange = (TDF_QUOTATIONDATE_CHANGE*)pSysMsg->pData;
		if (pChange)
		{
			char buff[256];
			sprintf(buff, "收到行情日期变更通知，即将自动重连！交易所：%s, 原日期:%d, 新日期：%d", pChange->szMarket, pChange->nOldDate, pChange->nNewDate);
			LOG4CXX_INFO(logger, buff);
		}
	}
	break;
	case MSG_SYS_MARKET_CLOSE:
	{
		TDF_MARKET_CLOSE* pCloseInfo = (TDF_MARKET_CLOSE*)pSysMsg->pData;
		if (pCloseInfo)
		{
			//convert gbk to utf-8
			char utf_info[64];
			size_t len2 = 64;
			size_t len1 = strlen(pCloseInfo->chInfo);
			code_convert("gbk", "utf-8", pCloseInfo->chInfo, &len1, utf_info, &len2);

			char buff[256];
			sprintf(buff, "Market:%s closed, time:%d, info:%s", pCloseInfo->szMarket, pCloseInfo->nTime, utf_info);
			LOG4CXX_INFO(logger, buff);
		}
	}
	break;
	case MSG_SYS_HEART_BEAT:
	{
		LOG4CXX_INFO(logger, "Receiving heartbeat from server");
	}
	break;
	default:
		//assert(0);  log??
		break;
	}
}

std::set<std::string> subbedWindCodeList;

int subscribe(const char* ticker)
{
	if (hTDF == nullptr || strlen(ticker) == 0) return -1;

	std::string szWindCode(ticker);
	transform(szWindCode.begin(), szWindCode.end(), szWindCode.begin(), ::toupper); //将小写都转换成大写
	
	// server启动默认已订阅
	CServerIniFile serverIni;
	std::string stockList = std::string(serverIni.StockList);
	transform(stockList.begin(), stockList.end(), stockList.begin(), ::toupper); //将小写都转换成大写
	if (stockList.empty() || stockList.find(szWindCode) != std::string::npos)
	{
		return 0;
	}

	auto iter = vSubCodeTables.find(szWindCode);
	if (iter == vSubCodeTables.end())
	{
		char buff[128];
		sprintf(buff, "subscribe: can't find %s in codetable list!", ticker);
		LOG4CXX_ERROR(logger, buff);
		return -2;
	}
	else if (subbedWindCodeList.find(szWindCode) != subbedWindCodeList.end())
	{
		char buff[128];
		sprintf(buff, "repeat subscribe: %s!", ticker);
		LOG4CXX_ERROR(logger, buff);
		return -3;
	}

	// 期货大小写敏感，使用实际的代码订阅，不能使用WindCode订阅
	std::string subCode = iter->second;
	int ret = TDF_SetSubscription(hTDF, subCode.c_str(), SUBSCRIPTION_ADD);
	if (ret == 0)
		subbedWindCodeList.insert(szWindCode);
	char buff[128];
	sprintf(buff, "TDF_SetSubscription Add: %s ret=%d", subCode.c_str(), ret);
	LOG4CXX_INFO(logger, buff);
	return ret;
}

int unsubscribe(const char* ticker)
{
	// ticker实际是WindCode
	if (hTDF == nullptr || strlen(ticker) == 0) return -1;

	std::string szWindCode(ticker);
	transform(szWindCode.begin(), szWindCode.end(), szWindCode.begin(), ::toupper); //将小写都转换成大写
	auto iter = vSubCodeTables.find(szWindCode);
	if (iter == vSubCodeTables.end())
	{
		char buff[256];
		sprintf(buff, "unsubscribe: can't find %s in codetable list!", ticker);
		LOG4CXX_ERROR(logger, buff);
		return -2;
	}
	else if (subbedWindCodeList.find(szWindCode) == subbedWindCodeList.end())
	{
		char buff[256];
		sprintf(buff, "never subscribed: %s!", ticker);
		LOG4CXX_ERROR(logger, buff);
		return -3;
	}

	// 期货大小写敏感，使用实际的代码订阅，不能使用WindCode订阅
	std::string subCode = iter->second;
	int ret = TDF_SetSubscription(hTDF, subCode.c_str(), SUBSCRIPTION_DEL);
	if (ret == 0)
		subbedWindCodeList.erase(szWindCode);
	char buff[128];
	sprintf(buff, "TDF_SetSubscription Del: %s ret=%d", subCode.c_str(), ret);
	LOG4CXX_INFO(logger, buff);
	return ret;
}