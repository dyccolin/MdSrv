#include "Publisher.h"
#include "IniFile.h"
#include <unistd.h>

int subscribe(const char* ticker);
int unsubscribe(const char* ticker);

#define HEARTBEAT "HEARTBEAT"
#define CODETABLE "CODETABLE"

IMPLEMENT_SINGLETON(Publisher)
static log4cxx::LoggerPtr logger = log4cxx::Logger::getLogger("syslog");
Publisher::Publisher()
{
	MUTEX_INIT(&m_mutex);

	this->m_ctx = new zmq::context_t(1);
}

Publisher::~Publisher()
{
	MUTEX_DESTROY(&m_mutex);

	this->m_ctx->close();
}

void Publisher::Start()
{
	std::thread t0(std::bind(&Publisher::LvcThread, this));
	t0.detach();

	std::thread t1(std::bind(&Publisher::PubThread, this));
	t1.detach();

	std::thread t2(std::bind(&Publisher::HeartBeatThread, this));
	t2.detach();
}

void  Publisher::Add(shared_ptr<DataBase> item)
{
	if (item == nullptr) return;
	try
	{
		MUTEX_LOCK(&m_mutex);
		m_itemList.push(item);
		// 唤醒Get阻塞
		m_autoEvent.signal();
		MUTEX_UNLOCK(&m_mutex);
	}
	catch (exception ex)
	{
		throw ex;
	}
}

vector<shared_ptr<DataBase>> Publisher::Get(void)
{
	std::vector<shared_ptr<DataBase>> vec;

	if (m_itemList.size() == 0)
	{
		m_autoEvent.wait();
	}

	MUTEX_LOCK(&m_mutex);
	while (m_itemList.size() > 0)
	{
		vec.push_back(m_itemList.front());
		m_itemList.pop();
	}
	MUTEX_UNLOCK(&m_mutex);

	return vec;
}

void Publisher::PubThread()
{
	//std::map<std::pair<std::string, DataType>, int> m_mapLastMdTime;

	try
	{
		zmq::socket_t PubSocket(*m_ctx, ZMQ_PAIR);
		PubSocket.setsockopt(ZMQ_SNDHWM, 0);
		PubSocket.bind("inproc://backend");

		while (true)
		{
			std::vector<shared_ptr<DataBase>> vec = this->Get();

			for (auto iter = vec.begin(); iter != vec.end(); iter++)
			{
				int nTime = 0;
				std::string sTopic;
				msgpack::sbuffer sbuf; //??
				shared_ptr<DataBase> pBase = *iter;
				if (pBase != nullptr)
				{
					switch (pBase->eType)
					{
					case eStockQuote:
					{
						shared_ptr<StockQuote> data = std::dynamic_pointer_cast<StockQuote>(pBase);

						//int nDayTime = /*data->nActionDay * 1000000000 +*/ data->nTime; // 暂不考虑跨日
						//auto pair = std::make_pair(data->szWindCode, data->eType);
						//if (m_mapLastMdTime.find(pair) != m_mapLastMdTime.end() && m_mapLastMdTime[pair] >= nDayTime) continue;
						//m_mapLastMdTime[pair] = nDayTime;

						msgpack::pack(sbuf, *data.get());
						sTopic = data->szWindCode;
						nTime = data->nTime;
						break;
					}
					case eFutureQuote:
					{
						shared_ptr<FutureQuote> data = std::dynamic_pointer_cast<FutureQuote>(pBase);

						//int nDayTime = /*data->nActionDay * 1000000000 +*/ data->nTime; // 暂不考虑跨日
						//auto pair = std::make_pair(data->szWindCode, data->eType);
						//if (m_mapLastMdTime.find(pair) != m_mapLastMdTime.end() && m_mapLastMdTime[pair] >= nDayTime) continue;
						//m_mapLastMdTime[pair] = nDayTime;

						msgpack::pack(sbuf, *data.get());
						sTopic = data->szWindCode;
						nTime = data->nTime;
						break;
					}
					case eIndexQuote:
					{
						shared_ptr<IndexQuote> data = std::dynamic_pointer_cast<IndexQuote>(pBase);

						//int nDayTime = /*data->nActionDay * 1000000000 +*/ data->nTime; // 暂不考虑跨日
						//auto pair = std::make_pair(data->szWindCode, data->eType);
						//if (m_mapLastMdTime.find(pair) != m_mapLastMdTime.end() && m_mapLastMdTime[pair] >= nDayTime) continue;
						//m_mapLastMdTime[pair] = nDayTime;

						msgpack::pack(sbuf, *data.get());
						sTopic = data->szWindCode;
						nTime = data->nTime;
						break;
					}
					case eTransaction:
					{
						shared_ptr<Transaction> data = std::dynamic_pointer_cast<Transaction>(pBase);

						//int nDayTime = /*data->nActionDay * 1000000000 +*/ data->nTime; // 暂不考虑跨日
						//auto pair = std::make_pair(data->szWindCode, data->eType);
						//if (m_mapLastMdTime.find(pair) != m_mapLastMdTime.end() && m_mapLastMdTime[pair] >= nDayTime) continue;
						//m_mapLastMdTime[pair] = nDayTime;

						msgpack::pack(sbuf, *data.get());
						sTopic = data->szWindCode;
						break;
					}
					case eOrder:
					{
						shared_ptr<Order> data = std::dynamic_pointer_cast<Order>(pBase);

						//int nDayTime = /*data->nActionDay * 1000000000 +*/ data->nTime; // 暂不考虑跨日
						//auto pair = std::make_pair(data->szWindCode, data->eType);
						//if (m_mapLastMdTime.find(pair) != m_mapLastMdTime.end() && m_mapLastMdTime[pair] >= nDayTime) continue;
						//m_mapLastMdTime[pair] = nDayTime;

						msgpack::pack(sbuf, *data.get());
						sTopic = data->szWindCode;
						break;
					}
					case eOrderQueue:
					{
						shared_ptr<OrderQueue> data = std::dynamic_pointer_cast<OrderQueue>(pBase);

						//int nDayTime = /*data->nActionDay * 1000000000 +*/ data->nTime; // 暂不考虑跨日
						//auto pair = std::make_pair(data->szWindCode, data->eType);
						//if (m_mapLastMdTime.find(pair) != m_mapLastMdTime.end() && m_mapLastMdTime[pair] >= nDayTime) continue;
						//m_mapLastMdTime[pair] = nDayTime;

						msgpack::pack(sbuf, *data.get());
						sTopic = data->szWindCode;
						break;
					}
					case eCodeTable:
					{
						shared_ptr<CodeTable> data = std::dynamic_pointer_cast<CodeTable>(pBase);

						//int nDayTime = /*data->nActionDay * 1000000000 +*/ data->nTime; // 暂不考虑跨日
						//auto pair = std::make_pair(data->szWindCode, data->eType);
						//if (m_mapLastMdTime.find(pair) != m_mapLastMdTime.end() && m_mapLastMdTime[pair] >= nDayTime) continue;
						//m_mapLastMdTime[pair] = nDayTime;

						msgpack::pack(sbuf, *data.get());
						sTopic = data->szWindCode;
						break;
					}
					case eHeartbeat:
					{
						shared_ptr<Heartbeat> data = std::dynamic_pointer_cast<Heartbeat>(pBase);

						//int nDayTime = /*data->nActionDay * 1000000000 +*/ data->nTime; // 暂不考虑跨日
						//auto pair = std::make_pair(data->szWindCode, data->eType);
						//if (m_mapLastMdTime.find(pair) != m_mapLastMdTime.end() && m_mapLastMdTime[pair] >= nDayTime) continue;
						//m_mapLastMdTime[pair] = nDayTime;

						msgpack::pack(sbuf, *data.get());
						sTopic = HEARTBEAT;
						break;
					}
					default:
					{
						printf("Unknown DataType\n");
						continue;
					}
					}

					// 调用ZMQ广播
					zmq::message_t topic(sTopic.c_str(), sTopic.length());

					char buff[32];
					memset(buff, 0, sizeof(buff));
					sprintf(buff, "%d", pBase->eType);
					zmq::message_t dataType(&pBase->eType, sizeof(pBase->eType));;
					zmq::message_t msg(sbuf.data(), sbuf.size());

					// 发送到LVC thread
					PubSocket.send(topic, ZMQ_SNDMORE);		// topic
					PubSocket.send(dataType, ZMQ_SNDMORE);	// DataType
					PubSocket.send(msg);

					//printf("Send data: topic=%s type=%d nTime=%d\n", sTopic.c_str(), pBase->eType, nTime);

					// 更新最后一次消息发送时间
					time(&m_LastPubMsgTime);
				}
			}
		}

		PubSocket.close();
		//context.close();

		LOG4CXX_INFO(logger, "Pub thread ended");
	}
	catch (const std::exception& ex)
	{
		printf(ex.what());
		LOG4CXX_ERROR(logger, ex.what());
	}
}

void Publisher::LvcThread()
{
	string configFileName("Server.ini");
	IniFile ini(configFileName);

	// ZMQ配置
	char pubAddr[128];
	ini.read_profile_string("zmq", "Port", pubAddr, sizeof(pubAddr), nullptr, configFileName.c_str());

	if (strlen(pubAddr) == 0)
	{
		LOG4CXX_ERROR(logger, "zmq port is empty");
		exit(0);
	}

	try
	{
		zmq::socket_t frontend(*m_ctx, ZMQ_PAIR);
		zmq::socket_t backend(*m_ctx, ZMQ_XPUB); 

		// 收发无高水位限制高水位
		frontend.setsockopt(ZMQ_RCVHWM, 0);
		backend.setsockopt(ZMQ_SNDHWM, 0);
		//  Configure socket to not wait at close time
		int linger = 0;
		backend.setsockopt(ZMQ_LINGER, &linger, sizeof(linger));
		// 属性值是1表示允许所有的订阅信息通过传输到上游
		backend.setsockopt(ZMQ_XPUB_VERBOSE, 1);
		frontend.connect("inproc://backend");
		backend.bind(pubAddr);

		//  Store last instance of each topic in a cache
		// <Ticker,<DataType,MsgBody>>
		std::map<std::string, std::map<DataType, std::string>> cacheMap;

		while (true)
		{
			zmq::pollitem_t items[] = { { frontend, 0, ZMQ_POLLIN, 0 },{ backend, 0, ZMQ_POLLIN, 0 } };
			zmq::poll(items, 2, -1);
			
			//  Any new topic data we cache and then forward
			if (items[0].revents & ZMQ_POLLIN) {
				zmq::message_t msgTopic;
				zmq::message_t msgDataType;
				zmq::message_t msgBody;
				frontend.recv(&msgTopic);
				frontend.recv(&msgDataType);
				frontend.recv(&msgBody);
				//DataType eDataType = *dataType.data<DataType>();

				std::string sTopic = std::string(static_cast<char*>(msgTopic.data()), msgTopic.size());
				DataType eDataType = *msgDataType.data<DataType>();
				std::string sMsgBody = std::string(static_cast<char*>(msgBody.data()), msgBody.size());

				if (eDataType == eStockQuote || eDataType == eFutureQuote || eDataType == eIndexQuote || eDataType == eCodeTable || eDataType == eHeartbeat)
				{
					auto iter_Ticker = cacheMap.find(sTopic);
					if (iter_Ticker == cacheMap.end())
					{
						cacheMap[sTopic] = std::map<DataType, string>();
					}
					else
					{
						auto iter_Type = iter_Ticker->second.find(eDataType);

						if (iter_Type != iter_Ticker->second.end())
						{
							iter_Ticker->second.erase(iter_Type);
						}
					}
					cacheMap[sTopic][eDataType] = sMsgBody;
				}

				// 发送到Client
				if (eDataType == eCodeTable)
				{
					// codetable只在订阅时推送一次
				}
				else
				{
					backend.send(msgTopic, ZMQ_SNDMORE);
					backend.send(msgDataType, ZMQ_SNDMORE);
					backend.send(msgBody);

					//char buff[32];
					//sprintf(buff, "Publish data:%s %d", sTicker.c_str(), eDataType);
					//LOG4CXX_INFO(logger, buff);
				}

				//LOG4CXX_DEBUG(logger, msgBody);
			}

			//  When we get a new subscription, we pull data from the cache:
			if (items[1].revents & ZMQ_POLLIN) {
				char buff[32];
				int size = backend.recv(buff, sizeof(buff), 0);
				assert(size >= 1);
				// 指定topic订阅
				char* topic = (char*)malloc(size);
				memset(topic, 0, size);
				memcpy(topic, buff + 1, size - 1);
				std::string toUpper(topic);
				transform(toUpper.begin(), toUpper.end(), toUpper.begin(), ::toupper); //将小写都转换成大写

				// Event is one byte 0 = unsub or 1 = sub, followed by topic
				auto event = buff[0];
				char logStr[256];
				sprintf(logStr, "event:%d size:%d topic:%s", event, size, topic);
				LOG4CXX_INFO(logger, logStr);

				if (event == 1)
				{
					if (size == 1)
					{
						// 全订阅
						for (auto iter_Topic = cacheMap.begin(); iter_Topic != cacheMap.end(); iter_Topic++)
						{
							zmq::message_t topic(iter_Topic->first.c_str(), iter_Topic->first.length());
							for (auto iter_Type = iter_Topic->second.begin(); iter_Type != iter_Topic->second.end(); iter_Type++)
							{
								zmq::message_t dataType(&iter_Type->first, sizeof(iter_Type->first));
								zmq::message_t msgBody(iter_Type->second.c_str(), iter_Type->second.length());

								// 推送行情数据
								if (iter_Type->first == eStockQuote || iter_Type->first == eFutureQuote || iter_Type->first == eIndexQuote)
								{
									backend.send(topic, ZMQ_SNDMORE);
									backend.send(dataType, ZMQ_SNDMORE);
									backend.send(msgBody);

									//printf("LVC all sub:%s %d\n", iter_Ticker->first.c_str(), iter_Type->first);
								}
							}
						}
					}
					else
					{
						// 如果是订阅codetable
						if (strcasecmp(topic, CODETABLE) == 0)
						{
							////zmq::message_t zmqmsgTopic(topic, strlen(topic)); // 不能在此处定义topic，可能每个zmqmsg发送后即失效
							//for (auto iter_Ticker = cacheMap.begin(); iter_Ticker != cacheMap.end(); iter_Ticker++)
							//{
							//	for (auto iter_Type = iter_Ticker->second.begin(); iter_Type != iter_Ticker->second.end(); iter_Type++)
							//	{
							//		if (iter_Type->first == eCodeTable)
							//		{
							//			zmq::message_t msgTopic(topic, strlen(topic));
							//			zmq::message_t dataType(&iter_Type->first, sizeof(iter_Type->first));
							//			zmq::message_t msgBody(iter_Type->second.c_str(), iter_Type->second.length());

							//			backend.send(msgTopic, ZMQ_SNDMORE);
							//			backend.send(dataType, ZMQ_SNDMORE);
							//			backend.send(msgBody);

							//			//printf("LVC codetable:%s %d\n", iter_Ticker->first.c_str(), iter_Type->first);
							//		}
							//	}
							//}

							////printf("LVC codetable\n");
						}
						// 如果是订阅heartbeat
						else if (strcasecmp(topic, HEARTBEAT) == 0)
						{
							auto iter = cacheMap.find(HEARTBEAT);
							if (iter != cacheMap.end())
							{
								for (auto iter_Type = iter->second.begin(); iter_Type != iter->second.end(); iter_Type++)
								{
									if (iter_Type->first == eHeartbeat)
									{
										zmq::message_t msgTopic(topic, strlen(topic));
										zmq::message_t dataType(&iter_Type->first, sizeof(iter_Type->first));
										zmq::message_t msgBody(iter_Type->second.c_str(), iter_Type->second.length());

										backend.send(msgTopic, ZMQ_SNDMORE);
										backend.send(dataType, ZMQ_SNDMORE);
										backend.send(msgBody);

										//printf("LVC heartbeat:%s %d\n", iter_Ticker->first.c_str(), iter_Type->first);
									}
								}
							}
						}
						else if (strlen(topic) > 5)
						{
							// 订阅行情
							subscribe(toUpper.c_str());

							auto iter_Ticker = cacheMap.find(toUpper);
							if (iter_Ticker != cacheMap.end()) {
								// 先推送CodeTable
								auto iter_Type = iter_Ticker->second.find(eCodeTable);
								if (iter_Type != iter_Ticker->second.end()) {
									zmq::message_t msgTopic(topic, strlen(topic));
									zmq::message_t msgDataType(&iter_Type->first, sizeof(iter_Type->first));
									zmq::message_t msgBody(iter_Type->second.c_str(), iter_Type->second.length());

									backend.send(msgTopic, ZMQ_SNDMORE);
									backend.send(msgDataType, ZMQ_SNDMORE);
									backend.send(msgBody);

									//printf("LVC one sub:%s %d\n", iter_Ticker->first.c_str(), iter_Type->first);
								}

								// 再推送行情数据
								for (auto iter_Type = iter_Ticker->second.begin(); iter_Type != iter_Ticker->second.end(); iter_Type++)
								{
									if (iter_Type->first == eStockQuote || iter_Type->first == eFutureQuote || iter_Type->first == eIndexQuote)
									{
										zmq::message_t msgTopic(iter_Ticker->first.c_str(), iter_Ticker->first.length());
										zmq::message_t msgDataType(&iter_Type->first, sizeof(iter_Type->first));
										zmq::message_t msgBody(iter_Type->second.c_str(), iter_Type->second.length());

										backend.send(msgTopic, ZMQ_SNDMORE);
										backend.send(msgDataType, ZMQ_SNDMORE);
										backend.send(msgBody);

										//printf("LVC one sub:%s %d\n", iter_Ticker->first.c_str(), iter_Type->first);
									}
								}
							}
						}
					}
				}
				//else if (event == 0)
				//{
				//	// size>1表示非全订阅
				//	if (size > 1 && strlen(topic) > 5)
				//	{
				//		if (strcasecmp(topic, CODETABLE) != 0 && strcasecmp(topic, HEARTBEAT) != 0)
				//		{
				//			// 计数+1
				//			m_mapSubscribeCounter[toUpper]--;

				//			sprintf(logStr, "unsub %s cnt--:%d", toUpper.c_str(), m_mapSubscribeCounter[toUpper]);
				//			LOG4CXX_INFO(logger, logStr);

				//			if (m_mapSubscribeCounter[toUpper] == 0)
				//			{
				//				// 取消订阅
				//				 unsubscribe(toUpper.c_str());
				//			}
				//		}
				//	}
				//}

				free(topic);
			}
		}

		frontend.close();
		backend.close();
		//context.close();

		LOG4CXX_INFO(logger, "LVC thread ended");
	}
	catch (const std::exception& ex)
	{
		printf(ex.what());
		LOG4CXX_ERROR(logger, ex.what());
	}
} 

void Publisher::HeartBeatThread()
{
	time_t m_SrvStartTime;
	time(&m_SrvStartTime);

	// 无数据传输时间
	int REQUEST_TIMEOUT = 10;

	while (true)
	{
		if (time(NULL) - m_LastPubMsgTime  > REQUEST_TIMEOUT)
		{
			std::shared_ptr<Heartbeat> hb = std::make_shared<Heartbeat>();
			hb->SrvStartTime = m_SrvStartTime;
			Publisher::Instance()->Add(hb);

			char logbuf[128];
			sprintf(logbuf, "Send Heartbeat:SrvStartTime=%ld", hb->SrvStartTime);
			LOG4CXX_INFO(logger, logbuf);
		}

		usleep(REQUEST_TIMEOUT * 1000 * 1000);
	}
}