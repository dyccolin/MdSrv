#pragma once
#include <stdio.h>  
#include <iostream>  
#include <fstream>  
#include <sstream> 
#include "IOUtil.h"

#include <aes.h>  
#include <filters.h>  
#include <modes.h>

using namespace std;

class LicenseHelper
{
public:
	LicenseHelper() {
		initKV("HzTDFGw");
	};
	~LicenseHelper() {};

	void initKV(string txtKey)
	{
		//memset( key, 0x00, CryptoPP::AES::DEFAULT_KEYLENGTH );  
		//memset( iv, 0x00, CryptoPP::AES::BLOCKSIZE );  

		txtKey.resize(CryptoPP::AES::DEFAULT_KEYLENGTH, ' ');
		for (int j = 0; j < CryptoPP::AES::DEFAULT_KEYLENGTH; ++j)
		{
			key[j] = txtKey.at(j);
		}

		for (int i = 0; i < CryptoPP::AES::BLOCKSIZE; ++i)
		{
			iv[i] = txtKey.at(i);
		}
	};

	string encrypt(string plainText)
	{
		if (plainText.empty()) return "";

		string cipherText;
		string cipherTextHex;

		try {
			//  
			CryptoPP::AES::Encryption aesEncryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
			CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncryption(aesEncryption, iv);
			CryptoPP::StreamTransformationFilter stfEncryptor(cbcEncryption, new CryptoPP::StringSink(cipherText));
			stfEncryptor.Put(reinterpret_cast<const unsigned char*>(plainText.c_str()), plainText.length() + 1);
			stfEncryptor.MessageEnd();

			for (int i = 0; i < cipherText.size(); i++)
			{
				char ch[3] = { 0 };
				sprintf(ch, "%02x", static_cast<byte>(cipherText[i]));
				cipherTextHex += ch;
			}
		}
		catch (const CryptoPP::Exception &e)
		{
			char buf[256];
			printf(buf, "CryptoPP::Exception caught:%s", e.what());
			return "";
		}
		catch (const std::exception &e)
		{
			char buf[256];
			printf(buf, "std::exception caught:%s", e.what());
			return "";
		}


		return cipherTextHex;
	};

	string decrypt(string cipherTextHex)
	{
		if (cipherTextHex.empty()) return "";

		string cipherText;
		string decryptedText;
		try {
			int i = 0;
			while (true)
			{
				char c;
				int x;
				stringstream ss;
				ss << hex << cipherTextHex.substr(i, 2).c_str();
				ss >> x;
				c = (char)x;
				cipherText += c;
				if (i >= cipherTextHex.length() - 2)break;
				i += 2;
			}

			//  
			CryptoPP::AES::Decryption aesDecryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
			CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption(aesDecryption, iv);
			CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink(decryptedText));
			stfDecryptor.Put(reinterpret_cast<const unsigned char*>(cipherText.c_str()), cipherText.size());

			stfDecryptor.MessageEnd();
		}
		catch (const CryptoPP::Exception &e)
		{
			char buf[256];
			printf(buf, "CryptoPP::Exception caught:%s", e.what());
			return "";
		}
		catch (const std::exception &e)
		{
			char buf[256];
			printf(buf, "std::exception caught:%s", e.what());
			return "";
		}
		
		return decryptedText;
	};

	string readCipher()
	{
		char path[128];
		string configFileName("Server.ini");
		IniFile ini(configFileName);
		ini.read_profile_string("license", "path", path, sizeof(path), "./HzTDFGw.dat", configFileName.c_str());
		ifstream in(path);

		string line;
		string decryptedText;
		while (getline(in, line))
		{
			if (line.length() > 1)
			{
				decryptedText += decrypt(line) + "\n";
			}
			line.clear();
		}

		//cout << "readCipher finish " << endl;
		in.close();

		return decryptedText;
	};

	bool IsNumber(const char * num)
	{
		int length = strlen(num);
		for (int i = 0; i < length; i++)
		{
			if (i == 0 && (num[i] == '+' || num[i] == '-'))
			{
				if (length > 1)
					continue;
				return false;
			}
			if (!isdigit(num[i]))
				return false;
		}
		return true;
	};

private:
	byte key[CryptoPP::AES::DEFAULT_KEYLENGTH];
	byte iv[CryptoPP::AES::BLOCKSIZE];
};