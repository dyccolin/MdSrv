#pragma once
#include <iostream>  
#include <fstream> 

#include "IniFile.h"

using namespace std;

class CServerIniFile
{
public:
	char IP[32];
	char Port[8];
	char User[32];
	char Password[32];
	char StockList[2048];
	char Markets[200];
	unsigned int Date;
	unsigned int Time;
	char DataType[200];
	string Subscriptions;

	char Log4cxxProperties[200];

	char MaketDataServerPort[100];

	bool bLoginCTP;

public:

	CServerIniFile()
	{
		Read();
	}

	~CServerIniFile()
	{
	}

private:
	void Read()
	{
		string configFileName("Server.ini");
		IniFile ini(configFileName);
		// ���
		ini.read_profile_string("honghui", "IP", IP, sizeof(IP), nullptr, configFileName.c_str());
		ini.read_profile_string("honghui", "Port", Port, sizeof(Port), nullptr, configFileName.c_str());
		ini.read_profile_string("honghui", "User", User, sizeof(User), nullptr, configFileName.c_str());
		ini.read_profile_string("honghui", "Password", Password, sizeof(Password), nullptr, configFileName.c_str());
		ini.read_profile_string("honghui", "StockList", StockList, sizeof(StockList), nullptr, configFileName.c_str());
		ini.read_profile_string("honghui", "Markets", Markets, sizeof(Markets), nullptr, configFileName.c_str());
		ini.read_profile_string("honghui", "DataType", DataType, sizeof(DataType), nullptr, configFileName.c_str());
		
		char strDate[32];
		char strTime[32];

		ini.read_profile_string("honghui", "Date", strDate, sizeof(strDate), nullptr, configFileName.c_str());
		ini.read_profile_string("honghui", "Time", strTime, sizeof(strTime), nullptr, configFileName.c_str());

		Date = atoi(strDate);
		Time = atoi(strTime);

		// loginCTP
		char bLogin[8];
		ini.read_profile_string("ctp", "bLogin", bLogin, sizeof(bLogin), nullptr, configFileName.c_str());
		bLoginCTP = strcasecmp("true", bLogin) == 0;

		// Log4cxxProperties����
		ini.read_profile_string("log4cxx", "log4cxx_properties", Log4cxxProperties, sizeof(Log4cxxProperties), nullptr, configFileName.c_str());

		//// ��ȡszSubScriptions
		//std::ifstream file("SubScriptions.txt");
		//getline(file, Subscriptions);
		//file.close();
	}
};

