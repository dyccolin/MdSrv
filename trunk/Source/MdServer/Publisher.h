#pragma once

#include "Common.h"
#include "singleton.h"
#include "AutoResetEvent.h"
#include "MdData.h"
#include <zmq.hpp>

using namespace std;

class Publisher
{
	DECLARE_SINGLETON(Publisher)
public:
	Publisher();
	~Publisher();

	void Start();
	void Add(shared_ptr<DataBase> item);
	vector<shared_ptr<DataBase>> Get(void);
	void PubThread();
	void LvcThread();
	void HeartBeatThread();

private:
	std::queue<shared_ptr<DataBase>> m_itemList;
	MUTEX_T m_mutex;
	AutoResetEvent m_autoEvent;
	time_t m_LastPubMsgTime;

	int LvcPort;
	int PubPort;

	zmq::context_t* m_ctx;
};

