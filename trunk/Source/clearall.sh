#clear MdServer
rm -rf ./MdServer/obj/x64/Release/
rm -rf ./MdServer/bin/x64/Release/

rm -r ./Server.ini
rm -r ./MdServer.log4cxx
rm -r ./MdServer.out

#clear MdClient
rm -rf ./MdClient/obj/x64/Release/
rm -rf ./MdCom/obj/x64/Release/
rm -rf ./MdApi/obj/x64/Release/
rm -rf ./MdClient/bin/x64/Release/
rm -rf ./MdCom/bin/x64/Release/
rm -rf ./MdApi/bin/x64/Release/

rm -r ./Api.ini
rm -r ./MdClient.log4cxx
rm -r ./SubScriptions.txt
rm -r ./MdClient.out
rm -f ./deamon_outfile

#clear logs
rm -rf ./logs
rm -rf ./TDFAPI27_LOG*

#clear Api
rm -rf ./Api

#clear link
rm -rf ../ThirdParty/ZMQ/lib/libzmq.so
rm -rf ../ThirdParty/msgpack/lib/libmsgpackc.so
rm -rf ../ThirdParty/log4cxx/lib/liblog4cxx.so
rm -rf ../ThirdParty/log4cxx/lib/libapr-1.so
rm -rf ../ThirdParty/log4cxx/lib/libaprutil-1.so
rm -rf ../ThirdParty/log4cxx/lib/libexpat.so