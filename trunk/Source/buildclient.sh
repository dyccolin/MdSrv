#build MdServer release
export PATH=/usr/local/gcc-4.8.5/bin:$PATH

mkdir -p ./MdClient/obj/x64/Release/
mkdir -p ./MdCom/obj/x64/Release/
mkdir -p ./MdApi/obj/x64/Release/
mkdir -p ./MdClient/bin/x64/Release/
mkdir -p ./MdCom/bin/x64/Release/
mkdir -p ./MdApi/bin/x64/Release/

mkdir -p ./Api/include
ln -sf ../../MdApi/MdApi.h ./Api/include
ln -sf ../../MdCom/MdData.h ./Api/include
ln -sf ../../../ThirdParty/msgpack/include/msgpack ./Api/include
ln -sf ../../../ThirdParty/msgpack/include/msgpack.h ./Api/include
ln -sf ../../../ThirdParty/msgpack/include/msgpack.hpp ./Api/include

#创建link
ln -sf libzmq.so.5 ../ThirdParty/ZMQ/lib/libzmq.so
ln -sf libmsgpackc.so.2 ../ThirdParty/msgpack/lib/libmsgpackc.so
ln -sf liblog4cxx.so.10 ../ThirdParty/log4cxx/lib/liblog4cxx.so
ln -sf libapr-1.so.0 ../ThirdParty/log4cxx/lib/libapr-1.so
ln -sf libaprutil-1.so.0 ../ThirdParty/log4cxx/lib/libaprutil-1.so
ln -sf libexpat.so.0 ../ThirdParty/log4cxx/lib/libexpat.so

#编译
g++ -c -x c++ "./MdCom/IniFile.cpp" -I "./MdCom/../../ThirdParty/ZMQ/include" -I "./MdCom/../../ThirdParty/msgpack/include" -I "./MdCom/../../ThirdParty/log4cxx/include" -g1 -o "./MdCom/obj/x64/Release/IniFile.o" -Wall -Wswitch -W"no-deprecated-declarations" -W"empty-body" -Wconversion -W"return-type" -Wparentheses -W"no-format" -Wuninitialized -W"unreachable-code" -W"unused-function" -W"unused-value" -W"unused-variable" -O3 -fno-strict-aliasing -fomit-frame-pointer -DNDEBUG -fpic -fthreadsafe-statics -fexceptions -frtti -std=c++11
ar -rsc  "./MdCom/bin/x64/Release/libMdCom.a" "./MdCom/obj/x64/Release/IniFile.o"
g++ -c -x c++ "./MdApi/MdApi.cpp" -I "./MdApi/../MdCom" -I "./MdApi/../../ThirdParty/ZMQ/include" -I "./MdApi/../../ThirdParty/msgpack/include" -I "./MdApi/../../ThirdParty/log4cxx/include" -g1 -o "./MdApi/obj/x64/Release/MdApi.o" -Wall -Wswitch -W"no-deprecated-declarations" -W"empty-body" -Wconversion -W"return-type" -Wparentheses -W"no-format" -Wuninitialized -W"unreachable-code" -W"unused-function" -W"unused-value" -W"unused-variable" -O3 -fno-strict-aliasing -fomit-frame-pointer -DNDEBUG -fpic -fthreadsafe-statics -fexceptions -frtti -std=c++11
g++ -c -x c++ "./MdApi/MdApiImp.cpp" -I "./MdApi/../MdCom" -I "./MdApi/../../ThirdParty/ZMQ/include" -I "./MdApi/../../ThirdParty/msgpack/include" -I "./MdApi/../../ThirdParty/log4cxx/include" -g1 -o "./MdApi/obj/x64/Release/MdApiImp.o" -Wall -Wswitch -W"no-deprecated-declarations" -W"empty-body" -Wconversion -W"return-type" -Wparentheses -W"no-format" -Wuninitialized -W"unreachable-code" -W"unused-function" -W"unused-value" -W"unused-variable" -O3 -fno-strict-aliasing -fomit-frame-pointer -DNDEBUG -fpic -fthreadsafe-statics -fexceptions -frtti -std=c++11
g++ -o "./MdApi/bin/x64/Release/libMdApi.so.1.0" -Wl,--no-undefined -Wl,-L"../ThirdParty/ZMQ/lib" -Wl,-L"../ThirdParty/msgpack/lib" -Wl,-L"../ThirdParty/log4cxx/lib" -Wl,-z,relro -Wl,-z,now -Wl,-z,noexecstack -shared "./MdApi/obj/x64/Release/MdApi.o" "./MdApi/obj/x64/Release/MdApiImp.o" "./MdCom/bin/x64/Release/libMdCom.a" -lzmq -lmsgpackc -llog4cxx -l"apr-1" -l"aprutil-1" -lexpat
ln -sf "../../MdApi/bin/x64/Release/libMdApi.so.1.0" ./Api/lib
g++ -c -x c++ "./MdClient/Client_Test.cpp" -I "./MdClient/../MdApi" -I "./MdClient/../MdCom" -I "./MdClient/../../ThirdParty/msgpack/include" -g1 -o "./MdClient/obj/x64/Release/Client_Test.o" -Wall -Wswitch -W"no-deprecated-declarations" -W"empty-body" -Wconversion -W"return-type" -Wparentheses -W"no-format" -Wuninitialized -W"unreachable-code" -W"unused-function" -W"unused-value" -W"unused-variable" -O3 -fno-strict-aliasing -fomit-frame-pointer -DNDEBUG -fthreadsafe-statics -fexceptions -frtti -std=c++11
g++ -o "./MdClient/bin/x64/Release/MdClient.out" -Wl,--no-undefined -Wl,-L"../ThirdParty/TDFAPI/lib" -Wl,-L"../ThirdParty/ZMQ/lib" -Wl,-L"../ThirdParty/msgpack/lib" -Wl,-L"../ThirdParty/log4cxx/lib" -Wl,-z,relro -Wl,-z,now -Wl,-z,noexecstack "./MdClient/obj/x64/Release/Client_Test.o" "./MdApi/bin/x64/Release/libMdApi.so.1.0" -lTDFAPI_v2.7 -lzmq -lmsgpackc -llog4cxx -l"apr-1" -l"aprutil-1" -lexpat
ln -sf "./MdClient/bin/x64/Release/MdClient.out" ./

ln -sf ./MdClient/Api.ini ./
ln -sf ./MdClient/MdClient.log4cxx ./