﻿#include <thread>
#include <map>
#include "MdApi.h"
#include "MdApiData.h"

#ifdef _WIN32
#include <windows.h>
#define SLEEP(x) Sleep(x*1000);
#else
#include <unistd.h>
#define SLEEP(x) sleep(x);
#endif // WIN32


using namespace MdApi;
using namespace std;

std::map<string, string> codetableMap;

void CALLBACK OnStockQuoteCallback(StockQuote* data)
{
	if (codetableMap.find(data->szWindCode) != codetableMap.end())
		printf("OnStockQuoteCallback:%s %s %.02f %.02f nTime:%d\n", data->szWindCode, codetableMap[data->szWindCode].c_str(), data->nAskPrice[0] / 10000.0, data->nBidPrice[0] / 10000.0, data->nTime);
	else
		printf("OnStockQuoteCallback:%s %s %.02f %.02f nTime:%d\n", data->szWindCode, "missing CNName", data->nAskPrice[0] / 10000.0, data->nBidPrice[0] / 10000.0, data->nTime);
	//...
}

void CALLBACK OnFutureQuoteCallback(FutureQuote* data)
{
	if (codetableMap.find(data->szWindCode) != codetableMap.end())
		printf("OnFutureQuoteCallback:%s %s %.02f %.02f nTime:%d\n", data->szWindCode, codetableMap[data->szWindCode].c_str(), data->nAskPrice[0] / 10000.0, data->nBidPrice[0] / 10000.0, data->nTime);
	//...
}

void CALLBACK OnIndexQuoteCallback(IndexQuote* data)
{
	if (codetableMap.find(data->szWindCode) != codetableMap.end())
		printf("OnIndexQuoteCallback:%s %s nTime:%d\n", data->szWindCode, codetableMap[data->szWindCode].c_str(), data->nTime);
	//...
}

void CALLBACK OnTransactionCallback(Transaction* data)
{
	if (codetableMap.find(data->szWindCode) != codetableMap.end())
		printf("OnTransactionCallback:%s %s nTime:%d\n", data->szWindCode, codetableMap[data->szWindCode], data->nTime);
	//...
}

void CALLBACK OnOrderQueueCallback(OrderQueue* data)
{
	if (codetableMap.find(data->szWindCode) != codetableMap.end())
		printf("OnOrderQueueCallback:%s %s nTime:%d\n", data->szWindCode, codetableMap[data->szWindCode], data->nTime);
	//...
}

void CALLBACK OnOrderCallback(Order* data)
{
	if (codetableMap.find(data->szWindCode) != codetableMap.end())
		printf("OnOrderCallback:%s %s nTime:%d\n", data->szWindCode, codetableMap[data->szWindCode], data->nTime);
	//...
}

void CALLBACK OnCodeTableCallback(CodeTable* data)
{
	printf("OnCodeTableCallback:%s %s nType:%d\n", data->szWindCode, data->szCNName, data->nType);
	//...

	codetableMap[data->szWindCode] = data->szCNName;
}

int main()
{
    printf("hello from MdClient!\n");

	HzTDFSrvApi_Connect();

	// 订阅才有数据传输

	//HzTDFSrvApi_Subscribe("IF1708.CF");
	HzTDFSrvApi_Subscribe("RB1809.shf");
	//HzTDFSrvApi_Subscribe("101315.SZ");
	//HzTDFSrvApi_Subscribe("pp1709.dce");
	//HzTDFSrvApi_Subscribe("ZC709.CZC");
	//HzTDFSrvApi_Subscribe("399001.SZ");
	//HzTDFSrvApi_Subscribe("399002.SZ");
	//HzTDFSrvApi_Subscribe("399300.SZ");
	//HzTDFSrvApi_Subscribe("999987.SH");
	//HzTDFSrvApi_Subscribe("990905.SH");
	//HzTDFSrvApi_Subscribe("600000.SH");
	//HzTDFSrvApi_Subscribe("601208.SH");
	//HzTDFSrvApi_Subscribe("IF1702.CF");


	// ##################################################
	//         回调展示推送获取行情方式
	// ##################################################

	// 股票行情回调
	SetStockQuoteCallback(OnStockQuoteCallback);
	// 指数行情回调
	SetIndexQuoteCallback(OnIndexQuoteCallback);
	// 期货行情回调
	SetFutureQuoteCallback(OnFutureQuoteCallback);
	// 逐笔成交回调
	SetTransactionCallback(OnTransactionCallback);
	// 委托队列回调
	SetOrderQueueCallback(OnOrderQueueCallback);
	// 逐笔委托回调
	SetOrderCallback(OnOrderCallback);
	// 代码表回调
	SetCodeTableCallback(OnCodeTableCallback);


	SLEEP(10000000);
	// ##################################################
	//         该线程展示主动获取行情方式（已废弃）
	// ##################################################
	//std::thread t1(&thread_Task);
	//t1.join();

	HzTDFSrvApi_Dispose();

	return 0;
}