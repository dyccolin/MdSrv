#build client demo release
export PATH=/usr/local/gcc-4.8.5/bin:$PATH

mkdir -p ./MdClient/obj/x64/Release/
mkdir -p ./MdClient/bin/x64/Release/

mkdir -p ./Api/include
cp -f ./MdApi/MdApi.h ./Api/include
cp -f ./MdCom/MdData.h ./Api/include
ln -sf ../../../ThirdParty/msgpack/include/msgpack ./Api/include
ln -sf ../../../ThirdParty/msgpack/include/msgpack.h ./Api/include
ln -sf ../../../ThirdParty/msgpack/include/msgpack.hpp ./Api/include


mkdir -p ./Api/lib
#创建link
ln -sf libzmq.so.5 ../ThirdParty/ZMQ/lib/libzmq.so
ln -sf libmsgpackc.so.2 ../ThirdParty/msgpack/lib/libmsgpackc.so
ln -sf liblog4cxx.so.10 ../ThirdParty/log4cxx/lib/liblog4cxx.so
ln -sf libapr-1.so.0 ../ThirdParty/log4cxx/lib/libapr-1.so
ln -sf libaprutil-1.so.0 ../ThirdParty/log4cxx/lib/libaprutil-1.so
ln -sf libexpat.so.0 ../ThirdParty/log4cxx/lib/libexpat.so

ln -sf ../../../ThirdParty/TDFAPI/lib/libTDFAPI_v2.7.so ./Api/lib/libTDFAPI_v2.7.so
ln -sf ../../../ThirdParty/ZMQ/lib/libzmq.so ./Api/lib/libzmq.so.5
ln -sf ../../../ThirdParty/msgpack/lib/libmsgpackc.so ./Api/lib/libmsgpackc.so.2
ln -sf ../../../ThirdParty/log4cxx/lib/liblog4cxx.so ./Api/lib/liblog4cxx.so.10
ln -sf ../../../ThirdParty/log4cxx/lib/libapr-1.so ./Api/lib/libapr-1.so.0
ln -sf ../../../ThirdParty/log4cxx/lib/libaprutil-1.so ./Api/lib/libaprutil-1.so.0
ln -sf ../../../ThirdParty/log4cxx/lib/libexpat.so ./Api/lib/libexpat.so.0

g++ -c -x c++ "./MdClient/Client_Test.cpp" -I "./MdClient/../MdApi" -I "./MdClient/../MdCom" -I "./MdClient/../../ThirdParty/msgpack/include" -g1 -o "./MdClient/obj/x64/Release/Client_Test.o" -Wall -Wswitch -W"no-deprecated-declarations" -W"empty-body" -Wconversion -W"return-type" -Wparentheses -W"no-format" -Wuninitialized -W"unreachable-code" -W"unused-function" -W"unused-value" -W"unused-variable" -O3 -fno-strict-aliasing -fomit-frame-pointer -DNDEBUG -fthreadsafe-statics -fexceptions -frtti -std=c++11
g++ -o "./MdClient/bin/x64/Release/MdClient.out" -Wl,--no-undefined -Wl,-L"../ThirdParty/TDFAPI/lib" -Wl,-L"../ThirdParty/ZMQ/lib" -Wl,-L"../ThirdParty/msgpack/lib" -Wl,-L"../ThirdParty/log4cxx/lib" -Wl,-z,relro -Wl,-z,now -Wl,-z,noexecstack "./MdClient/obj/x64/Release/Client_Test.o" "./Api/lib/libMdApi.so.1.0" -lTDFAPI_v2.7 -lzmq -lmsgpackc -llog4cxx -l"apr-1" -l"aprutil-1" -lexpat
ln -sf "./MdClient/bin/x64/Release/MdClient.out" ./

export LD_LIBRARY_PATH=./Api/lib
./MdClient.out
