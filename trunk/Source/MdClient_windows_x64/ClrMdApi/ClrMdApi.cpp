// This is the main DLL file.

#include "stdafx.h"
#include "vector"

#include "ClrMdApi.h"
#include "McToNative.h"
#include "MdApi.h"
#include "ClrMdData.h"

using namespace System;
using namespace System::Runtime::InteropServices;
using namespace ClrMdApiLib;

ClrMdApi::ClrMdApi()
{

}

ClrMdApi::~ClrMdApi()
{

}

int ClrMdApi::Connect()
{
 	_StockQuoteNativeResponseHandler = gcnew StockQuoteNativeResponseHandler(this, &ClrMdApi::OnStockQuoteCallback);
 	_IndexQuoteNativeResponseHandler = gcnew IndexQuoteNativeResponseHandler(this, &ClrMdApi::OnIndexQuoteCallback);
 	_FutureQuoteNativeResponseHandler = gcnew FutureQuoteNativeResponseHandler(this, &ClrMdApi::OnFutureQuoteCallback);
 
 	_TransactionNativeResponseHandler = gcnew TransactionNativeResponseHandler(this, &ClrMdApi::OnTransactionCallback);
 	_OrderNativeResponseHandler = gcnew OrderNativeResponseHandler(this, &ClrMdApi::OnOrderCallback);
 	_OrderQueueNativeResponseHandler = gcnew OrderQueueNativeResponseHandler(this, &ClrMdApi::OnOrderQueueCallback);

	_CodeTableNativeResponseHandler = gcnew CodeTableNativeResponseHandler(this, &ClrMdApi::OnCodeTableCallback);

 	SetStockQuoteCallback(static_cast<StockQuoteCallback>(Marshal::GetFunctionPointerForDelegate(_StockQuoteNativeResponseHandler).ToPointer()));
 	SetIndexQuoteCallback(static_cast<IndexQuoteCallback>(Marshal::GetFunctionPointerForDelegate(_IndexQuoteNativeResponseHandler).ToPointer()));
 	SetFutureQuoteCallback(static_cast<FutureQuoteCallback>(Marshal::GetFunctionPointerForDelegate(_FutureQuoteNativeResponseHandler).ToPointer()));
 
 	SetTransactionCallback(static_cast<TransactionCallback>(Marshal::GetFunctionPointerForDelegate(_TransactionNativeResponseHandler).ToPointer()));
 	SetOrderCallback(static_cast<OrderCallback>(Marshal::GetFunctionPointerForDelegate(_OrderNativeResponseHandler).ToPointer()));
 	SetOrderQueueCallback(static_cast<OrderQueueCallback>(Marshal::GetFunctionPointerForDelegate(_OrderQueueNativeResponseHandler).ToPointer()));

	SetCodeTableCallback(static_cast<CodeTableCallback>(Marshal::GetFunctionPointerForDelegate(_CodeTableNativeResponseHandler).ToPointer()));
	return HzTDFSrvApi_Connect();
}

int ClrMdApi::Unconnect()
{
	HzTDFSrvApi_Dispose();
	return 1;
}

//bool ClrMdApi::SubscribeList(List<System::String^>^ subList)
//{
// 	vector<string> nativeSubList;
// 	for (int ii =0; ii < subList->Count; ii++)
// 	{
// 		nativeSubList.push_back(McToNative::Convert(subList[ii]));
// 	}
// 
// 	return HzTDFSrvApi_SubscribeList(nativeSubList);
//}

bool ClrMdApi::Subscribe(System::String^ sub)
{
 	std::string navSub = McToNative::Convert(sub);
 	return HzTDFSrvApi_Subscribe(navSub.c_str());
}

//bool ClrMdApi::UnSubscribeList(List<System::String^>^ subList)
//{
//	vector<string> nativeSubList;
//	for (int ii =0; ii < subList->Count; ii++)
//	{
//		nativeSubList.push_back(McToNative::Convert(subList[ii]));
//	}
//
//	return HzTDFSrvApi_UnSubscribeList(nativeSubList);
//}

bool ClrMdApi::UnSubscribe(System::String^ sub)
{
 	std::string navSub = McToNative::Convert(sub);
 	return HzTDFSrvApi_UnSubscribe(navSub.c_str());;
}

//ClrStockQuote ClrMdApi::GetLatestStockQuote(System::String^ ticker)
//{
//	std::string navTicker = McToNative::Convert(ticker);
//	return gcnew ClrStockQuote(GetLatestStockQuote(navTicker));
//}
//
//ClrIndexQuote ClrMdApi::GetLatestIndexQuote(System::String^ ticker)
//{
//	std::string navTicker = McToNative::Convert(ticker);
//	return gcnew ClrIndexQuote(GetLatestIndexQuote(navTicker));
//}
//
//ClrFutureQuote ClrMdApi::GetLatestFutureQuote(System::String^ ticker)
//{
//	std::string navTicker = McToNative::Convert(ticker);
//	return gcnew ClrFutureQuote(GetLatestFutureQuote(navTicker));
//}
//
//ClrTransaction ClrMdApi::GetLatestTransactionQuote(System::String^ ticker)
//{
//	std::string navTicker = McToNative::Convert(ticker);
//	return gcnew ClrTransaction(GetLatestTransactionQuote(navTicker));
//}
//
//ClrOrder ClrMdApi::GetLatestOrderQuote(System::String^ ticker)
//{
//	std::string navTicker = McToNative::Convert(ticker);
//	return gcnew ClrOrder(GetLatestOrderQuote(navTicker));
//}
//
//ClrOrderQueue ClrMdApi::GetLatestOrderQueueQuote(System::String^ ticker)
//{
//	std::string navTicker = McToNative::Convert(ticker);
//	return gcnew ClrOrderQueue(GetLatestOrderQueueQuote(navTicker));
//}
//
//bool ClrMdApi::GetLatestStockQuoteList( List<System::String^>^ tickerList,List<ClrStockQuote^>^ vecRtn, System::String^ errMsg)
//{
//	return true;
//}
//
//bool ClrMdApi::GetLatestIndexQuoteList( List<System::String^>^ tickerList,List<ClrIndexQuote^>^ vecRtn, System::String^ errMsg)
//{
//	return true;
//}
//
//bool ClrMdApi::GetLatestFutureQuoteList( List<System::String^>^ tickerList,List<ClrFutureQuote^>^ vecRtn, System::String^ errMsg)
//{
//	return true;
//}
//
//bool ClrMdApi::GetLatestTransactionQuoteList( List<System::String^>^ tickerList,List<ClrTransaction^>^ vecRtn, System::String^ errMsg)
//{
//	return true;
//}
//
//bool ClrMdApi::GetLatestOrderQuoteList( List<System::String^>^ tickerList,List<ClrOrder^>^ vecRtn, System::String^ errMsg)
//{
//	return true;
//}
//
//bool ClrMdApi::GetLatestOrderQueueQuoteList( List<System::String^>^ tickerList,List<ClrOrderQueue^>^ vecRtn, System::String^ errMsg)
//{
//	return true;
//}

void ClrMdApi::OnStockQuoteCallback(StockQuote* quote)
{
	//if (StockQuoteResponsed != nullptr)
	{
		StockQuoteResponsed(gcnew ClrStockQuote(*quote));
	}
}

void ClrMdApi::OnFutureQuoteCallback(FutureQuote* quote)
{
	//if (FutureQuoteResponsed != nullptr)
	{
		FutureQuoteResponsed(gcnew ClrFutureQuote(*quote));
	}
}
void ClrMdApi::OnIndexQuoteCallback(IndexQuote* quote)
{
	//if (IndexQuoteResponsed != nullptr)
	{
		IndexQuoteResponsed(gcnew ClrIndexQuote(*quote));
	}
}
void ClrMdApi::OnTransactionCallback(Transaction* trans)
{
	//if (TransactionResponsed != nullptr)
	{
		TransactionResponsed(gcnew ClrTransaction(*trans));
	}
}
void ClrMdApi::OnOrderCallback(Order* order)
{
	//if (OrderResponsed != nullptr)
	{
		OrderResponsed(gcnew ClrOrder(*order));
	}
}
void ClrMdApi::OnOrderQueueCallback(OrderQueue* orderQueue)
{
	//if (OrderQueueResponsed != nullptr)
	{
		OrderQueueResponsed(gcnew ClrOrderQueue(*orderQueue));
	}
}

void ClrMdApi::OnCodeTableCallback(CodeTable* codeTable)
{
	//if (OrderQueueResponsed != nullptr)
	{
		CodeTableResponsed(gcnew ClrCodeTable(*codeTable));
	}
}