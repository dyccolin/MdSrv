#include "stdafx.h"
#include "McToNative.h"

using namespace System;
using namespace std;
using namespace Runtime::InteropServices;

using namespace ClrMdApiLib;

std::string McToNative::Convert(System::String^ clrStr)
{
	std::string str;
	if (clrStr == nullptr)
		return str;

	//pin_ptr<const wchar_t> chars = PtrToStringChars(s);
	char* chars = (char*)(Marshal::StringToHGlobalAnsi(clrStr)).ToPointer();
	str = chars;
	Marshal::FreeHGlobal(IntPtr((void*)chars));
	return str;
}