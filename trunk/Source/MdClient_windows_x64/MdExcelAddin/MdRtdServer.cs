﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExcelDna.Integration.Rtd;
using Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;

using ClrMdApiLib;

namespace MdExcelAddin
{
    public class MdRtdServer : ExcelRtdServer
    {
        const String TICKER_TYPE_STOCK = "1";
        const String TICKER_TYPE_FUTURE = "2";
        const String TICKER_TYPE_INDEX = "3";
        private Dictionary<String, RealStockData> stockDatas;
        ClrMdApi _Api = null;
        bool _Connected = false;

        public MdRtdServer()
        {
            _Connected = false;
        }

        protected override bool ServerStart()
        {
            _Api = new ClrMdApi();
            _Connected = _Api.Connect()==1;
            //_Api.Subscribe("");
            _Api.StockQuoteResponsed += new StockQuoteResponseHandler(OnStockQuoteResponsed);
            _Api.FutureQuoteResponsed += new FutureQuoteResponseHandler(OnFutureQuoteResponsed);
            _Api.IndexQuoteResponsed += new IndexQuoteResponseHandler(OnIndexQuoteResponsed);

            return base.ServerStart();
        }

        protected override object ConnectData(Topic topicId, IList<string> topicInfo, ref bool newValues)
        {
            //Retrive new values from RTD server when connected.
            newValues = true;

            if (!_Connected)
            {
                return "Unconnected server!";
            }
            
            //Get field name from Excel.
            string strStockCode = topicInfo[0];
            string strTickerType = topicInfo[1];
            string strFiledKey = topicInfo[2].ToLower();
            string strDataKey = GetDataKey(strStockCode, strTickerType, strFiledKey);

            // 订阅行情
            _Api.Subscribe(strStockCode);

            if (stockDatas == null)
            {
                stockDatas = new Dictionary<String, RealStockData>();
            }

            if (!stockDatas.ContainsKey(strDataKey))
            {
                var rsd = new RealStockData() { TopicId = topicId, StockCode = strStockCode, FiledKey = strFiledKey };
                stockDatas.Add(strDataKey, rsd);
            }
            else
            {
                stockDatas[strDataKey].TopicId = topicId;
                return stockDatas[strDataKey].FiledValue;
            }

            return "Loading...";
        }

        protected override void DisconnectData(Topic TopicID)
        {
            if (stockDatas == null) return;

            foreach (var data in stockDatas)
            {
                if (data.Value.TopicId == TopicID)
                {
                    stockDatas.Remove(data.Key);
                    break;
                }
            }
            return;
        }

        protected override ExcelRtdServer.Topic CreateTopic(int topicId, IList<string> topicInfo)
        {
            return base.CreateTopic(topicId, topicInfo);
        }

        protected override void ServerTerminate()
        {
            if (_Api != null)
            {
                _Api.Dispose();
                _Api = null;
            }
        }

        String GetDataKey(String stockcode, String tickerType, String filedKey)
        {
            return stockcode + "_" + tickerType + "_" + filedKey;
        }

        void OnStockQuoteResponsed(ClrStockQuote quote)
        {
            UpdateData(quote.szWindCode, TICKER_TYPE_STOCK, "lastprice", quote.nMatch);
            UpdateData(quote.szWindCode, TICKER_TYPE_STOCK, "preclose", quote.nPreClose);
            UpdateData(quote.szWindCode, TICKER_TYPE_STOCK, "high", quote.nHigh);
            UpdateData(quote.szWindCode, TICKER_TYPE_STOCK, "low", quote.nLow);
            UpdateData(quote.szWindCode, TICKER_TYPE_STOCK, "open", quote.nOpen);
            UpdateData(quote.szWindCode, TICKER_TYPE_STOCK, "highlimited", quote.nHighLimited);
            UpdateData(quote.szWindCode, TICKER_TYPE_STOCK, "lowlimited", quote.nLowLimited);
            UpdateData(quote.szWindCode, TICKER_TYPE_STOCK, "totalvolume", quote.iVolume);
            UpdateData(quote.szWindCode, TICKER_TYPE_STOCK, "turnover", quote.iTurnover);

            UpdateData(quote.szWindCode, TICKER_TYPE_STOCK, "askprice1", quote.nAskPrice[0]);
            UpdateData(quote.szWindCode, TICKER_TYPE_STOCK, "askprice2", quote.nAskPrice[1]);
            UpdateData(quote.szWindCode, TICKER_TYPE_STOCK, "askprice3", quote.nAskPrice[2]);
            UpdateData(quote.szWindCode, TICKER_TYPE_STOCK, "askprice4", quote.nAskPrice[3]);
            UpdateData(quote.szWindCode, TICKER_TYPE_STOCK, "askprice5", quote.nAskPrice[4]);
            UpdateData(quote.szWindCode, TICKER_TYPE_STOCK, "bidprice1", quote.nBidPrice[0]);
            UpdateData(quote.szWindCode, TICKER_TYPE_STOCK, "bidprice2", quote.nBidPrice[1]);
            UpdateData(quote.szWindCode, TICKER_TYPE_STOCK, "bidprice3", quote.nBidPrice[2]);
            UpdateData(quote.szWindCode, TICKER_TYPE_STOCK, "bidprice4", quote.nBidPrice[3]);
            UpdateData(quote.szWindCode, TICKER_TYPE_STOCK, "bidprice5", quote.nBidPrice[4]);
            UpdateData(quote.szWindCode, TICKER_TYPE_STOCK, "askvol1", quote.nAskVol[0]);
            UpdateData(quote.szWindCode, TICKER_TYPE_STOCK, "askvol2", quote.nAskVol[1]);
            UpdateData(quote.szWindCode, TICKER_TYPE_STOCK, "askvol3", quote.nAskVol[2]);
            UpdateData(quote.szWindCode, TICKER_TYPE_STOCK, "askvol4", quote.nAskVol[3]);
            UpdateData(quote.szWindCode, TICKER_TYPE_STOCK, "askvol5", quote.nAskVol[4]);
            UpdateData(quote.szWindCode, TICKER_TYPE_STOCK, "bidvol1", quote.nBidVol[0]);
            UpdateData(quote.szWindCode, TICKER_TYPE_STOCK, "bidvol2", quote.nBidVol[1]);
            UpdateData(quote.szWindCode, TICKER_TYPE_STOCK, "bidvol3", quote.nBidVol[2]);
            UpdateData(quote.szWindCode, TICKER_TYPE_STOCK, "bidvol4", quote.nBidVol[3]);
            UpdateData(quote.szWindCode, TICKER_TYPE_STOCK, "bidvol5", quote.nBidVol[4]);
        }

        void OnFutureQuoteResponsed(ClrFutureQuote quote)
        {
            UpdateData(quote.szWindCode, TICKER_TYPE_FUTURE, "lastprice", quote.nMatch);
            UpdateData(quote.szWindCode, TICKER_TYPE_FUTURE, "preclose", quote.nPreClose);
            UpdateData(quote.szWindCode, TICKER_TYPE_FUTURE, "high", quote.nHigh);
            UpdateData(quote.szWindCode, TICKER_TYPE_FUTURE, "low", quote.nLow);
            UpdateData(quote.szWindCode, TICKER_TYPE_FUTURE, "open", quote.nOpen);
            UpdateData(quote.szWindCode, TICKER_TYPE_FUTURE, "highlimited", quote.nHighLimited);
            UpdateData(quote.szWindCode, TICKER_TYPE_FUTURE, "lowlimited", quote.nLowLimited);
            UpdateData(quote.szWindCode, TICKER_TYPE_FUTURE, "totalvolume", quote.iVolume);
            UpdateData(quote.szWindCode, TICKER_TYPE_FUTURE, "turnover", quote.iTurnover);

            UpdateData(quote.szWindCode, TICKER_TYPE_FUTURE, "askprice1", quote.nAskPrice[0]);
            UpdateData(quote.szWindCode, TICKER_TYPE_FUTURE, "askprice2", quote.nAskPrice[1]);
            UpdateData(quote.szWindCode, TICKER_TYPE_FUTURE, "askprice3", quote.nAskPrice[2]);
            UpdateData(quote.szWindCode, TICKER_TYPE_FUTURE, "askprice4", quote.nAskPrice[3]);
            UpdateData(quote.szWindCode, TICKER_TYPE_FUTURE, "askprice5", quote.nAskPrice[4]);
            UpdateData(quote.szWindCode, TICKER_TYPE_FUTURE, "bidprice1", quote.nBidPrice[0]);
            UpdateData(quote.szWindCode, TICKER_TYPE_FUTURE, "bidprice2", quote.nBidPrice[1]);
            UpdateData(quote.szWindCode, TICKER_TYPE_FUTURE, "bidprice3", quote.nBidPrice[2]);
            UpdateData(quote.szWindCode, TICKER_TYPE_FUTURE, "bidprice4", quote.nBidPrice[3]);
            UpdateData(quote.szWindCode, TICKER_TYPE_FUTURE, "bidprice5", quote.nBidPrice[4]);
            UpdateData(quote.szWindCode, TICKER_TYPE_FUTURE, "askvol1", quote.nAskVol[0]);
            UpdateData(quote.szWindCode, TICKER_TYPE_FUTURE, "askvol2", quote.nAskVol[1]);
            UpdateData(quote.szWindCode, TICKER_TYPE_FUTURE, "askvol3", quote.nAskVol[2]);
            UpdateData(quote.szWindCode, TICKER_TYPE_FUTURE, "askvol4", quote.nAskVol[3]);
            UpdateData(quote.szWindCode, TICKER_TYPE_FUTURE, "askvol5", quote.nAskVol[4]);
            UpdateData(quote.szWindCode, TICKER_TYPE_FUTURE, "bidvol1", quote.nBidVol[0]);
            UpdateData(quote.szWindCode, TICKER_TYPE_FUTURE, "bidvol2", quote.nBidVol[1]);
            UpdateData(quote.szWindCode, TICKER_TYPE_FUTURE, "bidvol3", quote.nBidVol[2]);
            UpdateData(quote.szWindCode, TICKER_TYPE_FUTURE, "bidvol4", quote.nBidVol[3]);
            UpdateData(quote.szWindCode, TICKER_TYPE_FUTURE, "bidvol5", quote.nBidVol[4]);
        }
        void OnIndexQuoteResponsed(ClrIndexQuote quote)
        {
            UpdateData(quote.szWindCode, TICKER_TYPE_INDEX, "lastprice", quote.nLastIndex);
            UpdateData(quote.szWindCode, TICKER_TYPE_INDEX, "preclose", quote.nPreCloseIndex);
            UpdateData(quote.szWindCode, TICKER_TYPE_INDEX, "high", quote.nHighIndex);
            UpdateData(quote.szWindCode, TICKER_TYPE_INDEX, "low", quote.nLowIndex);
            UpdateData(quote.szWindCode, TICKER_TYPE_INDEX, "open", quote.nOpenIndex);
            UpdateData(quote.szWindCode, TICKER_TYPE_INDEX, "totalvolume", quote.iTotalVolume);
            UpdateData(quote.szWindCode, TICKER_TYPE_INDEX, "turnover", quote.iTurnover);
        }

        void UpdateData(string stockCode, string tickerType, string filedKey, object filedValue)
        {
            string key = GetDataKey(stockCode, tickerType, filedKey);
            if (stockDatas.ContainsKey(key))
            {
                stockDatas[key].FiledValue = filedValue;
                // 通知excel数据更新
                stockDatas[key].TopicId.UpdateValue(filedValue);
            }
        }

        class RealStockData
        {
            //该次请求的股票代码
            public string StockCode { get; set; }
            //该次请求的字段名称
            public string FiledKey { get; set; }
            //该次请求Excel分配的TopicID
            public Topic TopicId { get; set; }
            //该次请求的返回值
            public object FiledValue { get; set; }
        }
    }
}
