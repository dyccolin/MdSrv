﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExcelDna.Integration;
using Microsoft.Office.Interop.Excel;
using Microsoft.Office.Core;
using System.Text.RegularExpressions;
using System.Windows;

namespace MdExcelAddin
{
    public class MdExcelAddin : IExcelAddIn
    {
        static Dictionary<string, string> _TdIndex2TDFDict = new Dictionary<string, string>();
        static Dictionary<string, string> _TdExchangeIdDict = new Dictionary<string, string>();
        const string _CFFEX = "CF";
        const string _SHFE = "SHF";
        const string _DCE = "DCE";
        const string _CZCE = "CZC";
        static bool _loaded = false;

        //private Microsoft.Office.Interop.Excel.Application mExcelApp = null;
        public void AutoClose()
        {
        }
        public void AutoOpen()
        {
            MessageBox.Show("System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName\n" + System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);
            MessageBox.Show("System.Environment.CurrentDirectory\n" + System.Environment.CurrentDirectory);
            MessageBox.Show("System.IO.Directory.GetCurrentDirectory()\n" + System.IO.Directory.GetCurrentDirectory());
            MessageBox.Show("System.AppDomain.CurrentDomain.BaseDirectory\n" + System.AppDomain.CurrentDomain.BaseDirectory);
            MessageBox.Show("System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase\n" + AppDomain.CurrentDomain.SetupInformation.ApplicationBase);

            _TdExchangeIdDict.Add("IF", _CFFEX);
            _TdExchangeIdDict.Add("IC", _CFFEX);
            _TdExchangeIdDict.Add("IH", _CFFEX);
            _TdExchangeIdDict.Add("T", _CFFEX);
            _TdExchangeIdDict.Add("TF", _CFFEX);

            _TdExchangeIdDict.Add("a", _DCE);
            _TdExchangeIdDict.Add("b", _DCE);
            _TdExchangeIdDict.Add("bb", _DCE);
            _TdExchangeIdDict.Add("c", _DCE);
            _TdExchangeIdDict.Add("cs", _DCE);
            _TdExchangeIdDict.Add("fb", _DCE);
            _TdExchangeIdDict.Add("i", _DCE);
            _TdExchangeIdDict.Add("j", _DCE);
            _TdExchangeIdDict.Add("jd", _DCE);
            _TdExchangeIdDict.Add("jm", _DCE);
            _TdExchangeIdDict.Add("l", _DCE);
            _TdExchangeIdDict.Add("m", _DCE);
            _TdExchangeIdDict.Add("p", _DCE);
            _TdExchangeIdDict.Add("pp", _DCE);
            _TdExchangeIdDict.Add("v", _DCE);
            _TdExchangeIdDict.Add("y", _DCE);

            _TdExchangeIdDict.Add("ag", _SHFE);
            _TdExchangeIdDict.Add("al", _SHFE);
            _TdExchangeIdDict.Add("au", _SHFE);
            _TdExchangeIdDict.Add("bu", _SHFE);
            _TdExchangeIdDict.Add("cu", _SHFE);
            _TdExchangeIdDict.Add("fu", _SHFE);
            _TdExchangeIdDict.Add("hc", _SHFE);
            _TdExchangeIdDict.Add("ni", _SHFE);
            _TdExchangeIdDict.Add("pb", _SHFE);
            _TdExchangeIdDict.Add("rb", _SHFE);
            _TdExchangeIdDict.Add("ru", _SHFE);
            _TdExchangeIdDict.Add("sn", _SHFE);
            _TdExchangeIdDict.Add("wr", _SHFE);
            _TdExchangeIdDict.Add("zn", _SHFE);

            _TdExchangeIdDict.Add("CF", _CZCE);
            _TdExchangeIdDict.Add("FG", _CZCE);
            _TdExchangeIdDict.Add("JR", _CZCE);
            _TdExchangeIdDict.Add("LR", _CZCE);
            _TdExchangeIdDict.Add("MA", _CZCE);
            _TdExchangeIdDict.Add("OI", _CZCE);
            _TdExchangeIdDict.Add("PM", _CZCE);
            _TdExchangeIdDict.Add("RI", _CZCE);
            _TdExchangeIdDict.Add("RM", _CZCE);
            _TdExchangeIdDict.Add("RS", _CZCE);
            _TdExchangeIdDict.Add("SF", _CZCE);
            _TdExchangeIdDict.Add("SM", _CZCE);
            _TdExchangeIdDict.Add("SR", _CZCE);
            _TdExchangeIdDict.Add("TA", _CZCE);
            _TdExchangeIdDict.Add("WH", _CZCE);
            _TdExchangeIdDict.Add("ZC", _CZCE);

            _TdIndex2TDFDict.Add("000001", "999999");
            _TdIndex2TDFDict.Add("000002", "999998");
            _TdIndex2TDFDict.Add("000003", "999997");
            _TdIndex2TDFDict.Add("000004", "999996");
            _TdIndex2TDFDict.Add("000005", "999995");
            _TdIndex2TDFDict.Add("000006", "999994");
            _TdIndex2TDFDict.Add("000007", "999993");
            _TdIndex2TDFDict.Add("000008", "999992");
            _TdIndex2TDFDict.Add("000010", "999991");
            _TdIndex2TDFDict.Add("000011", "999990");
            _TdIndex2TDFDict.Add("000012", "999989");
            _TdIndex2TDFDict.Add("000013", "999988");
            _TdIndex2TDFDict.Add("000016", "999987");
            _TdIndex2TDFDict.Add("000015", "999986");
            _TdIndex2TDFDict.Add("000300", "000300");
        }

        enum TICKER_DIV
        {
            STOCK = 1,
            FUTURE,
            INDEX,
        }

        static private string GetTicker(string strTicker, TICKER_DIV div)
        {
            //string strTicker = inputStr.ToUpper();
            if (strTicker.Contains(".")) return strTicker;

            if (!_loaded)
            {
                
                _loaded = true;
            }

            switch (div)
            {
                case TICKER_DIV.STOCK:
                    {
                        int id = 0;
                        if (int.TryParse(strTicker, out id))
                        {
                            strTicker = string.Format("{0:000000}.{1}", id, id >= 500000 ? "SH" : "SZ");
                        }
                    }
                    break;
                case TICKER_DIV.INDEX:
                    {
                        int id = 0;
                        if (int.TryParse(strTicker, out id))
                        {
                            string strid = id.ToString("000000");
                            if (_TdIndex2TDFDict.ContainsKey(strid))
                            {
                                strTicker = string.Format("{0:000000}.{1}", _TdIndex2TDFDict[strid], "SH");
                            }
                            else if (strid.StartsWith("00"))
                            {
                                strTicker = string.Format("{0:990000}.{1}", id, "SH");
                            }
                            else
                            {
                                strTicker = string.Format("{0:000000}.{1}", id, id >= 500000 ? "SH" : "SZ");
                            }
                        }
                    }
                    break;
                case TICKER_DIV.FUTURE:
                    {
                        // 期货合约
                        Regex r = new Regex(@"[a-zA-Z]+");
                        Match m = r.Match(strTicker);
                        string prefix = m.Value;

                        if (_TdExchangeIdDict.ContainsKey(prefix))
                        {
                            strTicker = string.Format("{0}.{1}", strTicker, _TdExchangeIdDict[prefix]);
                        }
                    }
                    break;
            }

            return strTicker;
        }

        public static T ParseEnum<T>(object value)
        {
            return (T)Enum.Parse(typeof(T), value.ToString(), true);
        }

        private const string RTD_NAME_QuoteService = "MdExcelAddin.MdRtdServer";    // 固定的name
        private const string RTD_SERVER = null;
        [ExcelFunction("Get live price from quote service")]
        public static object HZ_GetQuote(
            [ExcelArgument(Name = "Ticker", Description = "Ticker, likes IF1810 or rb1810, 600000,000012")] object objectTicker,
            [ExcelArgument(Name = "TickerDiv", Description = "Ticker Type, as 1 for stock, 2 for future, 3 for index")] object objectTickerDiv,
            [ExcelArgument(Name = "QuoteType", Description = "Quote Type, [Optional] Types: lastprice,preclose,open,high,low,highlimited,lowlimited,askprice1,askvol1,bidprice1,bidvol1,totalvolume,turnover...")] object objectQuoteType)
        {
            try
            {
                TICKER_DIV div = ParseEnum<TICKER_DIV>(objectTickerDiv);
                string ticker = GetTicker(objectTicker.ToString(), div);
                return XlCall.RTD(RTD_NAME_QuoteService, RTD_SERVER, ticker, objectTickerDiv.ToString(), objectQuoteType.ToString());
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
}
