﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ClrMdApiLib;
using MdUI;

namespace TradingUI.UI
{
    public delegate void QuoteControlDeletingHandler(QuoteControl quoteControl);
    /// <summary>
    /// Interaction logic for QuoteControl.xaml
    /// </summary>
    public partial class QuoteControl : UserControl
    {
        public event QuoteControlDeletingHandler QuoteControlDeleting;

        public StockQuote StockQuote { get; private set; }
        public FutureQuote FutureQuote { get; private set; }
        public IndexQuote IndexQuote { get; private set; }

        public QuoteControl()
        {
            InitializeComponent();
        }

        public void SetStockQuote(StockQuote quote)
        {
            this.DataContext = quote;
            StockQuote = quote;
        }

        public void SetFutureQuote(FutureQuote quote)
        {
            this.DataContext = quote;
            FutureQuote = quote;
        }

        public void SetIndexQuote(IndexQuote quote)
        {
            this.DataContext = quote;
            IndexQuote = quote;
        }

        private void MenuItemDelete_Click(object sender, RoutedEventArgs e)
        {
            if (QuoteControlDeleting != null)
            {
                QuoteControlDeleting(this);
            }
        }
    }
}
