﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ClrMdApiLib;
using TradingUI.UI;
using System.Windows.Interop;
using System.IO;
using System.Text.RegularExpressions;

namespace MdUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ClrMdApi _Api = null;

        Dictionary<string, StockQuote> _StockQuoteDict = new Dictionary<string, StockQuote>();
        Dictionary<string, FutureQuote> _FutureQuoteDict = new Dictionary<string,FutureQuote>();
        Dictionary<string, IndexQuote> _IndexQuoteDict = new Dictionary<string, IndexQuote>();
        Dictionary<string, ClrCodeTable> _CodeTableDict = new Dictionary<string, ClrCodeTable>();

        Dictionary<string, string> _TdIndex2TDFDict = new Dictionary<string, string>();

        Dictionary<string,string> _TdExchangeIdDict = new Dictionary<string,string>(StringComparer.OrdinalIgnoreCase);
        const string _CFFEX = "CF";
        const string _SHFE = "SHF";
        const string _DCE = "DCE";
        const string _CZCE = "CZC";

        enum ADD_DIV
        {
            STOCK,
            INDEX,
            FUTURE
        }

        public MainWindow()
        {
            InitializeComponent();
            this.Icon = Imaging.CreateBitmapSourceFromHIcon(Properties.Resources.app.Handle, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());

            _TdExchangeIdDict.Add("IF", _CFFEX);
            _TdExchangeIdDict.Add("IC", _CFFEX);
            _TdExchangeIdDict.Add("IH", _CFFEX);
            _TdExchangeIdDict.Add("T", _CFFEX);
            _TdExchangeIdDict.Add("TF", _CFFEX);

            _TdExchangeIdDict.Add("a", _DCE);
            _TdExchangeIdDict.Add("b", _DCE);
            _TdExchangeIdDict.Add("bb", _DCE);
            _TdExchangeIdDict.Add("c", _DCE);
            _TdExchangeIdDict.Add("cs", _DCE);
            _TdExchangeIdDict.Add("fb", _DCE);
            _TdExchangeIdDict.Add("i", _DCE);
            _TdExchangeIdDict.Add("j", _DCE);
            _TdExchangeIdDict.Add("jd", _DCE);
            _TdExchangeIdDict.Add("jm", _DCE);
            _TdExchangeIdDict.Add("l", _DCE);
            _TdExchangeIdDict.Add("m", _DCE);
            _TdExchangeIdDict.Add("p", _DCE);
            _TdExchangeIdDict.Add("pp", _DCE);
            _TdExchangeIdDict.Add("v", _DCE);
            _TdExchangeIdDict.Add("y", _DCE);

            _TdExchangeIdDict.Add("ag", _SHFE);
            _TdExchangeIdDict.Add("al", _SHFE);
            _TdExchangeIdDict.Add("au", _SHFE);
            _TdExchangeIdDict.Add("bu", _SHFE);
            _TdExchangeIdDict.Add("cu", _SHFE);
            _TdExchangeIdDict.Add("fu", _SHFE);
            _TdExchangeIdDict.Add("hc", _SHFE);
            _TdExchangeIdDict.Add("ni", _SHFE);
            _TdExchangeIdDict.Add("pb", _SHFE);
            _TdExchangeIdDict.Add("rb", _SHFE);
            _TdExchangeIdDict.Add("ru", _SHFE);
            _TdExchangeIdDict.Add("sn", _SHFE);
            _TdExchangeIdDict.Add("wr", _SHFE);
            _TdExchangeIdDict.Add("zn", _SHFE);

            _TdExchangeIdDict.Add("CF", _CZCE);
            _TdExchangeIdDict.Add("FG", _CZCE);
            _TdExchangeIdDict.Add("JR", _CZCE);
            _TdExchangeIdDict.Add("LR", _CZCE);
            _TdExchangeIdDict.Add("MA", _CZCE);
            _TdExchangeIdDict.Add("OI", _CZCE);
            _TdExchangeIdDict.Add("PM", _CZCE);
            _TdExchangeIdDict.Add("RI", _CZCE);
            _TdExchangeIdDict.Add("RM", _CZCE);
            _TdExchangeIdDict.Add("RS", _CZCE);
            _TdExchangeIdDict.Add("SF", _CZCE);
            _TdExchangeIdDict.Add("SM", _CZCE);
            _TdExchangeIdDict.Add("SR", _CZCE);
            _TdExchangeIdDict.Add("TA", _CZCE);
            _TdExchangeIdDict.Add("WH", _CZCE);
            _TdExchangeIdDict.Add("ZC", _CZCE);

            _TdIndex2TDFDict.Add("000001", "999999");
            _TdIndex2TDFDict.Add("000002", "999998");
            _TdIndex2TDFDict.Add("000003", "999997");
            _TdIndex2TDFDict.Add("000004", "999996");
            _TdIndex2TDFDict.Add("000005", "999995");
            _TdIndex2TDFDict.Add("000006", "999994");
            _TdIndex2TDFDict.Add("000007", "999993");
            _TdIndex2TDFDict.Add("000008", "999992");
            _TdIndex2TDFDict.Add("000010", "999991");
            _TdIndex2TDFDict.Add("000011", "999990");
            _TdIndex2TDFDict.Add("000012", "999989");
            _TdIndex2TDFDict.Add("000013", "999988");
            _TdIndex2TDFDict.Add("000016", "999987");
            _TdIndex2TDFDict.Add("000015", "999986");
            _TdIndex2TDFDict.Add("000300", "000300");
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (_Api != null)
            {
                _Api.Unconnect();
                _Api = null;
            }

            //string file = FileUtility.Instance.GetTickerFile();

            //using (FileStream stream = new FileStream(file, FileMode.Open, FileAccess.ReadWrite, FileShare.Read))
            //{
            //    stream.SetLength(0);
            //}

            //using (StreamWriter writer = new StreamWriter(new FileStream(file, FileMode.Open, FileAccess.Write, FileShare.Read)))
            //{
            //    foreach (QuoteControl control in QuotePanel.Children)
            //    {
            //        writer.WriteLine(control.Quote.Ticker);
            //    }
            //}
        }

        void OnStockQuoteResponsed(ClrStockQuote quote)
        {
            if (_StockQuoteDict.ContainsKey(quote.szWindCode))
            {
                _StockQuoteDict[quote.szWindCode].Update(quote);
            }
        }

        void OnFutureQuoteResponsed(ClrFutureQuote quote)
        {
            if (_FutureQuoteDict.ContainsKey(quote.szWindCode))
            {
                _FutureQuoteDict[quote.szWindCode].Update(quote);
            }
        }
        void OnIndexQuoteResponsed(ClrIndexQuote quote)
        {
            if (_IndexQuoteDict.ContainsKey(quote.szWindCode))
            {
                _IndexQuoteDict[quote.szWindCode].Update(quote);
            }
        }
        void OnCodeTableResponsed(ClrCodeTable quote)
        {
            if (_CodeTableDict.ContainsKey(quote.szWindCode))
            {
                ;
            }
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //if (DateTime.Today > new DateTime(2017, 3, 1))
            //{
            //    MessageBox.Show("已过试用期，请联系软件提供商!!");
            //    this.Close();
            //    return;
            //}

            _Api = new ClrMdApi();
            _Api.Connect();

            //var tickerList = GetTickerFromFile();
            ////lock (_StockQuoteDict)
            //{
            //    foreach (var ticker in tickerList)
            //    {
            //        var quote = new StockQuote(ticker);
            //        _StockQuoteDict.Add(ticker, quote);
            //        _Api.Subscribe(ticker);
            //        var control = new QuoteControl(quote);
            //        QuotePanel.Children.Add(control);
            //        control.QuoteControlDeleting += new QuoteControlDeletingHandler(OnQuoteControlDeleting);
            //    }
            //}
            _Api.StockQuoteResponsed += new StockQuoteResponseHandler(OnStockQuoteResponsed);
            _Api.FutureQuoteResponsed += new FutureQuoteResponseHandler(OnFutureQuoteResponsed);
            _Api.IndexQuoteResponsed += new IndexQuoteResponseHandler(OnIndexQuoteResponsed);
            _Api.CodeTableResponsed += new CodeTableResponseHandler(OnCodeTableResponsed);
        }

        private void ButtonAddStock_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(TextBoxTicker.Text))
            {
                MessageBox.Show("请输入证券代码");
                return;
            }

            string strTicker = GetTicker(TextBoxTicker.Text, ADD_DIV.STOCK);
            lock (_StockQuoteDict)
            {
                StockQuote quote = null;
                string szWindCode = strTicker.ToUpper();
                if (!_StockQuoteDict.ContainsKey(szWindCode))
                {
                    quote = new StockQuote(strTicker, TextBoxTicker.Text);
                    _StockQuoteDict.Add(szWindCode, quote);
                    _Api.Subscribe(strTicker);
                }
                else
                {
                    quote = _StockQuoteDict[szWindCode];
                }

                var control = new QuoteControl();
                control.SetStockQuote(quote);
                QuotePanel.Children.Add(control);

                control.QuoteControlDeleting += new QuoteControlDeletingHandler(OnQuoteControlDeleting);
            }
        }

        private void ButtonAddFuture_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(TextBoxTicker.Text))
            {
                MessageBox.Show("请输入证券代码");
                return;
            }

            string strTicker = GetTicker(TextBoxTicker.Text, ADD_DIV.FUTURE);
            lock (_FutureQuoteDict)
            {
                FutureQuote quote = null;
                string szWindCode = strTicker.ToUpper();
                if (!_FutureQuoteDict.ContainsKey(szWindCode))
                {
                    quote = new FutureQuote(strTicker, TextBoxTicker.Text);
                    _FutureQuoteDict.Add(szWindCode, quote);
                    _Api.Subscribe(strTicker);
                }
                else
                {
                    quote = _FutureQuoteDict[szWindCode];
                }

                var control = new QuoteControl();
                control.SetFutureQuote(quote);
                QuotePanel.Children.Add(control);

                control.QuoteControlDeleting += new QuoteControlDeletingHandler(OnQuoteControlDeleting);
            }
        }

        private void ButtonAddIndex_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(TextBoxTicker.Text))
            {
                MessageBox.Show("请输入证券代码");
                return;
            }

            string strTicker = GetTicker(TextBoxTicker.Text, ADD_DIV.INDEX);
            lock (_IndexQuoteDict)
            {
                IndexQuote quote = null;
                string szWindCode = strTicker.ToUpper();
                if (!_IndexQuoteDict.ContainsKey(szWindCode))
                {
                    quote = new IndexQuote(strTicker, TextBoxTicker.Text + "(指数)");
                    _IndexQuoteDict.Add(szWindCode, quote);
                    _Api.Subscribe(strTicker);
                }
                else
                {
                    quote = _IndexQuoteDict[szWindCode];
                }

                var control = new QuoteControl();
                control.SetIndexQuote(quote);
                QuotePanel.Children.Add(control);

                control.QuoteControlDeleting += new QuoteControlDeletingHandler(OnQuoteControlDeleting);
            }
        }

        //private List<string> GetTickerFromFile()
        //{
        //    List<string> tickerList = new List<string>();
        //    string file = FileUtility.Instance.GetTickerFile();

        //    using (StreamReader reader = new StreamReader(new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
        //    {
        //        while (!reader.EndOfStream)
        //        {
        //            string strLine = reader.ReadLine();
        //            if (string.IsNullOrWhiteSpace(strLine.Trim()))
        //            {
        //                continue;
        //            }

        //            string ticker = GetTicker(strLine.Trim());
        //            tickerList.Add(ticker);
        //        }
        //    }

        //    return tickerList;
        //}

        private string GetTicker(string strTicker, ADD_DIV div)
        {
            //string strTicker = inputStr.ToUpper();
            if (strTicker.Contains(".")) return strTicker;

            switch (div)
            {
                case ADD_DIV.STOCK:
                    {
                        int id = 0;
                        if (int.TryParse(strTicker, out id))
                        {
                            strTicker = string.Format("{0:000000}.{1}", id, id >= 500000 ? "SH" : "SZ");
                        }
                    }
                    break;
                case ADD_DIV.INDEX:
                    {
                        int id = 0;
                        if (int.TryParse(strTicker, out id))
                        {
                            string strid = id.ToString("000000");
                            if (_TdIndex2TDFDict.ContainsKey(strid))
                            {
                                strTicker = string.Format("{0:000000}.{1}", _TdIndex2TDFDict[strid], "SH");
                            }
                            else if (strid.StartsWith("00"))
                            {
                                strTicker = string.Format("{0:990000}.{1}", id, "SH");
                            }
                            else
                            {
                                strTicker = string.Format("{0:000000}.{1}", id, id >= 500000 ? "SH" : "SZ");
                            }
                        }
                    }
                    break;
                case ADD_DIV.FUTURE:
                    {
                        // 期货合约
                        Regex r = new Regex(@"[a-zA-Z]+");
                        Match m = r.Match(strTicker);
                        string prefix = m.Value;

                        if (_TdExchangeIdDict.ContainsKey(prefix))
                        {
                            strTicker = string.Format("{0}.{1}", strTicker, _TdExchangeIdDict[prefix]);
                        }
                    }
                    break;
            }
                   
            return strTicker;
        }

        private void OnQuoteControlDeleting(QuoteControl quoteControl)
        {
            //lock (_QuoteDict)
            {
                QuotePanel.Children.Remove(quoteControl);

                //if (quoteControl.StockQuote != null && !string.IsNullOrEmpty(quoteControl.StockQuote.Ticker))
                //{
                //    _StockQuoteDict.Remove(quoteControl.StockQuote.Ticker.ToUpper());
                //}
                //else if(quoteControl.FutureQuote != null && !string.IsNullOrEmpty(quoteControl.FutureQuote.Ticker))
                //{
                //    _FutureQuoteDict.Remove(quoteControl.FutureQuote.Ticker.ToUpper());
                //}
                //else if (quoteControl.IndexQuote != null && !string.IsNullOrEmpty(quoteControl.IndexQuote.Ticker))
                //{
                //    _IndexQuoteDict.Remove(quoteControl.IndexQuote.Ticker.ToUpper());
                //}
            }
        }
    }
}
