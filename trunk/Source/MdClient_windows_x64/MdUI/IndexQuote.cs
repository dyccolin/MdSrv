﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClrMdApiLib;

namespace MdUI
{
    public class IndexQuote : NotifyObject
    {
        string _Ticker;
        public string Ticker { get { return _Ticker; } set { UpdateProperty(ref _Ticker, value, "Ticker"); } }

        string _ExchTicker; 
        public string ExchTicker { get { return _ExchTicker; } set { UpdateProperty(ref _ExchTicker, value, "ExchTicker"); } }

        string szCode;
        int nActionDay;

        TimeSpan _Time;
        public TimeSpan Time { get { return _Time; } set { UpdateProperty(ref _Time, value, "Time"); } }

        // no pre close
        double _PreClose;
        public double PreClose { get { return _PreClose; } set { UpdateProperty(ref _PreClose, value, "PreClose");} }

        double _Open;
        public double Open { get { return _Open; } set { UpdateProperty(ref _Open, value, "Open"); } }

        double _High;
        public double High { get { return _High; } set { UpdateProperty(ref _High, value, "High"); } }

        double _Low;
        public double Low { get { return _Low; } set { UpdateProperty(ref _Low, value, "Low"); } }

        double _Match;
        public double Match
        {
            get { return _Match; }
            set
            {
                UpdateProperty(ref _Match, value, "Match");
                OnPropertyChanged("UpDown");
                OnPropertyChanged("UpDownPercent");
            }
        }

        Int64 iVolume;
        Int64 iTurnover;

        public double UpDown
        {
            get { return Match - PreClose; }
        }

        public double UpDownPercent
        {
            get { return (Match / PreClose) - 1.0; }
        }

        public IndexQuote(string ticker, string exchTicker)
        {
            Ticker = ticker;
            ExchTicker = exchTicker;
        }

        public void Update(ClrIndexQuote quote)
        {
            Ticker = quote.szWindCode;
            Time = new TimeSpan(quote.nTime / 10000000, (quote.nTime % 10000000) / 100000, (quote.nTime % 100000) / 1000);
            PreClose = quote.nPreCloseIndex;
            Open = quote.nOpenIndex;
            High = quote.nHighIndex;
            Low = quote.nLowIndex;
            Match = quote.nLastIndex;

            iVolume = quote.iTotalVolume;
            iTurnover = quote.iTurnover;
        }
    }
}
