//======================================================================================================
// md_gateway_client.h
// 网关客户端头文件
// lujun 
//
//======================================================================================================

#ifndef HTSC_MDC_GATEWAY_CLIENT_H
#define HTSC_MDC_GATEWAY_CLIENT_H

#include <string>
#include <vector>
#include <map>

#include "ace/SOCK_Stream.h"
#include "ace/SSL/SSL_SOCK_Stream.h"
#include "ace/Time_Value.h"
#include "ace/Recursive_Thread_Mutex.h"

#include "base_define.h"
#include "insight_message.h"
#include "message_queue.h"
#include "message_handle.h"
#include "work_thread_pool.h"
#include "htsc_client_interface.h"
#include "net_wrapper.h"
#include "dispatch_pool.h"

NAMESPACE_BEGIN
  
//对象析构时释放单个对象
template<class T> 
class DescReleaseObj {
public:
  DescReleaseObj(T*& obj) { obj_ = obj; }
  virtual~ DescReleaseObj() { DELETE_P(obj_); }
private:
  T* obj_;
};

//对象析构时释放对象数组
template<class T> 
class DescReleaseObjArray {
public:
  DescReleaseObjArray(T*& obj) { obj_ = obj; }
  virtual ~DescReleaseObjArray() { DELETE_P_ARRAY(obj_); }
private:
  T* obj_;
};

/**
 * gateway客户端定义类
 */
class LIB_EXPORT MdcGatewayClient : public HtscClientInterface {
public:
  MdcGatewayClient(bool ssl_for_discovery_service = false);
  virtual ~MdcGatewayClient();

private:
  DISALLOW_COPY_AND_ASSIGN(MdcGatewayClient);

public:
  /**
   * 注册处理对象，必须在login之前设置
   * @param[in] handle 处理对象
   * @param[in] env 环境
   */
  void RegistHandle(MessageHandle* handle);

  /**
   * 注册日志回调接口
   */
  void RegistLogHandle(LogInterface* handle);

  /**
   * 使用服务发现方式，通过网关获取服务列表进行链接，按照增加的先后顺序连接
   * @param[in] ip 服务网关地址
   * @param[in] port 服务网关端口
   * @param[in] user 用户名
   * @param[in] value 登录值（密码或token)
   * @param[in] password_is_token 是否为token true token, false 密码
   * @return 0 成功 < 0 失败，错误信息见错误号
   */
  int LoginByServiceDiscovery(const std::string& ip, int port, 
    const std::string& user, const std::string& value, bool password_is_token);  
  
  /**
  * 使用服务发现方式，通过网关获取服务列表进行链接，按照增加的先后顺序连接
  * @param[in] ip 服务网关地址
  * @param[in] port 服务网关端口
  * @param[in] user 用户名
  * @param[in] value 登录值（密码或token)
  * @param[in] password_is_token 是否为token
  * @param[in] backup_list 服务网关备选地址列表
  * @return 0 成功 < 0 失败，错误信息见错误号
  */
  int LoginByServiceDiscovery(const std::string& ip, int port,
	  const std::string& user, const std::string& value, bool password_is_token,
	  std::map<std::string, int>backup_list);

  /**
   * 增加目标服务器信息
   * @param[in] ip 服务地址
   * @param[in] port 端口
   */
  int AddServerInfo(const std::string& ip, int port);

  /**
   * 使用当前已设定的服务列表进行链接，按照增加的先后顺序连接
   * @param[in] user 用户名
   * @param[in] pasword 密码
   * @return 0 成功 < 0 失败，错误信息见错误号
   */
  int Login(const std::string& user, const std::string& password);

  /**
   * 使用当前已设定的服务列表进行链接，按照增加的先后顺序连接
   * @param[in] ips 地址字符串列表
   * @param[in] ports 端口号列表
   * @param[in] user 用户名
   * @param[in] token
   * @return 0 成功 < 0 失败，错误信息见错误号
   */
  int LoginByToken(const std::string& user, const std::string& token);

  /**
   * 订阅全市场数据
   * @input[in] action_type 操作类型
   * @input[in] data_types 数据类型列表，由调用方自己释放
   * @return 0 成功 < 0 错误号
   */
  int SubscribeAll(
    ::com::htsc::mdc::insight::model::ESubscribeActionType action_type,
    ::com::htsc::mdc::insight::model::SubscribeAll* subscribe_all);

  /**
   * 根据证券类型订阅
   * @input[in] action_type 操作类型
   * @input[in] type_details 类型列表，由调用方自己释放
   * @return 0 成功 < 0 错误号
   */
  int SubscribeBySourceType(
    ::com::htsc::mdc::insight::model::ESubscribeActionType action_type,
    ::com::htsc::mdc::insight::model::SubscribeBySourceType* source_type);

  /**
   * 根据证券编号订阅
   * @input[in] action_type 操作类型
   * @input[in] id 订阅id指针，由调用方自己释放
   * @return 0 成功 < 0 错误号
   */
  int SubscribeByID(
    ::com::htsc::mdc::insight::model::ESubscribeActionType action_type,
    ::com::htsc::mdc::insight::model::SubscribeByID* id);

  /**
   * 订阅数据
   * @param[in] sub_request 订阅请求，由调用方自己释放
   * @return 0 成功 < 0 错误号
   */
  int Subscribe(::com::htsc::mdc::insight::model::MDSubscribeRequest* request);

  /**
   * 请求回放
   * @param[in] request 已分配好空间的回放请求，由调用方自己释放
   * @return 0 成功 < 0 错误号
   */
  int RequestPlayback(::com::htsc::mdc::insight::model::PlaybackRequest* request);

  /**
   * 行情查询请求
   * @param[in] request 已分配好空间的回放请求，由调用方自己释放
   * @param[out] response 内部分配空间，由调用方自己释放
   * @return 0 成功 < 0 错误号
   */
  int RequestMDQuery(::com::htsc::mdc::insight::model::MDQueryRequest* request,
    std::vector< ::com::htsc::mdc::insight::model::MDQueryResponse*>*& mdresponses);

  /**
   * 释放行情查询结果
   * @param[in] mdresponses 查询结果
   */
  //void ReleaseMdQueryResponses(std::vector<::com::htsc::mdc::insight::model::MDQueryResponse*>& mdresponses);

  /**
   * 关闭，等待线程退出
   * @param[in] clear_members 是否清理成员，在reconnect时不能清理
   * @param[in] wait_threads_quit 等待线程退出
   */
  void Close();
  
  /**
   * 设置处理线程池线程数，默认为5个
   * 当消息的处理逻辑比较复杂，需要更多线程并发处理时，
   * 可以在调用LoginByServiceDiscovery登录服务端之前设置
   * @param[in] count 线程数
   */
  void set_handle_pool_thread_count(short count);

  /**
   * 获得处理线程数
   * @return 线程数
   */
  short handle_pool_thread_count() const;

public:
  /**
   * 被net_wrapper调用，用来发布消息
   * buf由内部释放
   */
  void PublishMessage(char* buf, int len);

  /**
   * 被dispatch调用，解析并分派消息到work_pool
   * buf由外部释放
   */
  void DispatchMessageToQueue(char* buf, int len);

  /**
   * 分派InsightMessage
   */
  void DispatchInsightMessage(InsightMessage* message);

  /**
   * 订阅消息
   * @param[in] insight_message 订阅的消息体，消息体由传入方释放
   * @return 0 成功 < 0 错误号
   */
  int SubscribeByMessage(InsightMessage* insight_message);

  /**
   * 重新订阅
   * @return 0 成功 < 0 错误号
   */
  int RedoSubscribe();

  /**
  * 增加订阅
  */
  void add_subscribe(InsightMessage* subscribe_message);

  /**
   * pool是否已启动
   */
  bool work_pool_start() { return work_pool_.is_start();}
  bool dispatch_pool_start() { return dispatch_pool_.is_start(); }
  //获取dispatch队列的当前长度
  size_t dispatch_queue_count() { return dispatch_pool_.queue_count(); }
public:
  friend class ClientTestCase;   

private:
  MessageHandle handle;   //默认处理handle
  NetWrapper* net_wrapper_; //网络包装对象
  WorkThreadPool work_pool_;   //处理线程池
  DispatchPool dispatch_pool_; //分发网络接收的原始数据

  MessageHandle* handle_; //消息处理handle

  MessageQueue* login_response_queue_;     //登录response消息队列
  MessageQueue* subscribe_response_queue_; //订阅response队列
  MessageQueue* playback_response_queue_;  //回放请求和控制命令response队列
  MessageQueue* mdquery_response_queue_;  //查询response队列

  std::vector<InsightMessage*> subscribe_messages_; //已订阅的消息
  ACE_Recursive_Thread_Mutex subscribe_messages_mutex_;//订阅队列的访问控制
 };

NAMESPACE_END

#endif //HTSC_MD_GATEWAY_CLIENT_H