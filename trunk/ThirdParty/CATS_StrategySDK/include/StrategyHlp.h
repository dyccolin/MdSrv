#ifndef __CITICS_SGHLP_H__
#define __CITICS_SGHLP_H__

#ifdef WIN32
	#ifdef CITICS_SGHLP_EXPORTS
		#define CITICS_SGHLP_API __declspec(dllexport)
	#else
		#define CITICS_SGHLP_API __declspec(dllimport)
	#endif
	
	#define CITICSSTDCALL	__stdcall		/* ensure stcall calling convention on NT */
#else
	#define CITICS_SGHLP_API
	#define CITICSSTDCALL				    /* leave blank for other systems */
#endif

#define ERROR_MSG_SIZE  512
#define PARAM_MAX_SIZE  1024
#define RESULT_MAX_SIZE 2048
//////////////////////////////////////////////////////////////////////////

typedef void *SGHLPCFGHANDLE;
typedef void *SGHLPHANDLE;
typedef void *SGHLPASYNCMSG;

#ifdef __cplusplus
extern "C"
{
#endif
    // 加载配置文件项
    CITICS_SGHLP_API int CITICSSTDCALL CITICs_SgHlp_LoadConfig(SGHLPHANDLE* hConfig, const char* szConfigFile);
    
    // 根据配置句柄，初始化一个连接对象
    CITICS_SGHLP_API int CITICSSTDCALL CITICs_SgHlp_Init(SGHLPHANDLE* HlpHandle, SGHLPCFGHANDLE hConfig);

    // 释放连接句柄系统资源
    CITICS_SGHLP_API int CITICSSTDCALL CITICs_SgHlp_Exit(SGHLPHANDLE HlpHandle);

    // 与服务器建立连接
    CITICS_SGHLP_API int CITICSSTDCALL CITICs_SgHlp_ConnectServer(SGHLPHANDLE HlpHandle);
    CITICS_SGHLP_API int CITICSSTDCALL CITICs_SgHlp_ConnectSubServer(SGHLPHANDLE HlpHandle);    
    CITICS_SGHLP_API int CITICSSTDCALL CITICs_SgHlp_DisConnect(SGHLPHANDLE HlpHandle); 
    CITICS_SGHLP_API int CITICSSTDCALL CITICs_SgHlp_DisConnectSub(SGHLPHANDLE HlpHandle);

    // 提交业务请求，如果szParam参数为NULL，则认为是通过SetValue函数设置的请求参数
    CITICS_SGHLP_API int CITICSSTDCALL CITICs_SgHlp_BizCallAndCommit(SGHLPHANDLE HlpHandle, int iFuncID, const char* szParam=0);

    // 获取记录行、列数，返回值为行、列数
    CITICS_SGHLP_API int CITICSSTDCALL CITICs_SgHlp_GetRowCount(SGHLPHANDLE HlpHandle);
    CITICS_SGHLP_API int CITICSSTDCALL CITICs_SgHlp_GetColCount(SGHLPHANDLE HlpHandle);

    // 获取字段名称，序号以0为基数
    CITICS_SGHLP_API int CITICSSTDCALL CITICs_SgHlp_GetColName(SGHLPHANDLE HlpHandle, int iColumn, char* szColName);

    // 获取下一行记录，第一次调用则首先打开结果集，并把第0行作为当前记录行
    CITICS_SGHLP_API int CITICSSTDCALL CITICs_SgHlp_GetNextRow(SGHLPHANDLE HlpHandle);

    // 根据字段名称，获取字段值
    CITICS_SGHLP_API int CITICSSTDCALL CITICs_SgHlp_GetValue(SGHLPHANDLE HlpHandle, const char* szKeyName, char* szValue);

    // 根据字段序号获取字段值，序号以0为基数
    CITICS_SGHLP_API int CITICSSTDCALL CITICs_SgHlp_GetValueByIndex(SGHLPHANDLE HlpHandle, int iColumn, char* szValue);

    // 获取当前错误代码和信息
    CITICS_SGHLP_API int CITICSSTDCALL CITICs_SgHlp_GetErrorMsg(SGHLPHANDLE HlpHandle, int* piErrorCode, char* szErrorMsg);
    

    /************************************************************************/
    // 另一种请求参数设置的方法，一个一个的设置
    // 初始化请求参数
    CITICS_SGHLP_API int CITICSSTDCALL CITICs_SgHlp_BeginParam(SGHLPHANDLE HlpHandle);
    // 设置每个请求参数字段名称和值
    CITICS_SGHLP_API int CITICSSTDCALL CITICs_SgHlp_SetValue(SGHLPHANDLE HlpHandle, const char* szKeyName, const char* szValue);

    /************************************************************************/
    // 消息订阅相关函数
    
    // 从队列中读取一条消息，如果队列中没有数据，该函数会一直阻塞不返回
    CITICS_SGHLP_API int CITICSSTDCALL CITICs_SgHlp_QueueGetMsg(SGHLPHANDLE HlpHandle, int& iMsgType);

    // 处理完成或者不需要该消息时，应及时删除该消息，否则消息将占用大量内存
    // 如果不删除有可能每次都Get到同一条消息
    CITICS_SGHLP_API int CITICSSTDCALL CITICs_SgHlp_QueueEraseMsg(SGHLPHANDLE HlpHandle);

#ifdef __cplusplus
}
#endif

#endif 
