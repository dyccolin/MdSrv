/** @file
* 演示T2_SDK进行打包、发包、收包、解包
* @author  T2小组
* @author  恒生电子股份有限公司
* @version 1.0
* @date    20090327
*/

#include <../../include/CITICs_HsT2Hlp.h>

// 全局连接对象
HSHLPHANDLE HlpHandle = nullptr;

int main()
{
	printf("before newconfig\n");
	// 通过T2SDK的引出函数，来获取一个新的CConfig对象
	// 此对象在创建连接对象时被传递，用于配置所创建的连接对象的各种属性（比如服务器IP地址、安全模式）
	HSHLPCFGHANDLE hConfig;
	int iRet = CITICs_HsHlp_LoadConfig(&hConfig, "./citics_hs_config.ini");
	if (iRet)
	{
		printf("加载配置文件失败[configFile.ini] ErrorCdoe=(%d)....\n", iRet);
		exit(0);
	}

	printf("after newconfig\n");
	

	// 通过getchar阻塞线程，等待服务端应答包到达
	// 演示断开重连时，可在此时关闭服务器，然后再恢复
	getchar();

	return 0;
}
