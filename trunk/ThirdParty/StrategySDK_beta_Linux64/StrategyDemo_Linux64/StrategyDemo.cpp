#include <stdio.h>
#include <pthread.h>
#include "StrategyHlp.h"
#include <unistd.h>
#include <sys/time.h>
#include <termios.h>
#include <errno.h>
#include <iostream>
#include <string.h>

typedef struct  
{
   bool state;
   bool manual_reset;
   pthread_mutex_t mutex;
   pthread_cond_t cond;
}event_t;
#define event_handle event_t*

struct tagSubsThreadArg 
{
    event_handle hExitEvent;
    event_handle hGetSubmitResponseEvent;
    char szOrderNo[32 + 1];
    //....其它需要的参数
};

int RunReqTestUnit(SGHLPHANDLE& HlpHandle, tagSubsThreadArg *hArg);
void* DealSubsMsg(void *Arg);
int ConnectServer(SGHLPHANDLE& HlpHandle);
int ConnectSubServer(SGHLPHANDLE& HlpHandle);
void ShowAnsData(SGHLPHANDLE HlpHandle);
void ShowErrMsg(SGHLPHANDLE HlpHandle, int iFunc);
int getch();
event_handle event_create(bool manual_reset, bool init_state);
int event_wait(event_handle hevent);
int event_timedwait(event_handle hevent, long milliseconds);
int event_set(event_handle hevent);
int event_reset(event_handle hevent);
void event_destroy(event_handle hevent);

int main(int argc, char* argv[])
{
    // 请求应答的链接
    SGHLPHANDLE HlpHandleReq = NULL;
    if (ConnectServer(HlpHandleReq))
    {
        return -1;
    }

    // 启动成交回报处理线程
    int hThread;
    pthread_t tid;
    tagSubsThreadArg trd_arg;
    trd_arg.hExitEvent = event_create(true, false);
    trd_arg.hGetSubmitResponseEvent = event_create(false, false);
    hThread = pthread_create(&tid, 0, DealSubsMsg, &trd_arg);

    // 启动请求应答测试模块
    if (RunReqTestUnit(HlpHandleReq, &trd_arg) != 0)
    {
        goto __RET;
    }
    sleep(1);

    // 准备退出
    printf("\nPress q to exit...\n");
    while(getch() != 'q');
    // 通知子线程退出
    
    printf("\nexit now\n");
    event_set(trd_arg.hExitEvent); 
    sleep(2); // 等待处理线程
    event_destroy(trd_arg.hExitEvent);

__RET:
    CITICs_SgHlp_DisConnect(HlpHandleReq);
    CITICs_SgHlp_Exit(HlpHandleReq);
    return 0;
}

// 请求应答测试模块
int RunReqTestUnit(SGHLPHANDLE& HlpHandle, tagSubsThreadArg *hArg)
{
    int iRet  = 0;    
    int iFunc = 0;
    int count = 0;
    int orderQty = 0;
    double orderPrice = 0.0;
    char symbols[3][12 + 1] = {{0}};
    char szParam[1024]      = {0};
    char acctType[4 + 1]    = {0};
    char acct[32 + 1]       = {0};
    char symbol[12 + 1]     = {0};
    char tradeSide[4 + 1]   = {0};
    char orderType[4 + 1]   = {0};
    char szValue[32 + 1]    = {0};
    char currentBalance[64 + 1] = {0};
    char enabledBalance[64 + 1] = {0};
    memset(szParam, 0x00, sizeof(szParam));
    memset(acctType, 0x00, sizeof(acctType));
    memset(acct, 0x00, sizeof(acct));
    memset(symbol, 0x00, sizeof(symbol));
    memset(currentBalance, 0x00, sizeof(currentBalance));
    memset(enabledBalance, 0x00, sizeof(enabledBalance));
    memset(symbols, 0x00, sizeof(symbols));

    //////////////////////////////////////////////////////////////////////////		
    strcpy(acctType, "S0");             //股票模拟账号类型为S0
    strcpy(acct, "000007001");          //账号代码
    strcpy(symbol, "600030.SH");        //证券代码
    orderQty = 10000;                   //委托数量
    orderPrice = 10;                    //委托价格
    strcpy(tradeSide, "1");             //买卖类别
    strcpy(orderType, "0");             //委托类型
    strcpy(symbols[0], "600000.SH");    //证券代码
    strcpy(symbols[1], "000001.SZ");    //证券代码
    strcpy(symbols[2], "000002.SZ");    //证券代码
    count = 3;

    printf("\n\n---===获取交易帐户资金===---\n");
    iFunc = 100001;
    sprintf(szParam, "acctType:");
    strcat(szParam, acctType);
    strcat(szParam, ",acct:");
    strcat(szParam, acct);
    iRet = CITICs_SgHlp_BizCallAndCommit(HlpHandle, iFunc, szParam);
    if (iRet)
    {
        ShowErrMsg(HlpHandle, iFunc);
        return iRet;
    }

    CITICs_SgHlp_GetValue(HlpHandle, "currentBalance", currentBalance);
    CITICs_SgHlp_GetValue(HlpHandle, "enabledBalance", enabledBalance);
    printf("\ncurrentBalance：\t%s", currentBalance);
    printf("\nenabledBalance：\t%s", enabledBalance);

    ShowAnsData(HlpHandle);

    printf("\n\n---===获取交易帐户持仓信息===---\n");
    iFunc = 100002; 
    CITICs_SgHlp_BeginParam(HlpHandle);
    CITICs_SgHlp_SetValue(HlpHandle, "acctType", acctType);
    CITICs_SgHlp_SetValue(HlpHandle, "acct", acct);
    iRet = CITICs_SgHlp_BizCallAndCommit(HlpHandle, iFunc, NULL);
    if (iRet)
    {
        ShowErrMsg(HlpHandle, iFunc);
        return iRet;
    }
    ShowAnsData(HlpHandle);

    printf("\n\n---===获取指定证券信息===---\n");
    iFunc = 200001; 
    CITICs_SgHlp_BeginParam(HlpHandle);
    CITICs_SgHlp_SetValue(HlpHandle, "symbol", symbol);
    iRet = CITICs_SgHlp_BizCallAndCommit(HlpHandle, iFunc, NULL);
    if (iRet)
    {
        ShowErrMsg(HlpHandle, iFunc);
        return iRet;
    }
    ShowAnsData(HlpHandle);

    printf("\n\n---===订阅证券快照行情===---\n");
    iFunc = 300001; 
    CITICs_SgHlp_BeginParam(HlpHandle);
    CITICs_SgHlp_SetValue(HlpHandle, "symbol", symbol);
    iRet = CITICs_SgHlp_BizCallAndCommit(HlpHandle, iFunc, NULL);
    if (iRet)
    {
        ShowErrMsg(HlpHandle, iFunc);
        return iRet;
    }
    ShowAnsData(HlpHandle);

    printf("\n\n---===订阅一组证券快照行情===---\n");
    iFunc = 300002; 
    CITICs_SgHlp_BeginParam(HlpHandle);
    memset(szValue, 0x00, sizeof(szValue));
    for (int i=0;i<count;i++)
    {
        strcat(szValue, symbols[i]);
        strcat(szValue, "|");
    }
    if (strlen(szValue) >= 1)
    {
        szValue[strlen(szValue)-1] = 0x00;
    }
    CITICs_SgHlp_SetValue(HlpHandle, "symbols", szValue);
    memset(szValue, 0x00, sizeof(szValue));
    sprintf(szValue, "%d", count);
    CITICs_SgHlp_SetValue(HlpHandle, "count", szValue);
    iRet = CITICs_SgHlp_BizCallAndCommit(HlpHandle, iFunc, NULL);
    if (iRet)
    {
        ShowErrMsg(HlpHandle, iFunc);
        return iRet;
    }
    ShowAnsData(HlpHandle);

    printf("\n\n---===订阅证券逐笔成交数据===---\n");
    iFunc = 300003; 
    CITICs_SgHlp_BeginParam(HlpHandle);
    CITICs_SgHlp_SetValue(HlpHandle, "symbol", symbol);
    iRet = CITICs_SgHlp_BizCallAndCommit(HlpHandle, iFunc, NULL);
    if (iRet)
    {
        ShowErrMsg(HlpHandle, iFunc);
        return iRet;
    }
    ShowAnsData(HlpHandle);

    printf("\n\n---===订阅交易帐户成交回报===---\n");
    iFunc = 500001; 
    CITICs_SgHlp_BeginParam(HlpHandle);
    CITICs_SgHlp_SetValue(HlpHandle, "acctType", acctType);
    CITICs_SgHlp_SetValue(HlpHandle, "acct", acct);
    iRet = CITICs_SgHlp_BizCallAndCommit(HlpHandle, iFunc, NULL);
    if (iRet)
    {
        ShowErrMsg(HlpHandle, iFunc);
        return iRet;
    }
    ShowAnsData(HlpHandle);

    for (int k=0; k<300; k++)
    {
        printf("\n\n---===委托===---\n");
        iFunc = 500002; 
        CITICs_SgHlp_BeginParam(HlpHandle);
        CITICs_SgHlp_SetValue(HlpHandle, "acctType", acctType);
        CITICs_SgHlp_SetValue(HlpHandle, "acct", acct);
        CITICs_SgHlp_SetValue(HlpHandle, "symbol", symbol);
        memset(szValue, 0x00, sizeof(szValue));
        sprintf(szValue, "%d", orderQty);
        CITICs_SgHlp_SetValue(HlpHandle, "orderQty", szValue);
        memset(szValue, 0x00, sizeof(szValue));
        orderPrice = orderPrice + 0.5;
        sprintf(szValue, "%f", orderPrice);
        CITICs_SgHlp_SetValue(HlpHandle, "orderPrice", szValue);
        CITICs_SgHlp_SetValue(HlpHandle, "tradeSide", tradeSide);
        CITICs_SgHlp_SetValue(HlpHandle, "orderType", orderType);
        iRet = CITICs_SgHlp_BizCallAndCommit(HlpHandle, iFunc, NULL);
        if (iRet)
        {
            ShowErrMsg(HlpHandle, iFunc);
            return iRet;
        }
        ShowAnsData(HlpHandle);

        int nIndex = event_timedwait(hArg->hGetSubmitResponseEvent, 2000);
        if (nIndex == 0)
        {
            printf("\n\n---===撤单===---\n");
            iFunc = 500003; 
            CITICs_SgHlp_BeginParam(HlpHandle);
            CITICs_SgHlp_SetValue(HlpHandle, "acctType", acctType);
            CITICs_SgHlp_SetValue(HlpHandle, "acct", acct);
            CITICs_SgHlp_SetValue(HlpHandle, "orderNo", hArg->szOrderNo);
            iRet = CITICs_SgHlp_BizCallAndCommit(HlpHandle, iFunc, NULL);
            if (iRet)
            {
                ShowErrMsg(HlpHandle, iFunc);
                return iRet;
            }
            ShowAnsData(HlpHandle);
        }
        else if (nIndex == 1)
        {
            continue;
        }
    }

    printf("\n\n---===不支持的功能===---\n");
    iFunc = 999999; 
    CITICs_SgHlp_BeginParam(HlpHandle);
    iRet = CITICs_SgHlp_BizCallAndCommit(HlpHandle, iFunc, NULL);
    if (iRet)
    {
        ShowErrMsg(HlpHandle, iFunc);
        return iRet;
    }
    ShowAnsData(HlpHandle);

    printf("\n\n---===通知策略代理退出===---\n");
    iFunc = 990001; 
    CITICs_SgHlp_BeginParam(HlpHandle);
    iRet = CITICs_SgHlp_BizCallAndCommit(HlpHandle, iFunc, NULL);
    if (iRet)
    {
        ShowErrMsg(HlpHandle, iFunc);
        return iRet;
    }
    ShowAnsData(HlpHandle);

    return iRet;
}

// 处理订阅消息
void* DealSubsMsg(void *Arg) 
{
    int iRet = 0;
    int iMsgType = 0;
    char orderNo[32 + 1]    = {0};

    // 消息订阅的链接，链接不能跨线程
    SGHLPHANDLE HlpHandleSub = NULL;
    if ((iRet = ConnectSubServer(HlpHandleSub)) != 0)
    {
        return NULL;
    }

    tagSubsThreadArg* trd_arg = (tagSubsThreadArg*)Arg;
    event_handle hEvent = trd_arg->hExitEvent;
    while (event_timedwait(hEvent, 1) != 0)
    {
        iMsgType = 0;
        iRet = CITICs_SgHlp_QueueGetMsg(HlpHandleSub, iMsgType);

        // 订阅信息处理
        if (iRet == 0)
        {
            printf("\n收到消息[%04d]:", iMsgType);
            ShowAnsData(HlpHandleSub);
            switch(iMsgType)
            {
            case 1002: // 退出通知
                {
                    event_set(hEvent);
                    break;
                }
            case 3001: // 快照行情
                break;
            case 4001: // 委托应答
                {
                    memset(orderNo, 0x00, sizeof(orderNo));
                    CITICs_SgHlp_GetValue(HlpHandleSub, "orderNo", orderNo);
                    memset(trd_arg->szOrderNo, 0x00, sizeof(trd_arg->szOrderNo));
                    strcpy(trd_arg->szOrderNo, orderNo);
                    event_set(trd_arg->hGetSubmitResponseEvent);
                    break;
                }
            default:  // 未知的消息                
                break;
            }
            // 处理完成一定删除该消息
            CITICs_SgHlp_QueueEraseMsg(HlpHandleSub);
        }

        usleep(1);
    }

    CITICs_SgHlp_DisConnect(HlpHandleSub);
    CITICs_SgHlp_Exit(HlpHandleSub);
    return NULL;
}

// 连接服务器
int ConnectServer(SGHLPHANDLE& HlpHandle)
{
    int iRet = 0;
    char szMsg[256];
    SGHLPCFGHANDLE hConfig; 

    memset(szMsg, 0X00, sizeof(szMsg));
    iRet = CITICs_SgHlp_LoadConfig(&hConfig, "Sgconfig.ini");
    if (iRet)
    {
        printf("加载配置文件失败[Sgconfig.ini] ErrorCdoe=(%d)....\n", iRet);
        return iRet;
    }

    iRet = CITICs_SgHlp_Init(&HlpHandle, hConfig); 
    if (iRet)
    {
        CITICs_SgHlp_GetErrorMsg(HlpHandle, NULL, szMsg);
        printf("初始化连接句柄失败 ErrorCdoe=(%d)....\n", iRet);
        return iRet;
    }

    // 连接服务器
    iRet = CITICs_SgHlp_ConnectServer(HlpHandle);
    if (iRet)
    { 
        CITICs_SgHlp_GetErrorMsg(HlpHandle, NULL, szMsg);
        printf("连接Server失败: ErrorCdoe=(%d) %s....", iRet, szMsg);
        return iRet;
    }
    else 
    {
        printf("Connect StrategyAgent OK......\n");
    }
    return iRet;
}

// 连接发布订阅服务器
int ConnectSubServer(SGHLPHANDLE& HlpHandle)
{
    int iRet = 0;
    char szMsg[256];
    SGHLPCFGHANDLE hConfig; 

    memset(szMsg, 0X00, sizeof(szMsg));
    iRet = CITICs_SgHlp_LoadConfig(&hConfig, "Sgconfig.ini");
    if (iRet)
    {
        printf("加载配置文件失败[Sgconfig.ini] ErrorCdoe=(%d)....\n", iRet);
        return iRet;
    }

    iRet = CITICs_SgHlp_Init(&HlpHandle, hConfig); 
    if (iRet)
    {
        CITICs_SgHlp_GetErrorMsg(HlpHandle, NULL, szMsg);
        printf("初始化连接句柄失败 ErrorCdoe=(%d)....\n", iRet);
        return iRet;
    }

    // 连接服务器
    iRet = CITICs_SgHlp_ConnectSubServer(HlpHandle);
    if (iRet)
    { 
        CITICs_SgHlp_GetErrorMsg(HlpHandle, NULL, szMsg);
        printf("连接Server失败: ErrorCdoe=(%d) %s....", iRet, szMsg);
        return iRet;
    }
    else 
    {
        printf("Connect StrategyPublishServer OK......\n");
    }

    return iRet;
}

// 打印接收信息
void ShowAnsData(SGHLPHANDLE HlpHandle)
{
    int iRow, iCol;
    char szKey[64], szValue[512];  	
  	struct timeval    tv;  
    struct timezone tz;        
    struct tm *p;        
    gettimeofday(&tv, &tz);        
    p = localtime(&tv.tv_sec);      
    
    iRow =CITICs_SgHlp_GetRowCount(HlpHandle);
    iCol =CITICs_SgHlp_GetColCount(HlpHandle);

    printf("\n------------------New Answers------------------");

    for(int i=0; i<iRow; i++)
    {
        if(0 == CITICs_SgHlp_GetNextRow(HlpHandle))
        {
            printf("\n[%03d]:------------------------------------\n", i+1);
            for(int j=0; j<iCol; j++)
            {
                memset(szKey, 0x00, sizeof(szKey));
                memset(szValue, 0x00, sizeof(szValue));
                CITICs_SgHlp_GetColName(HlpHandle,j, szKey);
                CITICs_SgHlp_GetValueByIndex(HlpHandle, j, szValue);
                printf("%s:%s, ", szKey, szValue);
            }
        }
    }
    printf("\n时间：%d-%d-%d %d:%d:%d.%d\n", (1900+p->tm_year),(1+p->tm_mon),p->tm_mday, p->tm_hour, p->tm_min, p->tm_sec, (int)tv.tv_usec); 
    printf("\n");
}

// 打印错误信息
void ShowErrMsg(SGHLPHANDLE HlpHandle, int iFunc)
{
    int nErr =0;
    char szMsg[ERROR_MSG_SIZE + 1]={0};

    CITICs_SgHlp_GetErrorMsg(HlpHandle, &nErr, szMsg);
    printf("请求业务失败[%d]: Error=(%d) %s\n", iFunc, nErr, szMsg);
}

int getch()
{
   struct termios tm, tm_old;
   int fd = STDIN_FILENO,c;
 
   if (tcgetattr(fd, &tm) < 0)
   {
      return -1;
   }
 
   tm_old = tm;
   cfmakeraw(&tm);
 
   if (tcsetattr(fd, TCSANOW, &tm) < 0)
   {
      return -1;
   }
 
   c = fgetc(stdin);
 
   if (tcsetattr(fd,TCSANOW,&tm_old) < 0)
   {
      return -1;
   }
 
   return c;
}

//返回值：NULL 出错
event_handle event_create(bool manual_reset, bool init_state)
{   
    event_handle hevent = new(std::nothrow) event_t;
    if (hevent == NULL)
    {
        return NULL;
    }
    hevent->state = init_state;
    hevent->manual_reset = manual_reset;
    if (pthread_mutex_init(&hevent->mutex, NULL))
    {
        delete hevent;
        return NULL;
    }
    if (pthread_cond_init(&hevent->cond, NULL))
    {
        pthread_mutex_destroy(&hevent->mutex);
        delete hevent;
        return NULL;
    }
    return hevent;
}

//返回值：0 等到事件，-1出错
int event_wait(event_handle hevent)
{
    if (pthread_mutex_lock(&hevent->mutex))   
    {      
        return -1;   
    }   
    while (!hevent->state)    
    {      
        if (pthread_cond_wait(&hevent->cond, &hevent->mutex))   
        {   
            pthread_mutex_unlock(&hevent->mutex); 
            return -1;   
        }   
    }   
    if (!hevent->manual_reset) 
    {
        hevent->state = false;
    }
    if (pthread_mutex_unlock(&hevent->mutex))   
    {     
        return -1;   
    }  
    return 0;
}

//返回值：0 等到事件，1 超时，-1出错
int event_timedwait(event_handle hevent, long milliseconds)
{
    int rc = 0;   
    struct timespec abstime;   
    struct timeval tv;   
    gettimeofday(&tv, NULL);   
    abstime.tv_sec  = tv.tv_sec + milliseconds / 1000;   
    abstime.tv_nsec = tv.tv_usec*1000 + (milliseconds % 1000)*1000000;   
    if (abstime.tv_nsec >= 1000000000)   
    {   
        abstime.tv_nsec -= 1000000000;   
        abstime.tv_sec++;   
    }   

    if (pthread_mutex_lock(&hevent->mutex) != 0)   
    {     
        return -1;   
    }   
    while (!hevent->state)    
    {      
        if ((rc = pthread_cond_timedwait(&hevent->cond, &hevent->mutex, &abstime)))   
        {   
            if (rc == ETIMEDOUT) break;   
            pthread_mutex_unlock(&hevent->mutex);    
            return -1;   
        }   
    }   
    if (rc == 0 && !hevent->manual_reset)   
    {
        hevent->state = false;
    }
    if (pthread_mutex_unlock(&hevent->mutex) != 0)   
    {      
        return -1;   
    }
    if (rc == ETIMEDOUT)
    {
        //timeout return 1
        return 1;
    }
    //wait event success return 0
    return 0;
}

//返回值：0 成功，-1出错
int event_set(event_handle hevent)
{
    if (pthread_mutex_lock(&hevent->mutex) != 0)
    {
        return -1;
    }

    hevent->state = true;

    if (hevent->manual_reset)
    {
        if(pthread_cond_broadcast(&hevent->cond))
        {
            return -1;
        }
    }
    else
    {
        if(pthread_cond_signal(&hevent->cond))
        {
            return -1;
        }
    }

    if (pthread_mutex_unlock(&hevent->mutex) != 0)
    {
        return -1;
    }

    return 0;
}

//返回值：0 成功，-1出错
int event_reset(event_handle hevent) 
{
    if (pthread_mutex_lock(&hevent->mutex) != 0)
    {
        return -1;
    }

    hevent->state = false;

    if (pthread_mutex_unlock(&hevent->mutex) != 0)
    {      
        return -1;
    }
    return 0;
}

//返回值：无
void event_destroy(event_handle hevent)
{
    pthread_cond_destroy(&hevent->cond);
    pthread_mutex_destroy(&hevent->mutex);
    delete hevent;
}
