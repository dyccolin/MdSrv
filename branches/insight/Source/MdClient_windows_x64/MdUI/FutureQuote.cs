﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClrMdApiLib;

namespace MdUI
{
    public class FutureQuote : NotifyObject
    {
        string _Ticker;
        public string Ticker { get { return _Ticker; } set { UpdateProperty(ref _Ticker, value, "Ticker"); } }

        string _CNName;
        public string CNName { get { return _CNName; } set { UpdateProperty(ref _CNName, value, "CNName"); } }

        string _ExchTicker;
        public string ExchTicker { get { return _ExchTicker; } set { UpdateProperty(ref _ExchTicker, value, "ExchTicker"); } }

        string szCode;
        int nActionDay;
        int nTradingDay;

        TimeSpan _Time;
        public TimeSpan Time { get { return _Time; } set { UpdateProperty(ref _Time, value, "Time"); } }

        int nStatus;

        double _PreClose;
        public double PreClose { get { return _PreClose; } set { UpdateProperty(ref _PreClose, value, "PreClose");} }

        double _PreSettlePrice;
        public double PreSettlePrice { get { return _PreSettlePrice; } set { UpdateProperty(ref _PreSettlePrice, value, "PreSettlePrice"); } }

        double _Open;
        public double Open { get { return _Open; } set { UpdateProperty(ref _Open, value, "Open"); } }

        double _High;
        public double High { get { return _High; } set { UpdateProperty(ref _High, value, "High"); } }

        double _Low;
        public double Low { get { return _Low; } set { UpdateProperty(ref _Low, value, "Low"); } }

        double _Match;
        public double Match
        {
            get { return _Match; }
            set
            {
                UpdateProperty(ref _Match, value, "Match");
                OnPropertyChanged("UpDown");
                OnPropertyChanged("UpDownPercent");
            }
        }

        double _Ask1;
        public double Ask1
        {
            get { return _Ask1; }
            set
            {
                UpdateProperty(ref _Ask1, value, "Ask1");
                OnPropertyChanged("Diff_Ask1");
                OnPropertyChanged("Diff_Bid1");

                OnPropertyChanged("Bid2");
                OnPropertyChanged("Bid3");
                OnPropertyChanged("Bid4");
                OnPropertyChanged("Bid5");
            }
        }

        double _Ask2;
        public double Ask2 { get { return _Ask2; } set { UpdateProperty(ref _Ask2, value, "Ask2"); OnPropertyChanged("Diff_Ask2"); } }

        double _Ask3;
        public double Ask3 { get { return _Ask3; } set { UpdateProperty(ref _Ask3, value, "Ask3"); OnPropertyChanged("Diff_Ask3"); } }

        double _Ask4;
        public double Ask4 { get { return _Ask4; } set { UpdateProperty(ref _Ask4, value, "Ask4"); OnPropertyChanged("Diff_Ask4"); } }

        double _Ask5;
        public double Ask5 { get { return _Ask5; } set { UpdateProperty(ref _Ask5, value, "Ask5"); OnPropertyChanged("Diff_Ask5"); } }

        double _Bid1;
        public double Bid1
        {
            get { return _Bid1; }
            set
            {
                UpdateProperty(ref _Bid1, value, "Bid1"); OnPropertyChanged("Diff_Ask1");
                OnPropertyChanged("Diff_Bid1");

                OnPropertyChanged("Ask2");
                OnPropertyChanged("Ask3");
                OnPropertyChanged("Ask4");
                OnPropertyChanged("Ask5");
            }
        }

        double _Bid2;
        public double Bid2 { get { return _Bid2; } set { UpdateProperty(ref _Bid2, value, "Bid2"); OnPropertyChanged("Diff_Bid2"); } }

        double _Bid3;
        public double Bid3 { get { return _Bid3; } set { UpdateProperty(ref _Bid3, value, "Bid3"); OnPropertyChanged("Diff_Bid3"); } }

        double _Bid4;
        public double Bid4 { get { return _Bid4; } set { UpdateProperty(ref _Bid4, value, "Bid4"); OnPropertyChanged("Diff_Bid4"); } }

        double _Bid5;
        public double Bid5 { get { return _Bid5; } set { UpdateProperty(ref _Bid5, value, "Bid5"); OnPropertyChanged("Diff_Bid5"); } }

        double _AskVol1;
        public double AskVol1 { get { return _AskVol1; } set { UpdateProperty(ref _AskVol1, value,"AskVol1"); } }

        double _AskVol2;
        public double AskVol2 { get { return _AskVol2; } set { UpdateProperty(ref _AskVol2, value, "AskVol2"); } }

        double _AskVol3;
        public double AskVol3 { get { return _AskVol3; } set { UpdateProperty(ref _AskVol3, value, "AskVol3"); } }

        double _AskVol4;
        public double AskVol4 { get { return _AskVol4; } set { UpdateProperty(ref _AskVol4, value, "AskVol4"); } }

        double _AskVol5;
        public double AskVol5 { get { return _AskVol5; } set { UpdateProperty(ref _AskVol5, value, "AskVol5"); } }

        double _BidVol1;
        public double BidVol1 { get { return _BidVol1; } set { UpdateProperty(ref _BidVol1, value, "BidVol1"); } }

        double _BidVol2;
        public double BidVol2 { get { return _BidVol2; } set { UpdateProperty(ref _BidVol2, value, "BidVol2"); } }

        double _BidVol3;
        public double BidVol3 { get { return _BidVol3; } set { UpdateProperty(ref _BidVol3, value, "BidVol3"); } }

        double _BidVol4;
        public double BidVol4 { get { return _BidVol4; } set { UpdateProperty(ref _BidVol4, value, "BidVol4"); } }

        double _BidVol5;
        public double BidVol5 { get { return _BidVol5; } set { UpdateProperty(ref _BidVol5, value, "BidVol5"); } }

        int nNumTrades;

        Int64 iPreOpenInterest;
        Int64 iVolume;
        Int64 iTurnover;
        Int64 iOpenInterest;
        double SettlePrice;

        double _HighLimited;
        public double HighLimited { get { return _HighLimited; } set { UpdateProperty(ref _HighLimited, value); } }

        double _LowLimited;
        public double LowLimited { get { return _LowLimited; } set { UpdateProperty(ref _LowLimited, value); } }

        double PreDelta;
        double CurrDelta;
        double AuctionPrice;
        int AuctionQty;
        double AvgPrice;

        #region diff

        public double Diff_Ask1 { get { return Math.Abs(Ask1) < 0.000001 ? 0.0 : Math.Round(Ask1 - Bid1, 2); } }

        public double Diff_Ask2 { get { return Math.Abs(Ask2) < 0.000001 ? 0.0 : Math.Round(Ask2 - Bid1, 2); } }

        public double Diff_Ask3 { get { return Math.Abs(Ask3) < 0.000001 ? 0.0 : Math.Round(Ask3 - Bid1, 2); } }

        public double Diff_Ask4 { get { return Math.Abs(Ask4) < 0.000001 ? 0.0 : Math.Round(Ask4 - Bid1, 2); } }

        public double Diff_Ask5 { get { return Math.Abs(Ask5) < 0.000001 ? 0.0 : Math.Round(Ask5 - Bid1, 2); } }

        public double Diff_Bid1 { get { return Math.Abs(Bid1) < 0.000001 ? 0.0 : Math.Round(Bid1 - Ask1, 2); } }

        public double Diff_Bid2 { get { return Math.Abs(Bid2) < 0.000001 ? 0.0 : Math.Round(Bid2 - Ask1, 2); } }

        public double Diff_Bid3 { get { return Math.Abs(Bid3) < 0.000001 ? 0.0 : Math.Round(Bid3 - Ask1, 2); } }

        public double Diff_Bid4 { get { return Math.Abs(Bid4) < 0.000001 ? 0.0 : Math.Round(Bid4 - Ask1, 2); } }

        public double Diff_Bid5 { get { return Math.Abs(Bid5) < 0.000001 ? 0.0 : Math.Round(Bid5 - Ask1, 2); } }

        #endregion diff

        public double UpDown
        {
            get { return Match - PreClose; }
        }

        public double UpDownPercent
        {
            get { return Math.Abs(Match) < 0.000001 ? 0.0 : (Match / PreClose) - 1.0; }
        }

        public FutureQuote(string ticker, string exchTicker)
        {
            Ticker = ticker;
            ExchTicker = exchTicker;
        }

        public void Update(ClrFutureQuote quote)
        {
            Ticker = quote.szWindCode;
            szCode = quote.szCode;
            CNName = quote.szCNName;
            Time = new TimeSpan(quote.nTime / 10000000, (quote.nTime % 10000000) / 100000, (quote.nTime % 100000) / 1000);
            iPreOpenInterest = quote.iPreOpenInterest;
            PreClose = quote.nPreClose;
            PreSettlePrice = quote.nPreSettlePrice;
            Open = quote.nOpen;
            High = quote.nHigh;
            Low = quote.nLow;
            Match = quote.nMatch;
            iOpenInterest = quote.iPreOpenInterest;
            SettlePrice = quote.nSettlePrice;

            Ask1 = quote.nAskPrice[0];
            Ask2 = quote.nAskPrice[1];
            Ask3 = quote.nAskPrice[2];
            Ask4 = quote.nAskPrice[3];
            Ask5 = quote.nAskPrice[4];

            Bid1 = quote.nBidPrice[0];
            Bid2 = quote.nBidPrice[1];
            Bid3 = quote.nBidPrice[2];
            Bid4 = quote.nBidPrice[3];
            Bid5 = quote.nBidPrice[4];

            AskVol1 = (int)(quote.nAskVol[0]);
            AskVol2 = (int)(quote.nAskVol[1]);
            AskVol3 = (int)(quote.nAskVol[2]);
            AskVol4 = (int)(quote.nAskVol[3]);
            AskVol5 = (int)(quote.nAskVol[4]);
                                       
            BidVol1 = (int)(quote.nBidVol[0]);
            BidVol2 = (int)(quote.nBidVol[1]);
            BidVol3 = (int)(quote.nBidVol[2]);
            BidVol4 = (int)(quote.nBidVol[3]);
            BidVol5 = (int)(quote.nBidVol[4]);

            HighLimited = quote.nHighLimited;
            LowLimited = quote.nLowLimited;

            PreDelta = quote.nPreDelta;
            CurrDelta = quote.nCurrDelta;
            AuctionPrice = quote.lAuctionPrice;
            AuctionQty = quote.lAuctionQty;
            AvgPrice = quote.lAvgPrice;
        }
    }
}
