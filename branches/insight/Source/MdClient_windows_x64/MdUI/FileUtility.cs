﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.Reflection;

namespace MdUI
{
    public class FileUtility
    {
        #region Singleton

        private static FileUtility _Instance;

        public static FileUtility Instance
        {
            get
            {
                LazyInitializer.EnsureInitialized(ref _Instance, () => new FileUtility());
                return _Instance;
            }
        }

        #endregion

        string _RootFolder = null;
        string _TickerFile = null;
        public string GetRootFolder()
        {
            if (_RootFolder == null)
            {
                _RootFolder = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            }
            return _RootFolder;
        }

        public string GetTickerFile()
        {
            if (_TickerFile == null)
            {
                _TickerFile = Path.Combine(GetRootFolder(), "Ticker.txt");
            }

            if (!File.Exists(_TickerFile))
            {
                using (var stream = File.Create(_TickerFile))
                {

                }
            }
            return _TickerFile;
        }
    }

}
