﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Interop;
using System.Collections.ObjectModel;

namespace TradingUI
{
    public class IntValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is double)
            {
                double dValue = (double)value;
                if (double.IsNaN(dValue))
                {
                    return "-";
                }

                return dValue.ToString("#0");
            }
            else if (value is int)
            {
                int iValue = (int)value;
                return iValue.ToString();
            }
            else
            {
                return "????";
            }
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(typeof(double), typeof(string))]
    public class PriceValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double dValue = (double)value;
            if (double.MaxValue == dValue || double.IsNaN(dValue))
            {
                return "-";
            }

            return dValue.ToString("0.0##");
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class DoubleValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double dValue = (double)value;
            if (double.IsNaN(dValue))
            {
                return "-";
            }

            return dValue.ToString();
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class AmountValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double dValue = (double)value;
            if (double.IsNaN(dValue))
            {
                return "-";
            }

            return ((dValue) / 10000).ToString("#0.00");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string strValue = value.ToString();
            double dValue;
            if (Double.TryParse(strValue, out dValue))
            {
                return dValue * 10000;
            }
            return DependencyProperty.UnsetValue;
        }
    }

    public class PercentValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double dValue = (double)value;
            if (double.IsNaN(dValue))
            {
                return "-";
            }

            return string.Format("{0:0.##}%", dValue * 100);
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(typeof(double), typeof(double))]
    public class PercentValueConverter2 : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return 0;
            }
            double dValue = (double)value;
            if (double.IsNaN(dValue))
            {
                return 0;
            }

            return dValue * 100;
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return 0;
            }
            double dValue = (double)value;
            return dValue / 100.0;
        }
    }

    public class DatetimeValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            DateTime dateTime = (DateTime)value;
            if (dateTime == DateTime.MinValue)
            {
                return "-";
            }

            return dateTime.ToString("HH:mm:ss");
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class QuantityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int iValue = (int)value;

            if (iValue == 0)
            {
                return "-";
            }
            else if (iValue < 0)
            {
                return "??";
            }
            else
            {
                return iValue.ToString();
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class TradeTimeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int iValue = (int)value;

            if (iValue == int.MinValue)
            {
                return "-";
            }
            else if (iValue < 0)
            {
                return "??";
            }
            else
            {
                return iValue.ToString();
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string strValue = value as string;
            int iValue;
            if (int.TryParse(strValue, out iValue))
            {
                if (iValue <= 0)
                {
                    return DependencyProperty.UnsetValue;
                }

                return iValue;
            }
            return DependencyProperty.UnsetValue;
        }
    }

    [ValueConversion(typeof(double), typeof(SolidColorBrush))]
    public class UpDownConvertor : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double dValue = (double)value;

            return new SolidColorBrush(dValue > 0 ? Colors.Red : Colors.Green);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(typeof(bool), typeof(string))]
    public class BurOrSellConverter : IValueConverter
    {
        private const string BUY = "买入";
        private const string SELL = "卖出";

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool bValue = (bool)value;
            return bValue ? BUY : SELL;
            
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(typeof(string), typeof(int))]
    public class ParentOpenTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int iValue = (int)value;

            return string.Format("开仓({0})", iValue);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(typeof(string), typeof(int))]
    public class ParentStockOpenTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int iValue = (int)value;

            return string.Format("现货开仓({0})", iValue);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(typeof(string), typeof(int))]
    public class ParentIndexFutureOpenTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int iValue = (int)value;

            return string.Format("期货开仓({0})", iValue);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(typeof(bool), typeof(int))]
    public class EnableByCountConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return "错误???";
            }

            int iValue = (int)value;

            return iValue > 0 ? true : false;
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }



    [ValueConversion(typeof(SolidColorBrush), typeof(double))]
    public class UpDownColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double dValue = (double)value;

            if (dValue == 0)
            {
                return new SolidColorBrush(Colors.Black);
            }
            else if (dValue > 0)
            {
                return new SolidColorBrush(Colors.Red);
            }
            else
            {
                return new SolidColorBrush(Colors.Green);
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
