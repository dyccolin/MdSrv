// ClrMdApi.h

#pragma once

#include <memory>

#include "ClrMdData.h"

using namespace System;

namespace ClrMdApiLib 
{
	public delegate void StockQuoteResponseHandler(ClrStockQuote^ quote);
	public delegate void FutureQuoteResponseHandler(ClrFutureQuote^ quote);
	public delegate void IndexQuoteResponseHandler(ClrIndexQuote^ quote);

	public delegate void TransactionResponseHandler(ClrTransaction^ trans);
	public delegate void OrderResponseHandler(ClrOrder^ order);
	public delegate void OrderQueueResponseHandler(ClrOrderQueue^ orderQueue);

	//public delegate void CodeTableResponseHandler(ClrCodeTable^ codeTable);

	public ref class ClrMdApi
	{
		delegate void StockQuoteNativeResponseHandler(StockQuote* quote);
		delegate void IndexQuoteNativeResponseHandler(IndexQuote* quote);
		delegate void FutureQuoteNativeResponseHandler(FutureQuote* quote);

		delegate void TransactionNativeResponseHandler(Transaction* trans);
		delegate void OrderNativeResponseHandler(Order* order);
		delegate void OrderQueueNativeResponseHandler(OrderQueue* orderQueue);

		//delegate void CodeTableNativeResponseHandler(CodeTable* codeTable);
	public:
		ClrMdApi();
		~ClrMdApi();

		event StockQuoteResponseHandler^ StockQuoteResponsed;
		event FutureQuoteResponseHandler^ FutureQuoteResponsed;
		event IndexQuoteResponseHandler^ IndexQuoteResponsed;

		event TransactionResponseHandler^ TransactionResponsed;
		event OrderResponseHandler^ OrderResponsed;
		event OrderQueueResponseHandler^ OrderQueueResponsed;

		//event CodeTableResponseHandler^ CodeTableResponsed;

	private:
		StockQuoteNativeResponseHandler^ _StockQuoteNativeResponseHandler;
		IndexQuoteNativeResponseHandler^ _IndexQuoteNativeResponseHandler;
		FutureQuoteNativeResponseHandler^ _FutureQuoteNativeResponseHandler;

		TransactionNativeResponseHandler^ _TransactionNativeResponseHandler;
		OrderNativeResponseHandler^ _OrderNativeResponseHandler;
		OrderQueueNativeResponseHandler^ _OrderQueueNativeResponseHandler;

		//CodeTableNativeResponseHandler^ _CodeTableNativeResponseHandler;
	public:
		// 0 - failed; 1 - success
		int Connect();
		int Unconnect();

		//bool SubscribeList( List<System::String^>^ subList);
		bool Subscribe(System::String^ sub);
		//bool UnSubscribeList( List<System::String^>^ subList);
		bool UnSubscribe(System::String^ sub);
// 
// 		ClrStockQuote GetLatestStockQuote(System::String^ ticker);
// 		ClrIndexQuote GetLatestIndexQuote(System::String^ ticker);
// 		ClrFutureQuote GetLatestFutureQuote(System::String^ ticker);
// 		ClrTransaction GetLatestTransactionQuote(System::String^ ticker);
// 		ClrOrder GetLatestOrderQuote(System::String^ ticker);
// 		ClrOrderQueue GetLatestOrderQueueQuote(System::String^ ticker);

		//bool GetLatestStockQuoteList( List<System::String^>^ tickerList,List<ClrStockQuote^>^ vecRtn, System::String^ errMsg);
		//bool GetLatestIndexQuoteList( List<System::String^>^ tickerList,List<ClrIndexQuote^>^ vecRtn, System::String^ errMsg);
		//bool GetLatestFutureQuoteList( List<System::String^>^ tickerList,List<ClrFutureQuote^>^ vecRtn, System::String^ errMsg);
		//bool GetLatestTransactionQuoteList( List<System::String^>^ tickerList,List<ClrTransaction^>^ vecRtn, System::String^ errMsg);
		//bool GetLatestOrderQuoteList( List<System::String^>^ tickerList,List<ClrOrder^>^ vecRtn, System::String^ errMsg);
		//bool GetLatestOrderQueueQuoteList( List<System::String^>^ tickerList,List<ClrOrderQueue^>^ vecRtn, System::String^ errMsg);

	internal:
		void OnStockQuoteCallback(StockQuote* quote);
		void OnFutureQuoteCallback(FutureQuote* quote);
		void OnIndexQuoteCallback(IndexQuote* quote);

		void OnTransactionCallback(Transaction* trans);
		void OnOrderCallback(Order* order);
		void OnOrderQueueCallback(OrderQueue* orderQueue);
		//void OnCodeTableCallback(CodeTable* codeTable);
		// TODO: Add your methods for this class here.
	};
}
