#pragma once

#include "MdApiData.h"
using namespace MdApi;

using namespace System;
using namespace System::Text;
using namespace System::Collections;
using namespace System::Collections::Generic;

namespace ClrMdApiLib
{

	public enum class ClrDataType
	{
		eDataBase = 0,
		eStockQuote = 1,
		eFutureQuote = 2,
		eIndexQuote = 3,
		eTransaction = 4,
		eOrder = 5,
		eOrderQueue = 6,
		//eCodeTable = 7
	};

public ref class ClrDataBase
{
public:
	//property String^ Ticker;
	property ClrDataType eType;

	ClrDataBase(ClrDataType type)
	{
		eType = type;
	}

	static double BPS = 0.0001;
};

public ref class ClrStockQuote : public ClrDataBase
{
public:
	property String^	szWindCode;
	property String^	szCode;
	property String^	szCNName;
	property int		nActionDay;
	property int		nTradingDay;
	property int		nTime;
	property int		nStatus;
	property double		nPreClose;
	property double		nOpen;
	property double		nHigh;
	property double		nLow;
	property double		nMatch;

	property List<double>^ nAskPrice;
	property List<int>^ nAskVol;
	property List<double>^ nBidPrice;
	property List<int>^ nBidVol;

	property unsigned int nNumTrades;

	property Int64 iVolume;
	property Int64 iTurnover;
	property Int64 nTotalBidVol;
	property Int64 nTotalAskVol;

	property double nWeightedAvgBidPrice;
	property double nWeightedAvgAskPrice;

	property int nIOPV;
	property int nYieldToMaturity;

	property double nHighLimited;
	property double nLowLimited;

	property int nSyl1;
	property int nSyl2;
	property int nSD2;

// 	ClrStockQuote(String^ ticker)
// 		:ClrDataBase(ClrDataType::eStockQuote)
// 	{
// 		Ticker = ticker;
// 	}

	ClrStockQuote(const StockQuote& quote)
			:ClrDataBase(ClrDataType::eStockQuote)
	{
		szWindCode = gcnew String(quote.szWindCode);
		szCode= gcnew String(quote.szCode);
		szCNName = gcnew String(quote.szCNName);
		nActionDay = quote.nActionDay;
		nTradingDay = quote.nTradingDay;
		nTime = quote.nTime;
		nStatus = quote.nStatus;
		nPreClose = quote.nPreClose * BPS;
		nOpen = quote.nOpen * BPS;
		nHigh = quote.nHigh * BPS;
		nLow = quote.nLow * BPS;
		nMatch = quote.nMatch * BPS;

		nAskPrice = gcnew List<double>();
		nAskVol= gcnew List<int>();
		nBidPrice= gcnew List<double>();
		nBidVol= gcnew List<int>();

		for (int i =0; i < 10; i++)
		{
			nAskPrice->Add(quote.nAskPrice[i]  * BPS);
		}

		for (int i = 0; i < 10; i++)
		{
			nAskVol->Add(quote.nAskVol[i]);
		}

		for (int i = 0; i < 10; i++)
		{
			nBidPrice->Add(quote.nBidPrice[i] * BPS);
		}

		for (int i = 0; i < 10; i++)
		{
			nBidVol->Add(quote.nBidVol[i]);
		}

		nNumTrades= quote.nNumTrades;

		iVolume= quote.iVolume;
		iTurnover= quote.iTurnover;
		nTotalBidVol= quote.nTotalBidVol;
		nTotalAskVol= quote.nTotalAskVol;

		nWeightedAvgBidPrice= quote.nWeightedAvgBidPrice * BPS;
		nWeightedAvgAskPrice= quote.nWeightedAvgAskPrice * BPS;

		nIOPV= quote.nIOPV;
		nYieldToMaturity= quote.nYieldToMaturity;

		nHighLimited= quote.nHighLimited * BPS;
		nLowLimited= quote.nLowLimited * BPS;

		nSyl1= quote.nSyl1;
		nSyl2= quote.nSyl2;
		nSD2= quote.nSD2;
	}
};

public ref class ClrFutureQuote : public ClrDataBase
{
public:
	property String^ szWindCode;
	property String^ szCode;
	property String^ szCNName;
	property int nActionDay;
	property int nTradingDay;
	property int nTime;
	property int nStatus;

	property Int64 iPreOpenInterest;

	property double nPreClose;
	property double nPreSettlePrice;
	property double nOpen;
	property double nHigh;
	property double nLow;
	property double nMatch;

	property Int64 iVolume;
	property Int64 iTurnover;
	property Int64 iOpenInterest;

	property double nClose;
	property double nSettlePrice;
	property double nHighLimited;
	property double nLowLimited;
	property double nPreDelta;
	property double nCurrDelta;

	property List<double>^ nAskPrice;
	property List<int>^ nAskVol;
	property List<double>^ nBidPrice;
	property List<int>^ nBidVol;

	property int lAuctionPrice;
	property int lAuctionQty;
	property int lAvgPrice;

// 	ClrFutureQuote(String^ ticker)
// 		:ClrDataBase(ClrDataType::eFutureQuote)
// 	{
// 		Ticker = ticker;
// 	}

	ClrFutureQuote(const FutureQuote& quote)
		:ClrDataBase(ClrDataType::eFutureQuote)
	{
		szWindCode = gcnew String(quote.szWindCode);
		szCode= gcnew String(quote.szCode);
		szCNName = gcnew String(quote.szCNName);
		nActionDay = quote.nActionDay;
		nTradingDay = quote.nTradingDay;
		nTime = quote.nTime;
		nStatus = quote.nStatus;

		iPreOpenInterest = quote.iPreOpenInterest;

		nPreClose = quote.nPreClose * BPS;
		nPreSettlePrice = quote.nPreSettlePrice * BPS;
		nOpen = quote.nOpen * BPS;
		nHigh = quote.nHigh * BPS;
		nLow = quote.nLow * BPS;
		nMatch = quote.nMatch * BPS;

		iVolume = quote.iVolume;
		iTurnover = quote.iTurnover;
		iOpenInterest = quote.iOpenInterest;

		nClose = quote.nClose * BPS;
		nSettlePrice = quote.nSettlePrice * BPS;
		nHighLimited = quote.nHighLimited * BPS;
		nLowLimited = quote.nLowLimited * BPS;
		nPreDelta = quote.nPreDelta;
		nCurrDelta = quote.nCurrDelta;

		nAskPrice = gcnew List<double>();
		nAskVol= gcnew List<int>();
		nBidPrice= gcnew List<double>();
		nBidVol= gcnew List<int>();

		for (int i = 0; i < 5; i++)
		{
			nAskPrice->Add(quote.nAskPrice[i] * BPS);
		}

		for (int i = 0; i < 5; i++)
		{
			nAskVol->Add(quote.nAskVol[i]);
		}

		for (int i = 0; i < 5; i++)
		{
			nBidPrice->Add(quote.nBidPrice[i] * BPS);
		}

		for (int i = 0; i < 5; i++)
		{
			nBidVol->Add(quote.nBidVol[i]);
		}

		lAuctionPrice= quote.lAuctionPrice * BPS;
		lAuctionQty= quote.lAuctionQty;
		lAvgPrice= quote.lAvgPrice * BPS;
	}
};

public ref class ClrIndexQuote : public ClrDataBase
{
public:
	property String^ szWindCode;
	property String^ szCode;
	property String^ szCNName;
	property int nActionDay;
	property int nTradingDay;
	property int nTime;

	property double nOpenIndex;
	property double nHighIndex;
	property double nLowIndex;
	property double nLastIndex;

	property Int64 iTotalVolume;
	property Int64 iTurnover;
	property double nPreCloseIndex;

// 	ClrIndexQuote(String^ ticker)
// 		:ClrDataBase(ClrDataType::eIndexQuote)
// 	{
// 		Ticker = ticker;
// 	}

	ClrIndexQuote(const IndexQuote& index)
		:ClrDataBase(ClrDataType::eIndexQuote)
	{
		szWindCode = gcnew String(index.szWindCode);
		szCode= gcnew String(index.szCode);
		szCNName = gcnew String(index.szCNName);
		nActionDay = index.nActionDay;
		nTradingDay = index.nTradingDay;
		nTime = index.nTime;

		nOpenIndex = index.nOpenIndex * BPS;
		nHighIndex = index.nHighIndex * BPS;
		nLowIndex = index.nLowIndex * BPS;
		nLastIndex = index.nLastIndex * BPS;

		iTotalVolume = index.iTotalVolume;
		iTurnover = index.iTurnover;
		nPreCloseIndex = index.nPreCloseIndex * BPS;
	}
};

public ref class ClrTransaction : public ClrDataBase
{
public:
	property String^    szWindCode; //600001.SH 
	property String^    szCode;     //原始Code
	property String^	szCNName;
	property int     nActionDay;     //自然日
	property int 	nTime;		    //成交时间(HHMMSSmmm)
	property int 	nIndex;		    //成交编号
	property int		nPrice;		    //成交价格
	property int 	nVolume;	    //成交数量
	property int		nTurnover;	    //成交金额
	property int     nBSFlag;        //买卖方向(买：'B', 卖：'A', 不明：' ')
	char    chOrderKind;    //成交类别
	char    chFunctionCode; //成交代码
	property int	    nAskOrder;	    //叫卖方委托序号
	property int	    nBidOrder;	    //叫买方委托序号

	property int		nType;			//证券类型

// 	ClrTransaction(String^ ticker)
// 		:ClrDataBase(ClrDataType::eTransaction)
// 	{
// 		Ticker = ticker;
// 	}

	ClrTransaction(const Transaction& trans)
	:ClrDataBase(ClrDataType::eTransaction)
	{
		szWindCode = gcnew String(trans.szWindCode);
		szCode= gcnew String(trans.szCode);
		szCNName = gcnew String(trans.szCNName);
		nActionDay = trans.nActionDay;
		nTime = trans.nTime;
		nIndex = trans.nIndex;

		nPrice = trans.nPrice;

		nVolume = trans.nVolume;
		nTurnover = trans.nTurnover;
		chOrderKind = trans.chOrderKind;
		chFunctionCode = trans.chFunctionCode;

		nAskOrder = trans.nAskOrder;
		nBidOrder = trans.nBidOrder;
	}
};

public ref class ClrOrder : public ClrDataBase
{
public:
	property String^    szWindCode; //600001.SH 
	property String^    szCode;     //原始Code
	property String^	szCNName;
	property int 	nActionDay;	    //委托日期(YYMMDD)
	property int 	nTime;			//委托时间(HHMMSSmmm)
	property int 	nOrder;	        //委托号
	property int		nPrice;			//委托价格
	property int 	nVolume;		//委托数量
	char    chOrderKind;	//委托类别
	char    chFunctionCode;	//委托代码('B','S','C')

	property int		nType;			//证券类型

// 	ClrOrder(String^ ticker)
// 		:ClrDataBase(ClrDataType::eOrder)
// 	{
// 		Ticker = ticker;
// 	}

	ClrOrder(const Order& order)
:ClrDataBase(ClrDataType::eOrder)
	{
		szWindCode = gcnew String(order.szWindCode);
		szCode= gcnew String(order.szCode);
		szCNName = gcnew String(order.szCNName);
		nActionDay = order.nActionDay;
		nTime = order.nTime;
		nOrder = order.nOrder;

		nPrice = order.nPrice;

		nVolume = order.nVolume;
		chOrderKind = order.chOrderKind;
		chFunctionCode = order.chFunctionCode;
	}
};

public ref class ClrOrderQueue : public ClrDataBase
{
public:
	property String^    szWindCode; //600001.SH 
	property String^    szCode;     //原始Code
	property String^	szCNName;
	property int     nActionDay;     //自然日
	property int 	nTime;			//时间(HHMMSSmmm)
	property int     nSide;			//买卖方向('B':Bid 'A':Ask)
	property int		nPrice;			//委托价格
	property int 	nOrders;		//订单数量
	property int 	nABItems;		//明细个数
	property List<int>^ 	nABVolume;	//订单明细

	property int		 nType;			//证券类型

// 	ClrOrderQueue(String^ ticker)
// 		:ClrDataBase(ClrDataType::eOrderQueue)
// 	{
// 		Ticker = ticker;
// 	}

	ClrOrderQueue(const OrderQueue& orderQueue)
		:ClrDataBase(ClrDataType::eOrderQueue)
	{
		szWindCode = gcnew String(orderQueue.szWindCode);
		szCode= gcnew String(orderQueue.szCode);
		szCNName = gcnew String(orderQueue.szCNName);
		nActionDay = orderQueue.nActionDay;
		nTime = orderQueue.nTime;

		nSide = orderQueue.nSide;

		nPrice = orderQueue.nPrice;

		nOrders = orderQueue.nOrders;
		nABItems = orderQueue.nABItems;
		nABVolume =gcnew List<int>();

		for(int ii=0; ii < 200; ii++)
		{
			nABVolume->Add(orderQueue.nABVolume[ii]);
		}
	}
};

//public ref class ClrCodeTable : public ClrDataBase
//{
//public:
//	property String^    szWindCode; //600001.SH 
//	property String^    szCode;     //原始Code
//	property String^    szMarket;   
//	property String^    szENName;
//	property String^    szCNName;
//	property int		nType;		//证券类型
//
//	// 	ClrOrderQueue(String^ ticker)
//	// 		:ClrDataBase(ClrDataType::eOrderQueue)
//	// 	{
//	// 		Ticker = ticker;
//	// 	}
//
//	ClrCodeTable(const CodeTable& codeTable)
//		:ClrDataBase(ClrDataType::eCodeTable)
//	{
//		szWindCode = gcnew String(codeTable.szWindCode);
//		szCode = gcnew String(codeTable.szCode);
//		szMarket = gcnew String(codeTable.szMarket);
//		szENName = gcnew String(codeTable.szENName);
//		szCNName = gcnew String(codeTable.szCNName);
//		nType = codeTable.nType;
//	}
//};
}