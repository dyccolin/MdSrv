#pragma once
using namespace System;
using namespace System::ComponentModel;
using namespace System::Runtime::CompilerServices;
using namespace System::Collections::Generic;

namespace ClrMdApiLib
{
	public ref class ClrNotifyObject : public INotifyPropertyChanged
	{
	public:
		ClrNotifyObject()
		{

		}

		virtual event PropertyChangedEventHandler^ PropertyChanged;

	protected:
		generic <typename T>
			void UpdateProperty(T properValue, T newValue, String^ properName)
			{
				if (System::Object::Equals(properValue, newValue))
					return;

				properValue = newValue;
				OnPropertyChanged(properName);
			}

			void OnPropertyChanged(String^ propertyName)
			{
				PropertyChanged(this, gcnew PropertyChangedEventArgs(propertyName));
			}
	};
}

