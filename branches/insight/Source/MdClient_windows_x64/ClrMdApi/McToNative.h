#pragma once

#include <string>

namespace ClrMdApiLib
{
	public ref class McToNative
	{
	public:
		static std::string Convert(System::String^ str);
	};
}

