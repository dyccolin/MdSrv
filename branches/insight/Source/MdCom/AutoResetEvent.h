#pragma once
#include "MutexHelper.h"

class AutoResetEvent
{
public:
	AutoResetEvent() : signalled_(false) {
		//pthread_cond_init(&hSignal_, nullptr);
		hSignal_ = PTHREAD_COND_INITIALIZER;
		MUTEX_INIT(&mutex_);
	}

	~AutoResetEvent() {
		MUTEX_DESTROY(&mutex_);
		pthread_cond_destroy(&hSignal_);
	}

	void signal()
	{
		MUTEX_LOCK(&mutex_);
		signalled_ = true;
		pthread_cond_signal(&hSignal_);
		MUTEX_UNLOCK(&mutex_);
	}

	void wait()
	{
		MUTEX_LOCK(&mutex_);
		pthread_cond_broadcast(&hSignal_);

		pthread_cond_wait(&hSignal_, &mutex_);
		MUTEX_UNLOCK(&mutex_);
	}

protected:
	MUTEX_T mutex_;
	pthread_cond_t hSignal_;
	bool signalled_;
};