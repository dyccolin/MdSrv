﻿#include <thread>
#include <map>
#include "MdApi.h"
#include "MdApiData.h"

#ifdef _WIN32
#include <windows.h>
#define SLEEP(x) Sleep(x*1000);
#else
#include <unistd.h>
#define SLEEP(x) sleep(x);
#endif // WIN32

unsigned long long GetCurrentTimeMsec()
{
#ifdef _WIN32
	struct timeval tv;
	time_t clock;
	struct tm tm;
	SYSTEMTIME wtm;

	GetLocalTime(&wtm);
	tm.tm_year = wtm.wYear - 1900;
	tm.tm_mon = wtm.wMonth - 1;
	tm.tm_mday = wtm.wDay;
	tm.tm_hour = wtm.wHour;
	tm.tm_min = wtm.wMinute;
	tm.tm_sec = wtm.wSecond;
	tm.tm_isdst = -1;
	clock = mktime(&tm);
	tv.tv_sec = clock;
	tv.tv_usec = wtm.wMilliseconds * 1000;
	return ((unsigned long long)tv.tv_sec * 1000 + (unsigned long long)tv.tv_usec / 1000);
#else
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return ((unsigned long long)tv.tv_sec * 1000 + (unsigned long long)tv.tv_usec / 1000);
#endif
}

using namespace MdApi;
using namespace std;

long long last_localtime = 0;

void CALLBACK OnStockQuoteCallback(StockQuote* data)
{
	long long curr_localtime = GetCurrentTimeMsec();
	long long diff = curr_localtime - last_localtime;
	
	//if (strcmp(data->szWindCode, "600000.SH") == 0) 
	{
		printf("OnStockQuoteCallback:%s %s %.02f %.02f nTime:%d\n", data->szWindCode, data->szCNName, data->nAskPrice[0] / 10000.0, data->nBidPrice[0] / 10000.0, data->nTime);
		//printf("%I64d OnStockQuoteCallback:%s %s %.02f %.02f nTime:%d\n", curr_localtime, data->szWindCode, data->szCNName, data->nAskPrice[0] / 10000.0, data->nBidPrice[0] / 10000.0, data->nTime);
		//printf("diff:%I64d\n", diff);
		last_localtime = curr_localtime;
	}
}

void CALLBACK OnFutureQuoteCallback(FutureQuote* data)
{
	printf("OnFutureQuoteCallback:%s %s %.02f %.02f nTime:%d\n", data->szWindCode, data->szCNName, data->nAskPrice[0] / 10000.0, data->nBidPrice[0] / 10000.0, data->nTime);
}

void CALLBACK OnIndexQuoteCallback(IndexQuote* data)
{
	printf("OnIndexQuoteCallback:%s %s nTime:%d\n", data->szWindCode, data->szCNName, data->nTime);
}

void CALLBACK OnTransactionCallback(Transaction* data)
{
	printf("OnTransactionCallback:%s %s nTime:%d\n", data->szWindCode, data->szCNName, data->nTime);
}

void CALLBACK OnOrderQueueCallback(OrderQueue* data)
{
	printf("OnOrderQueueCallback:%s %s nTime:%d\n", data->szWindCode, data->szCNName, data->nTime);
}

void CALLBACK OnOrderCallback(Order* data)
{
	printf("OnOrderCallback:%s %s nTime:%d\n", data->szWindCode, data->szCNName, data->nTime);
}

//void CALLBACK OnCodeTableCallback(CodeTable* data)
//{
//	printf("OnCodeTableCallback:%s %s nType:%d\n", data->szWindCode, data->szCNName, data->nType);
//	//...
//
//	codetableMap[data->szWindCode] = data->szCNName;
//}

int main()
{
    printf("hello from MdClient!\n");

	HzMdSrvApi_Connect();

	// ##################################################
	//         回调展示推送获取行情方式
	// ##################################################

	// 股票行情回调
	SetStockQuoteCallback(OnStockQuoteCallback);
	// 指数行情回调
	SetIndexQuoteCallback(OnIndexQuoteCallback);
	// 期货行情回调
	SetFutureQuoteCallback(OnFutureQuoteCallback);
	// 逐笔成交回调
	SetTransactionCallback(OnTransactionCallback);
	// 委托队列回调
	SetOrderQueueCallback(OnOrderQueueCallback);
	// 逐笔委托回调
	SetOrderCallback(OnOrderCallback);

	// 订阅才有数据传输，先设置回调，防止有数据丢失
	bool bSubAll = false;
	if (bSubAll)
	{
		// 最新接口不支持传空全订阅
		for (int i = 1; i < 700000; i++)
		{
			char ticker[32];
			sprintf(ticker, "%06d.%s", i, i > 500000 ? "SH" : "SZ");
			HzMdSrvApi_Subscribe(ticker);
		}
	}
	else
	{
		//HzMdSrvApi_Subscribe("603103.SH");

		//SLEEP(10);

		//HzMdSrvApi_Subscribe("IO2002-C-3600.CF");
		//HzMdSrvApi_Subscribe("IO2002-P-3600.CF");
		//HzMdSrvApi_Subscribe("rb1810.SHF");
		//HzMdSrvApi_Subscribe("ZC809.CZC");
		//HzMdSrvApi_Subscribe("002127.sz");
		//HzMdSrvApi_Subscribe("600535.SH");
		//HzMdSrvApi_Subscribe("600521.SH");
		//HzMdSrvApi_Subscribe("101315.SZ");
		//HzMdSrvApi_Subscribe("v1809.DCE");
		//HzMdSrvApi_Subscribe("ZC709.CZC");
		//HzMdSrvApi_Subscribe("399001.SZ");
		//HzMdSrvApi_Subscribe("399002.SZ");
		//HzMdSrvApi_Subscribe("399300.SZ");
		//HzMdSrvApi_Subscribe("999987.SH");
		//HzMdSrvApi_Subscribe("990905.SH");
		HzMdSrvApi_Subscribe("600000.SH");
		//HzMdSrvApi_Subscribe("601208.SH");
		//HzMdSrvApi_Subscribe("IF2001.CF");
		//HzMdSrvApi_Subscribe("IC1909.CF");
		//HzMdSrvApi_Subscribe("IH1908.CF");

		// 从缓存列表获取最新行情

	}

	SLEEP(10);

	HzMdSrvApi_UnSubscribe("399001.SZ");
	HzMdSrvApi_UnSubscribe("600535.SH");
	//HzMdSrvApi_UnSubscribe("600521.SH");
	//printf("HzMdSrvApi_UnSubscribe");
	SLEEP(10000000);
	// ##################################################
	//         该线程展示主动获取行情方式（已废弃）
	// ##################################################
	//std::thread t1(&thread_Task);
	//t1.join();

	HzMdSrvApi_Dispose();

	return 0;
}