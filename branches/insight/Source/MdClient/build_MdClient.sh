#build MdClient release
export PATH=/usr/local/gcc-4.8.5/bin:$PATH

#编译
g++ -c -x c++ Client_Test.cpp -I . -std=c++11
g++ -o MdClient.out -L. -Wl,-z,relro -Wl,-z,now -Wl,-z,noexecstack Client_Test.o -lzmq -lmsgpackc -llog4cxx -l"apr-1" -l"aprutil-1" -lexpat -lMdCom -lMdApi