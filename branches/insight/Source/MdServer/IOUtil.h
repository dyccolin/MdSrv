#pragma once

#include <string>
using namespace std;

#ifdef _WIN32
#include <objbase.h>
#include <windows.h>
#include <wincon.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <Nb30.h>
#include <winsock2.h>
#pragma comment(lib, "netapi32.lib")
#pragma comment(lib,"ws2_32.lib")
#else
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/socket.h>  
#include <net/if.h>
#include <sys/ioctl.h> 
#include <linux/hdreg.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <iconv.h>
#endif


class IOUtil
{
public:

	static std::string NewGuid(void)
	{
		char buf[64] = { 0 };
#ifdef _WIN32
		GUID guid;
		if (S_OK == ::CoCreateGuid(&guid))
		{
			_snprintf_s(buf, sizeof(buf)
				, "{%08X-%04X-%04x-%02X%02X-%02X%02X%02X%02X%02X%02X}"
				, guid.Data1
				, guid.Data2
				, guid.Data3
				, guid.Data4[0], guid.Data4[1]
				, guid.Data4[2], guid.Data4[3], guid.Data4[4], guid.Data4[5]
				, guid.Data4[6], guid.Data4[7]
			);
		}
#else
		struct timespec tp;
		clock_gettime(CLOCK_REALTIME, &tp);
		srand(tp.tv_sec * 1000000000 + tp.tv_nsec);

		const char *c = "89ab";
		char *p = buf;
		int n;
		for (n = 0; n < 16; ++n)
		{
			int b = rand() % 255;
			switch (n)
			{
			case 6:
				sprintf(p, "4%x", b % 15);
				break;
			case 8:
				sprintf(p, "%c%x", c[rand() % strlen(c)], b % 15);
				break;
			default:
				sprintf(p, "%02x", b);
				break;
			}

			p += 2;
			//switch (n)
			//{
			//case 3:
			//case 5:
			//case 7:
			//case 9:
			//	*p++ = '-';
			//	break;
			//}
		}
		*p = 0;
#endif
		return std::string(buf);
	}

	typedef std::basic_string<char>::size_type S_T;
	static void split(const std::string& src, const std::string& separator, std::vector<std::string>& dest)
	{
		S_T deli_len = separator.size();
		long npos = -1;
		long index = npos, last_search_position = 0;
		while ((index = src.find(separator, last_search_position)) != npos)
		{
			if (index == last_search_position)
				dest.push_back("");
			else
				dest.push_back(src.substr(last_search_position, index - last_search_position));

			last_search_position = index + deli_len;
		}
		std::string last_one = src.substr(last_search_position);
#ifdef _WIN32
		last_one = last_one.substr(0, last_one.length() - strlen("\r\n"));
#else
		last_one = last_one.substr(0, last_one.length() - strlen("\n"));
#endif // _WIN32
		dest.push_back(last_one);
	}

	static bool endWith(const string &str, const string &tail) {
		return str.compare(str.size() - tail.size(), tail.size(), tail) == 0;
	}

	static bool startWith(const string &str, const string &head) {
		return str.compare(0, head.size(), head) == 0;
	}

	void byte2Hex(unsigned char bData, unsigned char hex[])
	{
		int high = bData / 16, low = bData % 16;
		hex[0] = (high <10) ? ('0' + high) : ('A' + high - 10);
		hex[1] = (low <10) ? ('0' + low) : ('A' + low - 10);
	}

	static std::string GetLocalMac()
	{
		char mac_addr[64];
#ifdef _WIN32

		NCB ncb;
		typedef struct _ASTAT_
		{
			ADAPTER_STATUS   adapt;
			NAME_BUFFER   NameBuff[30];
		}ASTAT, *PASTAT;

		ASTAT Adapter;

		typedef struct _LANA_ENUM
		{
			UCHAR   length;
			UCHAR   lana[MAX_LANA];
		}LANA_ENUM;

		LANA_ENUM lana_enum;
		UCHAR uRetCode;
		memset(&ncb, 0, sizeof(ncb));
		memset(&lana_enum, 0, sizeof(lana_enum));
		ncb.ncb_command = NCBENUM;
		ncb.ncb_buffer = (unsigned char *)&lana_enum;
		ncb.ncb_length = sizeof(LANA_ENUM);
		uRetCode = Netbios(&ncb);

		if (uRetCode != NRC_GOODRET)
			return "";

		for (int lana = 0; lana<lana_enum.length; lana++)
		{
			ncb.ncb_command = NCBRESET;
			ncb.ncb_lana_num = lana_enum.lana[lana];
			uRetCode = Netbios(&ncb);
			if (uRetCode == NRC_GOODRET)
				break;
		}

		if (uRetCode != NRC_GOODRET)
			return "";

		memset(&ncb, 0, sizeof(ncb));
		ncb.ncb_command = NCBASTAT;
		ncb.ncb_lana_num = lana_enum.lana[0];
		strcpy((char*)ncb.ncb_callname, "*");
		ncb.ncb_buffer = (unsigned char *)&Adapter;
		ncb.ncb_length = sizeof(Adapter);
		uRetCode = Netbios(&ncb);

		if (uRetCode != NRC_GOODRET)
			return "";

		sprintf(mac_addr, "%02X-%02X-%02X-%02X-%02X-%02X",
			Adapter.adapt.adapter_address[0],
			Adapter.adapt.adapter_address[1],
			Adapter.adapt.adapter_address[2],
			Adapter.adapt.adapter_address[3],
			Adapter.adapt.adapter_address[4],
			Adapter.adapt.adapter_address[5]);
#else
		int sock_mac;

		struct ifreq ifr_mac;


		sock_mac = socket(AF_INET, SOCK_STREAM, 0);
		if (sock_mac == -1)
		{
			perror("create socket falise...mac\n");
			return "";
		}
		int i = 0;
		while (i < 5)
		{
			memset(&ifr_mac, 0, sizeof(ifr_mac));
			char eth[32] = { 0 };
			sprintf(eth, "eth%d", i);
			strncpy(ifr_mac.ifr_name, eth, sizeof(ifr_mac.ifr_name) - 1);

			if ((ioctl(sock_mac, SIOCGIFHWADDR, &ifr_mac)) < 0)
			{
				printf("%s mac ioctl error, retrying(%d)...\n", eth, ++i);
				continue;
			}
			else
			{
				break;
			}
		}

		sprintf(mac_addr, "%02x%02x%02x%02x%02x%02x",
			(unsigned char)ifr_mac.ifr_hwaddr.sa_data[0],
			(unsigned char)ifr_mac.ifr_hwaddr.sa_data[1],
			(unsigned char)ifr_mac.ifr_hwaddr.sa_data[2],
			(unsigned char)ifr_mac.ifr_hwaddr.sa_data[3],
			(unsigned char)ifr_mac.ifr_hwaddr.sa_data[4],
			(unsigned char)ifr_mac.ifr_hwaddr.sa_data[5]);
		close(sock_mac);
#endif
		
		return std::string(mac_addr);
	}

	static std::string GetLocalIP()
	{

		char pszIPBuf[64];

#ifdef _WIN32
		WSADATA wsaData;
		char name[255];
		PHOSTENT hostinfo;

		if (WSAStartup(MAKEWORD(2,0), &wsaData) == 0) {
			if (gethostname(name, sizeof(name)) == 0) {
				if ((hostinfo = gethostbyname(name)) != NULL) {
					sprintf(pszIPBuf, "%s", inet_ntoa(*(struct in_addr *)*hostinfo->h_addr_list));
				}
			}
			WSACleanup();
		}
#else
#define MAXINTERFACES 16   

		int fd = -1;
		int intrface = 0;
		struct ifreq buf[MAXINTERFACES];
		struct ifconf ifc;

		if ((fd = socket(AF_INET, SOCK_DGRAM, 0)) >= 0)
		{
			ifc.ifc_len = sizeof buf;
			ifc.ifc_buf = (caddr_t)buf;
			if (!ioctl(fd, SIOCGIFCONF, (char *)&ifc))
			{
				intrface = ifc.ifc_len / sizeof(struct ifreq); 
				while (intrface-- > 0)
				{
					if (strstr(buf[intrface].ifr_name, "eth") != NULL)
					{ 
						if (!(ioctl(fd, SIOCGIFADDR, (char *)&buf[intrface])))
						{
							if (inet_ntop(AF_INET,
								&(((struct sockaddr_in*)(&(buf[intrface].ifr_addr)))->sin_addr),
								pszIPBuf, 16))
							{
								// succeeded
							}
						}
					}
				}
			}

			close(fd);
		}
#endif

		return std::string(pszIPBuf);
	}

	static std::string GetHddsn()
	{

		char pszHddsnBuf[32];

#ifdef _WIN32
		// Luo support windows???
#else
#define MAX_SIZE 32
		int fd;
		struct hd_driveid hid;
		FILE *fp;
		char line[0x100], *disk, *root, *p;

		fp = fopen("/etc/mtab", "r");
		if (fp == NULL)
		{
			fprintf(stderr, "No /etc/mtab file./n");
			return "";
		}

		fd = -1;
		while (fgets(line, sizeof line, fp) != NULL)
		{
			disk = strtok(line, " ");
			if (disk == NULL)
			{
				continue;
			}

			root = strtok(NULL, " ");
			if (root == NULL)
			{
				continue;
			}

			if (strcmp(root, "/") == 0)
			{
				for (p = disk + strlen(disk) - 1; isdigit(*p); p--)
				{
					*p = '/0';
				}
				fd = open(disk, O_RDONLY);
				break;
			}
		}

		fclose(fp);

		if (fd < 0)
		{
			fprintf(stderr, "open hard disk device failed./n");
			return "";
		}

		if (ioctl(fd, HDIO_GET_IDENTITY, &hid) < 0)
		{
			fprintf(stderr, "ioctl error./n");
			return "";
		}

		close(fd);

		snprintf(pszHddsnBuf, MAX_SIZE, "%s", hid.serial_no);
#endif

		return std::string(pszHddsnBuf);
	}

	static bool IsFileExist(const char * file_path)
	{
#ifdef _WIN32
		// Luo support windows???
		return true;
#else
		if (file_path == nullptr)
			return false;
		if (access(file_path, F_OK) == 0)
			return true;
		return false;
#endif
	}

	static bool IsDirExist(const char * dir_path)
	{
#ifdef _WIN32
		// Luo support windows???
		return true;
#else

		if (dir_path == nullptr)
			return false;
		if (opendir(dir_path) == nullptr)
			return false;
		return true;
#endif
	}


#ifdef _WIN32
		
#else
		static int MakePath(std::string s, mode_t mode = 0755)
		{
			size_t pre = 0, pos;
			std::string dir;
			int mdret;

			if (s[s.size() - 1] != '/') {
				// force trailing / so we can handle everything in loop  
				s += '/';
			}

			while ((pos = s.find_first_of('/', pre)) != std::string::npos) {
				dir = s.substr(0, pos++);
				pre = pos;
				if (dir.size() == 0) continue; // if leading / first time is 0 length  
				if ((mdret = mkdir(dir.c_str(), mode)) && errno != EEXIST) {
					return mdret;
				}
			}
			return mdret;
		}

		static int code_convert(char *from_charset, char *to_charset, char *inbuf, size_t* inlen, char *outbuf, size_t* outlen)
		{
			iconv_t cd;
			int rc;
			char **pin = &inbuf;
			char **pout = &outbuf;

			cd = iconv_open(to_charset, from_charset);
			if (cd == 0)
				return -1;
			memset(outbuf, 0, *outlen);
			if (iconv(cd, pin, (size_t*)inlen, pout, (size_t*)outlen) == -1)
				return -1;
			iconv_close(cd);
			return 0;
		}
#endif
};

