#pragma once
#include <iostream>  
#include <fstream> 

#include "IniFile.h"

using namespace std;

class CServerIniFile
{
public:
	char IP[32];
	char Port[8];
	char User[32];
	char Password[32];

	char cert_folder[100];
	char export_folder[100];
	bool export_csv;

	char Log4cxxProperties[200];
	bool bLoginCTP;
	bool bLoginInsight;

public:

	CServerIniFile()
	{
		Read();
	}

	~CServerIniFile()
	{
	}

private:
	void Read()
	{
		string configFileName("Server.ini");
		IniFile ini(configFileName);
		// ���
		ini.read_profile_string("insight", "IP", IP, sizeof(IP), nullptr, configFileName.c_str());
		ini.read_profile_string("insight", "Port", Port, sizeof(Port), nullptr, configFileName.c_str());
		ini.read_profile_string("insight", "User", User, sizeof(User), nullptr, configFileName.c_str());
		ini.read_profile_string("insight", "Password", Password, sizeof(Password), nullptr, configFileName.c_str());

		ini.read_profile_string("insight", "cert_folder", cert_folder, sizeof(cert_folder), nullptr, configFileName.c_str());
		ini.read_profile_string("insight", "export_folder", export_folder, sizeof(export_folder), nullptr, configFileName.c_str());
		
		char bExportCsv[8];
		ini.read_profile_string("insight", "export_csv", bExportCsv, sizeof(bExportCsv), nullptr, configFileName.c_str());
		export_csv = _stricmp("true", bExportCsv) == 0;

		// loginCTP
		char loginCTP[8];
		ini.read_profile_string("ctp", "bLogin", loginCTP, sizeof(loginCTP), nullptr, configFileName.c_str());
		bLoginCTP = _stricmp("true", loginCTP) == 0;

		char loginInsight[8];
		ini.read_profile_string("insight", "bLogin", loginInsight, sizeof(loginInsight), nullptr, configFileName.c_str());
		bLoginInsight = _stricmp("true", loginInsight) == 0;

		// Log4cxxProperties����
		ini.read_profile_string("log4cxx", "log4cxx_properties", Log4cxxProperties, sizeof(Log4cxxProperties), nullptr, configFileName.c_str());
	}
};

