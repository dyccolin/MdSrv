#include "base_define.h"
#include "mdc_client_factory.h"
#include "client_interface.h"
#include "message_handle.h"


#include "CTPTrade.h"

#include "MdData.h"
#include "Common.h"

#include "Publisher.h"
#include "ServerIniFile.h"
#include "LicenseHelper.h"

#include <wchar.h>

#include <string>
#include <cassert>
#include <iostream>
#include <memory>
#include <algorithm>
#include <sstream>
#include <fstream>
#include <chrono>
#include <ctime>
#include  <stdlib.h>

#if defined(WIN32) || defined(_WIN32)
#include <windows.h> 
#include <direct.h>
#include <sys/types.h>
#include <sys/stat.h>
#else
#include <sys/stat.h> 
#include <unistd.h> 
#endif

#ifdef _WIN32
#define snprintf _snprintf
#include <windows.h>
#define SLEEP(x) Sleep(x*1000);
#else
#include <unistd.h>
#define SLEEP(x) sleep(x);
#endif // WIN32

#include <ctime>
#include  <stdlib.h>

using namespace com::htsc::mdc::gateway;
using namespace com::htsc::mdc::model;
using namespace com::htsc::mdc::insight::model;

static log4cxx::LoggerPtr logger = log4cxx::Logger::getLogger("syslog");

static int counter = 0;

std::map<std::string, std::string> vCodeTables;
std::map<std::string, std::string> mapWindCode2Index;
std::map<std::string, std::string> mapIndex2WindCode;
// ticker:such as 600000.SH;000002.SZ
int subscribe(const char* ticker);
int unsubscribe(const char* ticker);

std::string windCode2Index(std::string str)
{
	if (mapWindCode2Index.find(str) != mapWindCode2Index.end())
		return mapWindCode2Index[str];
	else
		return "00" + str.substr(2);
}

std::string index2WindCode(std::string str)
{
	if (mapIndex2WindCode.find(str) != mapIndex2WindCode.end())
		return mapIndex2WindCode[str];
	else
		return "99" + str.substr(2);
}

//GBK转化为UTF8格式
void ConvertGBKToUtf8(std::string &strGBK)
{
	int len = MultiByteToWideChar(CP_ACP, 0, strGBK.c_str(), -1, NULL, 0);
	wchar_t * wszUtf8 = new wchar_t[len];
	memset(wszUtf8, 0, len);
	MultiByteToWideChar(CP_ACP, 0, strGBK.c_str(), -1, wszUtf8, len);

	len = WideCharToMultiByte(CP_UTF8, 0, wszUtf8, -1, NULL, 0, NULL, NULL);
	char *szUtf8 = new char[len + 1];
	memset(szUtf8, 0, len + 1);
	WideCharToMultiByte(CP_UTF8, 0, wszUtf8, -1, szUtf8, len, NULL, NULL);

	strGBK = szUtf8;
	delete[] szUtf8;
	delete[] wszUtf8;
}

//处理类
class NewHandle : public MessageHandle {
public:
	NewHandle() :service_value_(0), reconnect_(false), login_success_(false), reconnect_count_(0), no_connections_(false) {}
	virtual ~NewHandle() {}

public:
	/**
	* 处理MDSecurityRecord
	* @param[in] data
	*/
	void OnMarketData(const com::htsc::mdc::insight::model::MarketData& data) {
		char buff[1024];
		if (++counter % 1000 == 0)
		{
			counter = 0; // reset counter
			LOG4CXX_INFO(logger, "insight md receiving...");
			
			sprintf(buff, "==========> NewHandle: process a market data:%d, has_mdstock:%d", data.marketdatatype(), data.has_mdstock());
			LOG4CXX_INFO(logger, buff);
		}

		//debug_print("==========> NewHandle: process a market data:%d, has_mdstock:%d", data.marketdatatype(), data.has_mdstock());
		switch (data.marketdatatype()) {
		case MD_TICK: {
			if (data.has_mdstock()) {
				//data.mdstock().PrintDebugString();

				const ::com::htsc::mdc::insight::model::MDStock& stock = data.mdstock();

				std::shared_ptr<StockQuote> quote(new StockQuote());
				quote->szWindCode = stock.htscsecurityid();
				quote->szCode = quote->szWindCode;

				auto iter_codetable = vCodeTables.find(quote->szWindCode);
				if (iter_codetable != vCodeTables.end()) {
					quote->szCNName = iter_codetable->second;
				}

				quote->nActionDay = stock.mddate();
				quote->nTradingDay = stock.mddate();
				quote->nTime = stock.mdtime();
				quote->nStatus = atoi(stock.tradingphasecode().c_str());

				quote->nPreClose = stock.preclosepx();
				quote->nOpen = stock.openpx();
				quote->nHigh = stock.highpx();
				quote->nLow = stock.lowpx();
				quote->nMatch = stock.lastpx();

				int size = stock.sellpricequeue().size();
				for (int i = 0; i < size; i++)
				{
					quote->nAskPrice.push_back(stock.sellpricequeue()[i]);
				}
			
				size = stock.sellorderqtyqueue().size();
				for (int i = 0; i < size; i++)
				{
					quote->nAskVol.push_back(stock.sellorderqtyqueue()[i]);
				}

				size = stock.buypricequeue().size();
				for (int i = 0; i < size; i++)
				{
					quote->nBidPrice.push_back(stock.buypricequeue()[i]);
				}

				size = stock.buyorderqtyqueue().size();
				for (int i = 0; i < size; i++)
				{
					quote->nBidVol.push_back(stock.buyorderqtyqueue()[i]);
				}

				quote->nNumTrades = stock.numtrades();
				quote->iVolume = stock.totalvolumetrade();
				quote->iTurnover = stock.totalvaluetrade();
				quote->nTotalBidVol = stock.totalbuyqty();
				quote->nTotalAskVol = stock.totalsellqty();

				quote->nWeightedAvgBidPrice = stock.weightedavgbuypx();
				quote->nWeightedAvgAskPrice = stock.weightedavgsellpx();
				quote->nIOPV = 0;
				quote->nYieldToMaturity = 0;
				quote->nHighLimited = stock.maxpx();
				quote->nLowLimited = stock.minpx();
				quote->chPrefix = "";

				quote->nSyl1 = 0;
				quote->nSyl2 = 0;
				quote->nSD2 = 0;

				//char buff[256];
				//sprintf(buff, "%s %d %d %d %s", stock.htscsecurityid().c_str(), stock.mddate(), stock.mdtime(), stock.lastpx(), stock.tradingphasecode().c_str());
				//LOG4CXX_INFO(logger, buff);

				Publisher::Instance()->Add(quote);

				//MUTEX_LOCK(&m_mtxStockQuote);
				//if (lastStockQuoteMap[quote->szWindCode] == nullptr || lastStockQuoteMap[quote->szWindCode]->different(*quote.get()))
				//{
				//	Publisher::Instance()->Add(quote);
				//}
				//else
				//{
				//	sprintf(buff, "Ignore equals quote:%s", quote->szWindCode.c_str());
				//	LOG4CXX_WARN(logger, buff);
				//}

				//lastStockQuoteMap[quote->szWindCode] = quote;
				//MUTEX_UNLOCK(&m_mtxStockQuote);
			}
			else if (data.has_mdfuture()) {
				//data.mdfuture().PrintDebugString();

				const ::com::htsc::mdc::insight::model::MDFuture& future = data.mdfuture();

				std::shared_ptr<FutureQuote> quote(new FutureQuote());
				quote->szWindCode = future.htscsecurityid();
				quote->szCode = quote->szWindCode;

				auto iter_codetable = vCodeTables.find(quote->szWindCode);
				if (iter_codetable != vCodeTables.end()) {
					quote->szCNName = iter_codetable->second;
				}

				quote->nActionDay = future.mddate();
				quote->nTradingDay = future.mddate();
				quote->nTime = future.mdtime();
				quote->nStatus = atoi(future.tradingphasecode().c_str());
				quote->iPreOpenInterest = future.preopeninterest();
				quote->nPreClose = future.preclosepx();
				quote->nPreSettlePrice = future.presettleprice();
				quote->nOpen = future.openpx();
				quote->nHigh = future.highpx();
				quote->nLow = future.lowpx();
				quote->nMatch = future.lastpx();

				quote->iVolume = future.totalvolumetrade();
				quote->iTurnover = future.totalvaluetrade();
				quote->iOpenInterest = future.openinterest();
				quote->nClose = future.closepx();
				quote->nSettlePrice = future.settleprice();
				quote->nHighLimited = future.maxpx();
				quote->nLowLimited = future.minpx();
				quote->nPreDelta = future.predelta();
				quote->nCurrDelta = future.currdelta();

				int size = future.sellpricequeue().size();
				for (int i = 0; i < size; i++)
				{
					quote->nAskPrice.push_back(future.sellpricequeue()[i]);
				}

				size = future.sellorderqtyqueue().size();
				for (int i = 0; i < size; i++)
				{
					quote->nAskVol.push_back(future.sellorderqtyqueue()[i]);
				}

				size = future.buypricequeue().size();
				for (int i = 0; i < size; i++)
				{
					quote->nBidPrice.push_back(future.buypricequeue()[i]);
				}

				size = future.buyorderqtyqueue().size();
				for (int i = 0; i < size; i++)
				{
					quote->nBidVol.push_back(future.buyorderqtyqueue()[i]);
				}

				quote->lAuctionPrice = 0;
				quote->lAuctionQty = 0;
				quote->lAvgPrice = 0;

				//char buff[256];
				//sprintf(buff, "%s %d %d %d %s", future.htscsecurityid().c_str(), future.mddate(), future.mdtime(), future.lastpx(), future.tradingphasecode().c_str());
				//LOG4CXX_INFO(logger, buff);

				Publisher::Instance()->Add(quote);

				//MUTEX_LOCK(&m_mtxFutureQuote);
				//if (lastFutureQuoteMap[quote->szWindCode] == nullptr || lastFutureQuoteMap[quote->szWindCode]->different(*quote.get()))
				//{
				//	Publisher::Instance()->Add(quote);
				//}
				//else
				//{
				//	sprintf(buff, "Ignore equals quote:%s", quote->szWindCode.c_str());
				//	LOG4CXX_WARN(logger, buff);
				//}

				//lastFutureQuoteMap[quote->szWindCode] = quote;
				//MUTEX_UNLOCK(&m_mtxFutureQuote);
			}
			else if (data.has_mdindex()) {
				//data.mdindex().PrintDebugString();

				const ::com::htsc::mdc::insight::model::MDIndex& index = data.mdindex();

				std::shared_ptr<IndexQuote> quote(new IndexQuote());

				// 上证指数特殊处理
				if (IOUtil::startWith(index.htscsecurityid(), "00"))
					quote->szWindCode = index2WindCode(index.htscsecurityid());
				else
					quote->szWindCode = index.htscsecurityid();
				quote->szCode = quote->szWindCode;

				auto iter_codetable = vCodeTables.find(quote->szWindCode);
				if (iter_codetable != vCodeTables.end()) {
					quote->szCNName = iter_codetable->second;
				}

				quote->nActionDay = index.mddate();
				quote->nTradingDay = index.mddate();
				quote->nTime = index.mdtime();

				quote->nOpenIndex = index.openpx();
				quote->nHighIndex = index.highpx();
				quote->nLowIndex = index.lowpx();
				quote->nLastIndex = index.lastpx();
				quote->iTotalVolume = index.totalvolumetrade();
				quote->iTurnover = index.totalvaluetrade();
				quote->nPreCloseIndex = index.preclosepx();

				//char buff[256];
				//sprintf(buff, "%s %d %d %d %s", index.htscsecurityid().c_str(), index.mddate(), index.mdtime(), index.lastpx(), index.tradingphasecode().c_str());
				//LOG4CXX_INFO(logger, buff);

				Publisher::Instance()->Add(quote);

				//MUTEX_LOCK(&m_mtxIndexQuote);
				//if (lastIndexQuoteMap[quote->szWindCode] == nullptr || lastIndexQuoteMap[quote->szWindCode]->different(*quote.get()))
				//{
				//	Publisher::Instance()->Add(quote);
				//}
				//else
				//{
				//	sprintf(buff, "Ignore equals quote:%s", quote->szWindCode.c_str());
				//	LOG4CXX_WARN(logger, buff);
				//}

				//lastIndexQuoteMap[quote->szWindCode] = quote;
				//MUTEX_UNLOCK(&m_mtxIndexQuote);
			}
			//else if (data.has_mdfund()) {
			//	//data.mdfund().PrintDebugString();

			//	const ::com::htsc::mdc::insight::model::MDFund& fund = data.mdfund();

			//	std::shared_ptr<StockQuote> quote(new StockQuote());
			//	quote->szWindCode = fund.htscsecurityid();
			//	quote->szCode = quote->szWindCode;

			//	auto iter_codetable = vCodeTables.find(quote->szWindCode);
			//	if (iter_codetable != vCodeTables.end()) {
			//		quote->szCNName = iter_codetable->second;
			//	}

			//	quote->nActionDay = fund.mddate();
			//	quote->nTradingDay = fund.mddate();
			//	quote->nTime = fund.mdtime();
			//	quote->nStatus = atoi(fund.tradingphasecode().c_str());

			//	quote->nPreClose = fund.preclosepx();
			//	quote->nOpen = fund.openpx();
			//	quote->nHigh = fund.highpx();
			//	quote->nLow = fund.lowpx();
			//	quote->nMatch = fund.lastpx();

			//	int size = fund.sellpricequeue().size();
			//	for (int i = 0; i < size; i++)
			//	{
			//		quote->nAskPrice.push_back(fund.sellpricequeue()[i]);
			//	}

			//	size = fund.sellorderqtyqueue().size();
			//	for (int i = 0; i < size; i++)
			//	{
			//		quote->nAskVol.push_back(fund.sellorderqtyqueue()[i]);
			//	}

			//	size = fund.buypricequeue().size();
			//	for (int i = 0; i < size; i++)
			//	{
			//		quote->nBidPrice.push_back(fund.buypricequeue()[i]);
			//	}

			//	size = fund.buyorderqtyqueue().size();
			//	for (int i = 0; i < size; i++)
			//	{
			//		quote->nBidVol.push_back(fund.buyorderqtyqueue()[i]);
			//	}

			//	quote->nNumTrades = fund.numtrades();
			//	quote->iVolume = fund.totalvolumetrade();
			//	quote->iTurnover = fund.totalvaluetrade();
			//	quote->nTotalBidVol = fund.totalbuyqty();
			//	quote->nTotalAskVol = fund.totalsellqty();

			//	quote->nWeightedAvgBidPrice = fund.weightedavgbuypx();
			//	quote->nWeightedAvgAskPrice = fund.weightedavgsellpx();
			//	quote->nIOPV = 0;
			//	quote->nYieldToMaturity = 0;
			//	quote->nHighLimited = fund.maxpx();
			//	quote->nLowLimited = fund.minpx();
			//	quote->chPrefix = "";

			//	quote->nSyl1 = 0;
			//	quote->nSyl2 = 0;
			//	quote->nSD2 = 0;

			//	//char buff[256];
			//	//sprintf(buff, "%s %d %d %d %s", fund.htscsecurityid().c_str(), fund.mddate(), fund.mdtime(), fund.lastpx(), fund.tradingphasecode().c_str());
			//	//LOG4CXX_INFO(logger, buff);

			//	auto iter = lastStockQuoteMap.find(quote->szWindCode);
			//	if (iter == lastStockQuoteMap.end() || iter->second == nullptr || iter->second->different(*quote.get()))
			//	{
			//		Publisher::Instance()->Add(quote);
			//	}
			//	else
			//	{
			//		sprintf(buff, "Ignore equals quote:%s", quote->szWindCode.c_str());
			//		LOG4CXX_WARN(logger, buff);
			//	}

			//	if (iter == lastStockQuoteMap.end())
			//		lastStockQuoteMap.insert(std::make_pair(quote->szWindCode, quote));
			//	else
			//		iter->second = quote;
			//}
			break;
		}
		case MD_TRANSACTION: {
			if (data.has_mdtransaction()) {
				//data.mdtransaction().PrintDebugString();

				const ::com::htsc::mdc::insight::model::MDTransaction& transaction = data.mdtransaction();

				std::shared_ptr<Transaction> quote(new Transaction());
				quote->szWindCode = transaction.htscsecurityid();
				quote->szCode = quote->szWindCode;

				auto iter_codetable = vCodeTables.find(quote->szWindCode);
				if (iter_codetable != vCodeTables.end()) {
					quote->szCNName = iter_codetable->second;
				}

				quote->nActionDay = transaction.mddate();
				quote->nTime = transaction.mdtime();

				quote->nIndex = transaction.tradeindex();

				quote->nPrice = transaction.tradeprice();
				quote->nVolume = transaction.tradeqty();
				quote->nTurnover = transaction.trademoney();

				quote->nBSFlag = transaction.tradebsflag();
				quote->chOrderKind = transaction.tradetype();
				quote->chFunctionCode = transaction.tradebsflag();   // 暂时使用tradeBsFlag成交方向字段

				quote->nAskOrder = transaction.tradesellno();
				quote->nBidOrder = transaction.tradebuyno();

				//quote->nType = pTransactionData->pCodeInfo->nType;

				//char buffer1[128];
				//snprintf(buffer1, sizeof(buffer1), "Transaction: %s %s %d %d %d %d %d %d %d %d %d %d %d", 
				//	pTransactionData->szWindCode, pTransactionData->szCode, pTransactionData->nActionDay, pTransactionData->nTime,
				//	pTransactionData->nIndex, pTransactionData->nPrice, pTransactionData->nVolume, pTransactionData->nTurnover,
				//	pTransactionData->nBSFlag, pTransactionData->chOrderKind, pTransactionData->chFunctionCode, 
				//	pTransactionData->nAskOrder, pTransactionData->nBidOrder);
				//LOG4CXX_INFO(logger, buffer1);

				Publisher::Instance()->Add(quote);

				//MUTEX_LOCK(&m_mtxTransaction);
				//if (lastStockTransactionMap[quote->szWindCode] == nullptr || lastStockTransactionMap[quote->szWindCode]->different(*quote.get()))
				//{
				//	Publisher::Instance()->Add(quote);
				//}
				//else
				//{
				//	sprintf(buff, "Ignore equals quote:%s", quote->szWindCode.c_str());
				//	LOG4CXX_WARN(logger, buff);
				//}

				//lastStockTransactionMap[quote->szWindCode] = quote;
				//MUTEX_UNLOCK(&m_mtxTransaction);
			}
			break;
		}
		case MD_ORDER: {
			if (data.has_mdorder()) {
				//data.mdorder().PrintDebugString();

				const ::com::htsc::mdc::insight::model::MDOrder& order = data.mdorder();
				std::shared_ptr<Order> quote(new Order());
				quote->szWindCode = order.htscsecurityid();
				quote->szCode = quote->szWindCode;

				auto iter_codetable = vCodeTables.find(quote->szWindCode);
				if (iter_codetable != vCodeTables.end()) {
					quote->szCNName = iter_codetable->second;
				}

				quote->nActionDay = order.mddate();
				quote->nTime = order.mdtime();

				quote->nOrder = order.orderindex();
				quote->nPrice = order.orderprice();
				quote->nVolume = order.orderqty();
				quote->chOrderKind = order.ordertype();
				quote->chFunctionCode = order.orderbsflag();   // 暂时使用tradeBsFlag成交方向字段
															   //quote->nType = pOrderData->pCodeInfo->nType;

				//char buffer1[128];
				//snprintf(buffer1, sizeof(buffer1), "Order: %s %s %d %d %d %d %d %d %d",
				//	pOrderData->szWindCode, pOrderData->szCode, pOrderData->nActionDay, pOrderData->nTime,
				//	pOrderData->nOrder, pOrderData->nPrice, pOrderData->nVolume,
				//	pOrderData->chOrderKind, pOrderData->chFunctionCode);
				//LOG4CXX_INFO(logger, buffer1);

				Publisher::Instance()->Add(quote);

				//MUTEX_LOCK(&m_mtxOrder);
				//if (lastStockOrderMap[quote->szWindCode] == nullptr || lastStockOrderMap[quote->szWindCode]->different(*quote.get()))
				//{
				//	Publisher::Instance()->Add(quote);
				//}
				//else
				//{
				//	sprintf(buff, "Ignore equals quote:%s", quote->szWindCode.c_str());
				//	LOG4CXX_WARN(logger, buff);
				//}

				//lastStockOrderMap[quote->szWindCode] = quote;
				//MUTEX_UNLOCK(&m_mtxOrder);
			}
			break;
		}
		}
		//printf("process a market data...\n");
		//printf("mdstock symbol is : %s\n", stock.symbol().c_str());
		//debug_print("mdstock symbol is : %s\n", stock.symbol().c_str());
		service_value_ = 1;
	}
	/**
	* OnServiceMessage查看是否订阅成功
	* @param[in] data_stream
	*/
	void OnServiceMessage(const ::com::htsc::mdc::insight::model::MarketDataStream& data_stream) {
		debug_print("==========> NewHandle: process a Service message");
		service_value_ = 1;
		/*google::protobuf::RepeatedPtrField<MarketData>::const_iterator it
		= data_stream.marketdatas().begin();
		google::protobuf::RepeatedPtrField<MarketData>::const_iterator end
		= data_stream.marketdatas().end();
		while (it != end) {
		std::string data_type = get_data_type_name(it->marketdatatype());
		std::string security_type = get_security_type_name(it->securitytype());
		debug_print("subscribe marketdatatype = %s successfully", data_type.c_str());
		debug_print("subscribe securitytype = %s successfully", security_type.c_str());
		}*/
	}

	void OnPlaybackStatus(const com::htsc::mdc::insight::model::PlaybackStatus& status) {
		debug_print("===========> NewHandle playback status=%d", status.taskstatus());
		service_value_ = status.taskstatus();
	}

	void OnPlaybackPayload(const com::htsc::mdc::insight::model::PlaybackPayload& payload) {
		service_value_ = 4;
		debug_print("------- PARSE message Playback payload, id:%s", payload.taskid().c_str());
		const MarketDataStream& stream = payload.marketdatastream();
		debug_print("total number=%d, serial=%d, isfinish=%d",
			stream.totalnumber(), stream.serial(), stream.isfinished());
		google::protobuf::RepeatedPtrField<MarketData>::const_iterator it
			= stream.marketdatalist().marketdatas().begin();
		while (it != stream.marketdatalist().marketdatas().end()) {
			OnMarketData(*it);
			++it;
		}
	}

	/**
	* 登录成功
	*/
	void OnLoginSuccess() {
		debug_print("-----------------------------");
		debug_print("------- OnLoginSuccess -------");
		login_success_ = true;
		debug_print("-----------------------------");
		LOG4CXX_INFO(logger, "OnLoginSuccess");
	}

	/**
	* 登录失败
	*/
	void OnLoginFailed(int error_no, const std::string& message) {
		error_print("-----------------------------");
		error_print("------- OnLoginFailed -------");
		login_success_ = false;
		error_print("------- server reply:%d,%s", error_no, message.c_str());
		error_print("-----------------------------");

		char logStr[1024];
		sprintf_s(logStr, "OnLoginFailed, server reply:%d,%s", error_no, message.c_str());
		LOG4CXX_ERROR(logger, logStr);
	}


	/**
	* 处理所有服务器都无法连接的情况
	*/
	void OnNoConnections() {
		error_print("-----------------------------");
		error_print("------- OnNoConnections -----");
		no_connections_ = true;
		reconnect_ = true;
		error_print("-----------------------------");

		LOG4CXX_INFO(logger, "OnNoConnections");
	}

	/**
	* 发生重连时
	*/
	void OnReconnect() {
		error_print("-----------------------------");
		error_print("------- OnReconnect -----");
		++reconnect_count_;
		reconnect_ = true;
		error_print("-----------------------------");

		LOG4CXX_INFO(logger, "OnReconnect");
	}

public:
	int service_value_;
	bool reconnect_;
	bool login_success_;
	bool no_connections_;
	int reconnect_count_;
};

ClientInterface* g_client = nullptr;
NewHandle* g_handle = NULL; // NewHandle不能为局部变量，否则会crash！！！
CCTPTrade* g_ctpTrade = nullptr;
int main()
{
	system("COLOR 0A");

	char logStr[1024];

	// licence check
	LicenseHelper licenseHlp;
	string decryptTxt = licenseHlp.readCipher();
	string mac = IOUtil::GetLocalMac();

	int sub_pos = decryptTxt.rfind("_");
	int str_len = decryptTxt.length();
	if (sub_pos < 0 || sub_pos + 1 > str_len)
	{
		sprintf_s(logStr, "License infomation string format error!\nThis machine ID is:[%s]", mac.c_str());
		LOG4CXX_INFO(logger, logStr);
		exit(0);
	}

	if (decryptTxt.find("any") == -1 &&
		decryptTxt.find("Any") == -1)
	{
		if (mac.empty())
		{
			sprintf_s(logStr, "Can't get the machine ID, license check failed, please contact the provider.");
			LOG4CXX_INFO(logger, logStr);
			exit(0);
		}
		else if (decryptTxt.find(mac) == -1)
		{
			sprintf_s(logStr, "License invalid, lease use after authorized!\nThis machine ID is:[%s]\n", mac.c_str());
			LOG4CXX_INFO(logger, logStr);
			exit(0);
		}
	}

	string auth_end_day = decryptTxt.substr(sub_pos + 1, str_len - sub_pos - 1);
	if (strlen(auth_end_day.c_str()) != 8)
	{
		sprintf_s(logStr, "License invalid: date format error(%s)", auth_end_day.c_str());
		LOG4CXX_INFO(logger, logStr);
		exit(0);
	}

	time_t timep;
	struct tm *p;
	time(&timep);
	p = localtime(&timep); /*取得当地时间*/
	char today[32];
	sprintf(today, "%04d%02d%02d", 1900 + p->tm_year, 1 + p->tm_mon, p->tm_mday);
	int nAuthEndDay = atoi(auth_end_day.c_str());
	if (atoi(today) > nAuthEndDay)
	{
		sprintf_s(logStr, "License invalid: out of end date(%s)", auth_end_day.c_str());
		LOG4CXX_INFO(logger, logStr);
		exit(0);
	}

	mapWindCode2Index["999999.SH"] = "000001.SH";
	mapWindCode2Index["999998.SH"] = "000002.SH";
	mapWindCode2Index["999997.SH"] = "000003.SH";
	mapWindCode2Index["999996.SH"] = "000004.SH";
	mapWindCode2Index["999995.SH"] = "000005.SH";
	mapWindCode2Index["999994.SH"] = "000006.SH";
	mapWindCode2Index["999993.SH"] = "000007.SH";
	mapWindCode2Index["999992.SH"] = "000008.SH";
	mapWindCode2Index["999991.SH"] = "000010.SH";
	mapWindCode2Index["999990.SH"] = "000011.SH";
	mapWindCode2Index["999989.SH"] = "000012.SH";
	mapWindCode2Index["999988.SH"] = "000013.SH";
	mapWindCode2Index["999987.SH"] = "000016.SH";
	mapWindCode2Index["999986.SH"] = "000015.SH";
	mapWindCode2Index["000300.SH"] = "000300.SH";

	mapIndex2WindCode["000001.SH"] = "999999.SH";
	mapIndex2WindCode["000002.SH"] = "999998.SH";
	mapIndex2WindCode["000003.SH"] = "999997.SH";
	mapIndex2WindCode["000004.SH"] = "999996.SH";
	mapIndex2WindCode["000005.SH"] = "999995.SH";
	mapIndex2WindCode["000006.SH"] = "999994.SH";
	mapIndex2WindCode["000007.SH"] = "999993.SH";
	mapIndex2WindCode["000008.SH"] = "999992.SH";
	mapIndex2WindCode["000010.SH"] = "999991.SH";
	mapIndex2WindCode["000011.SH"] = "999990.SH";
	mapIndex2WindCode["000012.SH"] = "999989.SH";
	mapIndex2WindCode["000013.SH"] = "999988.SH";
	mapIndex2WindCode["000016.SH"] = "999987.SH";
	mapIndex2WindCode["000015.SH"] = "999986.SH";
	mapIndex2WindCode["000300.SH"] = "000300.SH";

	CServerIniFile serverIni;
	int major, minor, patch;
	zmq_version(&major, &minor, &patch);
	printf("zmq_verion %d.%d.%d\n", major, minor, patch);
	PropertyConfigurator::configure("MdServer.log4cxx");
	LOG4CXX_INFO(logger, "Program starting...");

	// 启动CTP
	if (serverIni.bLoginCTP)
	{
		g_ctpTrade = new CCTPTrade();
		g_ctpTrade->Start();

		//CCTPMd* pMd = new CCTPMd();
		//pMd->Start();

		LOG4CXX_INFO(logger, "ctp gateway starting...");

		// 等待行情源初始化完成
		SLEEP(5);
	}
	
	if (serverIni.bLoginInsight)
	{
		init_env();
		open_trace();	// 打开流量日志
		open_file_log();// 输出日志到文件
		open_cout_log();// 输出日志到控制台

		// 启动insight
		g_client = ClientFactory::Instance()->CreateClient(true, serverIni.cert_folder);
		if (!g_client) {
			LOG4CXX_ERROR(logger, "create client failed!");
			ClientFactory::Uninstance();
			g_client = NULL;
			return -1;
		}
		g_handle = new NewHandle();
		//if (serverIni.export_csv) {
		//	handle = new SaveSubscribeDataToFileHandle(serverIni.export_folder);
		//}
		//else {
		//	handle = new NewHandle();
		//}
		g_client->RegistHandle(g_handle);

		//添加备用发现网关地址
		std::map<std::string, int> backup_list;
		backup_list.insert(std::pair<std::string, int>("221.226.112.140", 9362));
		backup_list.insert(std::pair<std::string, int>("153.3.219.107", 9362));
		backup_list.insert(std::pair<std::string, int>("221.131.138.171", 9362));

		//登录
		if (g_client->LoginByServiceDiscovery(serverIni.IP, atoi(serverIni.Port), serverIni.User, serverIni.Password, false, backup_list) != 0) {
			LOG4CXX_ERROR(logger, "login server failed!");
			ClientFactory::Uninstance();
			g_client = NULL;
			DELETE_P(g_handle);
			return -1;
		}

		//发送查询md请求信息
		MDQueryRequest* request = new MDQueryRequest();
		//request->set_querytype(QUERY_TYPE_LATEST_BASE_INFORMATION);
		//SecuritySourceType* type = request->add_securitysourcetype();
		//type->set_securityidsource(CCFX);
		//type->set_securitytype(FuturesType);


		request->set_querytype(QUERY_TYPE_LATEST_BASE_INFORMATION);
		request->add_securitysourcetype()->set_securityidsource(XSHG);
		request->add_securitysourcetype()->set_securityidsource(XSHE);
		request->add_securitysourcetype()->set_securityidsource(CCFX);
		request->add_securitysourcetype()->set_securityidsource(XDCE);
		request->add_securitysourcetype()->set_securityidsource(XSGE);
		request->add_securitysourcetype()->set_securityidsource(XZCE);
		std::vector<MDQueryResponse*>* responses;
		if (g_client->RequestMDQuery(request, responses) != 0) {
			LOG4CXX_ERROR(logger, "query md failed!");
			ClientFactory::Uninstance();
			DELETE_P(request);
			DELETE_P(g_handle);
			return -1;
		}
		/*
		//访问responses，成员如下：
		int32 queryType;             	     //查询类型
		bool isSuccess;                    // 请求是否成功
		InsightErrorContext errorContext;  // 错误信息
		MarketDataStream marketDataStream; //行情数据包
		*/

		sprintf_s(logStr, "query return message count=%d", responses->size());
		LOG4CXX_INFO(logger, logStr);

		//遍历查询内容
		for (unsigned int i = 0; i < responses->size(); ++i) {
			if (QUERY_TYPE_LATEST_BASE_INFORMATION != responses->at(i)->querytype()) {
				continue;
			}
			if (!responses->at(i)->issuccess()) {
				continue;
			}
			if (!responses->at(i)->has_marketdatastream()) {
				continue;
			}
			//遍历constants
			google::protobuf::RepeatedPtrField<MarketData>::const_iterator it
				= responses->at(i)->marketdatastream().marketdatalist().marketdatas().begin();
			google::protobuf::RepeatedPtrField<MarketData>::const_iterator end
				= responses->at(i)->marketdatastream().marketdatalist().marketdatas().end();
			while (it != end) {
				if (it->has_mdconstant()) {
					std::string htscsecurityid = it->mdconstant().htscsecurityid();
					std::string symbol = it->mdconstant().symbol();
					std::string cnName = utf8_to_gb2312(symbol);
					vCodeTables[htscsecurityid] = cnName;

					//std::shared_ptr<CodeTable> data(new CodeTable());
					//data->szWindCode = htscsecurityid;
					//data->szCode = htscsecurityid;
					//data->szMarket = it->mdconstant().securityidsource();
					//data->szENName = it->mdconstant().englishname();
					//data->szCNName = cnName;
					//data->nType = it->mdconstant().securitytype();

					////LOG4CXX_INFO(logger, pIndexData->szWindCode);
					//Publisher::Instance()->Add(data);
				}
				++it;
			}
		}

		//释放空间
		g_client->ReleaseQueryResult(responses);

		//订阅 SubscribeBySourceType
		ESubscribeActionType action_type2 = COVERAGE;
		std::unique_ptr<SubscribeBySourceType> source_type(new SubscribeBySourceType());

		//测试
		/*
		SubscribeBySourceTypeDetail* detail_1 = source_type->add_subscribebysourcetypedetail();
		SecuritySourceType* security_source_type_1 = new SecuritySourceType();
		security_source_type_1->set_securityidsource(CCFX);
		security_source_type_1->set_securitytype(FuturesType);
		detail_1->set_allocated_securitysourcetypes(security_source_type_1);
		detail_1->add_marketdatatypes(MD_TICK);
		*/
		////债券
		//SubscribeBySourceTypeDetail* detail_1 = source_type->add_subscribebysourcetypedetail();
		//SecuritySourceType* security_source_type_1 = new SecuritySourceType();
		//security_source_type_1->set_securityidsource(XSHG);
		//security_source_type_1->set_securitytype(BondType);
		//detail_1->set_allocated_securitysourcetypes(security_source_type_1);
		//detail_1->add_marketdatatypes(MD_TICK);

		//SubscribeBySourceTypeDetail* detail_2 = source_type->add_subscribebysourcetypedetail();
		//SecuritySourceType* security_source_type_2 = new SecuritySourceType();
		//security_source_type_2->set_securityidsource(XSHE);
		//security_source_type_2->set_securitytype(BondType);
		//detail_2->set_allocated_securitysourcetypes(security_source_type_2);
		//detail_2->add_marketdatatypes(MD_TICK);

		//股票
		SubscribeBySourceTypeDetail* detail_3 = source_type->add_subscribebysourcetypedetail();
		SecuritySourceType* security_source_type_3 = new SecuritySourceType();
		security_source_type_3->set_securityidsource(XSHG);
		security_source_type_3->set_securitytype(StockType);
		detail_3->set_allocated_securitysourcetypes(security_source_type_3);
		detail_3->add_marketdatatypes(MD_TICK);
		//detail_3->add_marketdatatypes(MD_TRANSACTION);
		//detail_3->add_marketdatatypes(MD_ORDER);

		SubscribeBySourceTypeDetail* detail_4 = source_type->add_subscribebysourcetypedetail();
		SecuritySourceType* security_source_type_4 = new SecuritySourceType();
		security_source_type_4->set_securityidsource(XSHE);
		security_source_type_4->set_securitytype(StockType);
		detail_4->set_allocated_securitysourcetypes(security_source_type_4);
		detail_4->add_marketdatatypes(MD_TICK);
		//detail_4->add_marketdatatypes(MD_TRANSACTION);
		//detail_4->add_marketdatatypes(MD_ORDER);

		////基金
		//SubscribeBySourceTypeDetail* detail_5 = source_type->add_subscribebysourcetypedetail();
		//SecuritySourceType* security_source_type_5 = new SecuritySourceType();
		//security_source_type_5->set_securityidsource(XSHG);
		//security_source_type_5->set_securitytype(FundType);
		//detail_5->set_allocated_securitysourcetypes(security_source_type_5);
		//detail_5->add_marketdatatypes(MD_TICK);
		//detail_5->add_marketdatatypes(MD_TRANSACTION);
		//detail_5->add_marketdatatypes(MD_ORDER);

		//SubscribeBySourceTypeDetail* detail_6 = source_type->add_subscribebysourcetypedetail();
		//SecuritySourceType* security_source_type_6 = new SecuritySourceType();
		//security_source_type_6->set_securityidsource(XSHE);
		//security_source_type_6->set_securitytype(FundType);
		//detail_6->set_allocated_securitysourcetypes(security_source_type_6);
		//detail_6->add_marketdatatypes(MD_TICK);
		//detail_6->add_marketdatatypes(MD_TRANSACTION);
		//detail_6->add_marketdatatypes(MD_ORDER);

		//指数
		SubscribeBySourceTypeDetail* detail_7 = source_type->add_subscribebysourcetypedetail();
		SecuritySourceType* security_source_type_7 = new SecuritySourceType();
		security_source_type_7->set_securityidsource(XSHG);
		security_source_type_7->set_securitytype(IndexType);
		detail_7->set_allocated_securitysourcetypes(security_source_type_7);
		detail_7->add_marketdatatypes(MD_TICK);

		SubscribeBySourceTypeDetail* detail_8 = source_type->add_subscribebysourcetypedetail();
		SecuritySourceType* security_source_type_8 = new SecuritySourceType();
		security_source_type_8->set_securityidsource(XSHE);
		security_source_type_8->set_securitytype(IndexType);
		detail_8->set_allocated_securitysourcetypes(security_source_type_8);
		detail_8->add_marketdatatypes(MD_TICK);

		//期货
		SubscribeBySourceTypeDetail* detail_9 = source_type->add_subscribebysourcetypedetail();
		SecuritySourceType* security_source_type_9 = new SecuritySourceType();
		security_source_type_9->set_securitytype(FuturesType);
		detail_9->set_allocated_securitysourcetypes(security_source_type_9);
		detail_9->add_marketdatatypes(MD_TICK);

		////现货行情
		//SubscribeBySourceTypeDetail* detail_10 = source_type->add_subscribebysourcetypedetail();
		//SecuritySourceType* security_source_type_10 = new SecuritySourceType();
		//security_source_type_10->set_securitytype(SpotType);
		//detail_10->set_allocated_securitysourcetypes(security_source_type_10);
		//detail_10->add_marketdatatypes(MD_TICK);

		////外汇
		//SubscribeBySourceTypeDetail* detail_11 = source_type->add_subscribebysourcetypedetail();
		//SecuritySourceType* security_source_type_11 = new SecuritySourceType();
		//security_source_type_11->set_securitytype(ForexType);
		//detail_11->set_allocated_securitysourcetypes(security_source_type_11);
		//detail_11->add_marketdatatypes(MD_TICK);

		//订阅
		if (g_client->SubscribeBySourceType(action_type2, &(*source_type)) != 0) {
			ClientFactory::Uninstance();
			return -1;
		}
	}

	// 启动发布线程
	Publisher::CreateInstance();
	Publisher::Instance()->Start();

	// do sth in loop  
	while (true)
	{
		char ch = getchar();
		if (ch == 'q' || ch == 'Q')
		{
			LOG4CXX_INFO(logger, "Program stopped");
			break;
		}

		//usleep(60 * 1000 * 1000);// 60 s  
	}

	if (g_ctpTrade != nullptr)
		delete g_ctpTrade;

	if (g_client != nullptr)
	{
		ClientFactory::Uninstance();

		close_trace();
		close_file_log();
		close_cout_log();
		fini_env();
	}

	return 0;
}

std::set<std::string> subbedWindCodeList;

int subscribe(const char* ticker)
{
	if (strlen(ticker) == 0) return -1;

	std::string szWindCode(ticker);
	transform(szWindCode.begin(), szWindCode.end(), szWindCode.begin(), ::toupper); //将小写都转换成大写

	if (subbedWindCodeList.find(szWindCode) != subbedWindCodeList.end())
	{
		char buff[128];
		sprintf(buff, "repeat subscribe: %s!", ticker);
		LOG4CXX_ERROR(logger, buff);
		return -1;
	}
	//else
	//{
	//	// 上证指数WindCode经过转换，不在insight代码表里，所以不check上证指数WindCode
	//	if (!IOUtil::startWith(ticker, "99") && vCodeTables.find(ticker) == vCodeTables.end())
	//	{
	//		char buff[256];
	//		sprintf(buff, "subscribe error: can't find %s in codetable list!", ticker);
	//		LOG4CXX_ERROR(logger, buff);
	//		return -1;

	//	}
	//}

	int pos = szWindCode.find(".");
	if (pos < 0) return -1;

	std::string suffix = szWindCode.substr(pos);
	if (suffix == ".SH" || suffix == ".SZ")
	{
		//if (g_client == nullptr) return -2;

		////订阅 SubscribeByID
		//ESubscribeActionType action_type = ADD;
		//std::unique_ptr<SubscribeByID> id(new SubscribeByID());
		////订阅 SubscribeByID
		//std::vector<std::string> securityIdList;

		//// 上证指数需特殊处理
		//if (IOUtil::startWith(ticker, "99"))
		//{
		//	securityIdList.push_back(windCode2Index(ticker));
		//}
		//else
		//{
		//	securityIdList.push_back(ticker);
		//}
		//
		//for (int index = 0; index < securityIdList.size(); index++) {
		//	SubscribeByIDDetail* id_detail = id->add_subscribebyiddetails();
		//	id_detail->set_htscsecurityid(securityIdList[index]);
		//	id_detail->add_marketdatatypes(MD_TICK);
		//}

		////订阅
		//int ret = g_client->SubscribeByID(action_type, &(*id));
		//if (ret == 0)
		//	subbedWindCodeList.insert(szWindCode);
		//char buff[128];
		//sprintf(buff, "Subscription Add: %s ret=%d", ticker, ret);
		//LOG4CXX_INFO(logger, buff);

		//return ret;
	}
	else if (suffix == ".CF"  ||
		suffix == ".SHF" ||
		suffix == ".DCE" ||
		suffix == ".CZC")
	{
		// 期货大小写敏感，使用实际的代码订阅，不能使用WindCode订阅
		if (g_ctpTrade != nullptr)
		{
			g_ctpTrade->Subscribe(ticker);

			char buff[128];
			sprintf(buff, "Subscription Add: %s", ticker);
			LOG4CXX_INFO(logger, buff);
		}
	}

	return 0;
}

int unsubscribe(const char* ticker)
{
	//// ticker实际是WindCode
	//if (hTDF == nullptr || strlen(ticker) == 0) return -1;

	//std::string szWindCode(ticker);
	//transform(szWindCode.begin(), szWindCode.end(), szWindCode.begin(), ::toupper); //将小写都转换成大写
	//auto iter = vSubCodeTables.find(szWindCode);
	//if (iter == vSubCodeTables.end())
	//{
	//	char buff[256];
	//	sprintf(buff, "unsubscribe: can't find %s in codetable list!", ticker);
	//	LOG4CXX_ERROR(logger, buff);
	//	return -2;
	//}
	//else if (subbedWindCodeList.find(szWindCode) == subbedWindCodeList.end())
	//{
	//	char buff[256];
	//	sprintf(buff, "never subscribed: %s!", ticker);
	//	LOG4CXX_ERROR(logger, buff);
	//	return -3;
	//}

	//// 期货大小写敏感，使用实际的代码订阅，不能使用WindCode订阅
	//std::string subCode = iter->second;
	//int ret = TDF_SetSubscription(hTDF, subCode.c_str(), SUBSCRIPTION_DEL);
	//if (ret == 0)
	//	subbedWindCodeList.erase(szWindCode);
	//char buff[128];
	//sprintf(buff, "Subscription Del: %s ret=%d", subCode.c_str(), ret);
	//LOG4CXX_INFO(logger, buff);
	//return ret;

	return 0;
}