#pragma once
#include "CTPMd.h"
#include "ThostFtdcTraderApi.h"
#include "ThostFtdcUserApiStruct.h"

#include <iostream>

static log4cxx::LoggerPtr logger_ctptrade = log4cxx::Logger::getLogger("CTPTrade");
class CCTPTrade : public CThostFtdcTraderSpi
{
public:
	//构造函数
	CCTPTrade();
	~CCTPTrade();

	///当客户端与交易后台建立起通信连接时（还未登录前），该方法被调用。
	virtual void OnFrontConnected();

	///客户端认证响应
	virtual void OnRspAuthenticate(CThostFtdcRspAuthenticateField *pRspAuthenticateField, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);

	///登录请求响应
	virtual void OnRspUserLogin(CThostFtdcRspUserLoginField *pRspUserLogin, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);

	///投资者结算结果确认响应
	virtual void OnRspSettlementInfoConfirm(CThostFtdcSettlementInfoConfirmField *pSettlementInfoConfirm, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);

	///请求查询合约响应
	virtual void OnRspQryInstrument(CThostFtdcInstrumentField *pInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);

	///错误应答
	virtual void OnRspError(CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);

	///当客户端与交易后台通信连接断开时，该方法被调用。当发生这个情况后，API会自动重新连接，客户端可不做处理。
	virtual void OnFrontDisconnected(int nReason);

	///心跳超时警告。当长时间未收到报文时，该方法被调用。
	virtual void OnHeartBeatWarning(int nTimeLapse);

	///报单通知
	virtual void OnRtnOrder(CThostFtdcOrderField *pOrder);

	///成交通知
	virtual void OnRtnTrade(CThostFtdcTradeField *pTrade);

	///报单录入请求响应
	virtual void OnRspOrderInsert(CThostFtdcInputOrderField *pInputOrder, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);

	///报单操作请求响应
	virtual void OnRspOrderAction(CThostFtdcInputOrderActionField *pInputOrderAction, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);

	///报单录入错误回报
	virtual void OnErrRtnOrderInsert(CThostFtdcInputOrderField *pInputOrder, CThostFtdcRspInfoField *pRspInfo);

	///报单操作错误回报
	virtual void OnErrRtnOrderAction(CThostFtdcOrderActionField *pOrderAction, CThostFtdcRspInfoField *pRspInfo);

private:
	CCTPMd* m_CtpMd;
	CThostFtdcTraderApi* m_ThostTradeApi;
	int m_RequestId;
	char account[32];
	char password[32];
	char broker[11];
	char appid[32];
	char auth_code[32];
	char prod_info[32];

	TThostFtdcOrderRefType	g_chOrderRef;
	///前置编号
	TThostFtdcFrontIDType	g_chFrontID;
	///会话编号
	TThostFtdcSessionIDType	g_chSessionID;
	///报单编号
	TThostFtdcOrderSysIDType	g_chOrderSysID;
	/// 交易所代码
	TThostFtdcExchangeIDType g_chExchangeID;

	std::vector<CThostFtdcInstrumentField> m_Instruments;
	MUTEX_T m_mtxInstrument;

	// 是否收到成功的响应
	bool IsErrorRspInfo(CThostFtdcRspInfoField *pRspInfo);
	int ReqAuthenticate();
	int ReqUserLogin();		// 登录
	int TestOrder();
	
public:
	void Start();
	void Subscribe(std::string ticker);
};

CCTPTrade::CCTPTrade():m_RequestId(0), m_ThostTradeApi(nullptr)
{
	MUTEX_INIT(&m_mtxInstrument);
}

CCTPTrade::~CCTPTrade()
{
	if (m_ThostTradeApi != nullptr)
	{
		m_ThostTradeApi->Release();
		m_ThostTradeApi = nullptr;
	}

	if (m_CtpMd != nullptr)
	{
		delete m_CtpMd;
		m_CtpMd = nullptr;
	}

	MUTEX_DESTROY(&m_mtxInstrument);
}

void CCTPTrade::OnFrontConnected()
{
	LOG4CXX_INFO(logger_ctptrade, "CTPTrade:OnFrontConnected");

	ReqAuthenticate();
	//ReqUserLogin();
}

inline void CCTPTrade::OnRspAuthenticate(CThostFtdcRspAuthenticateField * pRspAuthenticateField, CThostFtdcRspInfoField * pRspInfo, int nRequestID, bool bIsLast)
{
	printf("<OnRspAuthenticate>\n");
	if (pRspAuthenticateField)
	{
		printf("\tBrokerID [%s]\n", pRspAuthenticateField->BrokerID);
		printf("\tUserID [%s]\n", pRspAuthenticateField->UserID);
		printf("\tUserProductInfo [%s]\n", pRspAuthenticateField->UserProductInfo);
		printf("\tAppID [%s]\n", pRspAuthenticateField->AppID);
		printf("\tAppType [%c]\n", pRspAuthenticateField->AppType);
	}
	if (pRspInfo)
	{
		printf("\tErrorMsg [%s]\n", pRspInfo->ErrorMsg);
		printf("\tErrorID [%d]\n", pRspInfo->ErrorID);
	}
	printf("\tnRequestID [%d]\n", nRequestID);
	printf("\tbIsLast [%d]\n", bIsLast);
	printf("</OnRspAuthenticate>\n");

	if (!IsErrorRspInfo(pRspInfo))
	{
		ReqUserLogin();
	}
	else
	{
		char buff[1024];
		sprintf(buff, "CTPTrade:OnRspAuthenticate(UserID:%s):%s", pRspAuthenticateField->UserID, pRspInfo->ErrorMsg);
		LOG4CXX_ERROR(logger_ctptrade, buff);
	}
}

void CCTPTrade::OnRspUserLogin(CThostFtdcRspUserLoginField * pRspUserLogin, CThostFtdcRspInfoField * pRspInfo, int nRequestID, bool bIsLast)
{
	printf("<OnRspUserLogin>\n");
	if (pRspUserLogin)
	{
		printf("\tTradingDay [%s]\n", pRspUserLogin->TradingDay);
		printf("\tLoginTime [%s]\n", pRspUserLogin->LoginTime);
		printf("\tBrokerID [%s]\n", pRspUserLogin->BrokerID);
		printf("\tUserID [%s]\n", pRspUserLogin->UserID);
		printf("\tSystemName [%s]\n", pRspUserLogin->SystemName);
		printf("\tMaxOrderRef [%s]\n", pRspUserLogin->MaxOrderRef);
		printf("\tSHFETime [%s]\n", pRspUserLogin->SHFETime);
		printf("\tDCETime [%s]\n", pRspUserLogin->DCETime);
		printf("\tCZCETime [%s]\n", pRspUserLogin->CZCETime);
		printf("\tFFEXTime [%s]\n", pRspUserLogin->FFEXTime);
		printf("\tINETime [%s]\n", pRspUserLogin->INETime);
		printf("\tFrontID [%d]\n", pRspUserLogin->FrontID);
		printf("\tSessionID [%d]\n", pRspUserLogin->SessionID);
	}
	if (pRspInfo)
	{
		printf("\tErrorMsg [%s]\n", pRspInfo->ErrorMsg);
		printf("\tErrorID [%d]\n", pRspInfo->ErrorID);
	}
	printf("\tnRequestID [%d]\n", nRequestID);
	printf("\tbIsLast [%d]\n", bIsLast);
	printf("</OnRspUserLogin>\n");

	char buff[1024];
	if (!IsErrorRspInfo(pRspInfo))
	{
		CThostFtdcSettlementInfoConfirmField req;
		memset(&req, 0, sizeof(req));
		strcpy(req.BrokerID, broker);
		strcpy(req.InvestorID, account);
		int iResult = m_ThostTradeApi->ReqSettlementInfoConfirm(&req, m_RequestId++);
		
		sprintf(buff, "CTPTrade:ReqSettlementInfoConfirm(UserID:%s) %s (%d)", pRspUserLogin->UserID, iResult == 0 ? "Success" : "failure", iResult);
		LOG4CXX_INFO(logger_ctptrade, buff);
	}
	else
	{
		sprintf(buff, "CTPTrade:OnRspUserLogin(UserID:%s):%s", pRspUserLogin->UserID, pRspInfo->ErrorMsg);
		LOG4CXX_ERROR(logger_ctptrade, buff);
	}
}

void CCTPTrade::OnRspSettlementInfoConfirm(CThostFtdcSettlementInfoConfirmField * pSettlementInfoConfirm, CThostFtdcRspInfoField * pRspInfo, int nRequestID, bool bIsLast)
{
	printf("<OnRspSettlementInfoConfirm>\n");
	if (pSettlementInfoConfirm)
	{
		printf("\tBrokerID [%s]\n", pSettlementInfoConfirm->BrokerID);
		printf("\tInvestorID [%s]\n", pSettlementInfoConfirm->InvestorID);
		printf("\tConfirmDate [%s]\n", pSettlementInfoConfirm->ConfirmDate);
		printf("\tConfirmTime [%s]\n", pSettlementInfoConfirm->ConfirmTime);
		printf("\tAccountID [%s]\n", pSettlementInfoConfirm->AccountID);
		printf("\tCurrencyID [%s]\n", pSettlementInfoConfirm->CurrencyID);
		printf("\tSettlementID [%d]\n", pSettlementInfoConfirm->SettlementID);
	}
	if (pRspInfo)
	{
		printf("\tErrorMsg [%s]\n", pRspInfo->ErrorMsg);
		printf("\tErrorID [%d]\n", pRspInfo->ErrorID);
	}
	printf("\tnRequestID [%d]\n", nRequestID);
	printf("\tbIsLast [%d]\n", bIsLast);
	printf("</OnRspSettlementInfoConfirm>\n");

	char buff[1024];
	if (!IsErrorRspInfo(pRspInfo))
	{
		// 重新查询前清空合约
		m_Instruments.clear();

		CThostFtdcQryInstrumentField req;
		memset(&req, 0, sizeof(CThostFtdcQryInstrumentField));
		int iResult = m_ThostTradeApi->ReqQryInstrument(&req, m_RequestId++);

		sprintf(buff, "CTPTrade:ReqQryInstrument(UserID:%s) %s (%d)", pSettlementInfoConfirm->InvestorID, iResult == 0 ? "Success" : "failure", iResult);
		LOG4CXX_INFO(logger_ctptrade, buff);
	}
	else
	{
		sprintf(buff, "CTPTrade:OnRspSettlementInfoConfirm(UserID:%s):%s", pSettlementInfoConfirm->InvestorID, pRspInfo->ErrorMsg);
		LOG4CXX_ERROR(logger_ctptrade, buff);
	}
}

void CCTPTrade::OnRspQryInstrument(CThostFtdcInstrumentField * pInstrument, CThostFtdcRspInfoField * pRspInfo, int nRequestID, bool bIsLast)
{
	if (!IsErrorRspInfo(pRspInfo))
	{
		if (pInstrument != nullptr)
		{
			//if (strcmp(pInstrument->InstrumentID, "IO2002-C-3600") == 0)
			//{
			//	LOG4CXX_ERROR(logger_ctptrade, pInstrument->InstrumentID);
			//}

			if ((pInstrument->ProductClass == THOST_FTDC_PC_Futures || 
				pInstrument->ProductClass == THOST_FTDC_PC_SpotOption)&&
				pInstrument->InstLifePhase == THOST_FTDC_IP_Started)
			{
				//char buff[1024];
				//sprintf(buff, "CTPTrade:OnRspQryInstrument(Instrument:%s, ExchangeID:%s)", pInstrument->InstrumentID, pInstrument->ExchangeID);
				//LOG4CXX_INFO(logger_ctptrade, buff);

				CThostFtdcInstrumentField field;
				memcpy(&field, pInstrument, sizeof(CThostFtdcInstrumentField));
				MUTEX_LOCK(&m_mtxInstrument);
				m_Instruments.push_back(field);
				MUTEX_UNLOCK(&m_mtxInstrument);
				//// 订阅行情
				//m_pCTPGateway->Get_pCTPMdGateWay()->SubscribeMarketData(pInstrument->InstrumentID);

				//SRV_LOG(logger, SRV_LOG_LEVEL_INFO, "OnRspQryInstrument(%s,InstrumentName:%s,ExchangeID:%s,ExchangeInstID:%s,ProductID:%s,ProductClass:%c,VolumeMultiple:%d,PriceTick:%.2f,InstLifePhase:%c,IsTrading:%d,LongMarginRatio:%.2f,ShortMarginRatio:%.2f)",
				//	pInstrument->InstrumentID, pInstrument->InstrumentName, pInstrument->ExchangeID, pInstrument->ExchangeInstID, pInstrument->ProductID, pInstrument->ProductClass,
				//	pInstrument->VolumeMultiple,
				//	pInstrument->PriceTick,
				//	pInstrument->InstLifePhase,
				//	pInstrument->IsTrading,
				//	pInstrument->LongMarginRatio,
				//	pInstrument->ShortMarginRatio);

				//ReqQryInstrumentMarginRate(pInstrument->InstrumentID);
				//ReqQryInstrumentCommissionRate(pInstrument->InstrumentID);
			}
		}
	}

	if (bIsLast)
	{
		LOG4CXX_INFO(logger_ctptrade, "CTPTrade:OnRspQryInstrument ended");

		// 订阅期货行情
		//MUTEX_LOCK(&m_mtxInstrument);
		//m_CtpMd->SubscribeMarketData(m_Instruments);
		//MUTEX_UNLOCK(&m_mtxInstrument);
	}
}

void CCTPTrade::OnRspError(CThostFtdcRspInfoField * pRspInfo, int nRequestID, bool bIsLast)
{
	IsErrorRspInfo(pRspInfo);
}

void CCTPTrade::OnFrontDisconnected(int nReason)
{
	char buff[1024];
	sprintf(buff, "CTPTrade:OnFrontDisconnected:%d", nReason);
	LOG4CXX_ERROR(logger_ctptrade, buff);
}

void CCTPTrade::OnHeartBeatWarning(int nTimeLapse)
{
}

inline void CCTPTrade::OnRtnOrder(CThostFtdcOrderField * pOrder)
{
	strcpy_s(g_chOrderSysID, pOrder->OrderSysID);
	g_chFrontID = pOrder->FrontID;
	g_chSessionID = pOrder->SessionID;
	strcpy_s(g_chOrderRef, pOrder->OrderRef);
	strcpy_s(g_chExchangeID, pOrder->ExchangeID);

	printf("<OnRtnOrder>\n");
	if (pOrder)
	{
		printf("\tBrokerID [%s]\n", pOrder->BrokerID);
		printf("\tInvestorID [%s]\n", pOrder->InvestorID);
		printf("\tInstrumentID [%s]\n", pOrder->InstrumentID);
		printf("\tOrderRef [%s]\n", pOrder->OrderRef);
		printf("\tUserID [%s]\n", pOrder->UserID);
		printf("\tCombOffsetFlag [%s]\n", pOrder->CombOffsetFlag);
		printf("\tCombHedgeFlag [%s]\n", pOrder->CombHedgeFlag);
		printf("\tGTDDate [%s]\n", pOrder->GTDDate);
		printf("\tBusinessUnit [%s]\n", pOrder->BusinessUnit);
		printf("\tOrderLocalID [%s]\n", pOrder->OrderLocalID);
		printf("\tExchangeID [%s]\n", pOrder->ExchangeID);
		printf("\tParticipantID [%s]\n", pOrder->ParticipantID);
		printf("\tClientID [%s]\n", pOrder->ClientID);
		printf("\tExchangeInstID [%s]\n", pOrder->ExchangeInstID);
		printf("\tTraderID [%s]\n", pOrder->TraderID);
		printf("\tTradingDay [%s]\n", pOrder->TradingDay);
		printf("\tOrderSysID [%s]\n", pOrder->OrderSysID);
		printf("\tInsertDate [%s]\n", pOrder->InsertDate);
		printf("\tInsertTime [%s]\n", pOrder->InsertTime);
		printf("\tActiveTime [%s]\n", pOrder->ActiveTime);
		printf("\tSuspendTime [%s]\n", pOrder->SuspendTime);
		printf("\tUpdateTime [%s]\n", pOrder->UpdateTime);
		printf("\tCancelTime [%s]\n", pOrder->CancelTime);
		printf("\tActiveTraderID [%s]\n", pOrder->ActiveTraderID);
		printf("\tClearingPartID [%s]\n", pOrder->ClearingPartID);
		printf("\tUserProductInfo [%s]\n", pOrder->UserProductInfo);
		printf("\tStatusMsg [%s]\n", pOrder->StatusMsg);
		printf("\tActiveUserID [%s]\n", pOrder->ActiveUserID);
		printf("\tRelativeOrderSysID [%s]\n", pOrder->RelativeOrderSysID);
		printf("\tBranchID [%s]\n", pOrder->BranchID);
		printf("\tInvestUnitID [%s]\n", pOrder->InvestUnitID);
		printf("\tAccountID [%s]\n", pOrder->AccountID);
		printf("\tCurrencyID [%s]\n", pOrder->CurrencyID);
		printf("\tIPAddress [%s]\n", pOrder->IPAddress);
		printf("\tMacAddress [%s]\n", pOrder->MacAddress);
		printf("\tVolumeTotalOriginal [%d]\n", pOrder->VolumeTotalOriginal);
		printf("\tMinVolume [%d]\n", pOrder->MinVolume);
		printf("\tIsAutoSuspend [%d]\n", pOrder->IsAutoSuspend);
		printf("\tRequestID [%d]\n", pOrder->RequestID);
		printf("\tInstallID [%d]\n", pOrder->InstallID);
		printf("\tNotifySequence [%d]\n", pOrder->NotifySequence);
		printf("\tSettlementID [%d]\n", pOrder->SettlementID);
		printf("\tVolumeTraded [%d]\n", pOrder->VolumeTraded);
		printf("\tVolumeTotal [%d]\n", pOrder->VolumeTotal);
		printf("\tSequenceNo [%d]\n", pOrder->SequenceNo);
		printf("\tFrontID [%d]\n", pOrder->FrontID);
		printf("\tSessionID [%d]\n", pOrder->SessionID);
		printf("\tUserForceClose [%d]\n", pOrder->UserForceClose);
		printf("\tBrokerOrderSeq [%d]\n", pOrder->BrokerOrderSeq);
		printf("\tZCETotalTradedVolume [%d]\n", pOrder->ZCETotalTradedVolume);
		printf("\tIsSwapOrder [%d]\n", pOrder->IsSwapOrder);
		printf("\tOrderPriceType [%c]\n", pOrder->OrderPriceType);
		printf("\tDirection [%c]\n", pOrder->Direction);
		printf("\tTimeCondition [%c]\n", pOrder->TimeCondition);
		printf("\tVolumeCondition [%c]\n", pOrder->VolumeCondition);
		printf("\tContingentCondition [%c]\n", pOrder->ContingentCondition);
		printf("\tForceCloseReason [%c]\n", pOrder->ForceCloseReason);
		printf("\tOrderSubmitStatus [%c]\n", pOrder->OrderSubmitStatus);
		printf("\tOrderSource [%c]\n", pOrder->OrderSource);
		printf("\tOrderStatus [%c]\n", pOrder->OrderStatus);
		printf("\tOrderType [%c]\n", pOrder->OrderType);
		printf("\tLimitPrice [%.8lf]\n", pOrder->LimitPrice);
		printf("\tStopPrice [%.8lf]\n", pOrder->StopPrice);
	}
	printf("</OnRtnOrder>\n");
}

inline void CCTPTrade::OnRtnTrade(CThostFtdcTradeField * pTrade)
{
	printf("<OnRtnTrade>\n");
	if (pTrade)
	{
		printf("\tBrokerID [%s]\n", pTrade->BrokerID);
		printf("\tInvestorID [%s]\n", pTrade->InvestorID);
		printf("\tInstrumentID [%s]\n", pTrade->InstrumentID);
		printf("\tOrderRef [%s]\n", pTrade->OrderRef);
		printf("\tUserID [%s]\n", pTrade->UserID);
		printf("\tExchangeID [%s]\n", pTrade->ExchangeID);
		printf("\tTradeID [%s]\n", pTrade->TradeID);
		printf("\tOrderSysID [%s]\n", pTrade->OrderSysID);
		printf("\tParticipantID [%s]\n", pTrade->ParticipantID);
		printf("\tClientID [%s]\n", pTrade->ClientID);
		printf("\tExchangeInstID [%s]\n", pTrade->ExchangeInstID);
		printf("\tTradeDate [%s]\n", pTrade->TradeDate);
		printf("\tTradeTime [%s]\n", pTrade->TradeTime);
		printf("\tTraderID [%s]\n", pTrade->TraderID);
		printf("\tOrderLocalID [%s]\n", pTrade->OrderLocalID);
		printf("\tClearingPartID [%s]\n", pTrade->ClearingPartID);
		printf("\tBusinessUnit [%s]\n", pTrade->BusinessUnit);
		printf("\tTradingDay [%s]\n", pTrade->TradingDay);
		printf("\tInvestUnitID [%s]\n", pTrade->InvestUnitID);
		printf("\tVolume [%d]\n", pTrade->Volume);
		printf("\tSequenceNo [%d]\n", pTrade->SequenceNo);
		printf("\tSettlementID [%d]\n", pTrade->SettlementID);
		printf("\tBrokerOrderSeq [%d]\n", pTrade->BrokerOrderSeq);
		printf("\tDirection [%c]\n", pTrade->Direction);
		printf("\tTradingRole [%c]\n", pTrade->TradingRole);
		printf("\tOffsetFlag [%c]\n", pTrade->OffsetFlag);
		printf("\tHedgeFlag [%c]\n", pTrade->HedgeFlag);
		printf("\tTradeType [%c]\n", pTrade->TradeType);
		printf("\tPriceSource [%c]\n", pTrade->PriceSource);
		printf("\tTradeSource [%c]\n", pTrade->TradeSource);
		printf("\tPrice [%.8lf]\n", pTrade->Price);
	}
	printf("</OnRtnTrade>\n");
}

inline void CCTPTrade::OnRspOrderInsert(CThostFtdcInputOrderField * pInputOrder, CThostFtdcRspInfoField * pRspInfo, int nRequestID, bool bIsLast)
{
	printf("<OnRspOrderInsert>\n");
	if (pInputOrder)
	{
		printf("\tBrokerID [%s]\n", pInputOrder->BrokerID);
		printf("\tInvestorID [%s]\n", pInputOrder->InvestorID);
		printf("\tInstrumentID [%s]\n", pInputOrder->InstrumentID);
		printf("\tOrderRef [%s]\n", pInputOrder->OrderRef);
		printf("\tUserID [%s]\n", pInputOrder->UserID);
		printf("\tCombOffsetFlag [%s]\n", pInputOrder->CombOffsetFlag);
		printf("\tCombHedgeFlag [%s]\n", pInputOrder->CombHedgeFlag);
		printf("\tGTDDate [%s]\n", pInputOrder->GTDDate);
		printf("\tBusinessUnit [%s]\n", pInputOrder->BusinessUnit);
		printf("\tExchangeID [%s]\n", pInputOrder->ExchangeID);
		printf("\tInvestUnitID [%s]\n", pInputOrder->InvestUnitID);
		printf("\tAccountID [%s]\n", pInputOrder->AccountID);
		printf("\tCurrencyID [%s]\n", pInputOrder->CurrencyID);
		printf("\tClientID [%s]\n", pInputOrder->ClientID);
		printf("\tIPAddress [%s]\n", pInputOrder->IPAddress);
		printf("\tMacAddress [%s]\n", pInputOrder->MacAddress);
		printf("\tVolumeTotalOriginal [%d]\n", pInputOrder->VolumeTotalOriginal);
		printf("\tMinVolume [%d]\n", pInputOrder->MinVolume);
		printf("\tIsAutoSuspend [%d]\n", pInputOrder->IsAutoSuspend);
		printf("\tRequestID [%d]\n", pInputOrder->RequestID);
		printf("\tUserForceClose [%d]\n", pInputOrder->UserForceClose);
		printf("\tIsSwapOrder [%d]\n", pInputOrder->IsSwapOrder);
		printf("\tOrderPriceType [%c]\n", pInputOrder->OrderPriceType);
		printf("\tDirection [%c]\n", pInputOrder->Direction);
		printf("\tTimeCondition [%c]\n", pInputOrder->TimeCondition);
		printf("\tVolumeCondition [%c]\n", pInputOrder->VolumeCondition);
		printf("\tContingentCondition [%c]\n", pInputOrder->ContingentCondition);
		printf("\tForceCloseReason [%c]\n", pInputOrder->ForceCloseReason);
		printf("\tLimitPrice [%.8lf]\n", pInputOrder->LimitPrice);
		printf("\tStopPrice [%.8lf]\n", pInputOrder->StopPrice);
	}
	if (pRspInfo)
	{
		printf("\tErrorMsg [%s]\n", pRspInfo->ErrorMsg);
		printf("\tErrorID [%d]\n", pRspInfo->ErrorID);
	}
	printf("\tnRequestID [%d]\n", nRequestID);
	printf("\tbIsLast [%d]\n", bIsLast);
	printf("</OnRspOrderInsert>\n");
}

inline void CCTPTrade::OnRspOrderAction(CThostFtdcInputOrderActionField * pInputOrderAction, CThostFtdcRspInfoField * pRspInfo, int nRequestID, bool bIsLast)
{
	printf("<OnRspOrderAction>\n");
	if (pInputOrderAction)
	{
		printf("\tBrokerID [%s]\n", pInputOrderAction->BrokerID);
		printf("\tInvestorID [%s]\n", pInputOrderAction->InvestorID);
		printf("\tOrderRef [%s]\n", pInputOrderAction->OrderRef);
		printf("\tExchangeID [%s]\n", pInputOrderAction->ExchangeID);
		printf("\tOrderSysID [%s]\n", pInputOrderAction->OrderSysID);
		printf("\tUserID [%s]\n", pInputOrderAction->UserID);
		printf("\tInstrumentID [%s]\n", pInputOrderAction->InstrumentID);
		printf("\tInvestUnitID [%s]\n", pInputOrderAction->InvestUnitID);
		printf("\tIPAddress [%s]\n", pInputOrderAction->IPAddress);
		printf("\tMacAddress [%s]\n", pInputOrderAction->MacAddress);
		printf("\tOrderActionRef [%d]\n", pInputOrderAction->OrderActionRef);
		printf("\tRequestID [%d]\n", pInputOrderAction->RequestID);
		printf("\tFrontID [%d]\n", pInputOrderAction->FrontID);
		printf("\tSessionID [%d]\n", pInputOrderAction->SessionID);
		printf("\tVolumeChange [%d]\n", pInputOrderAction->VolumeChange);
		printf("\tActionFlag [%c]\n", pInputOrderAction->ActionFlag);
		printf("\tLimitPrice [%.8lf]\n", pInputOrderAction->LimitPrice);
	}
	if (pRspInfo)
	{
		printf("\tErrorMsg [%s]\n", pRspInfo->ErrorMsg);
		printf("\tErrorID [%d]\n", pRspInfo->ErrorID);
	}
	printf("\tnRequestID [%d]\n", nRequestID);
	printf("\tbIsLast [%d]\n", bIsLast);
	printf("</OnRspOrderAction>\n");
}

inline void CCTPTrade::OnErrRtnOrderInsert(CThostFtdcInputOrderField * pInputOrder, CThostFtdcRspInfoField * pRspInfo)
{
	printf("<OnErrRtnOrderInsert>\n");
	if (pInputOrder)
	{
		printf("\tBrokerID [%s]\n", pInputOrder->BrokerID);
		printf("\tInvestorID [%s]\n", pInputOrder->InvestorID);
		printf("\tInstrumentID [%s]\n", pInputOrder->InstrumentID);
		printf("\tOrderRef [%s]\n", pInputOrder->OrderRef);
		printf("\tUserID [%s]\n", pInputOrder->UserID);
		printf("\tCombOffsetFlag [%s]\n", pInputOrder->CombOffsetFlag);
		printf("\tCombHedgeFlag [%s]\n", pInputOrder->CombHedgeFlag);
		printf("\tGTDDate [%s]\n", pInputOrder->GTDDate);
		printf("\tBusinessUnit [%s]\n", pInputOrder->BusinessUnit);
		printf("\tExchangeID [%s]\n", pInputOrder->ExchangeID);
		printf("\tInvestUnitID [%s]\n", pInputOrder->InvestUnitID);
		printf("\tAccountID [%s]\n", pInputOrder->AccountID);
		printf("\tCurrencyID [%s]\n", pInputOrder->CurrencyID);
		printf("\tClientID [%s]\n", pInputOrder->ClientID);
		printf("\tIPAddress [%s]\n", pInputOrder->IPAddress);
		printf("\tMacAddress [%s]\n", pInputOrder->MacAddress);
		printf("\tVolumeTotalOriginal [%d]\n", pInputOrder->VolumeTotalOriginal);
		printf("\tMinVolume [%d]\n", pInputOrder->MinVolume);
		printf("\tIsAutoSuspend [%d]\n", pInputOrder->IsAutoSuspend);
		printf("\tRequestID [%d]\n", pInputOrder->RequestID);
		printf("\tUserForceClose [%d]\n", pInputOrder->UserForceClose);
		printf("\tIsSwapOrder [%d]\n", pInputOrder->IsSwapOrder);
		printf("\tOrderPriceType [%c]\n", pInputOrder->OrderPriceType);
		printf("\tDirection [%c]\n", pInputOrder->Direction);
		printf("\tTimeCondition [%c]\n", pInputOrder->TimeCondition);
		printf("\tVolumeCondition [%c]\n", pInputOrder->VolumeCondition);
		printf("\tContingentCondition [%c]\n", pInputOrder->ContingentCondition);
		printf("\tForceCloseReason [%c]\n", pInputOrder->ForceCloseReason);
		printf("\tLimitPrice [%.8lf]\n", pInputOrder->LimitPrice);
		printf("\tStopPrice [%.8lf]\n", pInputOrder->StopPrice);
	}
	if (pRspInfo)
	{
		printf("\tErrorMsg [%s]\n", pRspInfo->ErrorMsg);
		printf("\tErrorID [%d]\n", pRspInfo->ErrorID);
	}
	printf("</OnErrRtnOrderInsert>\n");
}

inline void CCTPTrade::OnErrRtnOrderAction(CThostFtdcOrderActionField * pOrderAction, CThostFtdcRspInfoField * pRspInfo)
{
	printf("<OnErrRtnOrderAction>\n");
	if (pOrderAction)
	{
		printf("\tBrokerID [%s]\n", pOrderAction->BrokerID);
		printf("\tInvestorID [%s]\n", pOrderAction->InvestorID);
		printf("\tOrderRef [%s]\n", pOrderAction->OrderRef);
		printf("\tExchangeID [%s]\n", pOrderAction->ExchangeID);
		printf("\tOrderSysID [%s]\n", pOrderAction->OrderSysID);
		printf("\tActionDate [%s]\n", pOrderAction->ActionDate);
		printf("\tActionTime [%s]\n", pOrderAction->ActionTime);
		printf("\tTraderID [%s]\n", pOrderAction->TraderID);
		printf("\tOrderLocalID [%s]\n", pOrderAction->OrderLocalID);
		printf("\tActionLocalID [%s]\n", pOrderAction->ActionLocalID);
		printf("\tParticipantID [%s]\n", pOrderAction->ParticipantID);
		printf("\tClientID [%s]\n", pOrderAction->ClientID);
		printf("\tBusinessUnit [%s]\n", pOrderAction->BusinessUnit);
		printf("\tUserID [%s]\n", pOrderAction->UserID);
		printf("\tStatusMsg [%s]\n", pOrderAction->StatusMsg);
		printf("\tInstrumentID [%s]\n", pOrderAction->InstrumentID);
		printf("\tBranchID [%s]\n", pOrderAction->BranchID);
		printf("\tInvestUnitID [%s]\n", pOrderAction->InvestUnitID);
		printf("\tIPAddress [%s]\n", pOrderAction->IPAddress);
		printf("\tMacAddress [%s]\n", pOrderAction->MacAddress);
		printf("\tOrderActionRef [%d]\n", pOrderAction->OrderActionRef);
		printf("\tRequestID [%d]\n", pOrderAction->RequestID);
		printf("\tFrontID [%d]\n", pOrderAction->FrontID);
		printf("\tSessionID [%d]\n", pOrderAction->SessionID);
		printf("\tVolumeChange [%d]\n", pOrderAction->VolumeChange);
		printf("\tInstallID [%d]\n", pOrderAction->InstallID);
		printf("\tActionFlag [%c]\n", pOrderAction->ActionFlag);
		printf("\tOrderActionStatus [%c]\n", pOrderAction->OrderActionStatus);
		printf("\tLimitPrice [%.8lf]\n", pOrderAction->LimitPrice);
	}
	if (pRspInfo)
	{
		printf("\tErrorMsg [%s]\n", pRspInfo->ErrorMsg);
		printf("\tErrorID [%d]\n", pRspInfo->ErrorID);
	}
	printf("</OnErrRtnOrderAction>\n");
}

bool CCTPTrade::IsErrorRspInfo(CThostFtdcRspInfoField * pRspInfo)
{
	// 如果ErrorID != 0, 说明收到了错误的响应
	bool bResult = pRspInfo && (pRspInfo->ErrorID != 0);
	if (bResult) {
		char buff[1024];
		sprintf(buff, "CTPTrade:(AccountID:%s):%s", account, pRspInfo->ErrorMsg);
		LOG4CXX_ERROR(logger_ctptrade, buff);
	}
	return bResult;
}

inline int CCTPTrade::ReqAuthenticate()
{
	CThostFtdcReqAuthenticateField req;
	memset(&req, 0, sizeof(req));
	strcpy(req.BrokerID, broker);
	strcpy(req.UserID, account);
	strcpy(req.AppID, appid);
	strcpy(req.AuthCode, auth_code);
	//strcpy(req.UserProductInfo, prod_info); // 传ProductInfo会验证失败

	int iRet = this->m_ThostTradeApi->ReqAuthenticate(&req, m_RequestId++);
	char buff[1024];
	sprintf(buff, "CTPTrade:ReqAuthenticate:(UserID:%s, BrokerID:%s) %s (%d)", req.UserID, req.BrokerID, iRet == 0 ? "Success" : "failure", iRet);
	LOG4CXX_INFO(logger_ctptrade, buff);

	return iRet;
}

int CCTPTrade::ReqUserLogin()
{
	CThostFtdcReqUserLoginField req;
	memset(&req, 0, sizeof(req));
	strcpy(req.BrokerID, broker);
	strcpy(req.UserID, account);
	strcpy(req.Password, password);

	char EncryptedPwd[32] = { 0 };
	int len = strlen(password);
	for (int i = 0; i < len; i++)
	{
		EncryptedPwd[i] = '*';
	}

	int iRet = this->m_ThostTradeApi->ReqUserLogin(&req, m_RequestId++);
	char buff[1024];
	sprintf(buff, "CTPTrade:ReqUserLogin:(UserID:%s, BrokerID:%s, Passowrd:%s) %s (%d)", req.UserID, req.BrokerID, EncryptedPwd, iRet == 0 ? "Success" : "failure", iRet);
	LOG4CXX_INFO(logger_ctptrade, buff);

	return iRet;
}

inline int CCTPTrade::TestOrder()
{
	for (int i = 0; i < 100; i++)
	{
		std::string instrumentID;
		std::string limit_price;
		std::string direction;
		std::string open_close;

		printf("请输入合约(instrumentID):\n");
		std::cin >> instrumentID;
		printf("请输入方向(direction 0:buy 1:sell):\n");
		std::cin >> direction;
		printf("请输入开平(open_close 0:open 1:close):\n");
		std::cin >> open_close;
		printf("请输入价格(limitprice):\n");
		std::cin >> limit_price;

		CThostFtdcInputOrderField ord = { 0 };
		//sprintf_s(ord.OrderRef, "%d", ++OrderRef);
		strcpy_s(ord.BrokerID, broker);
		strcpy_s(ord.InvestorID, account);
		strcpy_s(ord.InstrumentID, instrumentID.c_str());
		strcpy_s(ord.UserID, account);
		ord.OrderPriceType = THOST_FTDC_OPT_LimitPrice;
		ord.CombOffsetFlag[0] = open_close.compare("0") == 0 ? THOST_FTDC_OF_Open : THOST_FTDC_OF_Close;
		ord.Direction = direction.compare("0")==0? THOST_FTDC_D_Buy:THOST_FTDC_D_Sell;//买卖
		ord.CombHedgeFlag[0] = THOST_FTDC_HF_Speculation; // 投机
		ord.LimitPrice = atof(limit_price.c_str());
		ord.VolumeTotalOriginal = 1;
		ord.TimeCondition = THOST_FTDC_TC_GFD;//当日有效
		ord.VolumeCondition = THOST_FTDC_VC_AV;//任意数量
		ord.MinVolume = 1;
		ord.ContingentCondition = THOST_FTDC_CC_Immediately;
		//ord.StopPrice = 0;
		ord.ForceCloseReason = THOST_FTDC_FCC_NotForceClose;
		ord.IsAutoSuspend = 0;
		ord.UserForceClose = 0;
		strcpy_s(ord.ExchangeID, "SHFE");
		int ret = m_ThostTradeApi->ReqOrderInsert(&ord, m_RequestId++);
		(ret == 0) ? printf("报单操作请求......发送成功\n") : printf("报单操作请求......发送失败，序号=[%d]\n");

		Sleep(5000);

		CThostFtdcInputOrderActionField a = { 0 };
		strcpy_s(a.BrokerID, broker);
		strcpy_s(a.InvestorID, account);
		a.OrderActionRef = 1;
		strcpy_s(a.OrderRef, g_chOrderRef);
		//a.FrontID = g_chFrontID;
		//a.SessionID = g_chSessionID;
		strcpy_s(a.ExchangeID, "SHFE");
		strcpy_s(a.InstrumentID, instrumentID.c_str());
		strcpy_s(a.OrderSysID, g_chOrderSysID);
		a.ActionFlag = THOST_FTDC_AF_Delete;
		strcpy_s(a.UserID, account);
		ret = m_ThostTradeApi->ReqOrderAction(&a, m_RequestId++);
		(ret == 0) ? printf("撤单操作请求......发送成功\n") : printf("撤单操作请求......发送失败，序号=[%d]\n");

		Sleep(1000);

	}

	return 0;
}

inline void CCTPTrade::Start()
{
	// 先启动CTPMD，以便查询合约后订阅行情时，CTPMD已经登录
	m_CtpMd = new CCTPMd();
	m_CtpMd->Start();

	std::string configFileName("Server.ini");
	IniFile ini(configFileName);

	// ctp配置
	char frontAddr[128];
	ini.read_profile_string("ctp", "trade_frontAddr", frontAddr, sizeof(frontAddr), nullptr, configFileName.c_str());
	ini.read_profile_string("ctp", "broker", broker, sizeof(broker), nullptr, configFileName.c_str());
	ini.read_profile_string("ctp", "account", account, sizeof(account), nullptr, configFileName.c_str());
	ini.read_profile_string("ctp", "password", password, sizeof(password), nullptr, configFileName.c_str());
	ini.read_profile_string("ctp", "appid", appid, sizeof(appid), nullptr, configFileName.c_str());
	ini.read_profile_string("ctp", "auth_code", auth_code, sizeof(auth_code), nullptr, configFileName.c_str());
	ini.read_profile_string("ctp", "prod_info", prod_info, sizeof(prod_info), nullptr, configFileName.c_str());

	// 初始化UserApi
	m_ThostTradeApi = CThostFtdcTraderApi::CreateFtdcTraderApi();		// 创建UserApi
	LOG4CXX_INFO(logger_ctptrade, m_ThostTradeApi->GetApiVersion());
	m_ThostTradeApi->RegisterSpi(this);									// 注册事件类
	m_ThostTradeApi->SubscribePublicTopic(THOST_TERT_QUICK);			// 注册公有流
	m_ThostTradeApi->SubscribePrivateTopic(THOST_TERT_QUICK);			// 注册私有流
	m_ThostTradeApi->RegisterFront(frontAddr);							// 设置交易前置
	m_ThostTradeApi->Init();

	char buff[1024];
	sprintf(buff, "CTPTrade:Starting...");
	LOG4CXX_INFO(logger_ctptrade, buff);
	
	//Sleep(10000);
	//TestOrder();
}

inline void CCTPTrade::Subscribe(std::string ticker)
{
	if (m_CtpMd == nullptr) return;

	MUTEX_LOCK(&m_mtxInstrument);
	for (auto iter = m_Instruments.begin(); iter != m_Instruments.end(); iter++)
	{
		CThostFtdcInstrumentField* pInstrumentField = &*iter;
		if (pInstrumentField == nullptr || strlen(pInstrumentField->InstrumentID) <= 0) continue;

		int pos = ticker.find(".");
		if (pos > 0)
		{
			ticker = ticker.substr(0, pos);
		}

		if (_stricmp(ticker.c_str(), pInstrumentField->InstrumentID) == 0)
		{
			m_CtpMd->SubscribeMarketData(pInstrumentField->InstrumentID, pInstrumentField->ExchangeID);
		}
	}
	MUTEX_UNLOCK(&m_mtxInstrument);
}
