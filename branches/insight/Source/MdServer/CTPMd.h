#pragma once
#include "Common.h"
#include "Publisher.h"
#include "IniFile.h"
#include "MdData.h"
#include "ThostFtdcMdApi.h"
#include "ThostFtdcUserApiStruct.h"

static log4cxx::LoggerPtr logger_ctpmd = log4cxx::Logger::getLogger("syslog");
class CCTPMd : public CThostFtdcMdSpi
{
public:
	CCTPMd();
	~CCTPMd(void);

	///错误应答
	virtual void OnRspError(CThostFtdcRspInfoField *pRspInfo,
		int nRequestID, bool bIsLast);

	///当客户端与交易后台通信连接断开时，该方法被调用。当发生这个情况后，API会自动重新连接，客户端可不做处理。
	///@param nReason 错误原因
	///        0x1001 网络读失败
	///        0x1002 网络写失败
	///        0x2001 接收心跳超时
	///        0x2002 发送心跳失败
	///        0x2003 收到错误报文
	virtual void OnFrontDisconnected(int nReason);

	///心跳超时警告。当长时间未收到报文时，该方法被调用。
	///@param nTimeLapse 距离上次接收报文的时间
	virtual void OnHeartBeatWarning(int nTimeLapse);

	///当客户端与交易后台建立起通信连接时（还未登录前），该方法被调用。
	virtual void OnFrontConnected();

	///登录请求响应
	virtual void OnRspUserLogin(CThostFtdcRspUserLoginField *pRspUserLogin, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);

	///订阅行情应答
	virtual void OnRspSubMarketData(CThostFtdcSpecificInstrumentField *pSpecificInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);

	///取消订阅行情应答
	//virtual void OnRspUnSubMarketData(CThostFtdcSpecificInstrumentField *pSpecificInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);

	///深度行情通知
	virtual void OnRtnDepthMarketData(CThostFtdcDepthMarketDataField *pDepthMarketData);

	///请求查询组播合约响应
	//virtual void OnRspQryMulticastInstrument(CThostFtdcMulticastInstrumentField *pMulticastInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);

private:
	CThostFtdcMdApi* m_ThostMdApi;
	int counter;
	int m_RequestId;
	char account[32];
	char password[32];
	char broker[11];
	//char appid[32];
	//char auth_code[32];
	//char prod_info[32];
	std::map<std::string, CThostFtdcInstrumentField> m_Instruments;

	bool IsErrorRspInfo(CThostFtdcRspInfoField *pRspInfo);
	void SubscribeMarketData();
	
public:
	void Start();
	int ReqUserLogin();
	//int ReqQryMulticastInstrument();
	void SubscribeMarketData(std::vector<CThostFtdcInstrumentField> instrumentList);
	void SubscribeMarketData(std::string instrument, std::string exchange);
};

CCTPMd::CCTPMd() : counter(0), m_RequestId(0), m_ThostMdApi(nullptr)
{
}

CCTPMd::~CCTPMd(void)
{
	if (m_ThostMdApi != nullptr)
	{
		this->m_ThostMdApi->RegisterSpi(NULL);
		this->m_ThostMdApi->Release();
		this->m_ThostMdApi = NULL;
	}
}

void CCTPMd::OnRspError(CThostFtdcRspInfoField * pRspInfo, int nRequestID, bool bIsLast)
{
	IsErrorRspInfo(pRspInfo);
}

void CCTPMd::OnFrontDisconnected(int nReason)
{
	char buff[1024];
	sprintf(buff, "CTPMd:OnFrontDisconnected:%d", nReason);
	LOG4CXX_ERROR(logger_ctpmd, buff);
}

void CCTPMd::OnHeartBeatWarning(int nTimeLapse)
{
}

void CCTPMd::OnFrontConnected()
{
	char buff[1024];
	sprintf(buff, "CTPMd:OnFrontConnected");
	LOG4CXX_INFO(logger_ctpmd, buff);

	ReqUserLogin();
}

void CCTPMd::OnRspUserLogin(CThostFtdcRspUserLoginField * pRspUserLogin, CThostFtdcRspInfoField * pRspInfo, int nRequestID, bool bIsLast)
{
	if (!IsErrorRspInfo(pRspInfo))
	{
		// 重连的情况，重新订阅以前的合约
		SubscribeMarketData();

		// 查询合约
		//ReqQryMulticastInstrument();
	}
	else
	{
		char buff[1024];
		sprintf(buff, "OnRspUserLogin(UserID:%s):%s", pRspUserLogin->UserID, pRspInfo->ErrorMsg);
		LOG4CXX_ERROR(logger_ctpmd, buff);
	}
}

void CCTPMd::OnRspSubMarketData(CThostFtdcSpecificInstrumentField * pSpecificInstrument, CThostFtdcRspInfoField * pRspInfo, int nRequestID, bool bIsLast)
{
	char buff[1024];
	sprintf(buff, "CTPMd:%s订阅成功", pSpecificInstrument->InstrumentID);
	LOG4CXX_INFO(logger_ctpmd, buff);
}

std::string ConvertExchangeID(char* exchangeID)
{
	// 转换CTPExchangeID成宏汇市场后缀
	if (_stricmp(exchangeID, "CFFEX") == 0)
	{
		return "CF";
	}
	else if (_stricmp(exchangeID, "SHFE") == 0)
	{
		return "SHF";
	}
	else if (_stricmp(exchangeID, "CZCE") == 0)
	{
		return "CZC";
	}
	else if (_stricmp(exchangeID, "DCE") == 0)
	{
		return "DCE";
	}
	else if (_stricmp(exchangeID, "INE") == 0)
	{
		return "INE";
	}
	else
	{
		return "Unkown";
	}
}

void CCTPMd::OnRtnDepthMarketData(CThostFtdcDepthMarketDataField * pDepthMarketData)
{
	auto iter = m_Instruments.find(pDepthMarketData->InstrumentID);
	if (iter == m_Instruments.end()) return; // 未知的合约	

	std::shared_ptr<FutureQuote> quote(new FutureQuote());

	std::string strUppder = pDepthMarketData->InstrumentID;
	std::transform(strUppder.begin(), strUppder.end(), strUppder.begin(), ::toupper);

	char szWindCode[64];
	char* exchageId = strlen(pDepthMarketData->ExchangeID) == 0 ? iter->second.ExchangeID : pDepthMarketData->ExchangeID; // pDepthMarketData中ExchangeID为空，则从map里取
	sprintf(szWindCode, "%s.%s", strUppder.c_str(), ConvertExchangeID(exchageId).c_str());
	quote->szWindCode = szWindCode;
	//printf("szWindCode:%s, ExchangeID:%s\n", szWindCode, iter->second.ExchangeID);
	quote->szCode = pDepthMarketData->InstrumentID;
	quote->nActionDay = atoi(pDepthMarketData->ActionDay);
	quote->nTradingDay = atoi(pDepthMarketData->TradingDay);

	//if (strcmp(iter->second.ExchangeID, "CZCE")==0) printf("szWindCode:%s, ExchangeID:%s\n", szWindCode, iter->second.ExchangeID);


	std::string hh = std::string(pDepthMarketData->UpdateTime).substr(0, 2);
	std::string mm = std::string(pDepthMarketData->UpdateTime).substr(3, 2);
	std::string ss = std::string(pDepthMarketData->UpdateTime).substr(6, 2);

	std::string time = hh + mm + ss;
	quote->nTime = atoi(time.c_str()) * 1000 + pDepthMarketData->UpdateMillisec;
	quote->nStatus = 0; // 0:未设置
	quote->iPreOpenInterest = pDepthMarketData->PreOpenInterest;
	quote->nPreClose = pDepthMarketData->PreClosePrice * 10000;
	quote->nPreSettlePrice = pDepthMarketData->PreSettlementPrice * 10000;
	quote->nOpen = pDepthMarketData->OpenPrice * 10000;
	quote->nHigh = pDepthMarketData->HighestPrice * 10000;
	quote->nLow = pDepthMarketData->LowestPrice * 10000;
	quote->nMatch = pDepthMarketData->LastPrice * 10000;
	quote->iVolume = pDepthMarketData->Volume;
	quote->iTurnover = pDepthMarketData->Turnover;
	quote->iOpenInterest = pDepthMarketData->OpenInterest;
	quote->nClose = pDepthMarketData->ClosePrice * 10000;
	quote->nSettlePrice = pDepthMarketData->SettlementPrice * 10000;
	quote->nHighLimited = pDepthMarketData->UpperLimitPrice * 10000;
	quote->nLowLimited = pDepthMarketData->LowerLimitPrice * 10000;
	quote->nPreDelta = pDepthMarketData->PreDelta;
	quote->nCurrDelta = pDepthMarketData->CurrDelta;

	//for (int jj = 0; jj < 5; jj++)
	{
		quote->nAskPrice.push_back(pDepthMarketData->AskPrice1 * 10000);
		quote->nAskPrice.push_back(pDepthMarketData->AskPrice2 * 10000);
		quote->nAskPrice.push_back(pDepthMarketData->AskPrice3 * 10000);
		quote->nAskPrice.push_back(pDepthMarketData->AskPrice4 * 10000);
		quote->nAskPrice.push_back(pDepthMarketData->AskPrice5 * 10000);
		quote->nAskVol.push_back(pDepthMarketData->AskVolume1);
		quote->nAskVol.push_back(pDepthMarketData->AskVolume2);
		quote->nAskVol.push_back(pDepthMarketData->AskVolume3);
		quote->nAskVol.push_back(pDepthMarketData->AskVolume4);
		quote->nAskVol.push_back(pDepthMarketData->AskVolume5);
		quote->nBidPrice.push_back(pDepthMarketData->BidPrice1 * 10000);
		quote->nBidPrice.push_back(pDepthMarketData->BidPrice2 * 10000);
		quote->nBidPrice.push_back(pDepthMarketData->BidPrice3 * 10000);
		quote->nBidPrice.push_back(pDepthMarketData->BidPrice4 * 10000);
		quote->nBidPrice.push_back(pDepthMarketData->BidPrice5 * 10000);
		quote->nBidVol.push_back(pDepthMarketData->BidVolume1);
		quote->nBidVol.push_back(pDepthMarketData->BidVolume2);
		quote->nBidVol.push_back(pDepthMarketData->BidVolume3);
		quote->nBidVol.push_back(pDepthMarketData->BidVolume4);
		quote->nBidVol.push_back(pDepthMarketData->BidVolume5);
	}
#ifndef TDFAPI30
	quote->lAuctionPrice = 0;  // CTP找不到对应字段
	quote->lAuctionQty = 0;	   // CTP找不到对应字段
	quote->lAvgPrice = pDepthMarketData->AveragePrice * 100000;
#endif // !TDFAPI30

	if (++counter % 1000 == 0)
	{
		counter = 0; // reset counter
		LOG4CXX_INFO(logger_ctpmd, "ctp md receiving...");
		char buff[256];
		sprintf(buff, "%s %d %d %d", szWindCode, quote->nTime, quote->nMatch, quote->nStatus);
		LOG4CXX_INFO(logger_ctpmd, buff);
	}
	Publisher::Instance()->Add(quote);
}

//inline void CCTPMd::OnRspQryMulticastInstrument(CThostFtdcMulticastInstrumentField * pMulticastInstrument, CThostFtdcRspInfoField * pRspInfo, int nRequestID, bool bIsLast)
//{
//	if (pMulticastInstrument)
//	{
//		CThostFtdcInstrumentField instrumentField = {0};
//		strcpy(instrumentField.InstrumentID, pMulticastInstrument->InstrumentID);
//
//		m_Instruments[instrumentField.InstrumentID] = instrumentField;
//	}
//
//}

int CCTPMd::ReqUserLogin()
{
	CThostFtdcReqUserLoginField req;
	memset(&req, 0, sizeof(req));
	strcpy(req.BrokerID, broker);
	strcpy(req.UserID, account);
	strcpy(req.Password, password);

	char EncryptedPwd[32] = { 0 };
	int len = strlen(password);
	for (int i = 0; i < len; i++)
	{
		EncryptedPwd[i] = '*';
	}

	int iRet = this->m_ThostMdApi->ReqUserLogin(&req, 0);
	char buff[1024];
	sprintf(buff, "CTPMd:ReqUserLogin:(UserID:%s, BrokerID:%s, Passowrd:%s) %s (iRet=%d)", req.UserID, req.BrokerID, EncryptedPwd, iRet == 0 ? "Success" : "failure", iRet);
	LOG4CXX_INFO(logger_ctpmd, buff);

	return iRet;
}

//int CCTPMd::ReqQryMulticastInstrument()
//{
//	CThostFtdcQryMulticastInstrumentField req;
//	memset(&req, 0, sizeof(req));
//
//	//req.TopicID = 1001;
//
//	int iRet = this->m_ThostMdApi->ReqQryMulticastInstrument(&req, 0);
//	char buff[1024];
//	sprintf(buff, "CTPMd:ReqQryMulticastInstrument:(TopicID:%d, InstrumentID:%s) %s (iRet=%d)", req.TopicID, req.InstrumentID, iRet == 0 ? "Success" : "failure", iRet);
//	LOG4CXX_INFO(logger_ctpmd, buff);
//
//	return iRet;
//}

bool CCTPMd::IsErrorRspInfo(CThostFtdcRspInfoField * pRspInfo)
{
	// 如果ErrorID != 0, 说明收到了错误的响应
	bool bResult = pRspInfo && (pRspInfo->ErrorID != 0);
	if (bResult) {
		char buff[1024];
		sprintf(buff, "CTPMd:(AccountID:%s):%s", account, pRspInfo->ErrorMsg);
		LOG4CXX_ERROR(logger_ctpmd, buff);
	}
	return bResult;
}

void CCTPMd::SubscribeMarketData()
{
	std::vector<CThostFtdcInstrumentField> vSubField;
	for (auto iter = m_Instruments.begin(); iter != m_Instruments.end(); iter++)
	{
		vSubField.push_back(iter->second);
	}
	SubscribeMarketData(vSubField);
}

inline void CCTPMd::Start()
{
	std::string configFileName("Server.ini");
	IniFile ini(configFileName);

	// ctp配置
	char frontAddr[128];
	ini.read_profile_string("ctp", "md_frontAddr", frontAddr, sizeof(frontAddr), nullptr, configFileName.c_str());
	ini.read_profile_string("ctp", "broker", broker, sizeof(broker), nullptr, configFileName.c_str());
	ini.read_profile_string("ctp", "account", account, sizeof(account), nullptr, configFileName.c_str());
	ini.read_profile_string("ctp", "password", password, sizeof(password), nullptr, configFileName.c_str());
	//ini.read_profile_string("ctp", "appid", appid, sizeof(appid), nullptr, configFileName.c_str());
	//ini.read_profile_string("ctp", "auth_code", auth_code, sizeof(auth_code), nullptr, configFileName.c_str());
	//ini.read_profile_string("ctp", "prod_info", prod_info, sizeof(prod_info), nullptr, configFileName.c_str());

	// 初始化UserApi
	m_ThostMdApi = CThostFtdcMdApi::CreateFtdcMdApi();				// 创建MdApi
	this->m_ThostMdApi->RegisterSpi(this);							// 注册事件类
	this->m_ThostMdApi->RegisterFront(frontAddr);					// 设置交易前置
	this->m_ThostMdApi->Init();										// connect

	char buff[1024];
	sprintf(buff, "CTPMd:Starting...");
	LOG4CXX_INFO(logger_ctpmd, buff);
}

void CCTPMd::SubscribeMarketData(std::vector<CThostFtdcInstrumentField> instrumentList)
{
	if (m_ThostMdApi == nullptr) return;
	int nSize = instrumentList.size();
	if (nSize == 0) return;
	char **ppInstrumentID = new char *[nSize];
	int i = 0;
	std::string strSubs = "CTPMD:Prepare to sub-";
	for (auto iter = instrumentList.begin(); iter != instrumentList.end(); iter++, i++)
	{
		CThostFtdcInstrumentField instrumentField = *iter;
		ppInstrumentID[i] = new char[32];
		memset(ppInstrumentID[i], 0, 32);
		strcpy(ppInstrumentID[i], instrumentField.InstrumentID);

		m_Instruments[instrumentField.InstrumentID] = instrumentField;
		strSubs.append(instrumentField.InstrumentID);
		strSubs.append("|");
	}

	LOG4CXX_INFO(logger_ctpmd, strSubs.c_str());

	// 保存合约信息，以便断线重新订阅
	int iResult = m_ThostMdApi->SubscribeMarketData(ppInstrumentID, nSize);
	char buff[1024];
	sprintf(buff, "CTPMd:SubscribeMarketData %s", (iResult == 0) ? "Success" : "failure");
	LOG4CXX_INFO(logger_ctpmd, buff);

	// 清空指针
	for (int i = 0; i < nSize; i++)
		delete ppInstrumentID[i];
	delete[] ppInstrumentID;

	return;
}

void CCTPMd::SubscribeMarketData(std::string instrument, std::string exchange)
{
	if (m_ThostMdApi == nullptr) return;
	if (instrument.length() == 0) return;
	char **ppInstrumentID = new char *[1];
	CThostFtdcInstrumentField instrumentField;
	strcpy(instrumentField.InstrumentID, instrument.c_str());
	strcpy(instrumentField.ExchangeID, exchange.c_str());
	ppInstrumentID[0] = new char[32];
	memset(ppInstrumentID[0], 0, 32);
	strcpy(ppInstrumentID[0], instrument.c_str());

	m_Instruments[instrumentField.InstrumentID] = instrumentField;

	// 保存合约信息，以便断线重新订阅
	int iResult = m_ThostMdApi->SubscribeMarketData(ppInstrumentID, 1);
	char buff[1024];
	sprintf(buff, "CTPMd:SubscribeMarketData %s %s", instrument.c_str(), (iResult == 0) ? "Success" : "failure");
	LOG4CXX_INFO(logger_ctpmd, buff);

	// 清空指针
	delete ppInstrumentID[0];
	delete[] ppInstrumentID;

	return;
}