#pragma once
#include <memory>
#include <vector>
#include <map>
#include <set>
#include "zmq.hpp"
#include "MdApiData.h"
#include "AutoResetEvent.h"

using namespace std;

#ifdef _WIN32
#define CALLBACK __stdcall
#else
#define CALLBACK
#endif

// ����ص�
typedef void(CALLBACK *StockQuoteCallback)(MdApi::StockQuote*);
typedef void(CALLBACK *IndexQuoteCallback)(MdApi::IndexQuote*);
typedef void(CALLBACK *FutureQuoteCallback)(MdApi::FutureQuote*);
typedef void(CALLBACK *TransactionCallback)(MdApi::Transaction*);
typedef void(CALLBACK *OrderCallback)(MdApi::Order*);
typedef void(CALLBACK *OrderQueueCallback)(MdApi::OrderQueue*);
//typedef void(CALLBACK *CodeTableCallback)(MdApi::CodeTable*);

class MdApiImp
{
public:
	MdApiImp();
	~MdApiImp();

	bool Connect();
	void Dispose();
	bool SubscribeList(vector<string>& subList);
	bool Subscribe(const char* sub);
	bool UnSubscribeList(vector<string>& subList);
	bool UnSubscribe(const char* sub);

	shared_ptr<MdApi::StockQuote> GetLatestStockQuote(const char* szWindCode);
	shared_ptr<MdApi::IndexQuote> GetLatestIndexQuote(const char* szWindCode);
	shared_ptr<MdApi::FutureQuote> GetLatestFutureQuote(const char* szWindCode);
	shared_ptr<MdApi::Transaction> GetLatestTransactionQuote(const char* szWindCode);
	shared_ptr<MdApi::Order> GetLatestOrder(const char* szWindCode);
	shared_ptr<MdApi::OrderQueue> GetLatestOrderQueue(const char* szWindCode);

	void GetLatestData(MdApi::DataBase* pData);
	void GetLatestDatas(MdApi::DataBase** pDatas, int num);

	//bool GetLatestStockQuote(vector<string>& szWindCodeList, vector<shared_ptr<MdApi::StockQuote>>& vecRtn, string& errMsg);
	//bool GetLatestIndexQuote(vector<string>& szWindCodeList, vector<shared_ptr<MdApi::IndexQuote>>& vecRtn, string& errMsg);
	//bool GetLatestFutureQuote(vector<string>& szWindCodeList, vector<shared_ptr<MdApi::FutureQuote>>& vecRtn, string& errMsg);
	//bool GetLatestTransactionQuote(vector<string>& szWindCodeList, vector<shared_ptr<MdApi::Transaction>>& vecRtn, string& errMsg);
	//bool GetLatestOrder(vector<string>& szWindCodeList, vector<shared_ptr<MdApi::Order>>& vecRtn, string& errMsg);
	//bool GetLatestOrderQueue(vector<string>& szWindCodeList, vector<shared_ptr<MdApi::OrderQueue>>& vecRtn, string& errMsg);

	void SetStockQuoteCallback(StockQuoteCallback cb);
	void SetIndexQuoteCallback(IndexQuoteCallback cb);
	void SetFutureQuoteCallback(FutureQuoteCallback cb);
	void SetTransactionCallback(TransactionCallback cb);
	void SetOrderCallback(OrderCallback cb);
	void SetOrderQueueCallback(OrderQueueCallback cb);
	//void SetCodeTableCallback(CodeTableCallback cb);

private:
	zmq::context_t* _ZmqContext;
	zmq::socket_t* _Subscriber;
	char MaketDataServerPort[32];
	AutoResetEvent _AutoEvent;
	void _ThreadProc(void);
	bool _Connected;
	long _LastSrvStartTime;
	MUTEX_T m_mtxSubbedList;
	std::set<std::string> _subbedList;

	//map<string, shared_ptr<MdApi::StockQuote>> _StockMap;
	//MUTEX_T _StockLock;
	//map<string, shared_ptr<MdApi::IndexQuote>> _IndexMap;
	//MUTEX_T _IndexLock;
	//map<string, shared_ptr<MdApi::FutureQuote>> _FutureMap;
	//MUTEX_T _FutureLock;
	//map<string, shared_ptr<MdApi::Transaction>> _TransactionMap;
	//MUTEX_T _TransactionLock;
	//map<string, shared_ptr<MdApi::Order>> _OrderMap;
	//MUTEX_T _OrderLock;
	//map<string, shared_ptr<MdApi::OrderQueue>> _OrderQueueMap;
	//MUTEX_T _OrderQueueLock;

	StockQuoteCallback _StockQuoteCallback;
	IndexQuoteCallback _IndexQuoteCallback;
	FutureQuoteCallback _FutureQuoteCallback;
	TransactionCallback _TransactionCallback;
	OrderCallback _OrderCallback;
	OrderQueueCallback _OrderQueueCallback;
	//CodeTableCallback _CodeTableCallback;

	MUTEX_T m_mtxStockQuote;
	MUTEX_T m_mtxFutureQuote;
	MUTEX_T m_mtxIndexQuote;
	MUTEX_T m_mtxOrder;
	MUTEX_T m_mtxOrderQueue;
	MUTEX_T m_mtxTransaction;
	std::map<std::string, std::shared_ptr<MdApi::StockQuote>> lastStockQuoteMap;
	std::map<std::string, std::shared_ptr<MdApi::FutureQuote>> lastFutureQuoteMap;
	std::map<std::string, std::shared_ptr<MdApi::IndexQuote>> lastIndexQuoteMap;
	std::map<std::string, std::shared_ptr<MdApi::Order>> lastStockOrderMap;
	std::map<std::string, std::shared_ptr<MdApi::OrderQueue>> lastStockOrderQueueMap;
	std::map<std::string, std::shared_ptr<MdApi::Transaction>> lastStockTransactionMap;
};

