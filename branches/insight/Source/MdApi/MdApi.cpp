#include "MdApi.h"
#include "MdApiImp.h"

static MdApiImp* _imp = nullptr;
MdApiImp* ImpInstance()
{
	if (_imp == nullptr)
	{
		_imp = new MdApiImp;
	}
	return _imp;
}

HZ_MD_SRV_API bool HZSTDCALL HzMdSrvApi_Connect()
{
	return ImpInstance()->Connect();
}

HZ_MD_SRV_API void HZSTDCALL HzMdSrvApi_Dispose()
{
	ImpInstance()->Dispose();
	delete _imp;
	_imp = nullptr;
}

HZ_MD_SRV_API void HZSTDCALL SetStockQuoteCallback( StockQuoteCallback cb )
{
	ImpInstance()->SetStockQuoteCallback(cb);
}

HZ_MD_SRV_API void HZSTDCALL SetIndexQuoteCallback( IndexQuoteCallback cb )
{
	ImpInstance()->SetIndexQuoteCallback(cb);
}

HZ_MD_SRV_API void HZSTDCALL SetFutureQuoteCallback( FutureQuoteCallback cb )
{
	ImpInstance()->SetFutureQuoteCallback(cb);
}

HZ_MD_SRV_API void HZSTDCALL SetTransactionCallback( TransactionCallback cb )
{
	ImpInstance()->SetTransactionCallback(cb);
}

HZ_MD_SRV_API void HZSTDCALL SetOrderCallback( OrderCallback cb )
{
	ImpInstance()->SetOrderCallback(cb);
}

HZ_MD_SRV_API void HZSTDCALL SetOrderQueueCallback( OrderQueueCallback cb )
{
	ImpInstance()->SetOrderQueueCallback(cb);
}

//HZ_MD_SRV_API void HZSTDCALL SetCodeTableCallback(CodeTableCallback cb)
//{
//	ImpInstance()->SetCodeTableCallback(cb);
//}

HZ_MD_SRV_API bool HZSTDCALL HzMdSrvApi_SubscribeList( vector<string>& subList )
{
	return ImpInstance()->SubscribeList(subList);
}

HZ_MD_SRV_API bool HZSTDCALL HzMdSrvApi_Subscribe( const char* sub )
{
	return ImpInstance()->Subscribe(sub);
}

HZ_MD_SRV_API bool HZSTDCALL HzMdSrvApi_UnSubscribeList(vector<string>& subList)
{
	return ImpInstance()->UnSubscribeList(subList);
}

//HZ_MD_SRV_API int HZSTDCALL HzMdSrvApi_UnSubscribeList( vector<string>& subList )
//{
//	return ImpInstance()->UnSubscribeList(subList);
//}

HZ_MD_SRV_API bool HZSTDCALL HzMdSrvApi_UnSubscribe( const char* sub )
{
	return ImpInstance()->UnSubscribe(sub);
}

HZ_MD_SRV_API void HZSTDCALL GetLatestData(MdApi::DataBase * pData)
{
	return ImpInstance()->GetLatestData(pData);
}

HZ_MD_SRV_API void HZSTDCALL GetLatestDatas(MdApi::DataBase ** pDatas, int num)
{
	return ImpInstance()->GetLatestDatas(pDatas, num);
}

//HZ_MD_SRV_API int HZSTDCALL GetLatestStockQuoteList( vector<string>& szWindCodeList, vector<shared_ptr<MdApi::StockQuote>>& vecRtn, string& errMsg )
//{
//	return ImpInstance()->GetLatestStockQuote(szWindCodeList, vecRtn, errMsg);
//}
//
//HZ_MD_SRV_API int HZSTDCALL GetLatestIndexQuoteList( vector<string>& szWindCodeList, vector<shared_ptr<MdApi::IndexQuote>>& vecRtn, string& errMsg )
//{
//	return ImpInstance()->GetLatestIndexQuote(szWindCodeList, vecRtn, errMsg);
//}
//
//HZ_MD_SRV_API int HZSTDCALL GetLatestFutureQuoteList( vector<string>& szWindCodeList, vector<shared_ptr<MdApi::FutureQuote>>& vecRtn, string& errMsg )
//{
//	return ImpInstance()->GetLatestFutureQuote(szWindCodeList, vecRtn, errMsg);
//}
//
//HZ_MD_SRV_API int HZSTDCALL GetLatestTransactionList( vector<string>& szWindCodeList, vector<shared_ptr<MdApi::Transaction>>& vecRtn, string& errMsg )
//{
//	return ImpInstance()->GetLatestTransaction(szWindCodeList, vecRtn, errMsg);
//}
//
//HZ_MD_SRV_API int HZSTDCALL GetLatestOrderList( vector<string>& szWindCodeList, vector<shared_ptr<MdApi::Order>>& vecRtn, string& errMsg )
//{
//	return ImpInstance()->GetLatestOrder(szWindCodeList, vecRtn, errMsg);
//}
//
//HZ_MD_SRV_API int HZSTDCALL GetLatestOrderQueueList( vector<string>& szWindCodeList, vector<shared_ptr<MdApi::OrderQueue>>& vecRtn, string& errMsg )
//{
//	return ImpInstance()->GetLatestOrderQueue(szWindCodeList, vecRtn, errMsg);
//}
//
//HZ_MD_SRV_API int HZSTDCALL GetLatestStockQuote( MdApi::StockQuote* pData )
//{
//	if (pData == nullptr) return false;
//	return (pData = ImpInstance()->GetLatestStockQuote(pData->szWindCode).get()) == nullptr? false : true;
//}
//
//HZ_MD_SRV_API int HZSTDCALL GetLatestIndexQuote(MdApi::IndexQuote* pData )
//{
//	if (pData == nullptr) return false;
//	return (pData = ImpInstance()->GetLatestIndexQuote(pData->szWindCode).get()) == nullptr ? false : true;
//}
//
//HZ_MD_SRV_API int HZSTDCALL GetLatestFutureQuote(MdApi::FutureQuote* pData)
//{
//	if (pData == nullptr) return false;
//	return (pData = ImpInstance()->GetLatestFutureQuote(pData->szWindCode).get()) == nullptr ? false : true;
//}
//
//HZ_MD_SRV_API MdApi::Transaction* HZSTDCALL GetLatestTransactionQuote( const char* szWindCode )
//{
//	return ImpInstance()->GetLatestTransactionQuote(szWindCode).get();
//}
//
//HZ_MD_SRV_API MdApi::Order* HZSTDCALL GetLatestOrderQuote( const char* szWindCode )
//{
//	return ImpInstance()->GetLatestOrderQuote(szWindCode).get();
//}
//
//HZ_MD_SRV_API MdApi::OrderQueue* HZSTDCALL GetLatestOrderQueueQuote( const char* szWindCode )
//{
//	return ImpInstance()->GetLatestOrderQueueQuote(szWindCode).get();
//}
