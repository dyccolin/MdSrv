#include <thread>
#include "MdApiImp.h"
#include "MdData.h"
#include "ApiIniFile.h"
#include "MutexHelper.h"
#include "LicenseHelper.h"
#ifdef _WIN32
#pragma comment(lib, "User32.lib")
#include <time.h>
#include"windows.h"

#include <fstream>  
#define sprintf sprintf_s
#endif // WIN32

#include <log4cxx/logger.h>
#include <log4cxx/propertyconfigurator.h>

using namespace log4cxx;
using namespace log4cxx::helpers;

static log4cxx::LoggerPtr logger = log4cxx::Logger::getLogger("MdApi");
using namespace zmq;

MdApiImp::MdApiImp()
	: _Connected(false)
	, _Subscriber(NULL)
	, _StockQuoteCallback(NULL)
	, _IndexQuoteCallback(NULL)
	, _FutureQuoteCallback(NULL)
	, _TransactionCallback(NULL)
	, _OrderCallback(NULL)
	, _OrderQueueCallback(NULL)
	//, _CodeTableCallback(NULL)
	, _LastSrvStartTime(0)
{
	//MUTEX_INIT(&_StockLock);
	//MUTEX_INIT(&_IndexLock);
	//MUTEX_INIT(&_FutureLock);
	//MUTEX_INIT(&_TransactionLock);
	//MUTEX_INIT(&_OrderLock);
	//MUTEX_INIT(&_OrderQueueLock);

	MUTEX_INIT(&m_mtxSubbedList);
	MUTEX_INIT(&m_mtxStockQuote);
	MUTEX_INIT(&m_mtxFutureQuote);
	MUTEX_INIT(&m_mtxIndexQuote);
	MUTEX_INIT(&m_mtxOrder);
	MUTEX_INIT(&m_mtxOrderQueue);
	MUTEX_INIT(&m_mtxTransaction);

	PropertyConfigurator::configure("./MdApi.log4cxx");
	LOG4CXX_INFO(logger, "MdApi starting...");
}

MdApiImp::~MdApiImp()
{
	//MUTEX_DESTROY(&_StockLock);
	//MUTEX_DESTROY(&_IndexLock);
	//MUTEX_DESTROY(&_FutureLock);
	//MUTEX_DESTROY(&_TransactionLock);
	//MUTEX_DESTROY(&_OrderLock);
	//MUTEX_DESTROY(&_OrderQueueLock);

	MUTEX_DESTROY(&m_mtxSubbedList);
	MUTEX_DESTROY(&m_mtxStockQuote);
	MUTEX_DESTROY(&m_mtxFutureQuote);
	MUTEX_DESTROY(&m_mtxIndexQuote);
	MUTEX_DESTROY(&m_mtxOrder);
	MUTEX_DESTROY(&m_mtxOrderQueue);
	MUTEX_DESTROY(&m_mtxTransaction);
}

bool MdApiImp::Connect()
{
	if (_Connected) return true;

	CApiIniFile iniFile;
	if (strlen(iniFile.MaketDataServerPort) == 0)
	{
		printf("MaketDataServerPort can't be empty!\n");
		return false;
	}

#ifndef _WIN32

#else
	LicenseHelper licenseHlp;
	std::string decryptedText = licenseHlp.readCipher();
	//if (decryptedText.find("any") == -1 && decryptedText.find("Any") == -1)
	//{
	//	if (decryptedText.find(sMac) == -1)
	//	{
	//		result.ErrCode = SRV_ERR_CODE_LICENSE_CHECK_FAILED;
	//		result.ErrMsg = "授权验证失败：未获得授权";
	//		return false;
	//	}
	//}

	if (decryptedText.empty())
	{
		MessageBoxA(NULL, "授权验证失败：授权文件不存在，请更新！", "ERROR", MB_OK);
		exit(0);
	}
	
	int sub_pos = decryptedText.rfind("_");
	int str_len = decryptedText.length();
	if (sub_pos < 0 || sub_pos + 1 > str_len)
	{
		MessageBoxA(NULL, "授权验证失败：授权文件格式错误，请更新！", "ERROR", MB_OK);
		exit(0);
	}
	string auth_end_day = decryptedText.substr(sub_pos + 1, str_len - sub_pos - 1);
	if (strlen(auth_end_day.c_str()) != 8)
	{
		MessageBoxA(NULL, "授权验证失败：授权文件格式错误，请更新！", "ERROR", MB_OK);
		exit(0);
	}

	time_t timep;
	struct tm *p;
	time(&timep);
	p = localtime(&timep); /*取得当地时间*/
	char today[32];
	sprintf(today, "%04d%02d%02d", 1900 + p->tm_year, 1 + p->tm_mon, p->tm_mday);
	int nAuthEndDay = atoi(auth_end_day.c_str());
	if (atoi(today) > nAuthEndDay)
	{
		MessageBoxA(NULL, "授权验证失败：超过授权期限，请更新！", "WARNING", MB_OK);
		exit(0);
	}
#endif

	try
	{
		_ZmqContext = new context_t(1);
		_Subscriber = new socket_t(*_ZmqContext, ZMQ_SUB);

		//_Subscriber->setsockopt(ZMQ_SUBSCRIBE, DataType::eStockQuote);
		//_Subscriber->setsockopt(ZMQ_SUBSCRIBE, DataType::eFutureQuote);
		//_Subscriber->setsockopt(ZMQ_SUBSCRIBE, DataType::eIndexQuote);
		//_Subscriber->setsockopt(ZMQ_SUBSCRIBE, DataType::eTransaction);
		//_Subscriber->setsockopt(ZMQ_SUBSCRIBE, DataType::eOrder);
		//_Subscriber->setsockopt(ZMQ_SUBSCRIBE, DataType::eOrderQueue);
		//_Subscriber->setsockopt(ZMQ_SUBSCRIBE, DataType::eCodeTable);
		const char* codetable = "codetable";
		_Subscriber->setsockopt(ZMQ_SUBSCRIBE, codetable, strlen(codetable));

		const char* heartbeat = "heartbeat";
		_Subscriber->setsockopt(ZMQ_SUBSCRIBE, heartbeat, strlen(heartbeat));

		_Subscriber->setsockopt(ZMQ_RCVHWM, 0);
		_Subscriber->setsockopt(ZMQ_SNDHWM, 0);
		//  Configure socket to not wait at close time
		int linger = 0;
		_Subscriber->setsockopt(ZMQ_LINGER, &linger, sizeof(linger));

		_Subscriber->connect(iniFile.MaketDataServerPort);
	}
	catch (const std::exception& ex)
	{
		printf("%s\n", ex.what());
		return false;
	}

	printf("zmq connect %s successful!\n", iniFile.MaketDataServerPort);

	// 线程启动前必须先把Connected置为true
	_Connected = true;
	std::thread _Thread(std::bind(&MdApiImp::_ThreadProc, this));
	_Thread.detach();

	return true;
}

void MdApiImp::Dispose()
{
	// TODO,判断是否初始化？
	_Connected = false;

	if (_Subscriber != nullptr)
	{
		_Subscriber->close();
	}

	if (_ZmqContext != nullptr)
	{
		_ZmqContext->close();
	}
}

static int counter = 0;

void MdApiImp::_ThreadProc(void)
{
	char logStr[1024];

	std::map<std::pair<std::string, DataType>, long long> m_mapLastMdTime;  // <<Ticker,eDataType>, nTime>
	std::set<std::string> m_CodeTables;

	while (this->_Connected)
	{
		try
		{
			message_t msgTopic;
			message_t msgDataType;
			message_t msgData;
			this->_Subscriber->recv(&msgTopic);
			this->_Subscriber->recv(&msgDataType);
			this->_Subscriber->recv(&msgData);
			//DataType dataType = *msgDataType.data<DataType>();

			//string sTopic = string(static_cast<char*>(msgTopic.data()), msgTopic.size());
			DataType eDataType = *msgDataType.data<DataType>();
			string sMsgData = string(static_cast<char*>(msgData.data()), msgData.size());

			//printf("MdApi recv(topic:%s eDataType:%s)\n", strTopic.c_str(), strDataType.c_str());

			::msgpack::unpacked unpacked;
			::msgpack::unpack(unpacked, sMsgData.data(), sMsgData.size());
			auto obj = unpacked.get();

			switch (eDataType)
			{
			case eStockQuote:
				{
					StockQuote* data = new StockQuote();
					obj.convert_if_not_nil(data);

					// 过滤重复的时间戳，防止LVC重复推送，仅股票、期货、指数有效
					long long nDayTime = data->nActionDay * 1000000000 + data->nTime; // 考虑跨日
					auto pair = std::make_pair(data->szWindCode, eDataType);
					if (m_mapLastMdTime.find(pair) != m_mapLastMdTime.end() && m_mapLastMdTime[pair] >= nDayTime) continue;
					m_mapLastMdTime[pair] = nDayTime;

					//shared_ptr<MdApi::StockQuote> sharedPtr = std::make_shared<MdApi::StockQuote>(*(MdApi::StockQuote*)data.get());
					shared_ptr<MdApi::StockQuote> sharedPtr = std::make_shared<MdApi::StockQuote>();
					memcpy(sharedPtr->szWindCode, data->szWindCode.c_str(), sizeof(sharedPtr->szWindCode));
					memcpy(sharedPtr->szCode, data->szCode.c_str(), sizeof(sharedPtr->szCode));
					memcpy(sharedPtr->szCNName, data->szCNName.c_str(), sizeof(sharedPtr->szCNName));
					sharedPtr->nActionDay = data->nActionDay;
					sharedPtr->nTradingDay = data->nTradingDay;
					sharedPtr->nTime = data->nTime;
					sharedPtr->nStatus = data->nStatus;

					sharedPtr->nPreClose = data->nPreClose;
					sharedPtr->nOpen = data->nOpen;
					sharedPtr->nHigh = data->nHigh;
					sharedPtr->nLow = data->nLow;
					sharedPtr->nMatch = data->nMatch;

					int size = data->nAskPrice.size();
					for (int jj = 0; jj < size; jj++)
					{
						sharedPtr->nAskPrice[jj] = data->nAskPrice[jj];
					}

					size = data->nAskVol.size();
					for (int jj = 0; jj < size; jj++)
					{
						sharedPtr->nAskVol[jj] = data->nAskVol[jj];
					}

					size = data->nBidPrice.size();
					for (int jj = 0; jj < size; jj++)
					{
						sharedPtr->nBidPrice[jj] = data->nBidPrice[jj];
					}

					size = data->nBidVol.size();
					for (int jj = 0; jj < size; jj++)
					{
						sharedPtr->nBidVol[jj] = data->nBidVol[jj];
					}

					sharedPtr->nNumTrades = data->nNumTrades;
					sharedPtr->iVolume = data->iVolume;
					sharedPtr->iTurnover = data->iTurnover;
					sharedPtr->nTotalBidVol = data->nTotalBidVol;
					sharedPtr->nTotalAskVol = data->nTotalAskVol;

					sharedPtr->nWeightedAvgBidPrice = data->nWeightedAvgBidPrice;
					sharedPtr->nWeightedAvgAskPrice = data->nWeightedAvgAskPrice;
					sharedPtr->nIOPV = data->nIOPV;
					sharedPtr->nYieldToMaturity = data->nYieldToMaturity;
					sharedPtr->nHighLimited = data->nHighLimited;
					sharedPtr->nLowLimited = data->nLowLimited;
					memcpy(sharedPtr->chPrefix, data->chPrefix.c_str(), sizeof(sharedPtr->chPrefix));

					sharedPtr->nSyl1 = data->nSyl1;
					sharedPtr->nSyl2 = data->nSyl2;
					sharedPtr->nSD2 = data->nSD2;

					//MUTEX_LOCK(&this->_StockLock);
					//this->_StockMap[data->szWindCode] = sharedPtr;
					//MUTEX_UNLOCK(&this->_StockLock);

					delete data;


					MUTEX_LOCK(&m_mtxStockQuote);
					if (lastStockQuoteMap[sharedPtr->szWindCode] == nullptr || lastStockQuoteMap[sharedPtr->szWindCode]->different(*sharedPtr.get()))
					{
						if (this->_StockQuoteCallback != NULL && _subbedList.find(sharedPtr->szWindCode) != _subbedList.end())
							this->_StockQuoteCallback(sharedPtr.get());
					}
					else
					{
						sprintf(logStr, "Ignore equals quote:%s", sharedPtr->szWindCode);
						LOG4CXX_WARN(logger, logStr);
					}

					lastStockQuoteMap[sharedPtr->szWindCode] = sharedPtr;
					MUTEX_UNLOCK(&m_mtxStockQuote);
				}
				break;
			case eFutureQuote:
				{
					FutureQuote* data = new FutureQuote();
					obj.convert_if_not_nil(data);

					//// only for test
					//char logStr[1024];
					//sprintf_s(logStr, "%s %d %.2f bid:[%.2f, %d] ask:[%.2f, %d]", data->szWindCode.c_str(), data->nTime, data->nMatch / 10000.0,
					//	data->nBidPrice[0] / 10000.0, data->nBidVol[0], data->nAskPrice[0] / 10000.0, data->nAskVol[0]);
					//LOG4CXX_INFO(logger, logStr);

					// 过滤重复的时间戳，防止LVC重复推送，仅股票、期货、指数有效
					long long nDayTime = data->nActionDay * 1000000000 + data->nTime; // 考虑跨日
					auto pair = std::make_pair(data->szWindCode, eDataType);
					if (m_mapLastMdTime.find(pair) != m_mapLastMdTime.end() && m_mapLastMdTime[pair] >= nDayTime) continue;
					m_mapLastMdTime[pair] = nDayTime;

					//shared_ptr<MdApi::FutureQuote> sharedPtr = std::make_shared<MdApi::FutureQuote>(*(MdApi::FutureQuote*)data.get());
					shared_ptr<MdApi::FutureQuote> sharedPtr = std::make_shared<MdApi::FutureQuote>();
					memcpy(sharedPtr->szWindCode, data->szWindCode.c_str(), sizeof(sharedPtr->szWindCode));
					memcpy(sharedPtr->szCode, data->szCode.c_str(), sizeof(sharedPtr->szCode));
					memcpy(sharedPtr->szCNName, data->szCNName.c_str(), sizeof(sharedPtr->szCNName));
					sharedPtr->nActionDay = data->nActionDay;
					sharedPtr->nTradingDay = data->nTradingDay;
					sharedPtr->nTime = data->nTime;
					sharedPtr->nStatus = data->nStatus;
					sharedPtr->iPreOpenInterest = data->iPreOpenInterest;
					sharedPtr->nPreClose = data->nPreClose;
					sharedPtr->nPreSettlePrice = data->nPreSettlePrice;
					sharedPtr->nOpen = data->nOpen;
					sharedPtr->nHigh = data->nHigh;
					sharedPtr->nLow = data->nLow;
					sharedPtr->nMatch = data->nMatch;

					sharedPtr->iVolume = data->iVolume;
					sharedPtr->iTurnover = data->iTurnover;
					sharedPtr->iOpenInterest = data->iOpenInterest;
					sharedPtr->nClose = data->nClose;
					sharedPtr->nSettlePrice = data->nSettlePrice;
					sharedPtr->nHighLimited = data->nHighLimited;
					sharedPtr->nLowLimited = data->nLowLimited;
					sharedPtr->nPreDelta = data->nPreDelta;
					sharedPtr->nCurrDelta = data->nCurrDelta;

					int size = data->nAskPrice.size();
					for (int jj = 0; jj < size; jj++)
					{
						sharedPtr->nAskPrice[jj] = data->nAskPrice[jj];
					}

					size = data->nAskVol.size();
					for (int jj = 0; jj < size; jj++)
					{
						sharedPtr->nAskVol[jj] = data->nAskVol[jj];
					}

					size = data->nBidPrice.size();
					for (int jj = 0; jj < size; jj++)
					{
						sharedPtr->nBidPrice[jj] = data->nBidPrice[jj];
					}

					size = data->nBidVol.size();
					for (int jj = 0; jj < size; jj++)
					{
						sharedPtr->nBidVol[jj] = data->nBidVol[jj];
					}

					sharedPtr->lAuctionPrice = data->lAuctionPrice;
					sharedPtr->lAuctionQty = data->lAuctionQty;
					sharedPtr->lAvgPrice = data->lAvgPrice;
					
					//MUTEX_LOCK(&this->_FutureLock);
					//this->_FutureMap[data->szWindCode] = sharedPtr;
					//MUTEX_UNLOCK(&this->_FutureLock);

					delete data;

					MUTEX_LOCK(&m_mtxFutureQuote);
					if (lastFutureQuoteMap[sharedPtr->szWindCode] == nullptr || lastFutureQuoteMap[sharedPtr->szWindCode]->different(*sharedPtr.get()))
					{
						if (this->_FutureQuoteCallback != NULL && _subbedList.find(sharedPtr->szWindCode) != _subbedList.end())
							this->_FutureQuoteCallback(sharedPtr.get());
					}
					else
					{
						sprintf(logStr, "Ignore equals quote:%s", sharedPtr->szWindCode);
						LOG4CXX_WARN(logger, logStr);
					}

					lastFutureQuoteMap[sharedPtr->szWindCode] = sharedPtr;
					MUTEX_UNLOCK(&m_mtxFutureQuote);
				}
				break;
			case eIndexQuote:
				{
					IndexQuote* data = new IndexQuote();
					obj.convert_if_not_nil(data);

					// 过滤重复的时间戳，防止LVC重复推送，仅股票、期货、指数有效
					long long nDayTime = data->nActionDay * 1000000000 + data->nTime; // 考虑跨日
					auto pair = std::make_pair(data->szWindCode, eDataType);
					if (m_mapLastMdTime.find(pair) != m_mapLastMdTime.end() && m_mapLastMdTime[pair] >= nDayTime) continue;
					m_mapLastMdTime[pair] = nDayTime;

					//shared_ptr<MdApi::IndexQuote> sharedPtr = std::make_shared<MdApi::IndexQuote>(*(MdApi::IndexQuote*)data.get());
					shared_ptr<MdApi::IndexQuote> sharedPtr = std::make_shared<MdApi::IndexQuote>();
					memcpy(sharedPtr->szWindCode, data->szWindCode.c_str(), sizeof(sharedPtr->szWindCode));
					memcpy(sharedPtr->szCode, data->szCode.c_str(), sizeof(sharedPtr->szCode));
					memcpy(sharedPtr->szCNName, data->szCNName.c_str(), sizeof(sharedPtr->szCNName));
					sharedPtr->nActionDay = data->nActionDay;
					sharedPtr->nTradingDay = data->nTradingDay;
					sharedPtr->nTime = data->nTime;

					sharedPtr->nOpenIndex = data->nOpenIndex;
					sharedPtr->nHighIndex = data->nHighIndex;
					sharedPtr->nLowIndex = data->nLowIndex;
					sharedPtr->nLastIndex = data->nLastIndex;
					sharedPtr->iTotalVolume = data->iTotalVolume;
					sharedPtr->iTurnover = data->iTurnover;
					sharedPtr->nPreCloseIndex = data->nPreCloseIndex;
					
					//MUTEX_LOCK(&this->_IndexLock);
					//this->_IndexMap[data->szWindCode] = sharedPtr;
					//MUTEX_UNLOCK(&this->_IndexLock);

					delete data;

					MUTEX_LOCK(&m_mtxIndexQuote);
					if (lastIndexQuoteMap[sharedPtr->szWindCode] == nullptr || lastIndexQuoteMap[sharedPtr->szWindCode]->different(*sharedPtr.get()))
					{
						if (this->_IndexQuoteCallback != NULL&& _subbedList.find(sharedPtr->szWindCode) != _subbedList.end())
							this->_IndexQuoteCallback(sharedPtr.get());
					}
					else
					{
						sprintf(logStr, "Ignore equals quote:%s", sharedPtr->szWindCode);
						LOG4CXX_WARN(logger, logStr);
					}

					lastIndexQuoteMap[sharedPtr->szWindCode] = sharedPtr;
					MUTEX_UNLOCK(&m_mtxIndexQuote);
				}
				break;
			case eTransaction:
				{
					Transaction* data = new Transaction();
					obj.convert_if_not_nil(data);

					//int nDayTime = /*data->nActionDay * 1000000000 +*/ data->nTime; // 暂不考虑跨日
					//auto pair = std::make_pair(data->szWindCode, eDataType);
					//if (m_mapLastMdTime.find(pair) != m_mapLastMdTime.end() && m_mapLastMdTime[pair] >= nDayTime) continue;
					//m_mapLastMdTime[pair] = nDayTime;

					//shared_ptr<MdApi::Transaction> sharedPtr = std::make_shared<MdApi::Transaction>(*(MdApi::Transaction*)data.get());
					shared_ptr<MdApi::Transaction> sharedPtr = std::make_shared<MdApi::Transaction>();
					memcpy(sharedPtr->szWindCode, data->szWindCode.c_str(), sizeof(sharedPtr->szWindCode));
					memcpy(sharedPtr->szCode, data->szCode.c_str(), sizeof(sharedPtr->szCode));
					memcpy(sharedPtr->szCNName, data->szCNName.c_str(), sizeof(sharedPtr->szCNName));
					sharedPtr->nActionDay = data->nActionDay;
					sharedPtr->nTime = data->nTime;

					sharedPtr->nIndex = data->nIndex;

					sharedPtr->nPrice = data->nPrice;
					sharedPtr->nVolume = data->nVolume;
					sharedPtr->nTurnover = data->nTurnover;

					sharedPtr->nBSFlag = data->nBSFlag;
					sharedPtr->chOrderKind = data->chOrderKind;
					sharedPtr->chFunctionCode = data->chFunctionCode;

					sharedPtr->nAskOrder = data->nAskOrder;
					sharedPtr->nBidOrder = data->nBidOrder;
					
					//MUTEX_LOCK(&this->_TransactionLock);
					//this->_TransactionMap[data->szWindCode] = sharedPtr;
					//MUTEX_UNLOCK(&this->_TransactionLock);

					delete data;

					MUTEX_LOCK(&m_mtxTransaction);
					if (lastStockTransactionMap[sharedPtr->szWindCode] == nullptr || lastStockTransactionMap[sharedPtr->szWindCode]->different(*sharedPtr.get()))
					{
						if (this->_TransactionCallback != NULL)
							this->_TransactionCallback(sharedPtr.get());
					}
					else
					{
						sprintf(logStr, "Ignore equals quote:%s", sharedPtr->szWindCode);
						LOG4CXX_WARN(logger, logStr);
					}

					lastStockTransactionMap[sharedPtr->szWindCode] = sharedPtr;
					MUTEX_UNLOCK(&m_mtxTransaction);
				}
				break;
			case eOrder:
				{
					Order* data = new Order();
					obj.convert_if_not_nil(data);

					//int nDayTime = /*data->nActionDay * 1000000000*/ + data->nTime; // 暂不考虑跨日
					//auto pair = std::make_pair(data->szWindCode, eDataType);
					//if (m_mapLastMdTime.find(pair) != m_mapLastMdTime.end() && m_mapLastMdTime[pair] >= nDayTime) continue;
					//m_mapLastMdTime[pair] = nDayTime;

					//shared_ptr<MdApi::Order> sharedPtr = std::make_shared<MdApi::Order>(*(MdApi::Order*)data.get());
					shared_ptr<MdApi::Order> sharedPtr = std::make_shared<MdApi::Order>();
					memcpy(sharedPtr->szWindCode, data->szWindCode.c_str(), sizeof(sharedPtr->szWindCode));
					memcpy(sharedPtr->szCode, data->szCode.c_str(), sizeof(sharedPtr->szCode));
					memcpy(sharedPtr->szCNName, data->szCNName.c_str(), sizeof(sharedPtr->szCNName));
					sharedPtr->nActionDay = data->nActionDay;
					sharedPtr->nTime = data->nTime;

					sharedPtr->nOrder = data->nOrder;
					sharedPtr->nPrice = data->nPrice;
					sharedPtr->nVolume = data->nVolume;
					sharedPtr->chOrderKind = data->chOrderKind;
					sharedPtr->chFunctionCode = data->chFunctionCode;
					
					//MUTEX_LOCK(&this->_OrderLock);
					//this->_OrderMap[data->szWindCode] = sharedPtr;
					//MUTEX_UNLOCK(&this->_OrderLock);

					delete data;

					MUTEX_LOCK(&m_mtxOrder);
					if (lastStockOrderMap[sharedPtr->szWindCode] == nullptr || lastStockOrderMap[sharedPtr->szWindCode]->different(*sharedPtr.get()))
					{
						if (this->_OrderCallback != NULL)
							this->_OrderCallback(sharedPtr.get());
					}
					else
					{
						sprintf(logStr, "Ignore equals quote:%s", sharedPtr->szWindCode);
						LOG4CXX_WARN(logger, logStr);
					}

					lastStockOrderMap[sharedPtr->szWindCode] = sharedPtr;
					MUTEX_UNLOCK(&m_mtxOrder);
				}
				break;
			case eOrderQueue:
				{
					OrderQueue* data = new OrderQueue();
					obj.convert_if_not_nil(data);

					//int nDayTime = /*data->nActionDay * 1000000000*/ + data->nTime; // 暂不考虑跨日
					//auto pair = std::make_pair(data->szWindCode, eDataType);
					//if (m_mapLastMdTime.find(pair) != m_mapLastMdTime.end() && m_mapLastMdTime[pair] >= nDayTime) continue;
					//m_mapLastMdTime[pair] = nDayTime;

					//shared_ptr<MdApi::OrderQueue> sharedPtr = std::make_shared<MdApi::OrderQueue>(*(MdApi::OrderQueue*)data.get());
					shared_ptr<MdApi::OrderQueue> sharedPtr = std::make_shared<MdApi::OrderQueue>();
					memcpy(sharedPtr->szWindCode, data->szWindCode.c_str(), sizeof(sharedPtr->szWindCode));
					memcpy(sharedPtr->szCode, data->szCode.c_str(), sizeof(sharedPtr->szCode));
					memcpy(sharedPtr->szCNName, data->szCNName.c_str(), sizeof(sharedPtr->szCNName));
					sharedPtr->nActionDay = data->nActionDay;
					sharedPtr->nTime = data->nTime;

					sharedPtr->nSide = data->nSide;
					sharedPtr->nPrice = data->nPrice;
					sharedPtr->nOrders = data->nOrders;
					sharedPtr->nABItems = data->nABItems;

					int size = data->nABVolume.size();
					if (size > 200) size = 200;
					for (int jj = 0; jj < size; jj++)
					{
						sharedPtr->nABVolume[jj] = data->nABVolume[jj];
					}
					
					//MUTEX_LOCK(&this->_OrderQueueLock);
					//this->_OrderQueueMap[data->szWindCode] = sharedPtr;
					//MUTEX_UNLOCK(&this->_OrderQueueLock);

					delete data;

					MUTEX_LOCK(&m_mtxOrderQueue);
					if (lastStockOrderQueueMap[sharedPtr->szWindCode] == nullptr || lastStockOrderQueueMap[sharedPtr->szWindCode]->different(*sharedPtr.get()))
					{
						if (this->_OrderQueueCallback != NULL)
							this->_OrderQueueCallback(sharedPtr.get());
					}
					else
					{
						sprintf(logStr, "Ignore equals quote:%s", sharedPtr->szWindCode);
						LOG4CXX_WARN(logger, logStr);
					}

					lastStockOrderQueueMap[sharedPtr->szWindCode] = sharedPtr;
					MUTEX_UNLOCK(&m_mtxOrderQueue);
				}
				break;
			//case eCodeTable:
			//	{
			//		CodeTable* data = new CodeTable();
			//		obj.convert_if_not_nil(data);

			//		//int nDayTime = /*data->nActionDay * 1000000000*/ + data->nTime; // 暂不考虑跨日
			//		//auto pair = std::make_pair(data->szWindCode, eDataType);
			//		//if (m_mapLastMdTime.find(pair) != m_mapLastMdTime.end() && m_mapLastMdTime[pair] >= nDayTime) continue;
			//		//m_mapLastMdTime[pair] = nDayTime;

			//		//shared_ptr<MdApi::OrderQueue> sharedPtr = std::make_shared<MdApi::OrderQueue>(*(MdApi::OrderQueue*)data.get());
			//		shared_ptr<MdApi::CodeTable> sharedPtr = std::make_shared<MdApi::CodeTable>();
			//		memcpy(sharedPtr->szWindCode, data->szWindCode.c_str(), sizeof(sharedPtr->szWindCode));
			//		memcpy(sharedPtr->szCode, data->szCode.c_str(), sizeof(sharedPtr->szCode));
			//		memcpy(sharedPtr->szMarket, data->szMarket.c_str(), sizeof(sharedPtr->szMarket));
			//		memcpy(sharedPtr->szENName, data->szENName.c_str(), sizeof(sharedPtr->szENName));
			//		memcpy(sharedPtr->szCNName, data->szCNName.c_str(), sizeof(sharedPtr->szCNName));
			//		sharedPtr->nType = data->nType;

			//		//MUTEX_LOCK(&this->_OrderQueueLock);
			//		//this->_OrderQueueMap[data->szWindCode] = sharedPtr;
			//		//MUTEX_UNLOCK(&this->_OrderQueueLock);

			//		delete data;

			//		if (this->_CodeTableCallback != NULL)
			//		{
			//			// 检查是否是已经推送过的codetable(多Client会造成重复推送)
			//			if (m_CodeTables.find(sharedPtr->szWindCode) == m_CodeTables.end())
			//			{
			//				this->_CodeTableCallback(sharedPtr.get());
			//				m_CodeTables.insert(sharedPtr->szWindCode);
			//			}
			//		}
			//	}
			//	break;
			case eHeartbeat:
				{
					Heartbeat* data = new Heartbeat();
					obj.convert_if_not_nil(data);

					//int nDayTime = /*data->nActionDay * 1000000000*/ + data->nTime; // 暂不考虑跨日
					//auto pair = std::make_pair(data->szWindCode, eDataType);
					//if (m_mapLastMdTime.find(pair) != m_mapLastMdTime.end() && m_mapLastMdTime[pair] >= nDayTime) continue;
					//m_mapLastMdTime[pair] = nDayTime;

					//shared_ptr<MdApi::OrderQueue> sharedPtr = std::make_shared<MdApi::OrderQueue>(*(MdApi::OrderQueue*)data.get());
					if (this->_LastSrvStartTime != 0 || data->SrvStartTime > _LastSrvStartTime)
					{
						//Server重启，重新订阅

					}

					_LastSrvStartTime = data->SrvStartTime;

					printf("Recv data:heartbeat srvstarttime=%ld\n", data->SrvStartTime);

					//MUTEX_LOCK(&this->_OrderQueueLock);
					//this->_OrderQueueMap[data->szWindCode] = sharedPtr;
					//MUTEX_UNLOCK(&this->_OrderQueueLock);

					delete data;
				}
				break;
			default:
				break;
			}
		}
		catch (exception ex)
		{
			printf("%s\n", ex.what());
		}
	}
}

//bool MdApiImp::GetLatestStockQuote(vector<string>& szWindCodeList, vector<shared_ptr<MdApi::StockQuote>>& vecRtn, string& errMsg)
//{
//	bool bSucceed = true;
//	int nFailedNum = 0;
//	MUTEX_LOCK(&_StockLock); //如何较少lock？？
//	for (vector<string>::iterator iter = szWindCodeList.begin(); iter != szWindCodeList.end(); iter++)
//	{
//		string szWindCode = *iter;
//		auto keyIter = _StockMap.find(szWindCode);
//		if (keyIter != _StockMap.end())
//		{
//			vecRtn.push_back(keyIter->second);
//		}
//		else
//		{
//			vecRtn.push_back(NULL);
//			bSucceed = false;
//			errMsg.append(szWindCode + "|");
//			nFailedNum ++;
//		}
//	}
//	MUTEX_UNLOCK(&_StockLock);
//	if (!bSucceed)
//	{
//		char buf[64];
//		sprintf(buf, "行情获取失败, num:%d, szWindCodeList:", nFailedNum);
//		errMsg = string(buf) + errMsg;
//	}
//	return bSucceed;
//}
//
//bool MdApiImp::GetLatestIndexQuote(vector<string>& szWindCodeList, vector<shared_ptr<MdApi::IndexQuote>>& vecRtn, string& errMsg)
//{
//	bool bSucceed = true;
//	int nFailedNum = 0;
//	MUTEX_LOCK(&_IndexLock); //如何较少lock？？
//	for (vector<string>::iterator iter = szWindCodeList.begin(); iter != szWindCodeList.end(); iter++)
//	{
//		string szWindCode = *iter;
//		auto keyIter = _IndexMap.find(szWindCode);
//		if (keyIter != _IndexMap.end())
//		{
//			vecRtn.push_back(keyIter->second);
//		}
//		else
//		{
//			vecRtn.push_back(NULL);
//			bSucceed = false;
//			errMsg.append(szWindCode + "|");
//			nFailedNum ++;
//		}
//	}
//	MUTEX_UNLOCK(&_IndexLock);
//	if (!bSucceed)
//	{
//		char buf[64];
//		sprintf(buf, "行情获取失败, num:%d, szWindCodeList:", nFailedNum);
//		errMsg = string(buf) + errMsg;
//	}
//	return bSucceed;
//}
//
//bool MdApiImp::GetLatestFutureQuote(vector<string>& szWindCodeList, vector<shared_ptr<MdApi::FutureQuote>>& vecRtn, string& errMsg)
//{
//	bool bSucceed = true;
//	int nFailedNum = 0;
//	MUTEX_LOCK(&_FutureLock); //如何较少lock？？
//	for (vector<string>::iterator iter = szWindCodeList.begin(); iter != szWindCodeList.end(); iter++)
//	{
//		string szWindCode = *iter;
//		auto keyIter = _FutureMap.find(szWindCode);
//		if (keyIter != _FutureMap.end())
//		{
//			vecRtn.push_back(keyIter->second);
//		}
//		else
//		{
//			vecRtn.push_back(NULL);
//			bSucceed = false;
//			errMsg.append(szWindCode + "|");
//			nFailedNum ++;
//		}
//	}
//	MUTEX_UNLOCK(&_FutureLock);
//	if (!bSucceed)
//	{
//		char buf[64];
//		sprintf(buf, "行情获取失败, num:%d, szWindCodeList:", nFailedNum);
//		errMsg = string(buf) + errMsg;
//	}
//	return bSucceed;
//}
//
//bool MdApiImp::GetLatestTransactionQuote(vector<string>& szWindCodeList, vector<shared_ptr<MdApi::Transaction>>& vecRtn, string& errMsg)
//{
//	bool bSucceed = true;
//	int nFailedNum = 0;
//	MUTEX_LOCK(&_TransactionLock); //如何较少lock？？
//	for (vector<string>::iterator iter = szWindCodeList.begin(); iter != szWindCodeList.end(); iter++)
//	{
//		string szWindCode = *iter;
//		auto keyIter = _TransactionMap.find(szWindCode);
//		if (keyIter != _TransactionMap.end())
//		{
//			vecRtn.push_back(keyIter->second);
//		}
//		else
//		{
//			vecRtn.push_back(NULL);
//			bSucceed = false;
//			errMsg.append(szWindCode + "|");
//			nFailedNum ++;
//		}
//	}
//	MUTEX_UNLOCK(&_TransactionLock);
//	if (!bSucceed)
//	{
//		char buf[64];
//		sprintf(buf, "行情获取失败, num:%d, szWindCodeList:", nFailedNum);
//		errMsg = string(buf) + errMsg;
//	}
//	return bSucceed;
//}
//
//bool MdApiImp::GetLatestOrder(vector<string>& szWindCodeList, vector<shared_ptr<MdApi::Order>>& vecRtn, string& errMsg)
//{
//	bool bSucceed = true;
//	int nFailedNum = 0;
//	MUTEX_LOCK(&_OrderLock); //如何较少lock？？
//	for (vector<string>::iterator iter = szWindCodeList.begin(); iter != szWindCodeList.end(); iter++)
//	{
//		string szWindCode = *iter;
//		auto keyIter = _OrderMap.find(szWindCode);
//		if (keyIter != _OrderMap.end())
//		{
//			vecRtn.push_back(keyIter->second);
//		}
//		else
//		{
//			vecRtn.push_back(NULL);
//			bSucceed = false;
//			errMsg.append(szWindCode + "|");
//			nFailedNum ++;
//		}
//	}
//	MUTEX_UNLOCK(&_OrderLock);
//	if (!bSucceed)
//	{
//		char buf[64];
//		sprintf(buf, "行情获取失败, num:%d, szWindCodeList:", nFailedNum);
//		errMsg = string(buf) + errMsg;
//	}
//	return bSucceed;
//}
//
//bool MdApiImp::GetLatestOrderQueue(vector<string>& szWindCodeList, vector<shared_ptr<MdApi::OrderQueue>>& vecRtn, string& errMsg)
//{
//	bool bSucceed = true;
//	int nFailedNum = 0;
//	MUTEX_LOCK(&_OrderQueueLock); //如何较少lock？？
//	for (vector<string>::iterator iter = szWindCodeList.begin(); iter != szWindCodeList.end(); iter++)
//	{
//		// lock??
//		string szWindCode = *iter;
//		auto keyIter = _OrderQueueMap.find(szWindCode);
//		if (keyIter != _OrderQueueMap.end())
//		{
//			vecRtn.push_back(keyIter->second);
//		}
//		else
//		{
//			vecRtn.push_back(NULL);
//			bSucceed = false;
//			errMsg.append(szWindCode + "|");
//			nFailedNum ++;
//		}
//	}
//	MUTEX_UNLOCK(&_OrderQueueLock);
//	if (!bSucceed)
//	{
//		char buf[64];
//		sprintf(buf, "行情获取失败, num:%d, szWindCodeList:", nFailedNum);
//		errMsg = string(buf) + errMsg;
//	}
//	return bSucceed;
//}
//
//shared_ptr<MdApi::StockQuote> MdApiImp::GetLatestStockQuote( const char* szWindCode )
//{
//	shared_ptr<MdApi::StockQuote> pQuote;
//	MUTEX_LOCK(&_StockLock);
//	auto keyIter = _StockMap.find(szWindCode);
//	if (keyIter != _StockMap.end())
//	{
//		pQuote = keyIter->second;
//	}
//	MUTEX_UNLOCK(&_StockLock);
//	return pQuote;
//}
//
//shared_ptr<MdApi::IndexQuote> MdApiImp::GetLatestIndexQuote( const char* szWindCode )
//{
//	shared_ptr<MdApi::IndexQuote> pQuote;
//	MUTEX_LOCK(&_IndexLock);
//	auto keyIter = _IndexMap.find(szWindCode);
//	if (keyIter != _IndexMap.end())
//	{
//		pQuote = keyIter->second;
//	}
//	MUTEX_UNLOCK(&_IndexLock);
//	return pQuote;
//}
//
//shared_ptr<MdApi::FutureQuote> MdApiImp::GetLatestFutureQuote( const char* szWindCode )
//{
//	shared_ptr<MdApi::FutureQuote> pQuote;
//	MUTEX_LOCK(&_FutureLock);
//	auto keyIter = _FutureMap.find(szWindCode);
//	if (keyIter != _FutureMap.end())
//	{
//		pQuote = keyIter->second;
//	}
//	MUTEX_UNLOCK(&_FutureLock);
//	return pQuote;
//}
//
//shared_ptr<MdApi::Transaction> MdApiImp::GetLatestTransactionQuote( const char* szWindCode )
//{
//	shared_ptr<MdApi::Transaction> pQuote;
//	MUTEX_LOCK(&_TransactionLock);
//	auto keyIter = _TransactionMap.find(szWindCode);
//	if (keyIter != _TransactionMap.end())
//	{
//		pQuote = keyIter->second;
//	}
//	MUTEX_UNLOCK(&_TransactionLock);
//	return pQuote;
//}
//
//shared_ptr<MdApi::Order> MdApiImp::GetLatestOrder( const char* szWindCode )
//{
//	shared_ptr<MdApi::Order> pQuote;
//	MUTEX_LOCK(&_OrderLock);
//	auto keyIter = _OrderMap.find(szWindCode);
//	if (keyIter != _OrderMap.end())
//	{
//		pQuote = keyIter->second;
//	}
//	MUTEX_UNLOCK(&_OrderLock);
//	return pQuote;
//}
//
//shared_ptr<MdApi::OrderQueue> MdApiImp::GetLatestOrderQueue( const char* szWindCode )
//{
//	shared_ptr<MdApi::OrderQueue> pQuote;
//	MUTEX_LOCK(&_OrderQueueLock);
//	auto keyIter = _OrderQueueMap.find(szWindCode);
//	if (keyIter != _OrderQueueMap.end())
//	{
//		pQuote = keyIter->second;
//	}
//	MUTEX_UNLOCK(&_OrderQueueLock);
//	return pQuote;
//}

void MdApiImp::GetLatestData(MdApi::DataBase * pData)
{
	if (pData == nullptr) return;

	switch (pData->eType)
	{
	case eStockQuote:
	{
		MUTEX_LOCK(&m_mtxStockQuote);
		auto keyIter = lastStockQuoteMap.find(pData->szWindCode);
		if (keyIter != lastStockQuoteMap.end())
		{
			pData->data = keyIter->second.get();
		}
		MUTEX_UNLOCK(&m_mtxStockQuote);
		break;
	}
	case eIndexQuote:
	{
		MUTEX_LOCK(&m_mtxIndexQuote);
		auto keyIter = lastIndexQuoteMap.find(pData->szWindCode);
		if (keyIter != lastIndexQuoteMap.end())
		{
			pData->data = keyIter->second.get();
		}
		MUTEX_UNLOCK(&m_mtxIndexQuote);
		break;
	}
	case eFutureQuote:
	{
		MUTEX_LOCK(&m_mtxFutureQuote);
		auto keyIter = lastFutureQuoteMap.find(pData->szWindCode);
		if (keyIter != lastFutureQuoteMap.end())
		{
			pData->data = keyIter->second.get();
		}
		MUTEX_UNLOCK(&m_mtxFutureQuote);
		break;
	}
	//case eTransaction:
	//{
	//	MUTEX_LOCK(&m_mtxTransaction);
	//	auto keyIter = lastStockTransactionMap.find(pData->szWindCode);
	//	if (keyIter != lastStockTransactionMap.end())
	//	{
	//		pData->data = keyIter->second.get();
	//	}
	//	MUTEX_UNLOCK(&m_mtxTransaction);
	//	break;
	//}
	//case eOrder:
	//{
	//	MUTEX_LOCK(&m_mtxOrder);
	//	auto keyIter = lastStockOrderMap.find(pData->szWindCode);
	//	if (keyIter != lastStockOrderMap.end())
	//	{
	//		pData->data = keyIter->second.get();
	//	}
	//	MUTEX_UNLOCK(&m_mtxOrder);
	//	break;
	//}
	//case eOrderQueue:
	//{
	//	MUTEX_LOCK(&m_mtxOrderQueue);
	//	auto keyIter = lastStockOrderQueueMap.find(pData->szWindCode);
	//	if (keyIter != lastStockOrderQueueMap.end())
	//	{
	//		pData->data = keyIter->second.get();
	//	}
	//	MUTEX_UNLOCK(&m_mtxOrderQueue);
	//	break;
	//}
	default:
		break;
	}

	return;
}

void MdApiImp::GetLatestDatas(MdApi::DataBase ** pDatas, int num)
{
	if (pDatas == nullptr) return;

	for (size_t i = 0; i < num; i++)
	{
		MdApi::DataBase * pData = pDatas[i];
		GetLatestData(pData);
	}
	
	return;
}

void  MdApiImp::SetTransactionCallback(TransactionCallback cb)
{
	_TransactionCallback = cb;
}
void  MdApiImp::SetOrderCallback(OrderCallback cb)
{
	_OrderCallback = cb;
}
void  MdApiImp::SetOrderQueueCallback(OrderQueueCallback cb)
{
	_OrderQueueCallback = cb;
}

//void MdApiImp::SetCodeTableCallback(CodeTableCallback cb)
//{
//	_CodeTableCallback = cb;
//}

void MdApiImp::SetStockQuoteCallback( StockQuoteCallback cb )
{
	_StockQuoteCallback = cb;
}

void MdApiImp::SetIndexQuoteCallback( IndexQuoteCallback cb )
{
	_IndexQuoteCallback = cb;
}

void MdApiImp::SetFutureQuoteCallback( FutureQuoteCallback cb )
{
	_FutureQuoteCallback = cb;
}

bool MdApiImp::SubscribeList( vector<string>& subList )
{
	if (_Subscriber == nullptr) return false;

	for (vector<string>::iterator iter = subList.begin(); iter != subList.end(); iter++)
	{
		std::string szWindCode = *iter;
		Subscribe(szWindCode.c_str());
	}

	return true;
}

bool MdApiImp::Subscribe( const char* sub )
{
	if (_Subscriber == nullptr || strlen(sub) == 0) return false;
	
	std::string szWindCode = sub;
	transform(szWindCode.begin(), szWindCode.end(), szWindCode.begin(), ::toupper); //将小写都转换成大写

	int pos = szWindCode.find(".");
	if (pos < 0) return false;

	MUTEX_LOCK(&m_mtxSubbedList);
	std::string suffix = szWindCode.substr(pos);
	if (suffix == ".SH" || suffix == ".SZ")
	{
		if (_subbedList.size() == 0)
		{
			// 股票第一次直接订阅全部数据
			const char* subAll = "";
			_Subscriber->setsockopt(ZMQ_SUBSCRIBE, subAll, strlen(subAll));
			_subbedList.insert(subAll);
		}
	}
	else if (suffix == ".CF" ||
		suffix == ".SHF" ||
		suffix == ".DCE" ||
		suffix == ".CZC")
	{
		_Subscriber->setsockopt(ZMQ_SUBSCRIBE, szWindCode.c_str(), strlen(szWindCode.c_str()));
		_subbedList.insert(szWindCode);
	}

	if (_subbedList.find(szWindCode) == _subbedList.end())
	{
		_subbedList.insert(szWindCode);
	}
	MUTEX_UNLOCK(&m_mtxSubbedList);
	return true;
}

bool MdApiImp::UnSubscribeList( vector<string>& subList )
{
	if (_Subscriber == nullptr) return false;

	for (vector<string>::iterator iter = subList.begin(); iter != subList.end(); iter++)
	{
		std::string szWindCode = *iter;
		UnSubscribe(szWindCode.c_str());
	}

	return true;
}

bool MdApiImp::UnSubscribe( const char* sub )
{
	if (_Subscriber == nullptr || strlen(sub) == 0) return false;

	std::string toUpper = sub;
	transform(toUpper.begin(), toUpper.end(), toUpper.begin(), ::toupper); //将小写都转换成大写

	MUTEX_LOCK(&m_mtxSubbedList);
	if (_subbedList.find(toUpper) != _subbedList.end())
	{
		//_Subscriber->setsockopt(ZMQ_UNSUBSCRIBE, toUpper.c_str(), strlen(toUpper.c_str()));
		_subbedList.erase(toUpper);
	}
	MUTEX_UNLOCK(&m_mtxSubbedList);

	return true;
}
